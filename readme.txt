Bakal��sk� pr�ce
Fakulta informa�n�ch technologi�, �VUT

L�kov� encyklopedie 2
Ji�� R��i�ka
10. 01. 2019, Praha

Obsah elektronick� p��lohy
--------------------------

/lekova-encyclopedie-2
        Zdrojov� k�dy LE2, shodn� s git reposit��em git@gitlab.com:Jiriruzi/lekova-encyclopedie-2.git

/obrazky
	komponentovyModel.png	
	 	Obr�zek komponentov�ho modelu

/text-prace
    	thesis.pdf
        	Text bakal��sk� pr�ce

/text-zdrojove-soubory
	Zdrojov� soubory textu v Latex

/uzivatelske-testovani
    	Testov�n� L�kov� encyklopedie 2 - role bezny uzivatel.pdf
		Vysledky u�ivatelsk�ho testov�n� v roli b�n�ho u�ivatele

    	Testov�n� L�kov� encyklopedie 2 - role bezny uzivatel.pdf
		Vysledky u�ivatelsk�ho testov�n� v roli expert

/zatezove-testovani
	V�sledky, sc�n��e a objekty z�t�ov�ho testov�n�



