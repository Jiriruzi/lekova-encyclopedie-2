const server = browser.runtime.getManifest().server_name;
const cookieServer = `${server.split(":")[0]}:${server.split(":")[1]}`;
function goToserver() {
  browser.tabs.create({ url: `${server}/Medication` });
}
function redirect(item) {
  browser.tabs.create({ url: `${server}/EditMedicament/${item.key}` });
}
function getTimeString(now, item, language) {
  let time = "";
  console.log(now, item, language)
  if (item.alarm && item.nextDose && !isNaN(item.nextDose)) {
    let diff = new Date(item.nextDose) - now;
    if (!diff || diff < 0) {
      time = "";
    } else {
      const minutes = Math.floor(diff / 1000 / 60);
      if (minutes / 60 > 24) {
        now = Math.floor(now / (1000 * 60 * 60 * 24));
        diff =
          Math.floor(
            new Date(item.nextDose).getTime() / (1000 * 60 * 60 * 24)
          ) - now;
        time = `${diff} d`;
      } else if (diff < 60000) {
        time = `${Math.floor(diff / 1000)} s`;
      } else {
        let hoursStr = Math.floor(minutes / 60);
        let minutesStr = Math.floor(minutes % 60);
        if (hoursStr < 10) {
          hoursStr = `0${hoursStr}`;
        }
        if (minutesStr < 10) {
          minutesStr = `0${minutesStr}`;
        }
        time = `${hoursStr}:${minutesStr}`;
      }
    }
  } else {
    time = language === "en" ? "Not set" : "Nenastaveno";
  }
  return time;
}
function insertData(clonedData, language, hasInteraction) {
  const hasInteractionCell = document.getElementById("hasInteraction");
  let interactionText;
  if (hasInteraction) {
    hasInteractionCell.classList.add("error");
    interactionText =
      language === "en" ? "Interactions found" : "Nalezeny interakce";
  } else {
    hasInteractionCell.classList.add("success");
    interactionText =
      language === "en"
        ? "No interactions found"
        : "Žádné interakce nenalezeny";
  }
  hasInteractionCell.innerHTML = interactionText;
  const table = document.getElementById("table");
  const now = Date.now();
  let i = 0;
  let processedMed = [];
  while (clonedData[i]) {
    if (
      clonedData[i].nextDose &&
      !isNaN(clonedData[i].nextDose) &&
      now > clonedData[i].nextDose &&
      clonedData[i].howOften &&
      !isNaN(clonedData[i].howOften)
    ) {
      const diff = now - clonedData[i].nextDose;
      let times = diff / (clonedData[i].howOften * 60 * 60 * 1000);
      if (Math.ceil(times) === times) {
        times = times * 1 + 1;
      } else {
        times = Math.ceil(times);
      }
      clonedData[i].nextDose =
        clonedData[i].nextDose * 1 +
        times * clonedData[i].howOften * 60 * 60 * 1000;
    }
    processedMed.push(clonedData[i++]);
  }
  processedMed = processedMed.sort((a, b) => {
    if (!b.alarm && a.alarm) {
      return -1;
    }
    if (!a.alarm && b.alarm) {
      return 1;
    }
    if (!b.alarm && !a.alarm) {
      return 0;
    }
    if (!b.nextDose && a.nextDose) {
      return -1;
    }
    if (!a.nextDose && b.nextDose) {
      return 1;
    }
    if (!b.nextDose && !a.nextDose) {
      return 0;
    }
    if (isNaN(b.nextDose) && !isNaN(a.nextDose)) {
      return -1;
    }
    if (!isNaN(a.nextDose) && isNaN(b.nextDose)) {
      return 1;
    }
    if (a.nextDose < now && b.nextDose > now) {
      return 1;
    }
    if (a.nextDose > now && b.nextDose < now) {
      return -1;
    }
    return a.nextDose - b.nextDose;
  });
  i = 0;
  while (processedMed[i]) {
    const tr = table.rows[i + 1];
    tr.onclick = (function(j) {
      return function() {
        redirect(processedMed[j]);
      };
    })(i);
    tr.cells[0].innerHTML = processedMed[i].enc_title;
    tr.cells[1].innerHTML = getTimeString(now, processedMed[i++], language);
  }
  setTimeout(() => {
    insertData(processedMed, language, hasInteraction);
  }, 1000);
}

function tableCreate(data) {
  let hasInteraction = false;
  browser.cookies
    .get({ name: "language", url: cookieServer })
    .then(response => {
      const language = response ? response.value : "cs";
      const td1 = document.getElementById("th1");
      const td2 = document.getElementById("th2");
      const table = document.getElementById("table");
      td1.innerHTML = language && language === "en" ? "Drug" : "Lék";
      td2.appendChild(
        document.createTextNode(
          language && language === "en" ? "Next in" : "Příští za"
        )
      );
      const img = document.getElementById("logo");
      img.onclick = function() {
        browser.tabs.create({ url: `${server}/Medication` });
      };
      browser.cookies
        .get({ name: "username", url: cookieServer })
        .then(user => {
          const usr = document.getElementById("username");
          if (user && user.value !== "null") {
            usr.innerHTML =
              (language === "en" ? "Logged: " : "Přihlášen: ") + user.value;
          } else {
            usr.innerHTML =
              language === "en" ? "No one logged " : "Nikdo nepřihlášen ";
          }
        });

      let i = 0;
      let clonedData = JSON.parse(JSON.stringify(data));
      let hasInteraction = !!clonedData.hasInteraction;
      delete clonedData.hasInteraction;
      clonedData = Object.values(clonedData);
      while (clonedData[i]) {
        const tr = table.insertRow();
        tr.insertCell();
        tr.insertCell();
        i++;
      }
      insertData(clonedData, language, hasInteraction);
    });
}
function makePath(text) {
  text = text.replace(/\//g, "%2F");
  text = text.replace(":", "%3A");
  return text;
}
function handleResponse(message) {
  console.log(message);
}

function handleError(error) {
  console.log(JSON.stringify(error));
}

function refresh() {
  browser.runtime
    .sendMessage(browser.runtime.id, "ENC_UPDATE")
    .then(handleResponse, handleError)
    .then(() => window.location.reload());
}
const refrButt = document.getElementById("refresh");
refrButt.onclick = function() {
  refresh();
};

window.onload = function() {
  browser.storage.local.get().then(result => tableCreate(result));
};
