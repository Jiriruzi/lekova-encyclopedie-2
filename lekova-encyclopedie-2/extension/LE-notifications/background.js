const server = browser.runtime.getManifest().server_name;

function isChrome() {
  const isChromium = window.chrome;
  const winNav = window.navigator;
  const vendorName = winNav.vendor;
  const isOpera = typeof window.opr !== "undefined";
  const isIEedge = winNav.userAgent.indexOf("Edge") > -1;
  const isIOSChrome = winNav.userAgent.match("CriOS");

  if (isIOSChrome) {
    // is Google Chrome on IOS
  } else if (
    isChromium !== null &&
    typeof isChromium !== "undefined" &&
    vendorName === "Google Inc." &&
    isOpera === false &&
    isIEedge === false
  ) {
    return true;
  } else {
    return false;
  }
}

function escapeUrl(text) {
  text = text.replace(/\//g, "%2F");
  text = text.replace(":", "%3A");
  return text;
}

function createNotification(medicament, name, repeat) {
  browser.cookies
    .get({
      name: "language",
      url: `${server.split(":")[0]}:${server.split(":")[1]}`
    })
    .then(result => {
      let expM =
        (!result || result.value === "cs")
          ? "Je čas na vaší medikaci!"
          : "It`s time for your medication!";
      expM += ` \n${medicament[name].enc_title}`;
      const options = {
        title: "Léková encyklopedie",
        type: "basic",
        iconUrl: "./LE.png",
        message: expM
      };
      if (isChrome()) {
        options.buttons = [
          {
            title:
              result.value === "cs"
                ? "Posunout o 30 minut"
                : "Notificate in 30 minutes"
          }
        ];
      }
      browser.notifications
        .create(name + (repeat ? "-repeat" : ""), options)
        .then(() => {}, error => console.log(JSON.stringify(error)));
    });
}

function getFrequencyByType(medicament) {
  let howOften = medicament.howOften * 60 * 60 * 1000;
  if (medicament.howOftenType === "days") {
    howOften *= 24;
  } else if (medicament.howOftenType === "weeks") {
    howOften *= 24 * 7;
  } else if (medicament.howOftenType === "months") {
    howOften *= 24 * 30;
  }
  return howOften;
}

browser.alarms.onAlarm.addListener(alarm => {
  const [name, repeat] = `${alarm.name}`.split("-");
  browser.storage.local.get([name]).then(medicament => {
    createNotification(medicament, name);
    const myAudio = new Audio(); // create the audio object
    myAudio.src = "./notification.mp3"; // assign the audio file to its src
    myAudio.play();
    if (repeat !== "repeat") {
      if (medicament[name].howOften && medicament[name].howOften > 0) {
        browser.storage.local.set({
          [name]: {
            ...medicament[name],
            nextDose:
              medicament[name].nextDose + getFrequencyByType(medicament[name])
          }
        });
      } else {
        browser.storage.local.set({
          [name]: { ...medicament[name], nextDose: null }
        });
      }
    }
  });
});

function newNotification(nid) {
  const [name] = `${nid}`.split("-");
  browser.notifications.clear(nid);
  browser.storage.local.get([name]).then(medicament => {
    browser.alarms.create(`${name}-repeat`, { when: Date.now() + 1800000 });
  });
}

browser.notifications.onButtonClicked.addListener(newNotification);

function checkInteractionFetch(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

function getAllIngredients(item, context) {
  const promises = [];
  if (!item) {
    return Promise.all(promises);
  }
  if (item && item._type === "ingredient") {
    promises.push(
      fetch(`${server}/documents/le-ingredient-detail/${escapeUrl(item._id)}`, {
        method: "GET",
        credentials: "same-origin",
        headers: {
          "Content-Type": "*/*",
          Accept: "*/*"
        }
      })
        .then(response => checkInteractionFetch(response))
        .then(response => response.json())
    );
  } else if (!escapeUrl(item._id)) {
    promises.push({});
  } else {
    promises.push(
      fetch(
        `${server}/documents/le-medicinal-product-detail/${escapeUrl(
          item._id
        )}`,
        {
          method: "GET",
          credentials: "same-origin",
          headers: {
            "Content-Type": "*/*",
            Accept: "*/*"
          }
        }
      )
        .then(response => checkInteractionFetch(response))
        .then(response => response.json())
        .then(response => {
          const medPromises = [];
          if (!response[`${context}hasActiveIngredient`]) {
            return null;
          }

          if (response[`${context}hasActiveIngredient`] instanceof Array) {
            for (
              let j = 0;
              j < response[`${context}hasActiveIngredient`].length;
              j++
            ) {
              medPromises.push(
                fetch(
                  `${server}/documents/le-ingredient-detail/${escapeUrl(
                    response[`${context}hasActiveIngredient`][j]["@id"]
                  )}`,
                  {
                    method: "GET",
                    credentials: "same-origin",
                    headers: {
                      "Content-Type": "*/*",
                      Accept: "*/*"
                    }
                  }
                )
                  .then(response2 => checkInteractionFetch(response2))
                  .then(response2 => response2.json())
                  .then(response2 => ({
                    ...response2,
                    medicinalProduct: response
                  }))
              );
            }
          } else {
            medPromises.push(
              fetch(
                `${server}/documents/le-ingredient-detail/${escapeUrl(
                  response[`${context}hasActiveIngredient`]["@id"]
                )}`,
                {
                  method: "GET",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "*/*",
                    Accept: "*/*"
                  }
                }
              )
                .then(response2 => checkInteractionFetch(response2))
                .then(response2 =>
                  response2.json().then(response3 => ({
                    ...response3,
                    medicinalProduct: response
                  }))
                )
            );
          }
          return Promise.all(medPromises);
        })
    );
  }
  return Promise.all(promises);
}
function getMedication(medication, context = "enc:") {
  const promises = [];
  for (let i = 0; i < medication.length; i++) {
    promises.push(
      getAllIngredients(
        {
          ...medication[i],
          _type: "MedicinalProduct"
        },
        context
      )
    );
  }
  return Promise.all(promises);
}
function hasInteraction(medication, context = "enc:") {
  let hasInt = false;
  return getMedication(medication, "enc:").then(ingrs => {
    for (let i = 0; i < ingrs.length; i++) {
      for (let j = 0; j < ingrs[i].length; j++) {
        for (let k = j + 1; k < ingrs.length; k++) {
          for (let m = 0; m < ingrs[k].length; m++) {
            for (
              let n = 0;
              ingrs[k][context + hasInteraction] &&
              n < ingrs[k][context + hasInteraction].length;
              n++
            ) {
              console.log(
                ingrs[k][context + "hasInteraction"][
                  context + "hasInteractionWith"
                ]["@id"]
              );
              console.log(ingrs[i][j]["@id"]);
              if (
                ingrs[k][context + "hasInteraction"][
                  context + "hasInteractionWith"
                ]["@id"] === ingrs[i][j]["@id"]
              )
                return true;
            }
          }
        }
      }
    }
    return false;
  });
}
function saveMedicationAndSetAlarms(medication) {
  return browser.alarms
    .getAll()
    .then(alarms => {
      for (let i = 0; i < alarms.length; i++) {
        if (!`${alarms[i].name}`.includes("repeat")) {
          browser.alarms.clear(alarms[i].name);
        }
      }
    })
    .then(() => browser.storage.local.clear())
    .then(() => {
      for (let i = 0; i < medication.length; i++) {
        let medicament = medication[i];
        if (medication[i].nextDose) {
          let nextDose = new Date(medication[i].nextDose).getTime();
          const nowTime = new Date(Date.now()).getTime();
          if (
            nextDose < nowTime &&
            medication[i].howOften &&
            medication[i].howOftenType &&
            medication[i].howOften > 0
          ) {
            const diff = nowTime - nextDose;
            const howOften = getFrequencyByType(medicament);
            const rest = diff % howOften;
            nextDose = nowTime + (rest > 0 ? howOften - rest : 0);
          }
          if (nextDose > nowTime && medication[i].alarm) {
            if (medication[i].howOften && medication[i].howOften > 0) {
              browser.alarms.create(`${medicament.key}`, {
                when: nextDose,
                periodInMinutes: medication[i].howOften * 60
              });
            } else {
              browser.alarms.create(`${medicament.key}`, { when: nextDose });
            }
          }
          medicament = { ...medication[i], nextDose };
        }
        browser.storage.local.set({ [medicament.key]: medicament });
        hasInteraction(medication).then(hasInt => {
          browser.storage.local.set({
            hasInteraction: hasInt
          });
        });
      }
      return "OK";
    });
}

function refresh() {
  const server = browser.runtime.getManifest().server_name;
  return fetch(`${server}/user/get_medication`, {
    method: "GET",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      if (!response.ok) {
        browser.storage.local.clear();
        browser.alarms.clearAll();
        throw Error(response.statusText);
      }
      return response.json();
    })
    .then(response => saveMedicationAndSetAlarms(Object.values(response)));
}

refresh();

browser.runtime.onMessage.addListener(refresh);
