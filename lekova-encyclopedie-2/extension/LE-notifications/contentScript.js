function handleResponse(message) {
    console.log(message);
}

function handleError(error) {
    console.log(error);
}

function sendMessage(e) {
    browser.runtime.sendMessage(browser.runtime.id, 'ENC_UPDATE').then(handleResponse, handleError);
}

window.addEventListener('message', sendMessage);
