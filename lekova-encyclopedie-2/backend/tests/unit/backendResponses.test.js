const fetch = require("node-fetch");
const Cookie = require("request-cookies").Cookie;

let sessionCookie;

function setCookie(response) {
  var rawcookies = response.headers.raw()["set-cookie"];
  for (var i in rawcookies) {
    var cookie = new Cookie(rawcookies[i]);
    if (cookie.key === "AuthSession") {
      sessionCookie = cookie.value;
    }
  }
}
test("User registration works well with valid data and role user", done => {
  fetch("http://localhost:8087/user/register", {
    method: "POST",
    body: JSON.stringify({
      role: "user",
      birthday: { day: 29, month: 11, year: 2018 },
      username: "jirka",
      email: "jirka.email@email.cz",
      password: "qqq",
      passwordConfirmation: "qqq",
      gender: "male"
    })
  }).then(response => {
    expect(response.status).toEqual(201);
    done();
  });
});
test("Registration does not work when the username is already used", done => {
  fetch("http://localhost:8087/user/register", {
    method: "POST",
    body: JSON.stringify({
      role: "user",
      birthday: { day: 29, month: 11, year: 2018 },
      username: "jirka",
      email: "jirka.email@email.cz",
      password: "qqq",
      passwordConfirmation: "qqq",
      gender: "male"
    })
  }).then(response => {
    expect(response.status).toEqual(409);
    done();
  });
});

test("Registration does not work when data are invalid", done => {
  fetch("http://localhost:8087/user/register", {
    method: "POST",
    body: JSON.stringify({
      role: "",
      birthday: { day: 0, month: 0, year: 0 },
      username: "",
      password: "qqq",
      gender: "semale"
    })
  })
    .then(response => response.json())
    .then(response => {
      console.log(response);
      console.log(response);
      console.log(response);
      expect(response).toEqual({
        error: "invalid_data",
        reason: {
          data: {
            birthday: "Bad data",
            username: "Bad data",
            gender: "Bad data",
            password: "OK",
            email: "Bad data"
          },
          dataOk: false
        }
      });
      done();
    });
});
test("Login with wrong credentials does not login", done => {
  fetch("http://localhost:8087/user/login", {
    method: "POST",
    body: JSON.stringify({
      username: "jirka",
      password: "qq"
    })
  }).then(response => {
    expect(response.status).toEqual(401);
    expect(
      JSON.stringify(response.headers.raw()).includes("AuthSession")
    ).toBeFalsy();
    done();
  });
});

test("Login returns logsin if correct data are provided and contains cookie", done => {
  fetch("http://localhost:8087/user/login", {
    method: "POST",
    body: JSON.stringify({
      username: "jirka",
      password: "qqq"
    })
  })
    .then(response => {
      setCookie(response);
      return response.json();
    })
    .then(response => {
      expect(response.ok).toBeTruthy();
      expect(sessionCookie).toBeDefined();
      done();
    });
});

test("Get user works well", done => {
  fetch("http://localhost:8087/user/get_user", {
    method: "GET",
    headers: {
      Cookie: "AuthSession=" + sessionCookie + ";"
    }
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      expect(response).toEqual({
        birthday: { day: 29, month: 11, year: 2018 },
        email: "jirka.email@email.cz",
        gender: "male"
      });
      done();
    });
});

test("Edit user works well", done => {
  fetch("http://localhost:8087/user/edit_user", {
    method: "PUT",
    body: JSON.stringify({
      birthday: { day: 29, month: 11, year: 2018 },
      email: "jirkas.email@email.czssssssssssssssssssssssssssss",
      gender: "female"
    }),
    headers: {
      Cookie: "AuthSession=" + sessionCookie + ";"
    }
  })
    .then(response => {
      expect(response.status).toBe(201);
      return response.text();
    })
    .then(() =>
      fetch("http://localhost:8087/user/get_user", {
        method: "GET",
        headers: {
          Cookie: "AuthSession=" + sessionCookie
        }
      })
        .then(response => {
          return response.json();
        })
        .then(response => {
          expect(response).toEqual({
            birthday: { day: 29, month: 11, year: 2018 },
            email: "jirkas.email@email.czssssssssssssssssssssssssssss",
            gender: "female"
          });
          done();
        })
    );
});

test("Edit user does not work when session does not exist", done => {
  fetch("http://localhost:8087/user/edit_user", {
    method: "PUT",
    body: JSON.stringify({
      birthday: { day: 29, month: 11, year: 2018 },
      email: "jirkas.email@email.czssssssssssssssssssssssssssss",
      gender: "female"
    }),
    headers: {
      Cookie: "AuthSession=INVALIED_SESSION;"
    }
  }).then(response => {
    setCookie(response);
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});

test("Get user does not work when session does not exist", done => {
  fetch("http://localhost:8087/user/get_user", {
    headers: {
      Cookie: "AuthSession=INVALIED_SESSION;"
    }
  }).then(response => {
    setCookie(response);
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});

test("Update medicament works well when posting new data", done => {
  fetch("http://localhost:8087/user/update_medicament", {
    method: "POST",
    body: JSON.stringify({ medicament: { nextDose: null, enc_title: "ded" } }),
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  }).then(response => {
    setCookie(response);
    expect(response.status).toBe(201);
    done();
  });
});

test("Update medicament works well when updating existing medicament", done => {
  fetch("http://localhost:8087/user/update_medicament", {
    method: "POST",
    body: JSON.stringify({
      medicament: {
        nextDose: null,
        enc_title: "ded",
        key: 1,
        description: "aa"
      }
    }),
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  }).then(response => {
    setCookie(response);
    expect(response.status).toBe(201);
    done();
  });
});
test("Update medicament does not work when user is not authenticated", done => {
  fetch("http://localhost:8087/user/update_medicament", {
    method: "POST",
    body: JSON.stringify({ medicament: { nextDose: null, enc_title: "ded" } }),
    headers: {
      Cookie: "AuthSession=INVALID"
    }
  }).then(response => {
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});
test("Get medication works well", done => {
  fetch("http://localhost:8087/user/get_medication", {
    method: "GET",
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  })
    .then(response => {
      expect(response.status).toBe(200);
      return response.json();
    })
    .then(response => {
      expect(response).toEqual({
        "1": { nextDose: null, enc_title: "ded", key: 1, description: "aa" }
      });
      done();
    });
});

test("Get does not work when user is not authenticated", done => {
  fetch("http://localhost:8087/user/get_medication", {
    method: "GET",
    headers: {
      Cookie: "AuthSession=INVALID"
    }
  }).then(response => {
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});

test("Delete medicament works well", done => {
  fetch("http://localhost:8087/user/remove_medicament", {
    method: "DELETE",
    body: JSON.stringify(1),
    headers: {
      Cookie: "AuthSession=" + sessionCookie + ";"
    }
  })
    .then(response => {
      expect(response.status).toBe(201);
    })
    .then(() =>
      fetch("http://localhost:8087/user/get_medication", {
        method: "GET",
        headers: {
          Cookie: "AuthSession=" + sessionCookie
        }
      })
        .then(response => {
          return response.json();
        })
        .then(response => {
          expect(response).toEqual([]);
          done();
        })
    );
});

test("Delete medicament works well", done => {
  fetch("http://localhost:8087/user/remove_medicament", {
    method: "DELETE",
    body: JSON.stringify(1),
    headers: {
      Cookie: "AuthSession=INVALID;"
    }
  }).then(response => {
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});

test("user registration works well with valid data and expert role", done => {
  fetch("http://localhost:8087/user/register", {
    method: "POST",
    body: JSON.stringify({
      role: "expert",
      birthday: { day: 29, month: 11, year: 2018 },
      username: "jirkaExpert",
      email: "jirka.email@email.cz",
      password: "qqq",
      passwordConfirmation: "qqq",
      gender: "male"
    })
  }).then(response => {
    expect(response.status).toEqual(201);
    done();
  });
});
test("Login returns logsin if correct data are provided and contains cookie with expert role", done => {
  fetch("http://localhost:8087/user/login", {
    method: "POST",
    body: JSON.stringify({
      username: "jirkaExpert",
      password: "qqq"
    })
  })
    .then(response => {
      setCookie(response);
      return response.json();
    })
    .then(response => {
      expect(response.ok).toBeTruthy();
      expect(sessionCookie).toBeDefined();
      done();
    });
});
test("Update history does work", done => {
  fetch("http://localhost:8087/user/update_history", {
    method: "POST",
    body: JSON.stringify({
      "@type": "Ingredient",
      "@id": "id",
      time: 1544967351005
    }),
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  }).then(response => {
    expect(response.status).toBe(201);
    done();
  });
});
test("Update history does work", done => {
  fetch("http://localhost:8087/user/get_history", {
    method: "GET",
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  })
    .then(response => {
      return response.json();
    })
    .then(response => {
      expect(response).toEqual([
        {
          "@type": "Ingredient",
          "@id": "id",
          time: 1544967351005
        }
      ]);
      done();
    });
});

test("Change password works when correct password is provided", done => {
  fetch("http://localhost:8087/user/change_password", {
    method: "PUT",
    body: JSON.stringify({
      password: "qqq",
      newPassword: "www",
      username: "jirkaExpert"
    }),
    headers: {
      Cookie: "AuthSession=" + sessionCookie
    }
  }).then(response => {
    setCookie(response);
    expect(response.status).toBe(201);
    done();
  });
});

test("Login with new password works", done => {
  fetch("http://localhost:8087/user/login", {
    method: "POST",
    body: JSON.stringify({
      username: "jirkaExpert",
      password: "www"
    })
  })
    .then(response => {
      setCookie(response);
      return response.json();
    })
    .then(response => {
      expect(response.ok).toBeTruthy();
      expect(sessionCookie).toBeDefined();
      done();
    });
});

test("Change password does not work when incorrect password is provided", done => {
  fetch("http://localhost:8087/user/login", {
    method: "PUT",
    body: JSON.stringify({
      password: "qqq",
      newPassword: "qqq",
      username: "jirkaExpert"
    })
  }).then(response => {
    setCookie(response);
    expect(response.status).toBeGreaterThan(399);
    done();
  });
});

test("Login with old password does not work", done => {
  fetch("http://localhost:8087/user/login", {
    method: "POST",
    body: JSON.stringify({
      username: "jirkaExpert",
      password: "qqq"
    })
  })
    .then(response => {
      setCookie(response);
      return response.text();
    })
    .then(response => {
      expect(response.ok).toBeFalsy();
      expect(sessionCookie).toBeDefined();
      done();
    });
});

test("Cookie is empty valid after logout", done => {
  fetch("http://localhost:8087/user/logout", {
    method: "DELETE",
    headers: {
      Cookie: "AuthSession=" + sessionCookie + ";"
    }
  }).then(response => {
    setCookie(response);
    expect(sessionCookie).toEqual("");
    done();
  });
});
