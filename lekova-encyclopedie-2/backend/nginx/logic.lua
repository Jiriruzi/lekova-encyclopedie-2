require "usr.local.openresty.nginx.conf.utils"

local cjson = require("cjson")

function badData(data)
 return  cjson.encode({
    error = "invalid_data",
    reason =  cjson.decode(data)
  })
end

function wrongMethod(method)
  return  cjson.encode({
     error = "method_not_allowed",
     reason = "Only" .. method .. "allowed"
   })
end

function register()
  if (ngx.req.get_method() ~= "POST") then
    ngx.status = 405
    ngx.say(wrongMethod("POST"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    local data = checkUserData(args)
    if (data.dataOk == false) then
      ngx.status = 400
      ngx.say(badData(cjson.encode(data)))
    else
      local res =
      ngx.location.capture(
      "/user_local/couch_session",
      {
        method = ngx.HTTP_POST,
        body = cjson.encode({name = ngx.var.couchdbAdmin, password = ngx.var.couchdbAdminPassword})
      }
      )
      ngx.status = res.status
      if (res.status <= 202) then
        local cookie = res.header["Set-Cookie"]
        ngx.req.set_header("Cookie", cookie)

        local resUser =
        ngx.location.capture(
        "/user_local/couch_user/" .. args.username,
        {
          method = ngx.HTTP_PUT,
          body = cjson.encode(
          {
            links_salt = createLinkSalt(32),
            name = args.username,
            password = args.password,
            roles = {
              args.role
            },
            type = "user",
            email = args.email,
            birthday = args.birthday,
            gender = args.gender
          }
          )
        }
        )
        ngx.status = resUser.status
        ngx.say(resUser.body)
      end
    end
  end
end

function login()
  ngx.req.set_header("Host", "localhost:5984")
  ngx.req.set_header("Content-Type", "application/json")
  ngx.req.read_body()
  local args = cjson.decode(ngx.req.get_body_data())
  if
  (args.username == nil or args.username == cjson.null or args.username == "" or args.password == nil or
  args.password == cjson.null or
  args.password == "")
  then
    ngx.status = 400
    ngx.say(badData(cjson.encode({
      username= args.username == nil or args.username == cjson.null or args.username == "",
      password= args.password == nil or args.password == cjson.null or args.password == ""
    })))
  else
    local res =
    ngx.location.capture(
    "/user_local/couch_session",
    {method = ngx.HTTP_POST, body = cjson.encode({name = args.username, password = args.password})}
    )
    ngx.status = res.status
    if (res.status <= 202) then
      local cookie = res.header["Set-Cookie"]
      ngx.header["Set-Cookie"] = cookie
      ngx.say(res.body)
    else
      ngx.say(res.body)
    end
  end
end

function editUser()
  if (ngx.req.get_method() ~= "PUT") then
    ngx.status = 405
    ngx.say(wrongMethod("PUT"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    local data = checkEditData(args)
    if (data.dataOk == false) then
      ngx.status = 400
      ngx.say(cjson.encode(data))
    else
      local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
      if (resSess.status <= 202) then
        local name = cjson.decode(resSess.body).userCtx.name
        local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
        if (resDoc.status > 202) then
          ngx.status = resDoc.status
          ngx.say(resDoc.body)
        else
          local newDoc = cjson.decode(resDoc.body)

          if (newDoc.medication == nil or next(newDoc.medication) == nil) then
            newDoc.medication = cjson.empty_array
          end
          newDoc.email = args.email
          newDoc.gender = args.gender
          newDoc.birthday = args.birthday
          local resNewDoc =
          ngx.location.capture(
          "/user_local/couch_user/" .. name,
          {
            method = ngx.HTTP_PUT,
            body = cjson.encode(newDoc)
          }
          )
          ngx.status = resNewDoc.status
          ngx.say(resNewDoc.body)
        end
      else
        ngx.status = resSess.status
        ngx.say(resSess.body)
      end
    end
  end
end

function removeMedicament()
  if (ngx.req.get_method() ~= "DELETE") then
    ngx.status = 405
    ngx.say(wrongMethod("DELETE"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local id = tostring(cjson.decode(ngx.req.get_body_data()))
    local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
    if (resSess.status <= 202) then
      local name = cjson.decode(resSess.body).userCtx.name
      local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
      if (resDoc.status > 202) then
        ngx.status = resDoc.status
        ngx.say(resDoc.body)
      else
        local newDoc = cjson.decode(resDoc.body)
        if (newDoc.medication == nil or next(newDoc.medication) == nil) then
          newDoc.medication = cjson.empty_array
        else
          newDoc.medication[id] = nil
        end
        local resNewDoc =
        ngx.location.capture(
        "/user_local/couch_user/" .. name,
        {
          method = ngx.HTTP_PUT,
          body = cjson.encode(newDoc)
        }
        )
        ngx.status = resNewDoc.status
        ngx.say(resNewDoc.body)
      end
    else
      ngx.status = resSess.status
      ngx.say(resSess.body)
    end
  end
end

function updateMedicament()
  if (ngx.req.get_method() ~= "POST") then
    ngx.status = 405
    ngx.say(wrongMethod("POST"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    local tab = {}
    tab[0] = args.medicament
    local data = checkMedicationData(tab)
    if (data.dataOk == false) then
      ngx.status = 400
      ngx.say(badData(cjson.encode(data)))
    else
      local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
      if (resSess.status <= 202) then
        local name = cjson.decode(resSess.body).userCtx.name
        local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
        if (resDoc.status > 202) then
          ngx.status = resDoc.status
          ngx.say(resDoc.body)
        else
          local newDoc = cjson.decode(resDoc.body)
          local key = args.medicament.key
          if (newDoc.medication == nil or next(newDoc.medication)==nil) then
            newDoc.medication = {}
            ngx.log(ngx.STDERR, 'your message here')
          end
          if (newDoc.nextMed == nil) then
            newDoc.nextMed = 1
          end
          if (args.medicament.key == nil) then
            newDoc.medication[tostring(newDoc.nextMed)] = args.medicament
            newDoc.medication[tostring(newDoc.nextMed)].key = newDoc.nextMed
            key = newDoc.nextMed
            newDoc.nextMed = newDoc.nextMed + 1
          else
            newDoc.medication[tostring(args.medicament.key)] = args.medicament
          end
          local resNewDoc =
          ngx.location.capture(
          "/user_local/couch_user/" .. name,
          {
            method = ngx.HTTP_PUT,
            body = cjson.encode(newDoc)
          }
          )
          ngx.status = resNewDoc.status
          ngx.say(resNewDoc.body)
        end
      else
        ngx.status = resSess.status
        ngx.say(resSess.body)
      end
    end
  end
end

function updateHistory()
  if (ngx.req.get_method() ~= "POST") then
    ngx.status = 405
    ngx.say(wrongMethod("POST"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())

    local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
    if (resSess.status <= 202) then
      local name = cjson.decode(resSess.body).userCtx.name
      local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
      if (resDoc.status > 202) then
        ngx.status = resDoc.status
        ngx.say(resDoc.body)
      else
        local newDoc = cjson.decode(resDoc.body)
        if (newDoc.history == nil) then
          newDoc.history = {}
        end
        if(table.getn(newDoc.history) > 500) then
          table.remove(newDoc.history)
        end
        table.insert(newDoc.history,1, args)
        local resNewDoc =
        ngx.location.capture(
        "/user_local/couch_user/" .. name,
        {
          method = ngx.HTTP_PUT,
          body = cjson.encode(newDoc)
        }
        )
        ngx.status = resNewDoc.status
        ngx.say(resNewDoc.body)
      end
    else
      ngx.status = resSess.status
      ngx.say(resSess.body)
    end
  end
end

function getUser()
  if (ngx.req.get_method() ~= "GET") then
    ngx.status = 405
    ngx.say(wrongMethod("GET"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
    if (resSess.status <= 202) then
      local name = cjson.decode(resSess.body).userCtx.name
      local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
      ngx.status = resDoc.status
      if (resDoc.status > 202) then
        ngx.say(resDoc.body)
      else
        ngx.header["content-type"] = resDoc.header["content-type"]
        local data = cjson.decode(resDoc.body)
        local returnData = {
          email = data.email,
          gender = data.gender,
          birthday = data.birthday
        }
        ngx.say(cjson.encode(returnData))
      end
    else
      ngx.status = resSess.status
      ngx.say(resSess.body)
      return
    end
  end
end

function getMedication()
  if (ngx.req.get_method() ~= "GET") then
    ngx.status = 405
    ngx.say(wrongMethod("GET"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
    if (resSess.status <= 202) then
      local name = cjson.decode(resSess.body).userCtx.name
      local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
      ngx.status = resDoc.status
      if (resDoc.status > 202) then
        ngx.say(resDoc.body)
      else
        ngx.header["content-type"] = resDoc.header["content-type"]
        local medication = cjson.decode(resDoc.body).medication
        if (medication == nil or next(medication) == nil) then
          medication = cjson.empty_array
        end
        ngx.say(cjson.encode(medication))
      end
    else
      ngx.status = resSess.status
      ngx.say(resSess.body)
      return
    end
  end
end

function getHistory()
  if (ngx.req.get_method() ~= "GET") then
    ngx.status = 405
    ngx.say(wrongMethod("GET"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    local resSess = ngx.location.capture("/user_local/couch_session", {method = ngx.HTTP_GET})
    if (resSess.status <= 202) then
      local name = cjson.decode(resSess.body).userCtx.name
      local resDoc = ngx.location.capture("/user_local/couch_user/" .. name, {method = ngx.HTTP_GET})
      ngx.status = resDoc.status
      if (resDoc.status > 202) then
        ngx.say(resDoc.body)
      else
        ngx.header["content-type"] = resDoc.header["content-type"]
        local history = cjson.decode(resDoc.body).history
        if (history == nil) then
          history = cjson.empty_array
        end
        ngx.say(cjson.encode(history))
      end
    else
      ngx.status = resSess.status
      ngx.say(resSess.body)
      return
    end
  end
end

function getPositiveList()
  if (ngx.req.get_method() ~= "GET") then
    ngx.status = 405
    ngx.say(wrongMethod("GET"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    local idListReq =
    ngx.location.capture(
    "/documents/le-medicinal-product-packaging-detail/positive-list",
    {method = ngx.HTTP_GET}
    )
    local array = {}
    local ids = cjson.decode(idListReq.body)
    ids = ids["enc:positiveList"]
    for k, v in pairs(ids) do
      local packaging =
      ngx.location.capture(
      "/documents/le-medicinal-product-packaging-detail/" .. ngx.escape_uri(v),
      {method = ngx.HTTP_GET}
      )
      array[k] = ngx.escape_uri(v)
    end
    ngx.say(cjson.encode(array))
  end
end

function changePassword()
  if (ngx.req.get_method() ~= "PUT") then
    ngx.status = 405
    ngx.say(wrongMethod("PUT"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    local data = checkDataForChangePassword(args)
    if (data.dataOk == false) then
      ngx.status = 400
      ngx.say(cjson.encode({
        error = "invalid_data",
        reason = data
      }))
    else
      local resSess =
      ngx.location.capture(
      "/user_local/couch_session",
      {method = ngx.HTTP_POST, body = cjson.encode({name = args.username, password = args.password})}
      )
      if (resSess.status <= 202) then
        local resDoc = ngx.location.capture("/user_local/couch_user/" .. args.username, {method = ngx.HTTP_GET})
        if (resDoc.status > 202) then
          ngx.status = resDoc.status
          ngx.say(resDoc.body)
        else
          local newDoc = cjson.decode(resDoc.body)
          newDoc.password = args.newPassword
          local resNewDoc =
          ngx.location.capture(
          "/user_local/couch_user/" .. args.username,
          {
            method = ngx.HTTP_PUT,
            body = cjson.encode(newDoc)
          }
          )
          local resNewSess =
          ngx.location.capture(
          "/user_local/couch_session",
          {
            method = ngx.HTTP_POST,
            body = cjson.encode({name = args.username, password = args.newPassword})
          }
          )
          ngx.status = resNewDoc.status
          ngx.say(resNewDoc.body)
        end
      else
        ngx.status = resSess.status
        ngx.say(resSess.body)
        return
      end
    end
  end
end

function sendPasswordLink()
  if (ngx.req.get_method() ~= "POST") then
    ngx.status = 405
    ngx.say(wrongMethod("POST"))
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    if (args.username == nil or args.username == cjson.null) then
      ngx.status = 400
      ngx.say(cjson.encode({
        dataOk = false,
        data = {
          username= false
        }
      }))
    else
      local res =
      ngx.location.capture(
      "/user_local/couch_session",
      {
        method = ngx.HTTP_POST,
        body = cjson.encode({name = ngx.var.couchdbAdmin, password = ngx.var.couchdbAdminPassword})
      }
      )
      ngx.status = res.status
      local tries = 1
      local status = 0
      local text = ""
      if (res.status <= 202) then
        local sent = false
        while (sent == false and tries < 4) do
          local cookie = res.header["Set-Cookie"]
          ngx.req.set_header("Cookie", cookie)
          local resDoc = ngx.location.capture("/user_local/couch_user/" .. args.username, {method = ngx.HTTP_GET})
          ngx.status = resDoc.status
          if (resDoc.status > 202) then
            ngx.say(resDoc.body)
          else
            local user = cjson.decode(resDoc.body)
            local newDoc = user
            if (newDoc.medication == nil or next(newDoc.medication) == nil) then
              newDoc.medication = {}
            end

            local token = createToken(32, user.links_salt)
            if
            (user.token ~= nil and user.token.timestamp ~= nil and
            user.token.timestamp + 30 * 60 > os.time())
            then
              ngx.status = 401
              ngx.say(cjson.encode{
                error= "active_link",
                reason= "There already is active link for this account."
              })
              return
            end
            newDoc.token = {
              token = token,
              timestamp = os.time()
            }
            if (sendMailPasswordReset(user.email, token, args.language)) then
              local resNewDoc =
              ngx.location.capture(
              "/user_local/couch_user/" .. args.username,
              {
                method = ngx.HTTP_PUT,
                body = cjson.encode(newDoc)
              }
              )
              ngx.status = resNewDoc.status
              if (ngx.status < 202) then
                ngx.say(cjson.encode({result= "OK"}))
                sent = true;
              else
                status = 400
                text=resNewDoc.body
              end
            else
              status = 401
              text={error="email_not_sent", reason="Probably denied by google server, try again later"}
            end
          end
          tries = tries + 1
        end
        ngx.status = status
        ngx.say(cjson.encode(text))
      else
        ngx.status = res.status
        ngx.say(res.body)
        return
      end
    end
  end
end

function resetPassword()
  if (ngx.req.get_method() ~= "PUT") then
    ngx.status = 405
    ngx.say(wrongMethod"PUT"())
  else
    ngx.req.set_header("Host", "localhost:5984")
    ngx.req.set_header("Content-Type", "application/json")
    ngx.req.read_body()
    local args = cjson.decode(ngx.req.get_body_data())
    if (args.username == nil or args.username == cjson.null) then
      ngx.status = 400
      ngx.say(cjson.encode({
        dataOk = false,
        data = {
          username= false
        }
      }))
    else
      local res =
      ngx.location.capture(
      "/user_local/couch_session",
      {
        method = ngx.HTTP_POST,
        body = cjson.encode({name = ngx.var.couchdbAdmin, password = ngx.var.couchdbAdminPassword})
      }
      )
      ngx.status = res.status
      if (res.status <= 202) then
        local cookie = res.header["Set-Cookie"]
        ngx.req.set_header("Cookie", cookie)
        local resDoc = ngx.location.capture("/user_local/couch_user/" .. args.username, {method = ngx.HTTP_GET})
        ngx.status = resDoc.status
        if (resDoc.status > 202) then
          ngx.say(resDoc.body)
        else
          local user = cjson.decode(resDoc.body)
          local newDoc = user
          if (newDoc.medication == nil or next(newDoc.medication) == nil) then
            newDoc.medication = cjson.empty_array
          end
          if (args.token ~= user.token.token or user.token.timestamp + 30 * 60 < os.time()) then
            ngx.status = 400
            ngx.say(cjson.encode({error="password_not_reseted", error="outdated_link"}))
          else
            newDoc.token = {}
            newDoc.password = args.password
            local resNewDoc =
            ngx.location.capture(
            "/user_local/couch_user/" .. args.username,
            {
              method = ngx.HTTP_PUT,
              body = cjson.encode(newDoc)
            }
            )
            ngx.status = resNewDoc.status
            ngx.say(resNewDoc.body)
          end
        end
      else
        ngx.status = res.status
        ngx.say(res.body)
      end
    end
  end
end
