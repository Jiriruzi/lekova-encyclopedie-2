local cjson = require("cjson")
local mail = require "resty.mail"

local tokenCharacters = {}

for i = 48, 57 do
    table.insert(tokenCharacters, string.char(i))
end
for i = 97, 122 do
    table.insert(tokenCharacters, string.char(i))
end

function createToken(length, salt)
    math.randomseed(os.time() ^ 5 + salt)
    if length > 0 then
        return createToken(length - 1, salt) .. tokenCharacters[math.random(1, #tokenCharacters)]
    else
        return ""
    end
end

function createLinkSalt(length)
    math.randomseed(os.time())
    if length > 1 then
        return createLinkSalt(length - 1) .. math.random(0, 9)
    else
        return math.random(0, 9)
    end
end

function sendMailPasswordReset(email, token, language)
    local mailTitle
    local mailB2
    local mailLink = "http://" .. ngx.var.serverName .. "/ResetPassword/" .. token
    if (language == "cs") then
        mailB2 = "Na následující stránce dokončete změnu hesla: "
        mailTitle = "Změna hesla"
    else
        mailB2 = "On the following page finish the change of password: "
        mailTitle = "Password change"
    end
    local mailB1 = "<html><body><p><h1>" .. mailTitle .. "</h1>"
    local mailB3 = "<a href=" .. mailLink .. " >" .. mailLink .. "</a>"
    local mailB4 = "</p></body></html>"
    local mailBody = mailB1 .. mailB2 .. mailB3 .. mailB4

    local mailer, err =
        mail.new(
        {
            host = "smtp.gmail.com",
            port = 587,
            starttls = true,
            username = ngx.var.appMail,
            password = ngx.var.appMailPassword
        }
    )

    local ok, err =
        mailer:send(
        {
            from = "Léková encyklopedie <.." .. ngx.var.appMail .. ">",
            to = {email},
            subject = mailTitle,
            html = mailBody
        }
    )
    return ok
end

function stringToHex(str)
    return (str:gsub(
        ".",
        function(c)
            return string.format("%02X", string.byte(c))
        end
    ))
end

function leapYear(year)
    if (year % 4000 == 0) then
        return false
    elseif (year % 400 == 0) then
        return true
    elseif (year % 100 == 0) then
        return false
    elseif (year % 4 == 0) then
        return true
    end
    return false
end

function validEmail(str)
    if str == nil or str == cjson.null then
        return false
    end
    if (type(str) ~= "string") then
        error("Expected string")
        return false
    end
    local lastAt = str:find("[^%@]+$")
    local localPart = str:sub(1, (lastAt - 2)) -- Returns the substring before '@' symbol
    local domainPart = str:sub(lastAt, #str) -- Returns the substring after '@' symbol
    -- we werent able to split the email properly
    if localPart == nil or localPart == cjson.null then
        return false, "Local name is invalid"
    end

    if domainPart == nil or domainPart == cjson.null then
        return false, "Domain is invalid"
    end
    -- local part is maxed at 64 characters
    if #localPart > 64 then
        return false, "Local name must be less than 64 characters"
    end
    -- domains are maxed at 253 characters
    if #domainPart > 253 then
        return false, "Domain must be less than 253 characters"
    end
    -- somthing is wrong
    if lastAt >= 65 then
        return false, "Invalid @ symbol usage"
    end
    -- quotes are only allowed at the beginning of a the local name
    local quotes = localPart:find('["]')
    if type(quotes) == "number" and quotes > 1 then
        return false, "Invalid usage of quotes"
    end
    -- no @ symbols allowed outside quotes
    if localPart:find("%@+") and quotes == nil then
        return false, "Invalid @ symbol usage in local part"
    end
    -- no dot found in domain name
    if not domainPart:find("%.") then
        return false, "No TLD found in domain"
    end
    -- only 1 period in succession allowed
    if domainPart:find("%.%.") then
        return false, "Too many periods in domain"
    end
    if localPart:find("%.%.") then
        return false, "Too many periods in local part"
    end
    -- just a general match
    if not str:match("[%w]*[%p]*%@+[%w]*[%.]?[%w]*") then
        return false, "Email pattern test failed"
    end
    -- all our tests passed, so we are ok
    return true
end

function checkDataForChangePassword(args)
    local password = args.password ~= nil and args.password ~= cjson.null and args.password ~= ""
    local username = args.username ~= nil and args.username ~= cjson.null and args.username ~= ""
    local newPassword = args.newPassword ~= nil and args.newPassword ~= cjson.null and args.newPassword ~= ""
    return {
        dataOk= password and username and newPassword,
        data ={
        password=password,
        username=username,
        newPassword = newPassword }
    }
end

function daysInMonth(month, year)
    if (month == 1 or month == 3 or month == 5 or month == 7 or month == 8 or month == 10 or month == 12) then
        return 31
    elseif (month == 4 or month == 6 or month == 9 or month == 11) then
        return 30
    else
        if (leapYear(year) == false) then
            return 28
        else
            return 29
        end
    end
end

function isCorrectDate(day, month, year)
    day = tonumber(day)
    month = tonumber(month)
    year = tonumber(year)
    if
        (year and year > 1900 and month and month >= 0 and month < 13 and day and day > 0 and
            day <= daysInMonth(month, year))
     then
        return true
    end
    return false
end

function checkEditData(args)
    local validMail = validEmail(args.email)
    local validDate =
        args.birthday ~= nil and args.birthday ~= cjson.null and
        isCorrectDate(args.birthday.day, args.birthday.month, args.birthday.year)
    local validGender = (args.gender == "male" or args.gender == "female")

    if (validDate and validGender and validMail) then
        return {
            dataOk = true
        }
    end
    return {
        dataOk = false,
        data = {
            email = (validMail and "OK" or "Bad data"),
            birthday = (validDate and "OK" or "Bad data"),
            gender = (validGender and "OK" or "Bad data")
        }
    }
end

function checkUserData(args)
    local validUsername = (args.username ~= nil and args.username ~= cjson.null and args.username ~= "")
    local validPassword = (args.password ~= nil and args.password ~= cjson.null and args.password ~= "")
    local validMail = validEmail(args.email)
    local validDate =
        args.birthday ~= nil and args.birthday ~= cjson.null and
        isCorrectDate(args.birthday.day, args.birthday.month, args.birthday.year)
    local validGender = (args.gender == "male" or args.gender == "female")

    if (validDate and validUsername and validGender and validMail and validPassword) then
        return {
            dataOk = true
        }
    end
    return {
        dataOk = false,
        data = {
            username = (validUsername and "OK" or "Bad data"),
            password = (validPassword and "OK" or "Bad data"),
            email = (validMail and "OK" or "Bad data"),
            birthday = (validDate and "OK" or "Bad data"),
            gender = (validGender and "OK" or "Bad data")
        }
    }
end

function urlEncode(str)
    if (str) then
        str = string.gsub(str, "\n", "\r\n")
        str =
            string.gsub(
            str,
            "([^%w ])",
            function(c)
                return string.format("%%%02X", string.byte(c))
            end
        )
        str = string.gsub(str, " ", "+")
    end
    return str
end

function checkMedicationData(args)
    if (type(args) ~= "table") then
        return {dataOk = false}
    end
    for k, v in pairs(args) do -- for every key in the table with a corresponding non-nil value
        local hours, doseHour, doseMinute, fType, title, description
        if (v == nil or v == cjson.null) then
            return {dataOk = false}
        end
        if (v["howOften"] == nil or v["howOften"] == cjson.null or v["howOften"] == cjson.null) then
            hours = false
        else
            hours = tonumber(v.howOften) and tonumber(v.howOften) > 0
        end
        if (v["howOftenType"] == nil or v["howOftenType"] == cjson.null) then
            fType = false
        else
            fType =
                (v["howOftenType"] == "days" or v["howOftenType"] == "hours" or v["howOftenType"] == "months" or
                v["howOftenType"] == "weeks")
        end
        nextDose = (v["nextDose"] == nil or v["nextDose"] == cjson.null or type(v["nextDose"]) == "string")
        description = type(v.description) == "string" or v.description == nil or v.description == cjson.null
        title = type(v._title) == "string"
    end
    return {
        dataOk = hours and howOften and nextDose and description and title,
        data = {
            howOften = (hours and "OK" or "Bad data"),
            howOftenType = (howOften and "OK" or "Bad data"),
            nextDose = (nextDose and "OK" or "Bad data"),
            description = (description and "OK" or "Bad data"),
            title = (title and "OK" or "Bad data")
        }
    }
end
