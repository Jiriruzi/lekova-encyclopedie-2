const webpack = require("webpack");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");
const helpers = require("helpers");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

function getPlugins(env = { production: false, development: true }) {
  const plugins = [
    //       new BundleAnalyzerPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new HtmlWebpackPlugin({
      // handlebars template
      template: "./index.hbs",
      hash: true,
      environment: env
    })
  ];
  console.log(
    `Building in environment:${env.development ? "development" : "production"}`
  );
  if (!env.development) {
    plugins.push(
      new webpack.DefinePlugin({
        "process.env": JSON.stringify("production")
      })
    );
    plugins.push(new UglifyJsPlugin());
    plugins.push(
      // include uglify in production
      new CompressionPlugin({
        algorithm: helpers.gzipMaxLevel,
        regExp: /\.css$|\.html$|\.js$|\.map$/,
        threshold: 2 * 1024
      })
    );
  }
  return plugins;
}

module.exports = function(env) {
  return {
    context: __dirname,
    devtool: !env.development ? "cheap-module-source-map" : "inline-source-map",
    entry: ["babel-polyfill", "./src/index.js"],
    optimization: {
      minimizer: [new UglifyJsPlugin()]
    },
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules|bower_components)/,
          loader: "babel-loader",
          options: {
            presets: ["react", "es2015", "stage-0"],
            plugins: [
              "react-html-attrs",
              "transform-decorators-legacy",
              "transform-class-properties"
            ]
          }
        },
        {
          test: /\.(png|woff|woff2|eot|ttf|svg|gif)$/,
          loader: "url-loader",
          options: {
            limit: 100000
          }
        },
        {
          test: /\.(css|scss)$/,
          loader: "style-loader!css-loader"
        }
      ]
    },
    resolve: {
      alias: {
        Actions: path.resolve(__dirname, "src/actions"),
        Components: path.resolve(__dirname, "src/components"),
        Libs: path.resolve(__dirname, "src/libs"),
        Images: path.resolve(__dirname, "images"),
        Modules: path.resolve(__dirname, "node_modules")
      }
    },
    devServer: {
      proxy: {
        "/user": {
          target: "http://localhost:5900"
        },
        "/documents": {
          target: "http://localhost:5900"
        },
        "/solr": {
          target: "http://localhost:5900"
        },
        "*bundle.*": {
          target: "http://localhost:8080",
          bypass: function(req, res, proxyOptions) {
            console.log(req.url);
            console.log("^bundle");

            var number = req.url.substring(
              req.url.lastIndexOf("js/") + 3,
              req.url.lastIndexOf(".bundle")
            );
            if (req.url.includes("bundle.js?")) {
              console.log(
                "/js/" +
                  req.url.substring(
                    req.url.indexOf("bundle.js?"),
                    req.url.length - 1
                  )
              );

              return (
                "/js/" +
                req.url.substring(
                  req.url.indexOf("bundle.js?"),
                  req.url.length - 1
                )
              );
            }
            if (isNaN(number)) return req.url;
            console.log("/js/" + number + ".bundle.js");
            return "/js/" + number + ".bundle.js";
          }
        },
        "/": {
          target: "http://localhost:8080",
          bypass: function(req, res, proxyOptions) {
            console.log(req.url);
            console.log("^/");

            if (req.url.includes("bundle.js?")) {
              console.log(
                "/js/" +
                  req.url.substring(
                    req.url.indexOf("bundle.js?"),
                    req.url.length - 1
                  )
              );
              return (
                "/js/" +
                req.url.substring(
                  req.url.indexOf("bundle.js?"),
                  req.url.length - 1
                )
              );
            }
            if (
              req.url.includes("/user") ||
              req.url.includes("/solr") ||
              req.url.includes("/documents") ||
              req.url.includes("bundle")
            )
              return req.url;
            return "/";
          }
        },
        "*": {
          target: "http://localhost:8080",
          bypass: function(req, res, proxyOptions) {
            if (req.url.includes("bundle.js?")) {
              console.log(
                "/js/" +
                  req.url.substring(
                    req.url.indexOf("bundle.js?"),
                    req.url.length - 1
                  )
              );
              return (
                "/js/" +
                req.url.substring(
                  req.url.indexOf("bundle.js?"),
                  req.url.length - 1
                )
              );
            }
            return req.url;
          }
        }
      }
    },
    mode: env && env.development ? "development" : "production",
    output: {
      path: path.resolve(__dirname, "build"),
      filename: "js/bundle.js"
    },
    plugins: getPlugins(env)
  };
};
