import React from "react";
import RegistrationForm from "../../src/components/user/RegistrationForm.jsx";
import { errorValidator } from "../../src/components/user/RegistrationForm.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

import { createBrowserHistory as createHistory } from "history";

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("RegistrationForm renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <RegistrationForm history={history} />
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "doc" }
  });
  store.dispatch({
    type: "USER_FOUND",
    payload: {
      birthday: { day: 17, year: 2018, month: 9 },
      gender: "male",
      email: "email@em.em"
    }
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("RegistrationForm redirects when logged", () => {
  history.location.pathname = "/Registration";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <RegistrationForm history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/");
});

test("RegistrationForm does not redirect when not logged", () => {
  history.location.pathname = "/Registration";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <RegistrationForm history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/Registration");
});

test("error validator works correctly", () => {
  expect(
    errorValidator({
      birthday: Date.now(),
      username: "user",
      password: "pass",
      passwordConfirmation: "pass",
      email: "aa@aa.cz",
      gender: "male"
    })
  ).toEqual({
    birthday: null,
    username: null,
    password: null,
    passwordConfirmation: null,
    email: null,
    gender: null
  });
  expect(
    errorValidator({
      birthday: null,
      username: null,
      password: null,
      passwordConfirmation: null,
      email: null,
      gender: null
    })
  ).toEqual({
    birthday: "MissingDate",
    username: "MissingUsername",
    password: "MissingPassword",
    passwordConfirmation: "DifferentPasswords",
    email: "MissingEmail",
    gender: "MissingGender"
  });
  expect(
    errorValidator({
      birthday: Date.now(),
      username: "u°1s3##e66%%6/\*r",
      password: "pass",
      passwordConfirmation: "passeeee",
      email: "aaaa.cz",
      gender: "male"
    })
  ).toEqual({
    birthday: null,
    username: "InvalidUsername",
    password: null,
    passwordConfirmation: "DifferentPasswords",
    email: "InvalidEmail",
    gender: null
  });
});
