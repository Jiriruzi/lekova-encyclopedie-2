import React from "react";
import MedicationList from "../../src/components/user/MedicationList.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";
const store = createStore(allReducers, applyMiddleware(thunk));

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

let medication = {
  "1": {
    _type: "medicinalProduct",
    _id:
      "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
    enc_title: "ASDUTER 10 MG TABLETY ",
    howOftenType: "days",
    howOften: "1",
    nextDose: "2018-11-16T20:30:55.000Z",
    description: "desc",
    amount: "250",
    key: 1,
    type: "Emulsion"
  },
  "2": {
    _type: "medicinalProduct",
    _id:
      "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
    enc_title: "AASDUTER 20 MG TABLETY",
    howOftenType: "days",
    howOften: "1",
    nextDose: "2018-11-15T20:30:55.000Z",
    description: "desc",
    amount: "200",
    key: 2,
    type: "Emulsion"
  }
};

test("MedicationList renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <MedicationList
            language="cs"
            context="enc:"
            hasInteraction={false}
            medication={medication}
          />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("MedicationList applies sort", () => {
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <MedicationList
          hasInteraction={false}
          language="cs"
          context="enc:"
          medication={medication}
        />
      </BrowserRouter>
    </Provider>
  );
  wrapper.find("select").simulate("change", {
    target: { value: "title" }
  });
  expect(toJson(wrapper)).toMatchSnapshot();
});
