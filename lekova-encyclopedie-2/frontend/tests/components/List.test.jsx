import React from "react";
import List from "../../src/components/List/List.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";

const data = {
  _id: "http://linked.opendata.cz/resource/sukl/medicinal-product/APO-SIMVA-10",
  "enc:hasATCConcept": {
    "enc:broaderTransitive": [
      {
        "enc:notation": "C10",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10",
        "enc:title": [
          { "@value": "Lipid modifying agents", "@language": "en" },
          {
            "@value": "L\\u00E1tky upravuj\\u00EDc\\u00ED hladinu lipid\\u016F",
            "@language": "cs"
          }
        ]
      },
      {
        "enc:notation": "C",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C",
        "enc:title": [
          {
            "@value": "Kardiovaskul\\u00E1rn\\u00ED syst\\u00E9m",
            "@language": "cs"
          },
          { "@value": "Cardiovascular system", "@language": "en" }
        ]
      },
      {
        "enc:notation": "C10A",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10A",
        "enc:title": [
          { "@value": "Lipid modifying agents, plain", "@language": "en" },
          {
            "@value":
              "L\\u00E1tky upravuj\\u00EDc\\u00ED hladinu lipid\\u016F, samotn\\u00E9",
            "@language": "cs"
          }
        ]
      },
      {
        "enc:notation": "C10AA",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10AA",
        "enc:title": [
          { "@value": "Inhibitory HMG-CoA redukt\\u00E1zy", "@language": "cs" },
          { "@value": "HMG CoA reductase inhibitors", "@language": "en" }
        ]
      }
    ],
    "enc:notation": "C10AA01",
    "@type": "enc:ATCConcept",
    "@id": "http://linked.opendata.cz/resource/atc/C10AA01",
    "enc:title": [
      { "@value": "Simvastatin", "@language": "en" },
      { "@value": "Simvastatin", "@language": "cs" }
    ]
  },
  "enc:hasActiveIngredient": {
    "enc:description": {
      "@value":
        "A derivative of LOVASTATIN and potent competitive inhibitor of 3-hydroxy-3-methylglutaryl coenzyme A reductase (HYDROXYMETHYLGLUTARYL COA REDUCTASES), which is the rate-limiting enzyme in cholesterol biosynthesis. It may also interfere with steroid hormone production. Due to the induction of hepatic LDL RECEPTORS, it increases breakdown of LDL CHOLESTEROL.     ",
      "@language": "en"
    },
    "@type": "enc:Ingredient",
    "enc:hasPregnancyCategory": {
      "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/X"
    },
    "enc:contraindicatedWith": [
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000003527",
        "enc:title": [
          { "@value": "Selh\\u00E1n\\u00ED jater", "@language": "cs" },
          { "@value": "Liver failure", "@language": "en" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
        "enc:title": [
          { "@value": "Drug hypersensitivity", "@language": "en" },
          { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010195",
        "enc:title": { "@value": "Pregnancy", "@language": "en" }
      }
    ],
    "@id":
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0029425",
    "enc:hasPharmacologicalAction": [
      {
        "enc:description": [
          {
            "@value":
              "L\\u00E1tky u\\u017E\\u00EDvan\\u00E9 ke sn\\u00ED\\u017Een\\u00ED hladiny cholesterolu v plazm\\u011B.     ",
            "@language": "en"
          },
          {
            "@value":
              "Substances used to lower plasma CHOLESTEROL levels.     ",
            "@language": "en"
          }
        ],
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001378",
        "enc:title": [
          { "@value": "Anticholesteremic agents", "@language": "en" },
          { "@value": "Anticholesteremika", "@language": "cs" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Compounds that inhibit HMG-CoA reductases. They have been shown to directly lower cholesterol synthesis.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0028567",
        "enc:title": [
          { "@value": "Statiny", "@language": "cs" },
          {
            "@value": "Hydroxymethylglutaryl-coa reductase inhibitors",
            "@language": "en"
          }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Substances that lower the levels of certain LIPIDS in the BLOOD. They are used to treat HYPERLIPIDEMIAS.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001459",
        "enc:title": [
          { "@value": "Hypolipidemika", "@language": "cs" },
          { "@value": "Hypolipidemic agents", "@language": "en" }
        ]
      }
    ],
    "enc:title": [
      { "@value": "Simvastatin", "@language": "en" },
      "Simvastatinum",
      { "@value": "Simvastatin", "@language": "cs" }
    ]
  },
  "@id":
    "http://linked.opendata.cz/resource/sukl/medicinal-product/APO-SIMVA-10",
  "enc:title": { "@value": "APO-SIMVA 10", "@language": "cs" },
  "enc:hasPregnancyCategory": [
    {
      "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/X"
    },
    { "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C" }
  ]
};

const config = {
  list: [
    {
      property: "hasActiveIngredient",
      sublist: [
        {
          property: "hasPharmacologicalAction",
          label: "PharmacologicalAction",
          propOfProp: [["title"]]
        },
        {
          property: "hasPregnancyCategory",
          label: "PregnancyCategory",
          class: "not-active normal-text",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "ingredient" },
      class: "ingredient-list col-xs-12 col-md-6",
      label: "Ingredient"
    },
    {
      property: "hasATCConcept",
      value: {
        connect: [{ property: "broaderTransitive" }],
        property: [["notation"], ["title"]],
        class: "linked-item"
      },
      label: "ATCConcept",
      class: "linked-list col-xs-12 col-md-6"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    }
  ]
};

test("List renders correctly when no data available", () => {
  const component = renderer
    .create(<List context="enc:" language="cs" />)
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("List renders correctly when data available - list with sublist", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <List
          context="enc:"
          language="cs"
          confDoc={config.list[0]}
          datDoc={data}
        />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});


test("List renders correctly when data available - list with properties connected to one string", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <List
          context="enc:"
          language="cs"
          confDoc={config.list[1]}
          datDoc={data}
        />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});


test("List renders correctly when data available - basic list", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <List
          context="enc:"
          language="cs"
          confDoc={config.list[2]}
          datDoc={data}
        />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
