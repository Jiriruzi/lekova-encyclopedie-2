import React from "react";
import HomeButtons from "../../src/components/Home/HomeButtons.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";

test("HomeButtons renders correctly", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <HomeButtons />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
