import React from "react";
import EncHistory, {
  PureEncHistory
} from "../../src/components/user/EncHistory.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";
import moment from "moment";
import { createBrowserHistory as createHistory } from "history";

let history = createHistory();

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

const store = createStore(allReducers, applyMiddleware(thunk));

test("EncHistory renders correctly when history loading", () => {
  store.dispatch({
    type: "HISTORY_LOADING"
  });
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <EncHistory history={history} />
      </Router>
    </Provider>
  );
  expect(component.toJSON()).toMatchSnapshot();
});

test("EncHistory renders correctly when history loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <EncHistory history={history} />
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "GET_HISTORY",
    payload: [
      {
        "@type": "MedicinalProduct",
        "enc:title": "MP",
        time: now - 10,
        "@id": "id1"
      },
      {
        "@type": "Ingredient",
        "enc:title": "ING",
        time: now - 10,
        "@id": "id2"
      }
    ]
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("EncHistory does not redirect when logged correctly", () => {
  history.location.pathname = "/History";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "expert" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history} history={history}>
        <EncHistory history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/History");
});

test("EncHistory redirects when not logged", () => {
  history.location.pathname = "/History";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history} history={history}>
        <EncHistory history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/Login");
});

test("EncHistory redirects when logged with wrong role", () => {
  history.location.pathname = "/History";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EncHistory history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/");
});

test("Date filtering works correctly", () => {
  let instance = shallow(
    <PureEncHistory
      language="cs"
      userHistory={{
        history: [
          {
            "@type": "MedicinalProduct",
            title: "MP",
            time: now - 10,
            "@id": "id1"
          },
          { "@type": "Ingredient", title: "ING", time: now - 1, "@id": "id2" }
        ]
      }}
      store={store}
      getHistory={() => {}}
      history={history}
      context=""
    />
  ).instance();
  expect(instance.getFilteredHistory()).toEqual([
    {
      type: "MedicinalProduct",
      id: "id1",
      name: "MP"
    },
    { name: "ING", type: "Ingredient", id: "id2" }
  ]);
  instance.setDate({ target: { value: "0-" + (now - 2) } });
  expect(instance.getFilteredHistory()).toEqual([
    {
      type: "MedicinalProduct",
      id: "id1",
      name: "MP"
    }
  ]);
});

test("Type filtering works correctly", () => {
  let instance = shallow(
    <PureEncHistory
      language="cs"
      userHistory={{
        history: [
          {
            "@type": "MedicinalProduct",
            title: "MP",
            time: now - 10,
            "@id": "id1"
          },
          { "@type": "Ingredient", title: "ING", time: now - 1, "@id": "id2" }
        ]
      }}
      store={store}
      getHistory={() => {}}
      history={history}
      context=""
    />
  ).instance();
  expect(instance.getFilteredHistory()).toEqual([
    {
      type: "MedicinalProduct",
      id: "id1",
      name: "MP"
    },
    { name: "ING", type: "Ingredient", id: "id2" }
  ]);
  instance.setType({ target: { value: "Ingredient" } });
  expect(instance.getFilteredHistory()).toEqual([
    { name: "ING", type: "Ingredient", id: "id2" }
  ]);
});
