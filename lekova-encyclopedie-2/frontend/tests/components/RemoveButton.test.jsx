import React from "react";
import RemoveButton from "../../src/components/user/RemoveButton.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";
const store = createStore(allReducers, applyMiddleware(thunk));

test("RemoveButton renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <RemoveButton />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("RemoveButton does nothing on enter", () => {
  let a = 0;
  let b = 0;
  const component = shallow(
    <RemoveButton
      remove={(id, medicament) => {
        a++;
        b++;
      }}
      id={a}
      medicament={b}
    />
  );
  component.find("button").simulate("keypress", { key: "Enter" });
  expect(a).toBe(0);
  expect(b).toBe(0);
});

test("RemoveButton works on click", () => {
  let a = 0;
  let b = 0;
  const component = shallow(
    <RemoveButton
      remove={(id, medicament) => {
        a++;
        b++;
      }}
      id={a}
      medicament={b}
    />
  );
  component.find("button").simulate("click", new Event("click"));
  expect(a).toBe(1);
  expect(b).toBe(1);
});
