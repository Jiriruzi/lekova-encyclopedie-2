import React from "react";
import NoMatch from "../../src/components/NoMatch/NoMatch.jsx";
import renderer from "react-test-renderer";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

const store = createStore(allReducers, applyMiddleware(thunk));

test("NoMatch renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <NoMatch language="cs" />
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
