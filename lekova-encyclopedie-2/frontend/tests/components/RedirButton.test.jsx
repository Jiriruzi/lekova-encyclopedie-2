import React from "react";
import RedirButton from "../../src/components/user/RedirButton.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";
import faCheck from "@fortawesome/fontawesome-free-solid/faCheck";

const store = createStore(allReducers, applyMiddleware(thunk));

test("RedirButton renders correctly", () => {
  let a = 0;
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <RedirButton icon={faCheck} redir={() => a++} />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("RedirButton does nothing on enter", () => {
  let a = 0;
  const component = shallow(<RedirButton icon={faCheck} redir={() => a++} />);
  component.find("button").simulate("keypress", { key: "Enter" });
  expect(a).toBe(0);
});

test("RedirButton works on click", () => {
  let a = 0;
  const component = shallow(<RedirButton icon={faCheck} redir={() => a++} />);
  component.find("button").simulate("click", {});
  expect(a).toBe(1);
});
