import React from "react";
import Header from "../../src/components/App/Header.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

test("Header renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <Header />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("Changing state of showing userBox is works correctly", () => {
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <Header />
      </BrowserRouter>
    </Provider>
  );

  wrapper.find("#user-button").simulate("click");
  expect(store.getState().environment.showUserBox).toBe(true);
  wrapper.find("#user-button").simulate("click");
  expect(store.getState().environment.showUserBox).toBe(false);
});
