import React from "react";
import BasicInfo from "../../src/components/BasicInfo/BasicInfo.jsx";
import renderer from "react-test-renderer";
const data = {
  "enc:description": "This is description"
};

test("BasicInfo renders correctly", () => {
  const component = renderer
    .create(
      <BasicInfo
        context="enc:"
        language="cs"
        confDoc={{
          property: "description",
          value: {
            class: "normal-text"
          },
          class: "col-xs-12"
        }}
        datDoc={data}
      />
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
