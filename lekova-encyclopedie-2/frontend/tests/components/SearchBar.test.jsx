import React from "react";
import SearchBar from "../../src/components/Search/SearchBar.jsx";
import renderer from "react-test-renderer";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";

const store = createStore(allReducers, applyMiddleware(thunk));

test("SearchBar renders correctly", () => {
  const component = renderer
    .create(<SearchBar sections={[]} environment={{ language: "cs" }} />)
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("onChange and additionalOnChange works correctly", () => {
  let a = 0;
  const wrapper = mount(
    <SearchBar
      aditionalOnChange={() => a++}
      sections={[]}
      environment={{ language: "cs" }}
      store={store}
      value=""
      loading={false}
      setSearchValue={value =>
        store.dispatch({
          type: "SET_SEARCH_VALUE_HEADER",
          payload: value
        })
      }
      suggest={() => ["A", "b"]}
    />
  );
  wrapper.find("input").simulate("change", { target: { value: "V" } });
  expect(a).toBe(1);
  expect(store.getState().search.valueHeader).toBe("V");
});

test("onEnter works correctly when prop onEnter", () => {
  let a = 0;
  const wrapper = mount(
    <SearchBar
      value=""
      loading={false}
      sections={[]}
      environment={{ language: "cs" }}
      store={store}
      setSearchValue={value =>
        store.dispatch({
          type: "SET_SEARCH_VALUE_HEADER",
          payload: value
        })
      }
      onEnter={val => (a = val)}
      suggestions={["A", "b"]}
      value="value"
      onSuggestionSelected={(e, val) => (a = val)}
      suggest={() => ["A", "b"]}
    />
  );
  wrapper.find("input").simulate("keypress", { key: "Enter" });
  expect(a).toBe("value");
});

test("onSugestionSelected works correctly", () => {
  let a = 0;
  const wrapper = mount(
    <SearchBar
      value=""
      loading={false}
      sections={[]}
      environment={{ language: "cs" }}
      store={store}
      setSearchValue={value =>
        store.dispatch({
          type: "SET_SEARCH_VALUE_HEADER",
          payload: value
        })
      }
      suggestions={["A", "b"]}
      value="value"
      onSuggestionSelected={(e, val) => (a = val)}
      suggest={() => ["A", "b"]}
    />
  );
  wrapper.instance().onSuggestionSelected(new Event("keypressed"), "value");
  expect(a).toBe("value");
});
