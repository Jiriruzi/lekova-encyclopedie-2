import React from "react";
import PIL from "../../src/components/PIL/PIL.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";

const store = createStore(allReducers, applyMiddleware(thunk));

test("PIL renders correctly", () => {
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "PIL",
      doc: { pilHTML: { value: "<div>Hello PIL!</div>" } }
    }
  });
  const component = renderer.create(<PIL store={store} context="" />).toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("PIL renders correctly when loading", () => {
  store.dispatch({
    type: "DOCUMENT_LOADING",
    payload: "PIL"
  });
  const component = renderer.create(<PIL context="" store={store} />).toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("PIL renders correctly when error", () => {
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "PIL"
  });
  const component = renderer.create(<PIL language="cs" store={store} />).toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});