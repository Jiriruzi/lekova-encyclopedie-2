import React from "react";
import MedicinalProductPackaging from "../../src/components/MedicinalProductPackaging/MedicinalProductPackaging.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someConfig = {
  _id: "MedicinalProductPackaging",
  _rev: "1-26322ad183c4dd60c441bb00b6581c3e",
  basicInfo: [
    {
      property: "hasIndication",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "Indication"
    },
    {
      property: "hasIndicationGroup",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "IndicationGroup"
    },
    {
      justLabel: true,
      label: "PackagingDetail",
      class: "important-prop col-xs-12"
    },
    {
      property: "hasStrength",
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "Strength"
    },
    {
      property: "hasPackagingSize",
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "PackagingSize"
    },
    {
      property: [
        ["hasPriceSpecification", "gr:hasCurrencyValue"],
        ["hasPriceSpecification", "gr:hasCurrency"]
      ],
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "Price"
    },
    {
      property: [
        ["hasReimbursementSpecification", "gr:hasCurrencyValue"],
        ["hasReimbursementSpecification", "gr:hasCurrency"]
      ],
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "PriceReimbursement"
    },
    {
      property: "containsDefinedDailyDoses",
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "DefinedDailyDoses"
    },
    {
      property: "hasRouteOfAdministration",
      value: { class: "normal-text" },
      class: "col-md-2 col-xs-4 packaging-prop",
      label: "RouteOfAdministration"
    }
  ],
  list: [
    {
      property: "hasActiveIngredient",
      sublist: [
        {
          property: "hasPharmacologicalAction",
          label: "PharmacologicalAction",
          propOfProp: [["title"]]
        },
        {
          property: "hasPregnancyCategory",
          label: "PregnancyCategory",
          class: "not-active normal-text",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "ingredient" },
      class: "ingredient-list col-xs-12 col-md-6",
      label: "Ingredient"
    },
    {
      property: "hasATCConcept",
      value: {
        connect: [{ property: "broaderTransitive" }],
        property: [["notation"], ["title"]],
        class: "linked-item"
      },
      label: "ATCConcept",
      class: "linked-list col-xs-12 col-md-6"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    },
    {
      class: "product-list col-xs-12",
      property: "hasMedicinalProduct",
      value: { property: "title", class: "" },
      label: "MedicinalProductOne"
    }
  ]
};

const price = {
  _id:
    "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252",
  _rev: "1-febe3c1df978ae3de3cd05fda2869114",
  "@context": {
    enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
    gr: "http://purl.org/goodrelations/v1#",
    xsd: "http://www.w3.org/2001/XMLSchema#"
  },
  "@id":
    "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252",
  "@type": "enc:MedicinalProductPackaging",
  "enc:hasAveragePriceSpecification": [
    {
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/mzcr/2016-1-22",
      "@type": "gr:PriceSpecification",
      "gr:hasCurrency": "CZK",
      "gr:hasCurrencyValue": { "@type": "xsd:decimal", "@value": "128.12" },
      "gr:validFrom": { "@type": "xsd:date", "@value": "2016-1-22" },
      "gr:validThrough": { "@type": "xsd:date", "@value": "2016-2-21" },
      "gr:valueAddedTaxIncluded": { "@type": "xsd:integer", "@value": "1" }
    }
  ],
  "enc:hasMedicinalProduct": {
    "@id":
      "http://linked.opendata.cz/resource/sukl/medicinal-product/CORSIM-10",
    "@type": "enc:MedicinalProduct",
    "enc:title": { "@language": "cs", "@value": "CORSIM 10" }
  },
  "enc:hasPackagingSize": "100",
  "enc:hasPriceSpecification": [
    {
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/price/2014-11-19",
      "@type": "gr:PriceSpecification",
      "gr:hasCurrency": "CZK",
      "gr:hasCurrencyValue": { "@type": "xsd:decimal", "@value": "641.86" },
      "gr:validFrom": { "@type": "xsd:date", "@value": "2014-11-19" },
      "gr:validThrough": { "@type": "xsd:date", "@value": "2014-11-30" },
      "gr:valueAddedTaxIncluded": { "@type": "xsd:boolean", "@value": "true" }
    }
  ],
  "enc:hasReimbursementSpecification": [
    {
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/reimbursement/2014-10-02",
      "@type": "gr:PriceSpecification",
      "gr:hasCurrency": "CZK",
      "gr:hasCurrencyValue": { "@type": "xsd:decimal", "@value": "108.83" },
      "gr:validFrom": { "@type": "xsd:date", "@value": "2014-10-02" },
      "gr:validThrough": { "@type": "xsd:date", "@value": "2014-10-31" },
      "gr:valueAddedTaxIncluded": { "@type": "xsd:boolean", "@value": "true" }
    }
  ],
  "enc:hasStrength": "10MG",
  "enc:hasTitleSupplement": { "@language": "cs", "@value": "TBL FLM 100X10MG" },
  "enc:title": { "@language": "cs", "@value": "CORSIM 10" }
};
const someData = {
  _id:
    "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252",
  _rev: "1-20c6ba5a933d785344d980fa1ac1b75e",
  "enc:hasTitleSupplement": { "@value": "TBL FLM 100X10MG", "@language": "cs" },
  "enc:containsDefinedDailyDoses": {
    "@value": "33.3334",
    "@type": "xsd:float"
  },
  "enc:hasRouteOfAdministration": [
    { "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED", "@language": "cs" },
    { "@value": "Oral use", "@language": "en" },
    { "@value": "Usus peroralis", "@language": "la" }
  ],
  "@type": "enc:MedicinalProductPackaging",
  "enc:hasPregnancyCategory": {
    "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/X"
  },
  "enc:hasResumedMarketingDate": [
    { "@value": "2016-03-16", "@type": "xsd:date" },
    { "@value": "2009-09-01", "@type": "xsd:date" },
    { "@value": "2014-03-03", "@type": "xsd:date" }
  ],
  "enc:hasMedicinalProduct": {
    "@type": "enc:MedicinalProduct",
    "@id":
      "http://linked.opendata.cz/resource/sukl/medicinal-product/CORSIM-10",
    "enc:title": { "@value": "CORSIM 10", "@language": "cs" }
  },
  "enc:deliveredInLastThreeMonths": {
    "@value": "true",
    "@type": "xsd:boolean"
  },
  "@context": {
    xsd: "http://www.w3.org/2001/XMLSchema#",
    enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
    gr: "http://purl.org/goodrelations/v1#"
  },
  "enc:hasIndicationGroup": { "@value": "Hypolipidaemica", "@language": "cs" },
  "enc:title": { "@value": "CORSIM 10", "@language": "cs" },
  "enc:hasATCConcept": {
    "enc:broaderTransitive": [
      {
        "enc:notation": "C10",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10",
        "enc:title": [
          { "@value": "Lipid modifying agents", "@language": "en" },
          {
            "@value": "L\\u00E1tky upravuj\\u00EDc\\u00ED hladinu lipid\\u016F",
            "@language": "cs"
          }
        ]
      },
      {
        "enc:notation": "C10A",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10A",
        "enc:title": [
          {
            "@value":
              "L\\u00E1tky upravuj\\u00EDc\\u00ED hladinu lipid\\u016F, samotn\\u00E9",
            "@language": "cs"
          },
          { "@value": "Lipid modifying agents, plain", "@language": "en" }
        ]
      },
      {
        "enc:notation": "C10AA",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C10AA",
        "enc:title": [
          { "@value": "HMG CoA reductase inhibitors", "@language": "en" },
          { "@value": "Inhibitory HMG-CoA redukt\\u00E1zy", "@language": "cs" }
        ]
      },
      {
        "enc:notation": "C",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C",
        "enc:title": [
          { "@value": "Cardiovascular system", "@language": "en" },
          {
            "@value": "Kardiovaskul\\u00E1rn\\u00ED syst\\u00E9m",
            "@language": "cs"
          }
        ]
      }
    ],
    "enc:notation": "C10AA01",
    "@type": "enc:ATCConcept",
    "@id": "http://linked.opendata.cz/resource/atc/C10AA01",
    "enc:title": [
      { "@value": "Simvastatin", "@language": "cs" },
      { "@value": "Simvastatin", "@language": "en" }
    ]
  },
  "enc:hasRegistrationState": {
    "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
  },
  "enc:hasDefinedDailyDose": {
    "enc:hasUnit": { "@id": "http://linked.opendata.cz/resource/sukl/unit/MG" },
    "enc:hasValue": { "@value": "30.0", "@type": "xsd:float" },
    "@id":
      "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/ddd"
  },
  "enc:hasPriceSpecification": {
    "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
    "@type": "gr:PriceSpecification",
    "gr:valueAddedTaxIncluded": { "@value": "true", "@type": "xsd:boolean" },
    "gr:hasCurrency": "CZK",
    "gr:hasCurrencyValue": { "@value": "427.99", "@type": "xsd:decimal" },
    "@id":
      "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/price/2016-09-01"
  },
  "enc:hasPackagingSize": "100",
  "enc:hasReimbursementSpecification": {
    "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
    "@type": "gr:PriceSpecification",
    "gr:valueAddedTaxIncluded": { "@value": "true", "@type": "xsd:boolean" },
    "gr:hasCurrency": "CZK",
    "gr:hasCurrencyValue": { "@value": "98.11", "@type": "xsd:decimal" },
    "@id":
      "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252/reimbursement/2016-09-01"
  },
  "enc:hasActiveIngredient": {
    "enc:description": {
      "@value":
        "A derivative of LOVASTATIN and potent competitive inhibitor of 3-hydroxy-3-methylglutaryl coenzyme A reductase (HYDROXYMETHYLGLUTARYL COA REDUCTASES), which is the rate-limiting enzyme in cholesterol biosynthesis. It may also interfere with steroid hormone production. Due to the induction of hepatic LDL RECEPTORS, it increases breakdown of LDL CHOLESTEROL.     ",
      "@language": "en"
    },
    "@type": "enc:Ingredient",
    "enc:hasPregnancyCategory": {
      "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/X"
    },
    "enc:contraindicatedWith": [
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000003527",
        "enc:title": [
          { "@value": "Liver failure", "@language": "en" },
          { "@value": "Selh\\u00E1n\\u00ED jater", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
        "enc:title": [
          { "@value": "Drug hypersensitivity", "@language": "en" },
          { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010195",
        "enc:title": { "@value": "Pregnancy", "@language": "en" }
      }
    ],
    "@id":
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0029425",
    "enc:hasPharmacologicalAction": [
      {
        "enc:description": [
          {
            "@value":
              "L\\u00E1tky u\\u017E\\u00EDvan\\u00E9 ke sn\\u00ED\\u017Een\\u00ED hladiny cholesterolu v plazm\\u011B.     ",
            "@language": "en"
          },
          {
            "@value":
              "Substances used to lower plasma CHOLESTEROL levels.     ",
            "@language": "en"
          }
        ],
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001378",
        "enc:title": [
          { "@value": "Anticholesteremic agents", "@language": "en" },
          { "@value": "Anticholesteremika", "@language": "cs" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Substances that lower the levels of certain LIPIDS in the BLOOD. They are used to treat HYPERLIPIDEMIAS.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001459",
        "enc:title": [
          { "@value": "Hypolipidemic agents", "@language": "en" },
          { "@value": "Hypolipidemika", "@language": "cs" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Compounds that inhibit HMG-CoA reductases. They have been shown to directly lower cholesterol synthesis.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0028567",
        "enc:title": [
          { "@value": "Statiny", "@language": "cs" },
          {
            "@value": "Hydroxymethylglutaryl-coa reductase inhibitors",
            "@language": "en"
          }
        ]
      }
    ],
    "enc:title": [
      { "@value": "Simvastatin", "@language": "cs" },
      { "@value": "Simvastatin", "@language": "en" },
      "Simvastatinum"
    ]
  },
  "enc:hasSPCDocument":
    "http://www.sukl.cz/modules/medication/download.php?file=SPC64668.pdf&type=spc&as=corsim-10-spc",
  "enc:hasStrength": "10MG",
  "@id":
    "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252",
  "enc:hasBreakMarketingDate": [
    { "@value": "2015-11-24", "@type": "xsd:date" },
    { "@value": "2013-11-18", "@type": "xsd:date" }
  ]
};

test("MedicinalProductPackaging starts loading on mount", () => {
  expect(store.getState().documents.MedicinalProductPackaging).toBe(undefined)
  expect(store.getState().documents.Price).toBe(undefined)
  expect(store.getState().documents.PIL).toBe(undefined)
  expect(store.getState().documents.Configuration).toBe(undefined)

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProductPackaging
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
  expect(store.getState().documents.Price.loading).toBeTruthy();
  expect(store.getState().documents.PIL.loading).toBeTruthy();
  expect(store.getState().documents.MedicinalProductPackaging.loading).toBeTruthy();

});

test("MedicinalProductPackaging renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProductPackaging
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProductPackaging renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProductPackaging
          store={store}
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProductPackaging",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Price",
      doc: price
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProductPackaging renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProductPackaging
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "MedicinalProductPackaging"
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Price",
      doc: price
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProductPackaging renders cheapest packages when param is in url", () => {
  const wrapper = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProductPackaging
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
              }
            },
            location: {
              search: "?cheapest",
              pathname:
                "/MedicinalProductPackaging/http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0013252"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProductPackaging",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Price",
      doc: price
    }
  });
  expect(wrapper.toJSON()).toMatchSnapshot();
});

