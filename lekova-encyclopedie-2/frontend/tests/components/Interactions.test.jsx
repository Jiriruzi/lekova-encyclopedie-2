import React from "react";
import Interactions from "../../src/components/Interactions/Interactions.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someData = {
  _id:
    "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0010965",
  _rev: "1-d53ce0d319d332153cdfc2255040aa92",
  "enc:description": {
    "@value":
      "A nonsteroidal anti-inflammatory agent with analgesic properties used in the therapy of rheumatism and arthritis.     ",
    "@language": "en"
  },
  "@type": "enc:Ingredient",
  "enc:title": [
    { "@value": "Ibuprofen", "@language": "en" },
    "Ibuprofenum",
    { "@value": "Ibuprofen", "@language": "cs" }
  ],
  "enc:hasInteraction": [
    {
      "enc:hasDescription": [
        {
          "@value":
            "Současné užívání látek inhibujících syntézu prostaglandinů (např. ibuprofen, indometacin) může vést ke snížení hypotenzního účinku atenololu.",
          "enc:source": {
            "enc:url": "https://www.sukl.cz/",
            "enc:title": "sukl"
          }
        },
        {
          "@value": "Risk of inhibition of renal prostaglandins ",
          "enc:source": {
            "enc:url": "https://www.drugbank.ca/drugs/DB01050",
            "enc:title": "drugbank"
          }
        }
      ],
      "enc:interactionWith": {
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0001900",
        "enc:title": [
          { "@value": "Atenolol", "@language": "en" },
          "Natrii selenis",
          { "@value": "Atenolol", "@language": "cs" }
        ]
      }
    },
    {
      "enc:hasDescription": [
        {
          "@value": "Risk of inhibition of renal prostaglandins",
          "enc:source": {
            "enc:url": "https://www.drugbank.ca/",
            "enc:title": "drugbank"
          }
        }
      ],
      "enc:interactionWith": {
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/sukl/19690",
        "enc:title": [
          { "@value": "Refametinib", "@language": "en" },
          { "@value": "Refametinib", "@language": "cs" },
          "Refametinibum"
        ]
      },
      "enc:isCritical": true
    }
  ],
  "@id":
    "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0010965"
};

test("Interactions renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Interactions ingredients={someData} context="enc:" language="cs" />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Interactions renders correctly - critical first", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Interactions criticalFirst ingredients={someData} context="enc:" language="cs" />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});
test("Interactions renders correctly - with children and no interactions", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Interactions criticalFirst ingredients={{}} context="enc:" language="cs" >
        Children
        </Interactions>
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});
test("Interactions renders correctly - with children and avaiable interactions", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Interactions criticalFirst ingredients={someData} context="enc:" language="cs" >
        Children
        </Interactions>
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Interactions renders correctly when no data is available", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Interactions />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

