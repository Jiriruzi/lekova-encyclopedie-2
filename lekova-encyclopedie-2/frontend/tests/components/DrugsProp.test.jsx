import React from "react";
import DrugsProp from "../../src/components/DrugsProp/DrugsProp.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";

const store = createStore(allReducers, applyMiddleware(thunk));

const someConfig = {
  _id: "PhysiologicEffect",
  _rev: "1-53a343c682bd6134af5fa0a1b7bd05da",
  basicInfo: [
    {
      property: "description",
      class: "col-xs-12 desc"
    }
  ],
  list: [
    {
      sublist: [
        {
          property: "hasMedicinalProduct",
          class: "active",
          propOfProp: [["title"]]
        }
      ],
      property: "hasActiveIngredient",
      value: {
        property: "title",
        class: "finding-item"
      },
      class: "col-xs-12 finding-list"
    }
  ]
};

const someData = {
  _id:
    "http://linked.opendata.cz/resource/ndfrt/physiologic-effect/N0000008296",
  _rev: "1-e9d199309c54d5fb8a8b7f5b6f59ba59",
  "@type": "enc:PhysiologicEffect",
  "enc:hasActiveIngredient": [
    {
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0056682",
      "enc:title": {
        "@value": "Aluminum chloride",
        "@language": "en"
      }
    },
    {
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0067386",
      "enc:title": {
        "@value": "Aluminum chlorhydrate",
        "@language": "en"
      }
    }
  ],
  "@id":
    "http://linked.opendata.cz/resource/ndfrt/physiologic-effect/N0000008296",
  "enc:title": {
    "@value": "Antiperspirant Activity",
    "@language": "en"
  }
};

test("DrugsProp starts loading on mount", () => {
  expect(store.getState().documents.PhysiologicEffect).toBe(undefined);
  expect(store.getState().documents.Configuration).toBe(undefined);

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DrugsProp
          {...{
            match: {
              params: {
                type: "PhysiologicEffect",
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
              }
            },
            location: {
              pathname:
                "/DrugsProp/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
  expect(store.getState().documents.PhysiologicEffect.loading).toBeTruthy();
});

test("DrugsProp renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DrugsProp
          {...{
            match: {
              params: {
                type: "PhysiologicEffect",
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
              }
            },
            location: {
              pathname:
                "/DrugsProp/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("DrugsProp renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DrugsProp
          {...{
            match: {
              params: {
                type: "PhysiologicEffect",
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
              }
            },
            location: {
              pathname:
                "/DrugsProp/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "PhysiologicEffect",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("DrugsProp renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DrugsProp
          {...{
            match: {
              params: {
                type: "PhysiologicEffect",
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
              }
            },
            location: {
              pathname:
                "/DrugsProp/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fphysiologic-effect%2FN0000008296"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "PhysiologicEffect"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});
