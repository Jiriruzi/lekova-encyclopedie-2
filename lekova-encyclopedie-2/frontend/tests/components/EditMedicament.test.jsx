import React from "react";
import EditMedicament from "../../src/components/user/EditMedicament.jsx";
import { errorValidator } from "../../src/components/user/EditMedicament.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { Form } from "react-form";
import { mount, shallow } from "enzyme";
import { createBrowserHistory as createHistory } from "history";

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("EditMedicament renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <Router history={history}>
          <EditMedicament history={history} />
        </Router>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("EditMedicament redirects when not logged", () => {
  history.location.pathname="/AddMedicament";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EditMedicament history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/Login");
});

test("EditMedicament redirects when logged with wrong role", () => {
  history.location.pathname="/AddMedicament";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "expert" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EditMedicament history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/");
});

test("EditMedicament does not redirect when logged correctly", () => {
  history.location.pathname="/AddMedicament";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EditMedicament history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/AddMedicament");
});

test("error validator works correctly", () => {
  expect(
    errorValidator({
      alarm: true,
      howOften: "1",
      howOftenType: "hours",
      enc_title: "title"
    })
  ).toEqual({ howOften: false, enc_title: false });
  expect(
    errorValidator({
      alarm: true,
      howOften: "1",
      howOftenType: "",
      enc_title: "title"
    })
  ).toEqual({ howOften: true, enc_title: false });
  expect(
    errorValidator({
      alarm: true,
      howOften: "0",
      howOftenType: "hours",
      enc_title: "title"
    })
  ).toEqual({ howOften: true, enc_title: false });
  expect(
    errorValidator({
      alarm: false,
      howOften: "0",
      howOftenType: "hours",
      enc_title: "title"
    })
  ).toEqual({ howOften: false, enc_title: false });
  expect(
    errorValidator({
      alarm: false,
      howOften: "0",
      howOftenType: "hours",
      enc_title: ""
    })
  ).toEqual({ howOften: false, enc_title: true });
});
