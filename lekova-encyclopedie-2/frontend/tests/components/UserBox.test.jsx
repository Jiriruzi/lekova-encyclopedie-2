import React from "react";
import UserBox from "../../src/components/user/UserBox.jsx";
import { errorValidator } from "../../src/components/user/UserBox.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";
import toJson from "enzyme-to-json";
import { BrowserRouter } from "react-router-dom";


beforeEach(() => {
  const div = document.createElement("div");
  div.setAttribute("id", "test");
  window.domNode = div;
  document.body.appendChild(div);
});

const store = createStore(allReducers, applyMiddleware(thunk));

test("UserBox renders correctly when logged as user", () => {
  const component = mount(
    <Provider store={store}>
      <BrowserRouter>
        <UserBox show={true} language="cs" username="jirka" role="user" />
      </BrowserRouter>
    </Provider>,
    { attachTo: window.domNode }
  );
  expect(toJson(component)).toMatchSnapshot();
});

test("UserBox renders correctly when logged as expert", () => {
  const component = mount(
    <Provider store={store}>
      <BrowserRouter>
        <UserBox show={true} language="cs" username="jirka" role="expert" />
      </BrowserRouter>
    </Provider>,
    { attachTo: window.domNode }
  );
  expect(toJson(component)).toMatchSnapshot();
});

test("UserBox renders correctly not logged", () => {
  const component = mount(
    <Provider store={store}>
      <BrowserRouter>
        <UserBox show={true} language="cs" username="jirka" role="expert" />
      </BrowserRouter>
    </Provider>,
    { attachTo: window.domNode }
  );
  expect(toJson(component)).toMatchSnapshot();
});

test("UserBox does not render when prop show is false", () => {
  const component = mount(
    <Provider store={store}>
      <BrowserRouter>
        <UserBox show={false} language="cs" username="jirka" role="expert" />
      </BrowserRouter>
    </Provider>,
    { attachTo: window.domNode }
  );
  expect(toJson(component)).toMatchSnapshot();
});
