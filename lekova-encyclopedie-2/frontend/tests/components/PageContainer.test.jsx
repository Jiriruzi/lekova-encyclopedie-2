import React from "react";
import PageContainer from "../../src/components/PageContainer/PageContainer.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { Provider } from "react-redux";

const store = createStore(allReducers, applyMiddleware(thunk));

test("PageContainer renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <PageContainer
            routes={[
              { path: "route1", label: "Detail" },
              { path: "route2", label: "CheapestAtc" }
            ]}
          >Child</PageContainer>
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});


test("PageContainer renders correctly when loading", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <PageContainer
            routes={[
              { path: "route1", label: "Detail" },
              { path: "route2", label: "CheapestAtc" }
            ]}
            loading={true}
          >Child</PageContainer>
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("PageContainer renders correctly when error", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <PageContainer
            routes={[
              { path: "route1", label: "Detail" },
              { path: "route2", label: "CheapestAtc" }
            ]}
            error={true}
          >Child</PageContainer>
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
