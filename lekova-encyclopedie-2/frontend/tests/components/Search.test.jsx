import React from "react";
import Search from "../../src/components/Search/Search.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));
const suggestions = [
  {
    _type: "ingredient",
    enc_title: "item1ingredient",
    _id:
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0017141",
    _version_: 1616108253639344133
  },
  {
    _type: "ingredient",
    enc_title: "item2ingredient",
    _id:
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0017148",
    _version_: 1616108253639344138
  },
  {
    _type: "diseaseOrFinding",
    enc_title: "item3disease",
    _id: "http://linked.opendata.cz/resource/ndfrt/disease/N0000002449",
    _version_: 1616108252178677765
  }
];


test("Search starts loading on mount", () => {
  expect(store.getState().search.loadingResultsAll).toBeFalsy();
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Search location={{ search: "?item" }} />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().search.loadingResultsAll).toBeTruthy();
});

test("Search renders correctly when loading", () => {
  store.dispatch({
    type: "ALL_RESULTS_LOADING"
  });
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Search location={{ search: "?item" }} />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Search renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Search location={{ search: "?item" }} />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "ALL_RESULTS",
    payload: suggestions
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Search renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Search location={{ search: "?item" }} />
      </BrowserRouter>
    </Provider>
  );

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

