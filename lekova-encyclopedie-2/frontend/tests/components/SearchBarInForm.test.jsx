import React from "react";
import SearchBarInForm from "../../src/components/Search/SearchBarInForm.jsx";
import renderer from "react-test-renderer";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";


const store = createStore(allReducers, applyMiddleware(thunk));

test("SearchBarInForm renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <SearchBarInForm sections={[]} environment={{ language: "cs" }} />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
