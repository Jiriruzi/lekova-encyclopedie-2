import React from "react";
import InfoFromConf from "../../src/components/InfoFromConf/InfoFromConf.jsx";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import BasicInfo from "Components/BasicInfo/BasicInfo.jsx";
import List from "Components/List/List.jsx";
import Table from "Components/Table/Table.jsx";

test("InfoFromConf renders correctly", () => {
  const wrapper = shallow(
    <InfoFromConf
      context="enc:"
      document={{
        doc: {
          "enc:prop": "propB",
          "enc:list": ["propL", "propL2"],
          "enc:table": { prop: "propT" }
        }
      }}
      conf={{
        doc: {
          basicInfo: [{ property: "prop" }],
          list: [{ property: "list" }],
          table: [
            {
              label: "labelT",
              property: "table",
              items: [
                {
                  label: "propT",
                  path: [["prop"]],
                  class: "not-active"
                }
              ]
            }
          ]
        }
      }}
    />
  );
  expect(wrapper.find(BasicInfo)).toHaveLength(1);
  expect(wrapper.find(List)).toHaveLength(1);
  expect(wrapper.find(Table)).toHaveLength(1);
});
