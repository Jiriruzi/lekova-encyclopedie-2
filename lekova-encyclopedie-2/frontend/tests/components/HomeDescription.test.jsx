import React from "react";
import HomeDescription from "../../src/components/Home/HomeDescription.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";

test("HomeDescription renders correctly", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <HomeDescription language="cs" />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
