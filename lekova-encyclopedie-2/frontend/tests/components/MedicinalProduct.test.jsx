import React from "react";
import MedicinalProduct from "../../src/components/MedicinalProduct/MedicinalProduct.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someConfig = {
  _id: "MedicinalProduct",
  _rev: "1-04e223bacffc1082d6e4b3adb3965f32",
  basicInfo: [
    {
      property: "hasIndication",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "Indication"
    },
    {
      property: "hasIndicationGroup",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "IndicationGroup"
    }
  ],
  list: [
    {
      property: "hasActiveIngredient",
      sublist: [
        {
          property: "hasPharmacologicalAction",
          label: "PharmacologicalAction",
          propOfProp: [["title"]]
        },
        {
          property: "hasPregnancyCategory",
          label: "PregnancyCategory",
          class: "not-active normal-text",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "ingredient" },
      class: "ingredient-list col-xs-12 col-md-6",
      label: "Ingredient"
    },
    {
      property: "hasATCConcept",
      value: {
        connect: [{ property: "broaderTransitive" }],
        property: [["notation"], ["title"]],
        class: "linked-item"
      },
      label: "ATCConcept",
      class: "linked-list col-xs-12 col-md-6"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    }
  ],
  table: [
    {
      class: "packaging-table",
      label: "MedicinalProductPackaging",
      property: "hasMedicinalProductPackaging",
      items: [
        {
          label: "Delivered",
          path: [["deliveredInLastThreeMonths"]],
          class: "not-active"
        },
        {
          label: "Title",
          path: [["title"], ["hasTitleSupplement"]],
          class: "left"
        },
        {
          label: "ATCConcept",
          path: [["hasATCConcept", "notation"]],
          class: ""
        },
        {
          label: "PackagingSize",
          path: [["hasPackagingSize"]],
          class: "not-active"
        },
        { label: "Strength", path: [["hasStrength"]], class: "not-active" },
        {
          label: "Price",
          path: [["hasPriceSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "PriceReimbursement",
          path: [["hasReimbursementSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "RouteOfAdministration",
          path: [["hasRouteOfAdministration"]],
          class: "not-active"
        }
      ]
    }
  ]
};

const someData = {
  _id:
    "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG",
  _rev: "1-da575eb1e767ef54d62bf9603205b6b0",
  "enc:hasATCConcept": {
    "enc:broaderTransitive": [
      {
        "enc:notation": "B01AC",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/B01AC",
        "enc:title": [
          {
            "@value": "Platelet aggregation inhibitors excluding heparin",
            "@language": "en"
          },
          { "@value": "Antiagregancia krom\\u011B heparinu", "@language": "cs" }
        ]
      },
      {
        "enc:notation": "B01",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/B01",
        "enc:title": [
          { "@value": "Antikoagulancia, antitrombotika", "@language": "cs" },
          { "@value": "Antithrombotic agents", "@language": "en" }
        ]
      },
      {
        "enc:notation": "B01A",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/B01A",
        "enc:title": [
          { "@value": "Antithrombotic agents", "@language": "en" },
          { "@value": "Antikoagulancia, antitrombotika", "@language": "cs" }
        ]
      },
      {
        "enc:notation": "B",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/B",
        "enc:title": [
          {
            "@value": "Krev a krvetvorn\\u00E9 org\\u00E1ny",
            "@language": "cs"
          },
          { "@value": "Blood and blood forming organs", "@language": "en" }
        ]
      }
    ],
    "enc:notation": "B01AC06",
    "@type": "enc:ATCConcept",
    "@id": "http://linked.opendata.cz/resource/atc/B01AC06",
    "enc:title": [
      { "@value": "Acetylsalicylic acid", "@language": "en" },
      { "@value": "Kyselina acetylsalicylov\\u00E1", "@language": "cs" }
    ]
  },
  "@type": "enc:MedicinalProduct",
  "enc:hasActiveIngredient": {
    "enc:description": {
      "@value":
        "The prototypical analgesic used in the treatment of mild to moderate pain. It has anti-inflammatory and antipyretic properties and acts as an inhibitor of cyclooxygenase which results in the inhibition of the biosynthesis of prostaglandins. Aspirin also inhibits platelet aggregation and is used in the prevention of arterial and venous thrombosis. (From Martindale, The Extra Pharmacopoeia, 30th ed, p5)     ",
      "@language": "en"
    },
    "@type": "enc:Ingredient",
    "enc:contraindicatedWith": [
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010245",
        "enc:title": {
          "@value": "Pregnancy third trimester",
          "@language": "en"
        }
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010214",
        "enc:title": { "@value": "Adolescent", "@language": "en" }
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010203",
        "enc:title": { "@value": "Infant", "@language": "en" }
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010207",
        "enc:title": { "@value": "Young child", "@language": "en" }
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000498",
        "enc:title": [
          { "@value": "Asthma", "@language": "en" },
          { "@value": "Bronchi\\u00E1ln\\u00ED astma", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000003128",
        "enc:title": [
          { "@value": "Virov\\u00E9 nemoci", "@language": "cs" },
          { "@value": "Virus diseases", "@language": "en" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001485",
        "enc:title": [
          { "@value": "Hemorrhagic disorders", "@language": "en" },
          { "@value": "Hemoragick\\u00E9 poruchy", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000577",
        "enc:title": [
          { "@value": "Trombocytopatie", "@language": "cs" },
          { "@value": "Blood platelet disorders", "@language": "en" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000575",
        "enc:title": [
          { "@value": "Blood coagulation disorders", "@language": "en" },
          { "@value": "Koagulopatie", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002110",
        "enc:title": [
          { "@value": "Nasal polyps", "@language": "en" },
          { "@value": "Nosn\\u00ED polypy", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002634",
        "enc:title": [
          { "@value": "Rhinitis", "@language": "en" },
          { "@value": "Rinitida", "@language": "cs" }
        ]
      },
      {
        "@type": "enc:DiseaseOrFinding",
        "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
        "enc:title": [
          { "@value": "Drug hypersensitivity", "@language": "en" },
          { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" }
        ]
      }
    ],
    "@id":
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0001864",
    "enc:hasPharmacologicalAction": [
      {
        "enc:description": {
          "@value":
            "Drugs that are used to reduce body temperature in fever.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0028037",
        "enc:title": [
          { "@value": "Antipyretika", "@language": "cs" },
          { "@value": "Antipyretics", "@language": "en" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Drugs or agents which antagonize or impair any mechanism leading to blood platelet aggregation, whether during the phases of activation and shape change or following the dense-granule release reaction and stimulation of the prostaglandin-thromboxane system.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0017012",
        "enc:title": [
          {
            "@value": "Inhibitory agregace trombocyt\\u016F",
            "@language": "cs"
          },
          { "@value": "Platelet aggregation inhibitors", "@language": "en" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Fibrinolysin or agents that convert plasminogen to FIBRINOLYSIN.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0008434",
        "enc:title": [
          { "@value": "Fibrinolytika", "@language": "cs" },
          { "@value": "Fibrinolytic agents", "@language": "en" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Anti-inflammatory agents that are non-steroidal in nature. In addition to anti-inflammatory actions, they have analgesic, antipyretic, and platelet-inhibitory actions.They act by blocking the synthesis of prostaglandins by inhibiting cyclooxygenase, which converts arachidonic acid to cyclic endoperoxides, precursors of prostaglandins. Inhibition of prostaglandin synthesis accounts for their analgesic, antipyretic, and platelet-inhibitory actions; other mechanisms may contribute to their anti-inflammatory effects.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001335",
        "enc:title": [
          {
            "@value": "Anti-inflammatory agents, non-steroidal",
            "@language": "en"
          },
          { "@value": "Antiflogistika nesteroidn\\u00ED", "@language": "cs" }
        ]
      },
      {
        "enc:description": {
          "@value":
            "Compounds or agents that combine with cyclooxygenase (PROSTAGLANDIN-ENDOPEROXIDE SYNTHASES) and thereby prevent its substrate-enzyme combination with arachidonic acid and the formation of eicosanoids, prostaglandins, and thromboxanes.     ",
          "@language": "en"
        },
        "@type": "enc:PharmacologicalAction",
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0025664",
        "enc:title": [
          { "@value": "Cyclooxygenase inhibitors", "@language": "en" },
          { "@value": "Inhibitory cyklooxygen\\u00E1zy", "@language": "cs" }
        ]
      }
    ],
    "enc:title": [
      { "@value": "Aspirin", "@language": "en" },
      { "@value": "Aspirin", "@language": "cs" },
      { "@value": "Acetylsalicylic acid", "@language": "en" },
      "Acidum acetylsalicylicum",
      { "@value": "Kyselina acetylsalicylov\\u00E1", "@language": "cs" }
    ]
  },
  "enc:hasMedicinalProductPackaging": [
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 1X20X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Oral use", "@language": "en" },
        { "@value": "Usus peroralis", "@language": "la" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "false",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPackagingSize": "1X20",
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0200213"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 2X10X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Usus peroralis", "@language": "la" },
        { "@value": "Oral use", "@language": "en" }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "35.05", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0099295/price/2016-09-01"
      },
      "enc:hasPackagingSize": "2X10",
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "11.73", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0099295/reimbursement/2016-09-01"
      },
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0099295"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 30X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Oral use", "@language": "en" },
        { "@value": "Usus peroralis", "@language": "la" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "false",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPackagingSize": "30",
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0151142"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 56X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Usus peroralis", "@language": "la" },
        { "@value": "Oral use", "@language": "en" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "false",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPackagingSize": "56",
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0200214"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 3X20X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Usus peroralis", "@language": "la" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Oral use", "@language": "en" }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "80.46", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0125114/price/2016-09-01"
      },
      "enc:hasPackagingSize": "3X20",
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "35.18", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0125114/reimbursement/2016-09-01"
      },
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0125114"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 5X10X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Oral use", "@language": "en" },
        { "@value": "Usus peroralis", "@language": "la" }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "false",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPackagingSize": "5X10",
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0071960"
    },
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL NOB 100X100MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Oral use", "@language": "en" },
        { "@value": "Usus peroralis", "@language": "la" }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
        "@language": "cs"
      },
      "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
      "enc:hasATCConcept": {
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/B01AC06"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "71.9", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0203564/price/2016-09-01"
      },
      "enc:hasPackagingSize": "100",
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "58.62", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0203564/reimbursement/2016-09-01"
      },
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC84223.pdf"
      },
      "enc:hasStrength": "100MG",
      "enc:hasIndication": {
        "@value":
          "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0203564"
    }
  ],
  "enc:hasIndication": {
    "@value":
      "\n\n- \\u00A0\\u00A0\\u00A0Nestabiln\\u00ED angina pectoris - jako dopln\\u011Bk standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Akutn\\u00ED infarkt myokardu - jako sou\\u010D\\u00E1st standardn\\u00ED terapie.\n\n- \\u00A0\\u00A0\\u00A0Prevence reinfarktu.\n\n- \\u00A0\\u00A0\\u00A0Po arteri\\u00E1ln\\u00EDch c\\u00E9vn\\u011B chirurgick\\u00FDch nebo interven\\u010Dn\\u00EDch v\\u00FDkonech (nap\\u0159. po aortokoron\\u00E1rn\\u00EDm\nbypassu, p\\u0159i perkut\\u00E1nn\\u00ED translumin\\u00E1ln\\u00ED koron\\u00E1rn\\u00ED angioplastice).\n\n- \\u00A0\\u00A0\\u00A0Sekund\\u00E1rn\\u00ED prevence tranzitorn\\u00ED ischemick\\u00E9 ataky a mozkov\\u00E9ho infarktu.\n\nUpozorn\\u011Bn\\u00ED:\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k terapii bolestiv\\u00FDch stav\\u016F.\n\nTento p\\u0159\\u00EDpravek nen\\u00ED ur\\u010Den k pou\\u017Eit\\u00ED u d\\u011Bt\\u00ED a dosp\\u00EDvaj\\u00EDc\\u00EDch do 18 let\n\n",
    "@language": "cs"
  },
  "@id":
    "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG",
  "@context": {
    xsd: "http://www.w3.org/2001/XMLSchema#",
    enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
    gr: "http://purl.org/goodrelations/v1#"
  },
  "enc:title": { "@value": "ANOPYRIN 100 MG", "@language": "cs" },
  "enc:hasIndicationGroup": {
    "@value": "Anticoagulantia (fibrinolytica, antifibrinol.)",
    "@language": "cs"
  }
};

test("MedicinalProduct starts loading on mount and after receiving props", () => {
  expect(store.getState().documents.MedicinalProduct).toBe(undefined);
  expect(store.getState().documents.Configuration).toBe(undefined);
  expect(store.getState().documents.MedicinalProductPackaging).toBe(undefined);
  expect(store.getState().documents.PIL).toBe(undefined);

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProduct
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.MedicinalProduct.loading).toBeTruthy();
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProduct",
      doc: someData
    }
  });
  expect(
    store.getState().documents.MedicinalProductPackaging.loading
  ).toBeTruthy();
  expect(store.getState().documents.PIL.loading).toBeTruthy();
});
test("MedicinalProduct renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProduct
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProduct renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProduct
          store={store}
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProduct",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProduct renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProduct
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "MedicinalProduct"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("MedicinalProduct renders cheapest packages when param is in url", () => {
  const wrapper = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <MedicinalProduct
          {...{
            match: {
              params: {
                id:
                  "http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
              }
            },
            location: {
              search: "?pil",
              pathname:
                "/MedicinalProduct/http://linked.opendata.cz/resource/sukl/medicinal-product/ANOPYRIN-100-MG"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProduct",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  expect(wrapper.toJSON()).toMatchSnapshot();
});
