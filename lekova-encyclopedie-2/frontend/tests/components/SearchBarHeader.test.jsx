import React from "react";
import SearchBarHeader from "../../src/components/Search/SearchBarHeader.jsx";
import renderer from "react-test-renderer";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

const store = createStore(allReducers, applyMiddleware(thunk));

test("SearchBarHeader renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <SearchBarHeader sections={[]} environment={{ language: "cs" }} />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
