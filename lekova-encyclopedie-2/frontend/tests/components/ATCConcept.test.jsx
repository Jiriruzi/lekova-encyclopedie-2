import React from "react";
import ATCConcept from "../../src/components/ATCConcept/ATCConcept.jsx";
import Table from "../../src/components/Table/Table.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someData = {
  _id: "http://linked.opendata.cz/resource/atc/M01AE01",
  _rev: "1-38ac214cd9cccfc92f1d9afcc1db3835",
  "enc:broaderTransitive": [
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE" },
    { "@id": "http://linked.opendata.cz/resource/atc/M" }
  ],
  "enc:notation": "M01AE01",
  "@type": "enc:ATCConcept",
  "enc:hasMedicinalProduct": [
    {
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-APOTEX-100-MG-5-ML-PEROR\\u00C1LN\\u00CD-SUSPENZE",
      "enc:title": {
        "@value": "IBUPROFEN APOTEX 100 MG/5 ML PEROR\\u00C1LN\\u00CD SUSPENZE",
        "@language": "cs"
      }
    },
    {
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEDOL-RAPID-400-MG",
      "enc:title": { "@value": "BRUFEDOL RAPID 400 MG", "@language": "cs" }
    }
  ],
  "@id": "http://linked.opendata.cz/resource/atc/M01AE01",
  "enc:narrowerTransitive": [],
  "enc:hasCheapestAlternatives": [
    {
      "enc:hasTitleSupplement": {
        "@value": "TBL FLM 30X600MG",
        "@language": "cs"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Usus peroralis", "@language": "la" },
        { "@value": "Oral use", "@language": "en" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        }
      ],
      "@type": "enc:MedicinalProductPackaging",
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "enc:hasIndicationGroup": {
        "@value": "Antirheumatica, antiphlogistica, antiuratica",
        "@language": "cs"
      },
      "enc:title": { "@value": "IBALGIN 600", "@language": "cs" },
      "enc:hasATCConcept": {
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "95.3", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0011063/price/2016-09-01"
      },
      "enc:hasPackagingSize": "30",
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": {
          "@value": "36.54",
          "@type": "xsd:decimal"
        },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0011063/reimbursement/2016-09-01"
      },
      "enc:hasSPCDocument": {
        "@id":
          "http://linked.opendata.cz/resource/sukl/document/spc/SPC88731.pdf"
      },
      "enc:hasStrength": "600MG",
      "enc:hasIndication": {
        "@value":
          "\n\nP\\u0159\\u00EDpravek Ibalgin 600 potahovan\\u00E9 tablety je indikov\\u00E1n k symptomatick\\u00E9 l\\u00E9\\u010Db\\u011B z\\u00E1n\\u011Btliv\\u00FDch a degenerativn\\u00EDch\nchorob kloubn\\u00EDch, mimokloubn\\u00EDho revmatizmu a chorob p\\u00E1te\\u0159e; pou\\u017E\\u00EDv\\u00E1 se p\\u0159i revmatoidn\\u00ED artritid\\u011B, osteoartr\\u00F3ze,\nankylozuj\\u00EDc\\u00ED spondylitid\\u011B, psoriatick\\u00E9 artritid\\u011B, dnav\\u00E9 artritid\\u011B, chondrokalcin\\u00F3ze (pseudodna), p\\u0159i distorzi kloub\\u016F a\nzhmo\\u017Ed\\u011Bn\\u00ED pohybov\\u00E9ho apar\\u00E1tu.\n\n",
        "@language": "cs"
      },
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0011063"
    }
  ],
  "enc:title": [
    { "@value": "Ibuprofen", "@language": "cs" },
    { "@value": "Ibuprofen", "@language": "en" }
  ]
};

const someConfig = {
  _id: "ATCConcept",
  _rev: "1-d2a9d9b0c4efbb71a73963648800bed5",
  basicInfo: [
    {
      property: "description",
      value: { class: "normal-text" },
      class: "col-xs-12"
    }
  ],
  list: [
    {
      property: "broaderTransitive",
      value: { property: [["notation"], ["title"]], class: "linked-item" },
      label: "BroaderATC",
      class: "linked-list col-xs-12"
    },
    {
      property: "narrowerTransitive",
      value: { property: [["notation"], ["title"]], class: "linked-item" },
      label: "NarrowerATC",
      class: "linked-list col-xs-12"
    },
    {
      property: "hasMedicinalProduct",
      value: { property: "title" },
      label: "MedicinalProduct",
      class: "linked-list col-xs-12"
    }
  ]
};

const packConfig = {
  _id: "MedicinalProduct",
  _rev: "1-04e223bacffc1082d6e4b3adb3965f32",
  basicInfo: [
    {
      property: "hasIndication",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "Indication"
    },
    {
      property: "hasIndicationGroup",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "IndicationGroup"
    }
  ],
  list: [
    {
      property: "hasActiveIngredient",
      sublist: [
        {
          property: "hasPharmacologicalAction",
          label: "PharmacologicalAction",
          propOfProp: [["title"]]
        },
        {
          property: "hasPregnancyCategory",
          label: "PregnancyCategory",
          class: "not-active normal-text",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "ingredient" },
      class: "ingredient-list col-xs-12 col-md-6",
      label: "Ingredient"
    },
    {
      property: "hasATCConcept",
      value: {
        connect: [{ property: "broaderTransitive" }],
        property: [["notation"], ["title"]],
        class: "linked-item"
      },
      label: "ATCConcept",
      class: "linked-list col-xs-12 col-md-6"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    }
  ],
  table: [
    {
      class: "packaging-table",
      label: "MedicinalProductPackaging",
      property: "hasMedicinalProductPackaging",
      items: [
        {
          label: "Delivered",
          path: [["deliveredInLastThreeMonths"]],
          class: "not-active"
        },
        {
          label: "Title",
          path: [["title"], ["hasTitleSupplement"]],
          class: "left"
        },
        {
          label: "ATCConcept",
          path: [["hasATCConcept", "notation"]],
          class: ""
        },
        {
          label: "PackagingSize",
          path: [["hasPackagingSize"]],
          class: "not-active"
        },
        { label: "Strength", path: [["hasStrength"]], class: "not-active" },
        {
          label: "Price",
          path: [["hasPriceSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "PriceReimbursement",
          path: [["hasReimbursementSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "RouteOfAdministration",
          path: [["hasRouteOfAdministration"]],
          class: "not-active"
        }
      ]
    }
  ]
};


test("ATCConcept starts loading on mount", () => {
  expect(store.getState().documents.ATCConcept).toBe(undefined)
  expect(store.getState().documents.PackTable).toBe(undefined)
  expect(store.getState().documents.Configuration).toBe(undefined)

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
      <ATCConcept
         {...{
          match: {
            params: {
              id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
            }
          },
          location: {}
        }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
  expect(store.getState().documents.PackTable.loading).toBeTruthy();
  expect(store.getState().documents.ATCConcept.loading).toBeTruthy();

});

test("ATCConcept renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <ATCConcept
         {...{
          match: {
            params: {
              id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
            }
          },
          location: {}
        }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("ATCConcept renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <ATCConcept
          store={store}
          {...{
            match: {
              params: {
                id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "ATCConcept",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "PackTable",
      doc: packConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("ATCConcept renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <ATCConcept
          {...{
            match: {
              params: {
                id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "ATCConcept"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "PackTable",
      doc: packConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("getNarrower works correctly", () => {
  const wrapper = shallow(
    <ATCConcept
      store={store}
      context="enc:"
      {...{
        match: {
          params: {
            id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
          }
        },
        location: {}
      }}
    />
  );
  expect(
    wrapper
      .dive()
      .instance()
      .getOnlyClosestNarrower({
        "enc:narrowerTransitive": [
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE01" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE03" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE51" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE51R" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE11" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE02" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE023" },
          { "@id": "http://linked.opendata.cz/resource/atc/M01AE17" }
        ]
      })
  ).toEqual([
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE01" },
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE03" },
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE51" },
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE11" },
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE02" },
    { "@id": "http://linked.opendata.cz/resource/atc/M01AE17" }
  ]);
});

test("ATCConcept renders cheapest packages when param is in url", () => {
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <ATCConcept
          {...{
            match: {
              params: {
                id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
              }
            },
            location: {
              search: "?cheapest",
              pathname:
                "/ATCConcept/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "ATCConcept",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "PackTable",
      doc: packConfig
    }
  });

  expect(
    wrapper.find(Table)
  ).toBeTruthy();
});
