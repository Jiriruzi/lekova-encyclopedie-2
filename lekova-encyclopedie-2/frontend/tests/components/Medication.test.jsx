import React from "react";
import Medication from "../../src/components/user/Medication.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";
const store = createStore(allReducers, applyMiddleware(thunk));

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

test("Medication renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Medication />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Medication renders correctly when data loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Medication />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({ type: "INTERACTIONS_NOT_LOADING" });
  store.dispatch({
    type: "GET_MEDICATION",
    payload: {
      "1": {
        _type: "medicinalProduct",
        _id:
          "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
        enc_title: "ASDUTER 10 MG TABLETY ",
        howOftenType: "days",
        howOften: "1",
        nextDose: "2018-11-16T20:30:55.000Z",
        description: "desc",
        amount: "250",
        key: 1,
        type: "Emulsion"
      },
      "2": {
        _type: "medicinalProduct",
        _id:
          "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
        enc_title: "ASDUTER 20 MG TABLETY ",
        howOftenType: "days",
        howOften: "1",
        nextDose: "2018-11-15T20:30:55.000Z",
        description: "desc",
        amount: "200",
        key: 2,
        type: "Emulsion"
      }
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});
