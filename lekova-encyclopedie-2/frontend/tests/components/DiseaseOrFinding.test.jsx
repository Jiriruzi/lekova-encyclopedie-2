import React from "react";
import DiseaseOrFinding from "../../src/components/DiseaseOrFinding/DiseaseOrFinding.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someConfig = {
  _id: "DiseaseOrFinding",
  _rev: "1-fc273ec1f1c7326db6e78ae424ea24b0",
  basicInfo: [{ property: "description", class: "col-xs-12 desc" }],
  list: [
    {
      sublist: [
        {
          property: "hasMedicinalProduct",
          class: "active",
          propOfProp: [["title"]]
        }
      ],
      property: "mayTreat",
      value: { property: "title", class: "finding-item" },
      class: "finding-list",
      label: "MayBeTreated"
    },
    {
      property: "mayPrevent",
      sublist: [
        {
          property: "hasMedicinalProduct",
          class: "active",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "finding-item" },
      class: "finding-list",
      label: "MayBePrevented"
    },
    {
      property: "contraindicatedWith",
      sublist: [
        {
          property: "hasMedicinalProduct",
          class: "active",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "finding-item" },
      class: "finding-list",
      label: "Contraindication"
    }
  ]
};

const someData = {
  _id: "http://linked.opendata.cz/resource/ndfrt/disease/N0000001382",
  _rev: "1-ec426a98b9f7789e1db8080f43ac9fd0",
  "enc:mayPrevent": [
    {
      "enc:hasMedicinalProduct": [
        {
          "enc:hasATCConcept": {
            "enc:prefLabel": [
              { "@value": "Allopurinol", "@language": "en" },
              { "@value": "Alopurinol", "@language": "cs" }
            ],
            "enc:notation": "M04AA01",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/M04AA01"
          },
          "@type": "enc:MedicinalProduct",
          "@id":
            "http://linked.opendata.cz/resource/sukl/medicinal-product/MILURIT-100",
          "enc:title": { "@value": "MILURIT 100", "@language": "cs" }
        }
      ],
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0000745",
      "enc:title": [
        { "@value": "Alopurinol", "@language": "cs" },
        "Allopurinolum",
        { "@value": "Allopurinol", "@language": "en" }
      ]
    },
    {
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0004732",
      "enc:title": [
        { "@value": "Colchicine", "@language": "en" },
        "Colchicinum",
        { "@value": "Kolchicin", "@language": "cs" }
      ]
    }
  ],
  "enc:description": {
    "@value":
      "Hereditary metabolic disorder characterized by recurrent acute arthritis, hyperuricemia and deposition of sodium urate in and around the joints, sometimes with formation of uric acid calculi.     ",
    "@language": "en"
  },
  "@type": "enc:DiseaseOrFinding",
  "enc:contraindicatedWith": [
    {
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0018224",
      "enc:title": [
        { "@value": "Pyrazinamid", "@language": "cs" },
        { "@value": "Pyrazinamide", "@language": "en" },
        "Pyrazinamidum"
      ]
    }
  ],
  "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001382",
  "enc:mayTreat": [
    {
      "enc:hasMedicinalProduct": [
        {
          "enc:hasATCConcept": {
            "@id": "http://linked.opendata.cz/resource/atc/M04AA01"
          },
          "@type": "enc:MedicinalProduct",
          "@id":
            "http://linked.opendata.cz/resource/sukl/medicinal-product/MILURIT-300",
          "enc:title": { "@value": "MILURIT 300", "@language": "cs" }
        },
        {
          "enc:hasATCConcept": {
            "enc:prefLabel": [
              { "@value": "Allopurinol", "@language": "en" },
              { "@value": "Alopurinol", "@language": "cs" }
            ],
            "enc:notation": "M04AA01",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/M04AA01"
          },
          "@type": "enc:MedicinalProduct",
          "@id":
            "http://linked.opendata.cz/resource/sukl/medicinal-product/MILURIT-100",
          "enc:title": { "@value": "MILURIT 100", "@language": "cs" }
        }
      ],
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0000745",
      "enc:title": [
        { "@value": "Alopurinol", "@language": "cs" },
        "Allopurinolum",
        { "@value": "Allopurinol", "@language": "en" }
      ]
    },
    {
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0004732",
      "enc:title": [
        { "@value": "Colchicine", "@language": "en" },
        "Colchicinum",
        { "@value": "Kolchicin", "@language": "cs" }
      ]
    },
    {
      "enc:hasMedicinalProduct": [
        {
          "enc:hasATCConcept": {
            "@id": "http://linked.opendata.cz/resource/atc/M04AA03"
          },
          "@type": "enc:MedicinalProduct",
          "@id":
            "http://linked.opendata.cz/resource/sukl/medicinal-product/ADENURIC-120-MG",
          "enc:title": { "@value": "ADENURIC 120 MG", "@language": "cs" }
        },
        {
          "enc:hasATCConcept": {
            "enc:prefLabel": [
              { "@value": "Febuxost\\u00E1t", "@language": "cs" },
              { "@value": "Febuxostat", "@language": "en" }
            ],
            "enc:notation": "M04AA03",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/M04AA03"
          },
          "@type": "enc:MedicinalProduct",
          "@id":
            "http://linked.opendata.cz/resource/sukl/medicinal-product/ADENURIC-80-MG",
          "enc:title": { "@value": "ADENURIC 80 MG", "@language": "cs" }
        }
      ],
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0224789",
      "enc:title": [
        { "@value": "Febuxostat", "@language": "cs" },
        "Febuxostatum",
        { "@value": "Febuxostat", "@language": "en" }
      ]
    }
  ],
  "enc:title": [
    { "@value": "Gout", "@language": "en" },
    { "@value": "Dna (nemoc)", "@language": "cs" }
  ]
};
test("DiseaseOrFinding starts loading on mount", () => {
  expect(store.getState().documents.DiseaseOrFinding).toBe(undefined)
  expect(store.getState().documents.Configuration).toBe(undefined)

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DiseaseOrFinding
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
  expect(store.getState().documents.DiseaseOrFinding.loading).toBeTruthy();

});
test("DiseaseOrFinding renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DiseaseOrFinding
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("DiseaseOrFinding renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DiseaseOrFinding
          store={store}
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "DiseaseOrFinding",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("DiseaseOrFinding renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DiseaseOrFinding
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "DiseaseOrFinding"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("DiseaseOrFinding renders cheapest packages when param is in url", () => {
  const wrapper = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <DiseaseOrFinding
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
              }
            },
            location: {
              search: "?mayPrevent",
              pathname:
                "/DiseaseOrFinding/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000001382"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "DiseaseOrFinding",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  expect(wrapper.toJSON()).toMatchSnapshot();
});
