import React from "react";
import Table from "../../src/components/Table/Table.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";

const store = createStore(allReducers, applyMiddleware(thunk));

test("Table renders correctly", () => {
  const component = renderer
    .create(<BrowserRouter>
      <Table
        context=""
        language="cs"
        datDoc={{
          items: [
            { prop1Title: "item1prop1", prop2Title: "item1prop2" },
            { prop1Title: "item2prop1", prop2Title: "item2prop2" }
          ]
        }}
        confDoc={{
              label: "MedicinalProduct",
              property: "items",
              items: [
                { label: "Title", path: [["prop1Title"]], class: "" },
                {
                  label: "ATCConcept",
                  path: [["prop2Title"]],
                  class: ""
                }
          ]
        }}
        withTitle="MedicinalProduct"
      /></BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("Table renders correctly - without title", () => {
  const component = renderer
    .create(<BrowserRouter>
      <Table
        context=""
        language="cs"
        datDoc={{
          items: [
            { prop1Title: "item1prop1", prop2Title: "item1prop2" },
            { prop1Title: "item2prop1", prop2Title: "item2prop2" }
          ]
        }}
        confDoc={{
              label: "MedicinalProduct",
              property: "items",
              items: [
                { label: "Title", path: [["prop1Title"]], class: "" },
                {
                  label: "ATCConcept",
                  path: [["prop2Title"]],
                  class: ""
                }
          ]
        }}
        withoutTitle
      /></BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});



test("Table renders sorted items", () => {
  const component = renderer
    .create(<BrowserRouter>
      <Table
        context=""
        language="cs"
        datDoc={{
          items: [
            { prop1Title: "item2prop1", prop2Title: "item2prop2" },
            { prop1Title: "item1prop1", prop2Title: "item1prop2" }
          ]
        }}
        confDoc={{
              label: "MedicinalProduct",
              property: "items",
              items: [
                { label: "Title", path: [["prop1Title"]], class: "" },
                {
                  label: "ATCConcept",
                  path: [["prop2Title"]],
                  class: ""
                }
          ]
        }}
        withoutTitle
      /></BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});



test("Table renders correctly when noSort prop", () => {
  const component = renderer
    .create(<BrowserRouter>
      <Table
        context=""
        language="cs"
        datDoc={{
          items: [
            { prop1Title: "item2prop1", prop2Title: "item2prop2" },
            { prop1Title: "item1prop1", prop2Title: "item1prop2" }
          ]
        }}
        confDoc={{
              label: "MedicinalProduct",
              property: "items",
              items: [
                { label: "Title", path: [["prop1Title"]], class: "" },
                {
                  label: "ATCConcept",
                  path: [["prop2Title"]],
                  class: ""
                }
          ]
        }}
        doNotSort
      /></BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});



test("Table renders correctly when no data avaiable items", () => {
  const component = renderer
    .create(<BrowserRouter>
      <Table
        context=""
        language="cs"
        datDoc={{
        }}
        confDoc={{
              label: "MedicinalProduct",
              property: "items",
              items: [
                { label: "Title", path: [["prop1Title"]], class: "" },
                {
                  label: "ATCConcept",
                  path: [["prop2Title"]],
                  class: ""
                }
          ]
        }}
      /></BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});