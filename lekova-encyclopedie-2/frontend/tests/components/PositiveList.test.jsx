import React from "react";
import PositiveList from "../../src/components/PositiveList/PositiveList.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";
const store = createStore(allReducers, applyMiddleware(thunk));
let someData = {
  _id: "positive-list",
  "enc:hasMedicinalProductPackaging": [
    {
      "@type": "enc:MedicinalProductPackaging",
      "enc:hasPregnancyCategory": {
        "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C"
      },
      "enc:hasResumedMarketingDate": {
        "@value": "2012-05-15",
        "@type": "xsd:date"
      },
      _rev: "2-2f95194cdd6b938596c529fe8fd96179",
      "enc:hasMedicinalProduct": {
        "@type": "enc:MedicinalProduct",
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product/OMEPRAZOL-STADA-20-MG",
        "enc:title": { "@value": "OMEPRAZOL STADA 20 MG", "@language": "cs" }
      },
      "enc:hasATCConcept": {
        "enc:broaderTransitive": [
          {
            "enc:notation": "A02",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02",
            "enc:title": [
              {
                "@value":
                  "L\\u00E9\\u010Diva k terapii onemocn\\u011Bn\\u00ED spojen\\u00FDch s poruchou acidity",
                "@language": "cs"
              },
              {
                "@value": "Drugs for acid related disorders",
                "@language": "en"
              }
            ]
          },
          {
            "enc:notation": "A02BC",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02BC",
            "enc:title": [
              { "@value": "Proton pump inhibitors", "@language": "en" },
              {
                "@value": "Inhibitory protonov\\u00E9 pumpy",
                "@language": "cs"
              }
            ]
          },
          {
            "enc:notation": "A02B",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02B",
            "enc:title": [
              {
                "@value":
                  "Drugs for peptic ulcer and gastro-oesophageal reflux disease",
                "@language": "en"
              },
              {
                "@value":
                  "L\\u00E9\\u010Diva k terapii peptick\\u00E9ho v\\u0159edu a refluxn\\u00ED choroby j\\u00EDcnu",
                "@language": "cs"
              }
            ]
          },
          {
            "enc:notation": "A",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A",
            "enc:title": [
              {
                "@value": "Alimentary tract and metabolism",
                "@language": "en"
              },
              {
                "@value": "Tr\\u00E1vic\\u00ED trakt a metabolismus",
                "@language": "cs"
              }
            ]
          }
        ],
        "enc:notation": "A02BC01",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/A02BC01",
        "enc:title": [
          { "@value": "Omeprazol", "@language": "cs" },
          { "@value": "Omeprazole", "@language": "en" }
        ]
      },
      "enc:hasPackagingSize": "100",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140192",
      "enc:hasTitleSupplement": {
        "@value": "CPS ETD 100X20MG",
        "@language": "cs"
      },
      "enc:containsDefinedDailyDoses": {
        "@value": "100.0",
        "@type": "xsd:float"
      },
      "enc:hasRouteOfAdministration": [
        { "@value": "Oral use", "@language": "en" },
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Usus peroralis", "@language": "la" }
      ],
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "@context": {
        xsd: "http://www.w3.org/2001/XMLSchema#",
        enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
        gr: "http://purl.org/goodrelations/v1#"
      },
      "enc:hasIndicationGroup": {
        "@value":
          "Antacida (v\\u010Detn\\u011B antiulcerosn\\u00EDch l\\u00E9\\u010Div)",
        "@language": "cs"
      },
      "enc:title": { "@value": "OMEPRAZOL STADA 20 MG", "@language": "cs" },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasDefinedDailyDose": {
        "enc:hasUnit": {
          "@id": "http://linked.opendata.cz/resource/sukl/unit/MG"
        },
        "enc:hasValue": { "@value": "20.0", "@type": "xsd:float" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140192/ddd"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "197.69", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140192/price/2016-09-01"
      },
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "205.84", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140192/reimbursement/2016-09-01"
      },
      "enc:hasActiveIngredient": {
        "enc:description": {
          "@value":
            "A 4-methoxy-3,5-dimethylpyridyl, 5-methoxybenzimidazole derivative of timoprazole that is used in the therapy of STOMACH ULCERS and ZOLLINGER-ELLISON SYNDROME. The drug inhibits an H(+)-K(+)-EXCHANGING ATPASE which is found in GASTRIC PARIETAL CELLS.     ",
          "@language": "en"
        },
        "@type": "enc:Ingredient",
        "enc:hasPregnancyCategory": {
          "@id":
            "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C"
        },
        "enc:contraindicatedWith": {
          "@type": "enc:DiseaseOrFinding",
          "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
          "enc:title": [
            { "@value": "Drug hypersensitivity", "@language": "en" },
            { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" }
          ]
        },
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0015293",
        "enc:hasPharmacologicalAction": [
          {
            "enc:description": {
              "@value":
                "Compounds that inhibit H(+)-K(+)-EXCHANGING ATPASE. They are used as ANTI-ULCER AGENTS and sometimes in place of HISTAMINE H2 ANTAGONISTS for GASTROESOPHAGEAL REFLUX.     ",
              "@language": "en"
            },
            "@type": "enc:PharmacologicalAction",
            "@id":
              "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0504077",
            "enc:title": [
              {
                "@value": "Inhibitory protonov\\u00E9 pumpy",
                "@language": "cs"
              },
              { "@value": "Proton pump inhibitors", "@language": "en" }
            ]
          },
          {
            "enc:description": {
              "@value":
                "Various agents with different action mechanisms used to treat or ameliorate PEPTIC ULCER or irritation of the gastrointestinal tract. This has included ANTIBIOTICS to treat HELICOBACTER INFECTIONS; HISTAMINE H2 ANTAGONISTS to reduce GASTRIC ACID secretion; and ANTACIDS for symptomatic relief.     ",
              "@language": "en"
            },
            "@type": "enc:PharmacologicalAction",
            "@id":
              "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001340",
            "enc:title": [
              {
                "@value": "Protiv\\u0159edov\\u00E9 l\\u00E1tky",
                "@language": "cs"
              },
              { "@value": "Anti-ulcer agents", "@language": "en" }
            ]
          }
        ],
        "enc:title": [
          { "@value": "Omeprazol", "@language": "cs" },
          "Omeprazolum",
          { "@value": "Ingredient", "@language": "en" }
        ]
      },
      "enc:hasSPCDocument":
        "http://www.sukl.cz/modules/medication/download.php?file=SPC38264.pdf&type=spc&as=omeprazol-stada-20-mg-spc",
      "enc:hasStrength": "20MG",
      "enc:hasIndication": {
        "@value":
          "\n\nOmeprazol STADA je indikov\\u00E1n k:\n\nDosp\\u011Bl\\u00ED\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Prevenci relapsu duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B \\u017Ealude\\u010Dn\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Prevenci relapsu \\u017Ealude\\u010Dn\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Eradikaci Helicobacter pylori (H. pylori) u v\\u0159edov\\u00E9 choroby v kombinaci s vhodn\\u00FDmi antibiotiky\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B \\u017Ealude\\u010Dn\\u00EDch a duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F v souvislosti s pod\\u00E1v\\u00E1n\\u00EDm NSAID\n\n- \\u00A0\\u00A0\\u00A0Prevenci \\u017Ealude\\u010Dn\\u00EDch a duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F v souvislosti s pod\\u00E1v\\u00E1n\\u00EDm NSAID u rizikov\\u00FDch pacient\\u016F\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B refluxn\\u00ED ezofagitidy\n\n- \\u00A0\\u00A0\\u00A0Dlouhodob\\u00E9 l\\u00E9\\u010Db\\u011B pacient\\u016F se zhojenou refluxn\\u00ED ezofagitidou\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B symptomatick\\u00E9 refluxn\\u00ED choroby j\\u00EDcnu\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B Zollinger-Ellisonova syndromu\n\nPediatrick\\u00E1 populace\n\nD\\u011Bti star\\u0161\\u00ED ne\\u017E 1 rok a s hmotnost\\u00ED > 10 kg\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Dba refluxn\\u00ED ezofagitidy\n\n- \\u00A0\\u00A0\\u00A0Symptomatick\\u00E1 l\\u00E9\\u010Dba p\\u00E1len\\u00ED \\u017E\\u00E1hy a kysel\\u00E9 regurgitace u refluxn\\u00ED choroby j\\u00EDcnu\n\nD\\u011Bti star\\u0161\\u00ED ne\\u017E 4 roky a dosp\\u00EDvaj\\u00EDc\\u00ED\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Dba duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F zp\\u016Fsoben\\u00FDch H. pylori v kombinaci s antibiotiky\n1\n",
        "@language": "cs"
      },
      _id:
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140192",
      "enc:isOnPositiveList": true
    },
    {
      "@type": "enc:MedicinalProductPackaging",
      "enc:hasPregnancyCategory": {
        "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C"
      },
      "enc:hasResumedMarketingDate": {
        "@value": "2012-05-15",
        "@type": "xsd:date"
      },
      _rev: "2-c36b3990720ef84943bcae3b0dee5627",
      "enc:hasMedicinalProduct": {
        "@type": "enc:MedicinalProduct",
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product/OMEPRAZOL-STADA-20-MG",
        "enc:title": { "@value": "OMEPRAZOL STADA 20 MG", "@language": "cs" }
      },
      "enc:hasATCConcept": {
        "enc:broaderTransitive": [
          {
            "enc:notation": "A02",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02",
            "enc:title": [
              {
                "@value":
                  "L\\u00E9\\u010Diva k terapii onemocn\\u011Bn\\u00ED spojen\\u00FDch s poruchou acidity",
                "@language": "cs"
              },
              {
                "@value": "Drugs for acid related disorders",
                "@language": "en"
              }
            ]
          },
          {
            "enc:notation": "A02BC",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02BC",
            "enc:title": [
              { "@value": "Proton pump inhibitors", "@language": "en" },
              {
                "@value": "Inhibitory protonov\\u00E9 pumpy",
                "@language": "cs"
              }
            ]
          },
          {
            "enc:notation": "A02B",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A02B",
            "enc:title": [
              {
                "@value":
                  "Drugs for peptic ulcer and gastro-oesophageal reflux disease",
                "@language": "en"
              },
              {
                "@value":
                  "L\\u00E9\\u010Diva k terapii peptick\\u00E9ho v\\u0159edu a refluxn\\u00ED choroby j\\u00EDcnu",
                "@language": "cs"
              }
            ]
          },
          {
            "enc:notation": "A",
            "@type": "enc:ATCConcept",
            "@id": "http://linked.opendata.cz/resource/atc/A",
            "enc:title": [
              {
                "@value": "Alimentary tract and metabolism",
                "@language": "en"
              },
              {
                "@value": "Tr\\u00E1vic\\u00ED trakt a metabolismus",
                "@language": "cs"
              }
            ]
          }
        ],
        "enc:notation": "A02BC01",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/A02BC01",
        "enc:title": [
          { "@value": "Omeprazol", "@language": "cs" },
          { "@value": "Omeprazole", "@language": "en" }
        ]
      },
      "enc:hasPackagingSize": "30",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140187",
      "enc:hasTitleSupplement": {
        "@value": "CPS ETD 30X20MG",
        "@language": "cs"
      },
      "enc:containsDefinedDailyDoses": {
        "@value": "30.0",
        "@type": "xsd:float"
      },
      "enc:hasRouteOfAdministration": [
        {
          "@value": "Peror\\u00E1ln\\u00ED pod\\u00E1n\\u00ED",
          "@language": "cs"
        },
        { "@value": "Usus peroralis", "@language": "la" },
        { "@value": "Oral use", "@language": "en" }
      ],
      "enc:deliveredInLastThreeMonths": {
        "@value": "true",
        "@type": "xsd:boolean"
      },
      "@context": {
        xsd: "http://www.w3.org/2001/XMLSchema#",
        enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
        gr: "http://purl.org/goodrelations/v1#"
      },
      "enc:hasIndicationGroup": {
        "@value":
          "Antacida (v\\u010Detn\\u011B antiulcerosn\\u00EDch l\\u00E9\\u010Div)",
        "@language": "cs"
      },
      "enc:title": { "@value": "OMEPRAZOL STADA 20 MG", "@language": "cs" },
      "enc:hasRegistrationState": {
        "@id": "http://linked.opendata.cz/resource/sukl/registration-state/R"
      },
      "enc:hasDefinedDailyDose": {
        "enc:hasUnit": {
          "@id": "http://linked.opendata.cz/resource/sukl/unit/MG"
        },
        "enc:hasValue": { "@value": "20.0", "@type": "xsd:float" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140187/ddd"
      },
      "enc:hasPriceSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "59.3", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140187/price/2016-09-01"
      },
      "enc:hasReimbursementSpecification": {
        "gr:validFrom": { "@value": "2016-09-01", "@type": "xsd:date" },
        "@type": "gr:PriceSpecification",
        "gr:valueAddedTaxIncluded": {
          "@value": "true",
          "@type": "xsd:boolean"
        },
        "gr:hasCurrency": "CZK",
        "gr:hasCurrencyValue": { "@value": "61.76", "@type": "xsd:decimal" },
        "@id":
          "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140187/reimbursement/2016-09-01"
      },
      "enc:hasActiveIngredient": {
        "enc:description": {
          "@value":
            "A 4-methoxy-3,5-dimethylpyridyl, 5-methoxybenzimidazole derivative of timoprazole that is used in the therapy of STOMACH ULCERS and ZOLLINGER-ELLISON SYNDROME. The drug inhibits an H(+)-K(+)-EXCHANGING ATPASE which is found in GASTRIC PARIETAL CELLS.     ",
          "@language": "en"
        },
        "@type": "enc:Ingredient",
        "enc:hasPregnancyCategory": {
          "@id":
            "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C"
        },
        "enc:contraindicatedWith": {
          "@type": "enc:DiseaseOrFinding",
          "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
          "enc:title": [
            { "@value": "Drug hypersensitivity", "@language": "en" },
            { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" }
          ]
        },
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0015293",
        "enc:hasPharmacologicalAction": [
          {
            "enc:description": {
              "@value":
                "Compounds that inhibit H(+)-K(+)-EXCHANGING ATPASE. They are used as ANTI-ULCER AGENTS and sometimes in place of HISTAMINE H2 ANTAGONISTS for GASTROESOPHAGEAL REFLUX.     ",
              "@language": "en"
            },
            "@type": "enc:PharmacologicalAction",
            "@id":
              "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0504077",
            "enc:title": [
              {
                "@value": "Inhibitory protonov\\u00E9 pumpy",
                "@language": "cs"
              },
              { "@value": "Proton pump inhibitors", "@language": "en" }
            ]
          },
          {
            "enc:description": {
              "@value":
                "Various agents with different action mechanisms used to treat or ameliorate PEPTIC ULCER or irritation of the gastrointestinal tract. This has included ANTIBIOTICS to treat HELICOBACTER INFECTIONS; HISTAMINE H2 ANTAGONISTS to reduce GASTRIC ACID secretion; and ANTACIDS for symptomatic relief.     ",
              "@language": "en"
            },
            "@type": "enc:PharmacologicalAction",
            "@id":
              "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001340",
            "enc:title": [
              {
                "@value": "Protiv\\u0159edov\\u00E9 l\\u00E1tky",
                "@language": "cs"
              },
              { "@value": "Anti-ulcer agents", "@language": "en" }
            ]
          }
        ],
        "enc:title": [
          { "@value": "Omeprazol", "@language": "cs" },
          "Omeprazolum",
          { "@value": "Omeprazole", "@language": "en" }
        ]
      },
      "enc:hasSPCDocument":
        "http://www.sukl.cz/modules/medication/download.php?file=SPC38264.pdf&type=spc&as=omeprazol-stada-20-mg-spc",
      "enc:hasStrength": "20MG",
      "enc:hasIndication": {
        "@value":
          "\n\nOmeprazol STADA je indikov\\u00E1n k:\n\nDosp\\u011Bl\\u00ED\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Prevenci relapsu duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B \\u017Ealude\\u010Dn\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Prevenci relapsu \\u017Ealude\\u010Dn\\u00EDch v\\u0159ed\\u016F\n\n- \\u00A0\\u00A0\\u00A0Eradikaci Helicobacter pylori (H. pylori) u v\\u0159edov\\u00E9 choroby v kombinaci s vhodn\\u00FDmi antibiotiky\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B \\u017Ealude\\u010Dn\\u00EDch a duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F v souvislosti s pod\\u00E1v\\u00E1n\\u00EDm NSAID\n\n- \\u00A0\\u00A0\\u00A0Prevenci \\u017Ealude\\u010Dn\\u00EDch a duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F v souvislosti s pod\\u00E1v\\u00E1n\\u00EDm NSAID u rizikov\\u00FDch pacient\\u016F\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B refluxn\\u00ED ezofagitidy\n\n- \\u00A0\\u00A0\\u00A0Dlouhodob\\u00E9 l\\u00E9\\u010Db\\u011B pacient\\u016F se zhojenou refluxn\\u00ED ezofagitidou\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B symptomatick\\u00E9 refluxn\\u00ED choroby j\\u00EDcnu\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Db\\u011B Zollinger-Ellisonova syndromu\n\nPediatrick\\u00E1 populace\n\nD\\u011Bti star\\u0161\\u00ED ne\\u017E 1 rok a s hmotnost\\u00ED > 10 kg\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Dba refluxn\\u00ED ezofagitidy\n\n- \\u00A0\\u00A0\\u00A0Symptomatick\\u00E1 l\\u00E9\\u010Dba p\\u00E1len\\u00ED \\u017E\\u00E1hy a kysel\\u00E9 regurgitace u refluxn\\u00ED choroby j\\u00EDcnu\n\nD\\u011Bti star\\u0161\\u00ED ne\\u017E 4 roky a dosp\\u00EDvaj\\u00EDc\\u00ED\n\n- \\u00A0\\u00A0\\u00A0L\\u00E9\\u010Dba duoden\\u00E1ln\\u00EDch v\\u0159ed\\u016F zp\\u016Fsoben\\u00FDch H. pylori v kombinaci s antibiotiky\n1\n",
        "@language": "cs"
      },
      _id:
        "http://linked.opendata.cz/resource/sukl/medicinal-product-packaging/0140187",
      "enc:isOnPositiveList": true
    }
  ]
};
const someConfig = {
  _id: "MedicinalProduct",
  _rev: "1-04e223bacffc1082d6e4b3adb3965f32",
  basicInfo: [
    {
      property: "hasIndication",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "Indication"
    },
    {
      property: "hasIndicationGroup",
      value: { class: "normal-text" },
      class: "col-xs-12 col-md-6 important-prop",
      label: "IndicationGroup"
    }
  ],
  list: [
    {
      property: "hasActiveIngredient",
      sublist: [
        {
          property: "hasPharmacologicalAction",
          label: "PharmacologicalAction",
          propOfProp: [["title"]]
        },
        {
          property: "hasPregnancyCategory",
          label: "PregnancyCategory",
          class: "not-active normal-text",
          propOfProp: [["title"]]
        }
      ],
      value: { property: "title", class: "ingredient" },
      class: "ingredient-list col-xs-12 col-md-6",
      label: "Ingredient"
    },
    {
      property: "hasATCConcept",
      value: {
        connect: [{ property: "broaderTransitive" }],
        property: [["notation"], ["title"]],
        class: "linked-item"
      },
      label: "ATCConcept",
      class: "linked-list col-xs-12 col-md-6"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    }
  ],
  table: [
    {
      class: "packaging-table",
      label: "MedicinalProductPackaging",
      property: "hasMedicinalProductPackaging",
      items: [
        {
          label: "Delivered",
          path: [["deliveredInLastThreeMonths"]],
          class: "not-active"
        },
        {
          label: "Title",
          path: [["title"], ["hasTitleSupplement"]],
          class: "left"
        },
        {
          label: "ATCConcept",
          path: [["hasATCConcept", "notation"]],
          class: ""
        },
        {
          label: "PackagingSize",
          path: [["hasPackagingSize"]],
          class: "not-active"
        },
        { label: "Strength", path: [["hasStrength"]], class: "not-active" },
        {
          label: "Price",
          path: [["hasPriceSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "PriceReimbursement",
          path: [["hasReimbursementSpecification", "gr:hasCurrencyValue"]],
          class: "not-active"
        },
        {
          label: "RouteOfAdministration",
          path: [["hasRouteOfAdministration"]],
          class: "not-active"
        }
      ]
    }
  ]
};
test("PositiveList starts loading on mount", () => {
  expect(store.getState().documents.MedicinalProductPackaging).toBe(undefined)
  expect(store.getState().documents.Configuration).toBe(undefined)

  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <PositiveList
          {...{
            location: {
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.MedicinalProductPackaging.loading).toBeTruthy();
  expect(store.getState().documents.Configuration.loading).toBeTruthy();

});

test("PositiveList renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <PositiveList />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("PositiveList renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <PositiveList />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProductPackaging",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("PositiveList renders correctly when document some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <PositiveList />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "MedicinalProductPackaging"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Filtering works coorectly", done => {
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProductPackaging",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  const wrapper = shallow(<PositiveList store={store} />).dive();
  expect(
    wrapper.instance().getFilteredItems()["enc:hasMedicinalProductPackaging"]
      .length
  ).toBe(2);
  wrapper.find("input").value = "OME";
  wrapper.find("input").simulate("change", { target: { value: "OME" } });
  expect(wrapper.state().writing).toBeGreaterThan(0);
  expect(wrapper.state().ing).toBe("");
  setTimeout(() => {
    expect(
      wrapper.instance().getFilteredItems()["enc:hasMedicinalProductPackaging"]
        .length
    ).toBe(2);
    done();
  }, 1000);
});

