import React from "react";
import AddMedicamentSearch from "../../src/components/User/AddMedicamentSearch.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { Form } from "react-form";
import { mount, shallow } from "enzyme";
const store = createStore(allReducers, applyMiddleware(thunk));

test("AddMedicamentSearch renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <Form>
            {formApi => (
              <form>
                <AddMedicamentSearch formApi={formApi} />
              </form>
            )}
          </Form>
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("SuggestionSelected works correctly", () => {
  let api;
  mount(
    <Form>
      {formApi => {
        api = formApi;
        return null;
      }}
    </Form>
  );
  let wrapper = shallow(
    <Provider store={store}>
      <AddMedicamentSearch store={store} formApi={api} />
    </Provider>
  )
    .dive()
    .dive()
    .instance();
  wrapper.onSuggestionSelected(new Event("change"), {
    suggestion: { _type: "MedicinalProduct", enc_title: "title", _id: "id" }
  });

  expect(api.getValue()).toEqual({
    _type: "MedicinalProduct",
    enc_title: "title",
    _id: "id"
  });
});

test("onChange works correctly", () => {
  let api;
  mount(
    <Form>
      {formApi => {
        api = formApi;
        return null;
      }}
    </Form>
  );
  let wrapper = shallow(
    <Provider store={store}>
      <AddMedicamentSearch store={store} formApi={api} />
    </Provider>
  )
    .dive()
    .dive()
    .instance();
  wrapper.onSuggestionSelected(new Event("change"), {
    suggestion: { _type: "MedicinalProduct", enc_title: "title", _id: "id" }
  });
  wrapper.onChange("value");
  expect(api.getValue()).toEqual({
    enc_title: "value",
    _type: "MedicinalProduct"
  });
});
