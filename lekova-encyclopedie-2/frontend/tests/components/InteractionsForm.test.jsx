import React from "react";
import { Provider } from "react-redux";
import InteractionsForm from "../../src/components/Interactions/InteractionsForm.jsx";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";
import renderer from "react-test-renderer";
import { Form } from "react-form";

const store = createStore(allReducers, applyMiddleware(thunk));
const someData = {
  suggestion: {
    _type: "ingredient",
    enc_title: "Delta sleep-inducing peptide ",
    _id:
      "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0005794"
  },
  suggestionValue: "Delta sleep-inducing peptide",
  suggestionIndex: 1,
  sectionIndex: 0,
  method: "click"
};

test("InteractionsForm renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <Form>{formApi => <InteractionsForm formApi={formApi} />}</Form>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("Adding and removing items to interactions works correctly", () => {
  let formApi;
  const tmp = mount(
    <Form>
      {api => {
        formApi = api;
        return <div />;
      }}
    </Form>
  );
  const instance = shallow(
    <InteractionsForm store={store} {...store.getState()} formApi={formApi} />
  )
    .dive()
    .instance();
  instance.onSuggestionSelected(new Event("click"), someData);
  expect(store.getState().items).toEqual({
    items: [
      {
        _type: "ingredient",
        enc_title: "Delta sleep-inducing peptide ",
        _id:
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0005794"
      }
    ],
    keys: [0],
    nextKey: 1
  });
  instance.onSuggestionSelected(new Event("click"), someData);
  expect(store.getState().items).toEqual({
    items: [
      {
        _type: "ingredient",
        enc_title: "Delta sleep-inducing peptide ",
        _id:
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0005794"
      },
      {
        _type: "ingredient",
        enc_title: "Delta sleep-inducing peptide ",
        _id:
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0005794"
      }
    ],
    keys: [0, 1],
    nextKey: 2
  });
  instance.removeItem(0);
  expect(store.getState().items).toEqual({
    items: [
      {
        _type: "ingredient",
        enc_title: "Delta sleep-inducing peptide ",
        _id:
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0005794"
      }
    ],
    keys: [1],
    nextKey: 2
  });
});
