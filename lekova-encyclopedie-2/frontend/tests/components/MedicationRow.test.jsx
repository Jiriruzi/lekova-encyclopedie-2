import React from "react";
import MedicationRow from "../../src/components/user/MedicationRow.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import toJson from "enzyme-to-json";
const store = createStore(allReducers, applyMiddleware(thunk));

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

let medicament = {
  _type: "medicinalProduct",
  _id:
    "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
  enc_title: "ASDUTER 10 MG TABLETY ",
  howOftenType: "days",
  howOften: "1",
  nextDose: "2018-11-16T20:30:55.000Z",
  description: "desc",
  amount: "250",
  key: 1,
  type: "Emulsion"
};
test("MedicationRow renders correctly when collapsed", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <MedicationRow language="cs" context="enc:" medicament={medicament} />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("MedicationRow is expanding and collapsing on click", () => {
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <MedicationRow language="cs" context="enc:" medicament={medicament} />
      </BrowserRouter>
    </Provider>
  );
  wrapper
    .find("button")
    .at(4)
    .simulate("click");
  expect(wrapper.find("MedicamentDropdown")).toHaveLength(1);
  wrapper
    .find("button")
    .at(4)
    .simulate("click");
  expect(wrapper.find("MedicamentDropdown")).toHaveLength(0);
});
