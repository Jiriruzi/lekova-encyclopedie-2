import React from "react";
import SPC from "../../src/components/SPC/SPC.jsx";
import renderer from "react-test-renderer";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";
import { toJson } from "enzyme-to-json";
const store = createStore(allReducers, applyMiddleware(thunk));

test("SPC gets url correctly", () => {
  const wrapper1 = shallow(
    <SPC
      context=""
      doc={{ hasSPCDocument: "url" }}
      environment={{ language: "cs" }}
      language="cs" 
    />
  ).instance();
  const wrapper2 = shallow(
    <SPC
      context=""
      doc={{ hasSPCDocument: { "@id": "url" } }}
      environment={{ language: "cs" }}
      language="cs" 
    />
  ).instance();
  expect(wrapper1.getUrl()).toEqual({ realUrl: "url", url: "/sukl_spc/url" });
  expect(wrapper2.getUrl()).toEqual({ realUrl: "url", url: "/sukl_spc/url" });
});
