import React from "react";
import Ingredient from "../../src/components/Ingredient/Ingredient.jsx";
import Interactions from "../../src/components/Interactions/Interactions.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { shallow } from "enzyme";

const store = createStore(allReducers, applyMiddleware(thunk));

const someConfig = {
  _id: "Ingredient",
  _rev: "1-47f0347117b80c11cebdbd03c5b941f7",
  basicInfo: [{ property: "description", class: "col-xs-12 desc" }],
  list: [
    {
      property: "mayTreat",
      value: { property: "title", class: "finding-item" },
      class: "col-xs-6 col-md-3 finding-list",
      label: "Treatment"
    },
    {
      property: "mayPrevent",
      value: { property: "title", class: "finding-item" },
      class: "col-xs-6 col-md-3 finding-list",
      label: "Prevention"
    },
    {
      property: "contraindicatedWith",
      value: { property: "title", class: "finding-item" },
      class: "col-xs-6 col-md-3 finding-list",
      label: "Contraindication",
      rowEnd: true
    },
    {
      property: "hasPharmacologicalAction",
      value: { property: "title", class: "linked-item" },
      class: "linked-list col-xs-12 col-md-6",
      label: "PharmacologicalAction"
    },
    {
      property: "hasMechanismOfAction",
      value: { property: "title", class: "linked-item" },
      class: "linked-list col-xs-12 col-md-6",
      label: "MechanismOfAction"
    },
    {
      property: "hasPhysiologicEffect",
      value: { property: "title", class: "linked-item" },
      class: "linked-list col-xs-12 col-md-6",
      label: "PhysiologicEffect"
    },
    {
      property: "hasPharmacokinetics",
      value: { property: "title", class: "linked-item" },
      class: "linked-list col-xs-12 col-md-6",
      label: "Pharmacokinetics"
    },
    {
      property: "hasPregnancyCategory",
      value: { property: "title", class: "not-active normal-text" },
      class: "important-prop col-xs-12 col-md-6",
      label: "PregnancyCategory"
    }
  ],
  table: [
    {
      label: "MedicinalProduct",
      property: "hasMedicinalProduct",
      items: [
        { label: "Title", path: [["title"]], class: "" },
        {
          label: "ATCConcept",
          path: [["hasATCConcept", "notation"]],
          class: ""
        }
      ]
    }
  ]
};

const someData = {
  _id:
    "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0010965",
  _rev: "1-d53ce0d319d332153cdfc2255040aa92",
  "enc:mayPrevent": {
    "enc:description": [
      {
        "@value":
          'Jeden z nejtypi\\u010Dt\\u011Bj\\u0161\\u00EDch p\\u0159\\u00EDznak\\u016F onemocn\\u011Bn\\u00ED (z\\u00E1n\\u011Btu, \\u00FArazu, n\\u00E1doru aj.), jeho\\u017E biologick\\u00FDm smyslem je upozornit na vznikaj\\u00EDc\\u00ED chorobu. Mechanismus vzniku b. nen\\u00ED zcela jasn\\u00FD. Uplat\\u0148uj\\u00ED se n\\u011Bkter\\u00E9 chemick\\u00E9 l\\u00E1tky dr\\u00E1\\u017Ed\\u00EDc\\u00ED nervy, kter\\u00E9 pot\\u00E9 vedou podn\\u011Bty do ur\\u010Dit\\u00FDch oblast\\u00ED mozku. Krom\\u011B vlastn\\u00EDho onemocn\\u011Bn\\u00ED org\\u00E1n\\u016F m\\u016F\\u017Ee b\\u00FDt b. upravov\\u00E1na je\\u0161t\\u011B vlivy nervov\\u00FDmi (b. nejasn\\u00E9ho p\\u016Fvodu, r\\u016Fzn\\u00FD pr\\u00E1h pro vn\\u00EDm\\u00E1n\\u00ED b. u r\\u016Fzn\\u00FDch lid\\u00ED nebo u t\\u00E9\\u017Ee osoby v z\\u00E1vislosti na jej\\u00EDm du\\u0161evn\\u00EDm rozpolo\\u017Een\\u00ED, ztr\\u00E1ta vn\\u00EDm\\u00E1n\\u00ED b. apod.). M\\u00EDsto vn\\u00EDm\\u00E1n\\u00ED b. nemus\\u00ED rovn\\u011B\\u017E v\\u017Edy odpov\\u00EDdat posti\\u017Een\\u00E9mu m\\u00EDstu (p\\u0159enesen\\u00E1, \\u201Evyst\\u0159eluj\\u00EDc\\u00ED" b., srov. Haedovy z\\u00F3ny). B. m\\u016F\\u017Ee posti\\u017Een\\u00E9ho velmi su\\u017Eovat a krut\\u00E9 b. (nap\\u0159. po \\u00FArazu) mohou p\\u0159isp\\u00EDvat k rozvoji \\u0161oku. Proto je d\\u016Fle\\u017Eit\\u00E9 ji tlumit l\\u00E9ky (analgetika), ale u n\\u011Bkter\\u00FDch stav\\u016F by t\\u00E9to l\\u00E9\\u010Db\\u011B m\\u011Blo p\\u0159edch\\u00E1zet pozn\\u00E1n\\u00ED p\\u0159\\u00ED\\u010Diny (b. b\\u0159icha). (cit. Velk\\u00FD l\\u00E9ka\\u0159sk\\u00FD slovn\\u00EDk online, 2015 http://lekarske.slovniky.cz)     ',
        "@language": "cs"
      },
      {
        "@value":
          "An unpleasant sensation induced by noxious stimuli which are detected by NERVE ENDINGS of NOCICEPTIVE NEURONS.     ",
        "@language": "en"
      }
    ],
    "@type": "enc:DiseaseOrFinding",
    "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002278",
    "enc:title": [
      { "@value": "Bolest", "@language": "cs" },
      { "@value": "Pain", "@language": "en" }
    ]
  },
  "enc:description": {
    "@value":
      "A nonsteroidal anti-inflammatory agent with analgesic properties used in the therapy of rheumatism and arthritis.     ",
    "@language": "en"
  },
  "@type": "enc:Ingredient",
  "enc:hasPregnancyCategory": [
    {
      "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/C"
    },
    { "@id": "http://linked.opendata.cz/resource/fda-spl/pregnancy-category/D" }
  ],
  "enc:contraindicatedWith": [
    {
      "enc:description": [
        {
          "@value":
            "Inflammation of the NASAL MUCOSA, the mucous membrane lining the NASAL CAVITIES.     ",
          "@language": "en"
        },
        {
          "@value":
            "R\\u00FDma, z\\u00E1n\\u011Bt nosn\\u00ED sliznice. (cit. Velk\\u00FD l\\u00E9ka\\u0159sk\\u00FD slovn\\u00EDk online, 2013 http://lekarske.slovniky.cz/ )     ",
          "@language": "cs"
        }
      ],
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002634",
      "enc:title": [
        { "@value": "Rhinitis", "@language": "en" },
        { "@value": "Rinitida", "@language": "cs" }
      ]
    },
    {
      "enc:description": [
        {
          "@value":
            "A type of ectopic pregnancy in which the EMBRYO, MAMMALIAN implants in the ABDOMINAL CAVITY instead of in the ENDOMETRIUM of the UTERUS.     ",
          "@language": "en"
        },
        {
          "@value":
            "Druh mimod\\u011Blo\\u017En\\u00EDho t\\u011Bhotenstv\\u00ED, p\\u0159i n\\u011Bm\\u017E doch\\u00E1z\\u00ED k us\\u00EDdlen\\u00ED z\\u00E1rodku v peritone\\u00E1ln\\u00ED dutin\\u011B na pob\\u0159i\\u0161nici (n\\u011Bkdy po ruptu\\u0159e tub\\u00E1rn\\u00ED gravidity). Obv. dojde k odum\\u0159en\\u00ED a resorpci. (cit. Velk\\u00FD l\\u00E9ka\\u0159sk\\u00FD slovn\\u00EDk, 9. vyd.)     ",
          "@language": "cs"
        }
      ],
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002485",
      "enc:title": [
        { "@value": "Pregnancy, abdominal", "@language": "en" },
        {
          "@value": "T\\u011Bhotenstv\\u00ED abdomin\\u00E1ln\\u00ED",
          "@language": "cs"
        }
      ]
    },
    {
      "enc:description": {
        "@value":
          "Tendency of the smooth muscle of the tracheobronchial tree to contract more intensely in response to a given stimulus than it does in the response seen in normal individuals. This condition is present in virtually all symptomatic patients with asthma. The most prominent manifestation of this smooth muscle contraction is a decrease in airway caliber that can be readily measured in the pulmonary function laboratory.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000003431",
      "enc:title": [
        { "@value": "Bronchial hyperreactivity", "@language": "en" },
        { "@value": "Bronchy - hyperreaktivita", "@language": "cs" }
      ]
    },
    {
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000010245",
      "enc:title": { "@value": "Pregnancy third trimester", "@language": "en" }
    },
    {
      "enc:description": [
        {
          "@value":
            "Immunologically mediated adverse reactions to medicinal substances used legally or illegally.     ",
          "@language": "en"
        },
        {
          "@value":
            "Imunologicky zprost\\u0159edkovan\\u00E9 ne\\u017E\\u00E1douc\\u00ED reakce na l\\u00E9\\u010Div\\u00E9 l\\u00E1tky u\\u017Eit\\u00E9 leg\\u00E1ln\\u011B nebo neleg\\u00E1ln\\u011B.     ",
          "@language": "cs"
        }
      ],
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000999",
      "enc:title": [
        { "@value": "L\\u00E9kov\\u00E1 alergie", "@language": "cs" },
        { "@value": "Drug hypersensitivity", "@language": "en" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A form of bronchial disorder with three distinct components: airway hyper-responsiveness (RESPIRATORY HYPERSENSITIVITY), airway INFLAMMATION, and intermittent AIRWAY OBSTRUCTION. It is characterized by spasmodic contraction of airway smooth muscle, WHEEZING, and dyspnea (DYSPNEA, PAROXYSMAL).     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000498",
      "enc:title": [
        { "@value": "Asthma", "@language": "en" },
        { "@value": "Bronchi\\u00E1ln\\u00ED astma", "@language": "cs" }
      ]
    }
  ],
  "enc:hasMedicinalProduct": [
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-400",
      "enc:title": { "@value": "IBALGIN 400", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUBERL-PRO-D\\u011ATI-100MG-5ML-PEROR\\u00C1LN\\u00CD-SUSPENZE",
      "enc:title": {
        "@value":
          "IBUBERL PRO D\\u011ATI 100MG/5ML PEROR\\u00C1LN\\u00CD SUSPENZE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUCAN-400-MG",
      "enc:title": { "@value": "IBUCAN 400 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE51"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-GRIP-200-MG-5-MG",
      "enc:title": { "@value": "IBALGIN GRIP 200 MG/5 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-JUNIOR-40-MG-ML",
      "enc:title": { "@value": "IBALGIN JUNIOR 40 MG/ML", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M02AA13"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-KR\\u00C9M",
      "enc:title": { "@value": "IBALGIN KR\\u00C9M", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-600",
      "enc:title": { "@value": "IBALGIN 600", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-BABY",
      "enc:title": { "@value": "IBALGIN BABY", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M02AA13"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-DUO-EFFECT",
      "enc:title": { "@value": "IBALGIN DUO EFFECT", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M02AA13"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-GEL",
      "enc:title": { "@value": "IBALGIN GEL", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-RAPIDCAPS-200-MG-M\\u011AKK\\u00C9-TOBOLKY",
      "enc:title": {
        "@value": "IBALGIN RAPIDCAPS 200 MG M\\u011AKK\\u00C9 TOBOLKY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-RAPIDCAPS-400-MG-M\\u011AKK\\u00C9-TOBOLKY",
      "enc:title": {
        "@value": "IBALGIN RAPIDCAPS 400 MG M\\u011AKK\\u00C9 TOBOLKY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUNEX-200-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUNEX 200 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUNEX-400-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUNEX 400 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-400-MG-GALMED",
      "enc:title": { "@value": "IBUPROFEN 400 MG GALMED", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-AL-400",
      "enc:title": { "@value": "IBUPROFEN AL 400", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUMAX-200-MG",
      "enc:title": { "@value": "IBUMAX 200 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUMAX-400-MG",
      "enc:title": { "@value": "IBUMAX 400 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUMAX-600-MG",
      "enc:title": { "@value": "IBUMAX 600 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-DR--MAX-400-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN DR. MAX 400 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-GALPHARM-200-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN GALPHARM 200 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-GALPHARM-400-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN GALPHARM 400 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-BRIL-200-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN BRIL 200 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-BRIL-400-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN BRIL 400 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-BRIL-600-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN BRIL 600 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-DR--MAX-200-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "IBUPROFEN DR. MAX 200 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/ADVIL-RAPID",
      "enc:title": { "@value": "ADVIL RAPID", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-ZENTIVA-200-MG",
      "enc:title": { "@value": "IBUPROFEN ZENTIVA 200 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-ZENTIVA-400-MG",
      "enc:title": { "@value": "IBUPROFEN ZENTIVA 400 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/DOLGIT-800",
      "enc:title": { "@value": "DOLGIT 800", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M02AA13"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/DOLGIT-GEL",
      "enc:title": { "@value": "DOLGIT GEL", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "enc:prefLabel": [
          { "@value": "Ibuprofen", "@language": "cs" },
          { "@value": "Ibuprofen", "@language": "en" }
        ],
        "enc:notation": "M02AA13",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/M02AA13"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/DOLGIT-KR\\u00C9M",
      "enc:title": { "@value": "DOLGIT KR\\u00C9M", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEDOL-200-MG-\\u0160UMIV\\u00C9-GRANULE",
      "enc:title": {
        "@value": "BRUFEDOL 200 MG \\u0160UMIV\\u00C9 GRANULE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEN-SIRUP",
      "enc:title": { "@value": "BRUFEN SIRUP", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEDOL-400-MG-\\u0160UMIV\\u00C9-GRANULE",
      "enc:title": {
        "@value": "BRUFEDOL 400 MG \\u0160UMIV\\u00C9 GRANULE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEN-400",
      "enc:title": { "@value": "BRUFEN 400", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/BRUFEN-600-MG",
      "enc:title": { "@value": "BRUFEN 600 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBALGIN-200",
      "enc:title": { "@value": "IBALGIN 200", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IPROFENEX-100-MG-5-ML-PEROR\\u00C1LN\\u00CD-SUSPENZE",
      "enc:title": {
        "@value": "IPROFENEX 100 MG/5 ML PEROR\\u00C1LN\\u00CD SUSPENZE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE51"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/MODAFEN",
      "enc:title": { "@value": "MODAFEN", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "enc:prefLabel": [
          { "@value": "Ibuprofen", "@language": "cs" },
          { "@value": "Ibuprofen", "@language": "en" }
        ],
        "enc:notation": "C01EB16",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/C01EB16"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/PEDEA-5-MG-ML",
      "enc:title": { "@value": "PEDEA 5 MG/ML", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-200-MG",
      "enc:title": { "@value": "NUROFEN 200 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-400-MG",
      "enc:title": { "@value": "NUROFEN 400 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-4--POMERAN\\u010C",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI 4% POMERAN\\u010C",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-ACTIVE",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI ACTIVE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-JAHODA",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI JAHODA",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-\\u010C\\u00CDPKY-125-MG",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI \\u010C\\u00CDPKY 125 MG",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI",
      "enc:title": { "@value": "NUROFEN PRO D\\u011ATI", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-4--JAHODA",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI 4% JAHODA",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-RAPID-400-MG-CAPSULES",
      "enc:title": {
        "@value": "NUROFEN RAPID 400 MG CAPSULES",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE51"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-STOPGRIP",
      "enc:title": { "@value": "NUROFEN STOPGRIP", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-PRO-D\\u011ATI-\\u010C\\u00CDPKY-60-MG",
      "enc:title": {
        "@value": "NUROFEN PRO D\\u011ATI \\u010C\\u00CDPKY 60 MG",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/APO-IBUPROFEN-400-MG",
      "enc:title": { "@value": "APO-IBUPROFEN 400 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/APO-IBUPROFEN-RAPID-400-MG-SOFT-CAPSULES",
      "enc:title": {
        "@value": "APO-IBUPROFEN RAPID 400 MG SOFT CAPSULES",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/NUROFEN-RAPID-200-MG-CAPSULES",
      "enc:title": {
        "@value": "NUROFEN RAPID 200 MG CAPSULES",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-APOTEX-100-MG-5-ML-PEROR\\u00C1LN\\u00CD-SUSPENZE",
      "enc:title": {
        "@value": "IBUPROFEN APOTEX 100 MG/5 ML PEROR\\u00C1LN\\u00CD SUSPENZE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "enc:prefLabel": [
          { "@value": "Other cold preparations", "@language": "en" },
          {
            "@value": "Jin\\u00E1 l\\u00E9\\u010Diva proti nachlazen\\u00ED",
            "@language": "cs"
          }
        ],
        "enc:notation": "R05X",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/R05X"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/GRIPPECTON-200-MG-30-MG",
      "enc:title": { "@value": "GRIPPECTON 200 MG/30 MG", "@language": "cs" }
    },
    {
      "enc:hasATCConcept": {
        "enc:prefLabel": [
          { "@value": "Ibuprofen", "@language": "en" },
          { "@value": "Ibuprofen", "@language": "cs" }
        ],
        "enc:notation": "M01AE01",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/M01AE01"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/IBUPROFEN-ALKALOID-INT-100-MG-5-ML-PEROR\\u00C1LN\\u00CD-SUSPENZE",
      "enc:title": {
        "@value":
          "IBUPROFEN ALKALOID-INT 100 MG/5 ML PEROR\\u00C1LN\\u00CD SUSPENZE",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "@id": "http://linked.opendata.cz/resource/atc/M01AE51"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/COMBOGESIC-500-MG-150-MG-POTAHOVAN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "COMBOGESIC 500 MG/150 MG POTAHOVAN\\u00C9 TABLETY",
        "@language": "cs"
      }
    },
    {
      "enc:hasATCConcept": {
        "enc:prefLabel": [
          { "@value": "Ibuprofen, combinations", "@language": "en" },
          { "@value": "Ibuprofen, kombinace", "@language": "cs" }
        ],
        "enc:notation": "M01AE51",
        "@type": "enc:ATCConcept",
        "@id": "http://linked.opendata.cz/resource/atc/M01AE51"
      },
      "@type": "enc:MedicinalProduct",
      "@id":
        "http://linked.opendata.cz/resource/sukl/medicinal-product/ROBICOLD-200-MG-30-MG-OBALEN\\u00C9-TABLETY",
      "enc:title": {
        "@value": "ROBICOLD 200 MG/30 MG OBALEN\\u00C9 TABLETY",
        "@language": "cs"
      }
    }
  ],
  "@context": {
    xsd: "http://www.w3.org/2001/XMLSchema#",
    enc: "http://linked.opendata.cz/ontology/drug-encyclopedia/",
    gr: "http://purl.org/goodrelations/v1#"
  },
  "enc:title": [
    { "@value": "Ibuprofen", "@language": "en" },
    "Ibuprofenum",
    { "@value": "Ibuprofen", "@language": "cs" }
  ],
  "enc:hasInteraction": [
    {
      "enc:hasDescription": [
        {
          "@value":
            "Současné užívání látek inhibujících syntézu prostaglandinů (např. ibuprofen, indometacin) může vést ke snížení hypotenzního účinku atenololu.",
          "enc:source": {
            "enc:url": "https://www.sukl.cz/",
            "enc:title": "sukl"
          }
        },
        {
          "@value": "Risk of inhibition of renal prostaglandins ",
          "enc:source": {
            "enc:url": "https://www.drugbank.ca/drugs/DB01050",
            "enc:title": "drugbank"
          }
        }
      ],
      "enc:interactionWith": {
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0001900",
        "enc:title": [
          { "@value": "Atenolol", "@language": "en" },
          "Natrii selenis",
          { "@value": "Atenolol", "@language": "cs" }
        ]
      },
      "enc:isCritical": true
    },
    {
      "enc:hasDescription": [
        {
          "@value": "Risk of inhibition of renal prostaglandins",
          "enc:source": {
            "enc:url": "https://www.drugbank.ca/",
            "enc:title": "drugbank"
          }
        }
      ],
      "enc:interactionWith": {
        "@id":
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/sukl/19690",
        "enc:title": [
          { "@value": "Refametinib", "@language": "en" },
          { "@value": "Refametinib", "@language": "cs" },
          "Refametinibum"
        ]
      },
      "enc:isCritical": true
    }
  ],
  "enc:hasPharmacokinetics": [
    {
      "@type": "enc:Pharmacokinetics",
      "@id":
        "http://linked.opendata.cz/resource/ndfrt/pharmacokinetics/N0000000042",
      "enc:title": { "@value": "Renal Excretion", "@language": "en" }
    },
    {
      "@type": "enc:Pharmacokinetics",
      "@id":
        "http://linked.opendata.cz/resource/ndfrt/pharmacokinetics/N0000000026",
      "enc:title": { "@value": "Hepatic Metabolism", "@language": "en" }
    }
  ],
  "@id":
    "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0010965",
  "enc:hasPharmacologicalAction": [
    {
      "enc:description": {
        "@value":
          "Compounds or agents that combine with cyclooxygenase (PROSTAGLANDIN-ENDOPEROXIDE SYNTHASES) and thereby prevent its substrate-enzyme combination with arachidonic acid and the formation of eicosanoids, prostaglandins, and thromboxanes.     ",
        "@language": "en"
      },
      "@type": "enc:PharmacologicalAction",
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0025664",
      "enc:title": [
        { "@value": "Inhibitory cyklooxygen\\u00E1zy", "@language": "cs" },
        { "@value": "Cyclooxygenase inhibitors", "@language": "en" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A subclass of analgesic agents that typically do not bind to OPIOID RECEPTORS and are not addictive. Many non-narcotic analgesics are offered as NONPRESCRIPTION DRUGS.     ",
        "@language": "en"
      },
      "@type": "enc:PharmacologicalAction",
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0028038",
      "enc:title": [
        { "@value": "Neopioidn\\u00ED analgetika", "@language": "cs" },
        { "@value": "Analgesics, non-narcotic", "@language": "en" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "Anti-inflammatory agents that are non-steroidal in nature. In addition to anti-inflammatory actions, they have analgesic, antipyretic, and platelet-inhibitory actions.They act by blocking the synthesis of prostaglandins by inhibiting cyclooxygenase, which converts arachidonic acid to cyclic endoperoxides, precursors of prostaglandins. Inhibition of prostaglandin synthesis accounts for their analgesic, antipyretic, and platelet-inhibitory actions; other mechanisms may contribute to their anti-inflammatory effects.     ",
        "@language": "en"
      },
      "@type": "enc:PharmacologicalAction",
      "@id":
        "http://linked.opendata.cz/resource/drug-encyclopedia/pharmacological-action/M0001335",
      "enc:title": [
        {
          "@value": "Anti-inflammatory agents, non-steroidal",
          "@language": "en"
        },
        { "@value": "Antiflogistika nesteroidn\\u00ED", "@language": "cs" }
      ]
    }
  ],
  "enc:mayTreat": [
    {
      "enc:description": [
        {
          "@value":
            "A chronic inflammatory condition affecting the axial joints, such as the SACROILIAC JOINT and other intervertebral or costovertebral joints. It occurs predominantly in young males and is characterized by pain and stiffness of joints (ANKYLOSIS) with inflammation at tendon insertions.     ",
          "@language": "en"
        },
        {
          "@value":
            "Z\\u00E1n\\u011Btliv\\u00E9 onemocn\\u011Bn\\u00ED kloub\\u016F postihuj\\u00EDc\\u00ED zejm. klouby p\\u00E1te\\u0159e, k\\u0159\\u00ED\\u017Eov\\u00E9 (sakroiliak\\u00E1ln\\u00ED) klouby a n\\u011Bkter\\u00E9 v\\u011Bt\\u0161\\u00ED klouby kon\\u010Detin (ramena, ky\\u010Dle). Onemocn\\u011Bn\\u00ED se vyskytuje ve sv\\u00FDch t\\u011B\\u017E\\u0161\\u00EDch form\\u00E1ch p\\u0159ev\\u00E1\\u017En\\u011B u mu\\u017E\\u016F. P\\u0159edch\\u00E1zej\\u00ED mu n\\u011Bkdy z\\u00E1n\\u011Bty oka (iridocyklitida) a bolesti pat (Achillovy \\u0161lachy). Pozd\\u011Bji se objevuje bolest v k\\u0159\\u00ED\\u017Ei a v z\\u00E1dech, omezen\\u00ED hybnosti p\\u00E1te\\u0159e a v nel\\u00E9\\u010Den\\u00FDch p\\u0159\\u00EDpadech m\\u016F\\u017Ee doj\\u00EDt a\\u017E k jej\\u00EDmu naprost\\u00E9mu ztuhnut\\u00ED (ankyl\\u00F3zy p\\u00E1te\\u0159n\\u00EDch kloub\\u016F). V l\\u00E9\\u010Db\\u011B se pou\\u017E\\u00EDvaj\\u00ED antirevmatika, nesm\\u00EDrn\\u011B d\\u016Fle\\u017Eit\\u00E1 je rehabilitace. P\\u0159\\u00ED\\u010Dina nemoci nen\\u00ED zn\\u00E1ma, existuje vztah k n\\u011Bkter\\u00FDm HLA antigen\\u016Fm. (cit. Velk\\u00FD l\\u00E9ka\\u0159sk\\u00FD slovn\\u00EDk online, 2013 http://lekarske.slovniky.cz/ )     ",
          "@language": "cs"
        }
      ],
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002810",
      "enc:title": [
        { "@value": "Spondylitis, ankylosing", "@language": "en" },
        {
          "@value": "Ankyl\\u00F3zuj\\u00EDc\\u00ED spondylitida",
          "@language": "cs"
        }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A pathological process characterized by injury or destruction of tissues caused by a variety of cytologic and chemical reactions. It is usually manifested by typical signs of pain, heat, redness, swelling, and loss of function.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001687",
      "enc:title": [
        { "@value": "Inflammation", "@language": "en" },
        { "@value": "Z\\u00E1n\\u011Bt", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "Hereditary metabolic disorder characterized by recurrent acute arthritis, hyperuricemia and deposition of sodium urate in and around the joints, sometimes with formation of uric acid calculi.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001382",
      "enc:title": [
        { "@value": "Dna (nemoc)", "@language": "cs" },
        { "@value": "Gout", "@language": "en" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A combination of distressing physical, psychologic, or behavioral changes that occur during the luteal phase of the menstrual cycle. Symptoms of PMS are diverse (such as pain, water-retention, anxiety, cravings, and depression) and they diminish markedly 2 or 3 days after the initiation of menses.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002489",
      "enc:title": [
        { "@value": "Premenstrual syndrome", "@language": "en" },
        { "@value": "Premenstrua\\u010Dn\\u00ED syndrom", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "Inflammation or irritation of a bursa, the fibrous sac that acts as a cushion between moving structures of bones, muscles, tendons or skin.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000639",
      "enc:title": [
        { "@value": "Bursitida", "@language": "cs" },
        { "@value": "Bursitis", "@language": "en" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "Arthritis of children, with onset before 16 years of age. The terms juvenile rheumatoid arthritis (JRA) and juvenile idiopathic arthritis (JIA) refer to classification systems for chronic arthritis in children. Only one subtype of juvenile arthritis (polyarticular-onset, rheumatoid factor-positive) clinically resembles adult rheumatoid arthritis and is considered its childhood equivalent.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000481",
      "enc:title": [
        { "@value": "Arthritis, juvenile", "@language": "en" },
        {
          "@value": "Juveniln\\u00ED idiopatick\\u00E1 artritida",
          "@language": "cs"
        }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A chronic systemic disease, primarily of the joints, marked by inflammatory changes in the synovial membranes and articular structures, widespread fibrinoid degeneration of the collagen fibers in mesenchymal tissues, and by atrophy and rarefaction of bony structures. Etiology is unknown, but autoimmune mechanisms have been implicated.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000000482",
      "enc:title": [
        { "@value": "Arthritis, rheumatoid", "@language": "en" },
        { "@value": "Revmatoidn\\u00ED artritida", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "A progressive, degenerative joint disease, the most common form of arthritis, especially in older persons. The disease is thought to result not from the aging process but from biochemical changes and biomechanical stresses affecting articular cartilage. In the foreign literature it is often called osteoarthrosis deformans.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002244",
      "enc:title": [
        { "@value": "Osteoarthritis", "@language": "en" },
        { "@value": "Osteoartr\\u00F3za", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value":
          "An abnormal elevation of body temperature, usually as a result of a pathologic process.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001242",
      "enc:title": [
        { "@value": "Fever", "@language": "en" },
        { "@value": "Hore\\u010Dka", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value": "Excessive uterine bleeding during MENSTRUATION.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001993",
      "enc:title": [
        { "@value": "Menorrhagia", "@language": "en" },
        { "@value": "Menoragie", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value": "Pain during the period after surgery.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000002280",
      "enc:title": [
        { "@value": "Pain, postoperative", "@language": "en" },
        { "@value": "Poopera\\u010Dn\\u00ED bolest", "@language": "cs" }
      ]
    },
    {
      "enc:description": {
        "@value": "Painful menstruation.     ",
        "@language": "en"
      },
      "@type": "enc:DiseaseOrFinding",
      "@id": "http://linked.opendata.cz/resource/ndfrt/disease/N0000001025",
      "enc:title": [
        { "@value": "Dysmenorrhea", "@language": "en" },
        { "@value": "Dysmenorea", "@language": "cs" }
      ]
    }
  ],
  "enc:hasMechanismOfAction": {
    "@type": "enc:MechanismOfAction",
    "@id":
      "http://linked.opendata.cz/resource/ndfrt/mechanism-of-action/N0000000160",
    "enc:title": { "@value": "Cyclooxygenase Inhibitors", "@language": "en" }
  },
  "enc:hasPhysiologicEffect": [
    {
      "@type": "enc:PhysiologicEffect",
      "@id":
        "http://linked.opendata.cz/resource/ndfrt/physiologic-effect/N0000009006",
      "enc:title": {
        "@value": "Decreased Thromboxane Production",
        "@language": "en"
      }
    },
    {
      "@type": "enc:PhysiologicEffect",
      "@id":
        "http://linked.opendata.cz/resource/ndfrt/physiologic-effect/N0000008836",
      "enc:title": {
        "@value": "Decreased Prostaglandin Production",
        "@language": "en"
      }
    },
    {
      "@type": "enc:PhysiologicEffect",
      "@id":
        "http://linked.opendata.cz/resource/ndfrt/physiologic-effect/N0000008831",
      "enc:title": {
        "@value": "Decreased Platelet Activating Factor Production",
        "@language": "en"
      }
    }
  ]
};

test("Ingredient starts loading on mount", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Ingredient
          store={store}
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  expect(store.getState().documents.Ingredient.loading).toBeTruthy();
  expect(store.getState().documents.Configuration.loading).toBeTruthy();
});

test("Ingredient renders correctly when loading", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Ingredient
          {...{
            location: {
              id:
                "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Ingredient renders correctly when document is loaded", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Ingredient
          store={store}
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Ingredient",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Ingredient renders correctly when some documents did not load", () => {
  const component = renderer.create(
    <Provider store={store}>
      <BrowserRouter>
        <Ingredient
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
              }
            },
            location: {}
          }}
        />
      </BrowserRouter>
    </Provider>
  );
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "Ingredient"
  });

  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });

  let tree = component;
  expect(tree.toJSON()).toMatchSnapshot();
});

test("Ingredient renders interactions when param is in url", () => {
  const wrapper = shallow(
    <Provider store={store}>
      <BrowserRouter>
        <Ingredient
          {...{
            match: {
              params: {
                id:
                  "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
              }
            },
            location: {
              search: "?interactions",
              pathname:
                "/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965"
            }
          }}
        />
      </BrowserRouter>
    </Provider>
  )
    .dive()
    .dive();
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Ingredient",
      doc: someData
    }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Configuration",
      doc: someConfig
    }
  });
  expect(wrapper.find(Interactions)).toBeTruthy();
});
