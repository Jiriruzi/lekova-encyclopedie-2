import React from "react";
import MedicamentDropdown from "../../src/components/User/MedicamentDropdown.jsx";
import renderer from "react-test-renderer";

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

const medicament = {
  _type: "medicinalProduct",
  _id:
    "http://linked.opendata.cz/resource/sukl/medicinal-product/ASDUTER-10-MG-TABLETY",
  enc_title: "ASDUTER 10 MG TABLETY ",
  howOftenType: "days",
  howOften: "1",
  nextDose: "2018-11-16T20:30:55.000Z",
  description: "desc",
  amount: "250",
  key: 1,
  type: "Emulsion"
};

test("MedicamentDropdown renders correctly - table (more than 680 px wide) ", () => {
  const component = renderer
    .create(
      <MedicamentDropdown
        medicament={medicament}
        language="cs"
        context="enc:"
        width="686"
      />
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});



test("MedicamentDropdown renders correctly - card (less than 680px wide)", () => {
  const component = renderer
    .create(
      <MedicamentDropdown
        medicament={medicament}
        language="cs"
        context="enc:"
        width="474"
      />
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});