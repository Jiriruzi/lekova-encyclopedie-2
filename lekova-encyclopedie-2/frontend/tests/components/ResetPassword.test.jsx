import React from "react";
import ResetPassword from "../../src/components/user/ResetPassword.jsx";
import { errorValidator } from "../../src/components/user/ResetPassword.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

import { createBrowserHistory as createHistory } from "history";

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("ResetPassword renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <ResetPassword history={history} />
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "doc" }
  });
  store.dispatch({
    type: "USER_FOUND",
    payload: {
      birthday: { day: 17, year: 2018, month: 9 },
      gender: "male",
      email: "email@em.em"
    }
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("ResetPassword redirects when logged", () => {
  history.location.pathname = "/ResetPassword";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <ResetPassword history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/");
});

test("ResetPassword does not redirect when not logged", () => {
  history.location.pathname = "/ResetPassword";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <ResetPassword history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/ResetPassword");
});

test("error validator works correctly", () => {
  expect(errorValidator({})).toEqual({
    username: "MissingUsername",
    newPassword: "MissingPassword",
    passwordConfirmation: "DifferentPasswords"
  });
  expect(
    errorValidator({
      username: "aa",
      newPassword: "pass",
      passwordConfirmation: "ab"
    })
  ).toEqual({
    username: null,
    newPassword: null,
    passwordConfirmation: "DifferentPasswords"
  });
  expect(
    errorValidator({
      username: "aa",
      newPassword: "pass",
      passwordConfirmation: "pass"
    })
  ).toEqual({ username: null, newPassword: null, passwordConfirmation: null });
});
