import React from "react";
import HomeButton from "../../src/components/Home/HomeButton.jsx";
import renderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
const store = createStore(allReducers, applyMiddleware(thunk));

test("HomeButton renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <BrowserRouter>
          <HomeButton />
        </BrowserRouter>
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
test("HomeButton redirects correctly", () => {
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <HomeButton to="/Registration" />
      </BrowserRouter>
    </Provider>
  );
  wrapper.find("button").simulate("click");
  expect(
    wrapper
      .children()
      .first()
      .children()
      .first()
      .props().history.location.pathname
  ).toBe("/Registration");
});
