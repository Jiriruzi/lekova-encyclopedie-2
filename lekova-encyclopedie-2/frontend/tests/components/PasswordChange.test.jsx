import React from "react";
import PasswordChange from "../../src/components/user/PasswordChange.jsx";
import { errorValidator } from "../../src/components/user/PasswordChange.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

import { createBrowserHistory as createHistory } from "history";

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("PasswordChange renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <PasswordChange history={history} />
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "doc" }
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("PasswordChange does not redirect when logged", () => {
  history.location.pathname = "/PasswordChange";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <PasswordChange history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/PasswordChange");
});

test("PasswordChange redirects when not logged", () => {
  history.location.pathname = "/PasswordChange";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <PasswordChange history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/Login");
});

test("error validator works correctly", () => {
  expect(
    errorValidator({
      password: "1.6.1994",
      newPassword: "male",
      passwordConfirmation: "male"
    })
  ).toEqual({
    password: null,
    newPassword: null,
    passwordConfirmation: null
  });
  expect(
    errorValidator({
      newPassword: "male",
      passwordConfirmation: "male"
    })
  ).toEqual({
    password: "MissingPassword",
    newPassword: null,
    passwordConfirmation: null
  });
  expect(
    errorValidator({
      password: "1.6.1994",
      passwordConfirmation: "email@email.com"
    })
  ).toEqual({
    password: null,
    newPassword: "MissingPassword",
    passwordConfirmation: "DifferentPasswords"
  });
  expect(
    errorValidator({
      password: "1.6.1994",
      newPassword: "males",
      passwordConfirmation: "male"
    })
  ).toEqual({
    password: null,
    newPassword: null,
    passwordConfirmation: "DifferentPasswords"
  });
});
