import React from "react";
import Login from "../../src/components/user/Login.jsx";
import { errorValidator } from "../../src/components/user/Login.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

import { createBrowserHistory as createHistory } from "history";

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("Login renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <Login history={history} />
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "doc" }
  });
  store.dispatch({
    type: "USER_FOUND",
    payload: {
      birthday: { day: 17, year: 2018, month: 9 },
      gender: "male",
      email: "email@em.em"
    }
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("Login redirects when logged", () => {
  history.location.pathname = "/Login";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <Login history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/");
});

test("Login does not redirect when not logged", () => {
  history.location.pathname = "/Login";
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <Login history={history} />
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/Login");
});

test("error validator works correctly", () => {
  expect(errorValidator({})).toEqual({
    username: "MissingUsername",
    password: "MissingPassword"
  });
  expect(
    errorValidator({
      username: "aa"
    })
  ).toEqual({ username: null, password: "MissingPassword" });
  expect(
    errorValidator({
      password: "asd"
    })
  ).toEqual({ username: "MissingUsername", password: null });
});
