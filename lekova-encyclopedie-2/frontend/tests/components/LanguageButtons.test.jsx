import React from "react";
import LanguageButtons from "../../src/components/LanguageButtons/LanguageButtons.jsx";
import renderer from "react-test-renderer";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";

const store = createStore(allReducers, applyMiddleware(thunk));

test("LanguageButtons renders correctly", () => {
  const component = renderer
    .create(
      <Provider store={store}>
        <LanguageButtons />
      </Provider>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});
test("LanguageButtons changes language", () => {
  const wrapper = mount(
    <Provider store={store}>
      <LanguageButtons />
    </Provider>
  );
  wrapper.find("button").at(0).simulate("click");
  expect(store.getState().environment.language).toBe("en")
  wrapper.find("button").at(1).simulate("click");
  expect(store.getState().environment.language).toBe("cs")
});
