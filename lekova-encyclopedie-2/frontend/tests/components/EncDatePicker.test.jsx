import React from "react";
import EncDatePicker from "../../src/components/User/EncDatePicker.jsx";
import renderer from "react-test-renderer";
import { mount, shallow } from "enzyme";
import moment from "moment";

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

test("EncDatePicker renders correctly", () => {
  let value = "";
  let error = "";
  let touched = false;

  const component = renderer
    .create(
      <EncDatePicker
        setValue={v => (value = v)}
        setsetError={er => (error = er)}
        setTouched={t => (touched = t)}
      />
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("handleChange works correctly", () => {
  let error = "";
  let value = "";
  let touched = false;
  const instance = shallow(
    <EncDatePicker
      setTouched={v => (touched = v)}
      setValue={(v, date) => {
        value = date;
      }}
      value={value}
      setError={er => (error = er)}
    />
  ).instance();
  let date = moment();
  instance.handleChange(date);
  expect(value).toEqual(date._d);
});

test("onChangeRaw works correctly when valid date is given", () => {
  let error = "";
  let value = "";
  let touched = false;
  const instance = shallow(
    <EncDatePicker
      setTouched={v => (touched = v)}
      setValue={(v, date) => {
        value = date;
      }}
      language="cs"
      value={value}
      setError={er => (error = er)}
    />
  ).instance();
  moment.locale("cs");
  let date = moment("9. listopad 2018", "LLL");
  instance.onChangeRaw({ target: { value: "9. listopad 2018" } });
  expect(value).toEqual(date._d);
});

test("onChangeRaw works sets error when invalid date is given", () => {
  let error = "";
  let value = "";
  let touched = false;
  const instance = shallow(
    <EncDatePicker
      setTouched={v => (touched = v)}
      setValue={(v, date) => {
        value = date;
      }}
      language="cs"
      value={value}
      setError={(er,val) => (error = val)}
    />
  ).instance();
  moment.locale("cs");
  instance.onChangeRaw({ target: { value: "not a date" } });
  expect(error).toEqual("InvalidDate");
});