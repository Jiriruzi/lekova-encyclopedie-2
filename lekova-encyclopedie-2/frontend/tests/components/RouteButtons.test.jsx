import React from "react";
import RouteButtons from "../../src/components/Buttons/RouteButtonGroup.jsx";
import renderer from "react-test-renderer";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount } from "enzyme";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";

const store = createStore(allReducers, applyMiddleware(thunk));
test("Buttons renders correctly", () => {
  const component = renderer
    .create(
      <BrowserRouter>
        <RouteButtons
          store={store}
          routes={[
            { path: "detail", label: "Detail" },
            { path: "cheapest", label: "CheapestAtc" }
          ]}
        />
      </BrowserRouter>
    )
    .toJSON();
  let tree = component;
  expect(tree).toMatchSnapshot();
});

test("Buttons change search params", () => {
  let history;
  const wrapper = mount(
    <Provider store={store}>
      <BrowserRouter>
        <RouteButtons
          {...{
            match: {
              params: {
                id: "http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
              }
            },
            location: {
              search: "?cheapest",
              pathname:
                "/ATCConcept/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fatc%2FM01AE01"
            }
          }}
          routes={[
            { path: "detail", label: "Detail" },
            { path: "cheapest", label: "CheapestAtc" }
          ]}
        />
      </BrowserRouter>
    </Provider>
  );
  wrapper
    .find("button")
    .at(1)
    .simulate("click");
  expect(
    wrapper
      .children()
      .first()
      .children()
      .first()
      .props().history.location.search
  ).toBe("?cheapest");
  wrapper
  .find("button")
  .at(0)
  .simulate("click");
  expect(
    wrapper
      .children()
      .first()
      .children()
      .first()
      .props().history.location.search
  ).toBe("?detail");
});
