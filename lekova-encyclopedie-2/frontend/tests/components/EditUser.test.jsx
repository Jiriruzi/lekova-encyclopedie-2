import React from "react";
import EditUser from "../../src/components/user/EditUser.jsx";
import { errorValidator } from "../../src/components/user/EditUser.jsx";
import renderer from "react-test-renderer";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import allReducers from "../../src/reducers/index.js";
import thunk from "redux-thunk";
import { mount, shallow } from "enzyme";

import { createBrowserHistory as createHistory } from "history";

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

let history = createHistory();

const store = createStore(allReducers, applyMiddleware(thunk));

test("EditUser renders correctly", () => {
  const component = renderer.create(
    <Provider store={store}>
      <Router history={history}>
        <EditUser history={history}/>
      </Router>
    </Provider>
  );
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "doc" }
  });
  store.dispatch({
    type: "USER_FOUND",
    payload: {
      birthday: { day: 17, year: 2018, month: 9 },
      gender: "male",
      email: "email@em.em"
    }
  });
  expect(component.toJSON()).toMatchSnapshot();
});

test("EditUser does not redirect when logged", () => {
  history.location.pathname="/EditUser"
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: "petr", role: "user" }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EditUser history={history}/>
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();
  expect(component.props().history.location.pathname).toBe("/EditUser");
});

test("EditUser redirects when not logged", () => {
  history.location.pathname="/EditUser"
  store.dispatch({
    type: "USER_SESSION",
    payload: { username: null, role: null }
  });
  const component = mount(
    <Provider store={store}>
      <Router history={history}>
        <EditUser history={history}/>
      </Router>
    </Provider>
  )
    .children()
    .first()
    .children()
    .first();

  expect(component.props().history.location.pathname).toBe("/Login");
});


test("error validator works correctly", () => {
  expect(
    errorValidator({
      birthday: "1.6.1994",
      gender: "male",
      email: "email@email.com"
    })
  ).toEqual({ birthday: null, gender: null, email: null });
  expect(
    errorValidator({
      gender: "male",
      email: "email@email.com"
    })
  ).toEqual({ birthday: "MissingDate", gender: null, email: null });
  expect(
    errorValidator({
      birthday: "1.6.1994",
      email: "email@email.com"
    })
  ).toEqual({ birthday: null, gender: "MissingGender", email: null });
  expect(
    errorValidator({
      birthday: "1.6.1994",
      gender: "male"
    })
  ).toEqual({ birthday: null, gender: null, email: "MissingEmail" });
  expect(
    errorValidator({
      birthday: "1.6.1994",
      gender: "male",
      email: "emailemail.com"
    })
  ).toEqual({ birthday: null, gender: null, email: "InvalidEmail" });
});
