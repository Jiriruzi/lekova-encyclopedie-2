import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-interactions";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

Object.defineProperty(window.navigator, "language", { value: "en" });

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for interactions saves state correctly", () => {
  expect(store.getState().state).toEqual({
    items: [],
    loading: false,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "INTERACTIONS_LOADING"
  });
  expect(store.getState().state).toEqual({
    items: [],
    loading: true,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "ADD_INTERACTION_ITEM",
    payload: [{ value: "ITEM1" }]
  });
  store.dispatch({
    type: "ADD_INTERACTION_ITEM",
    payload: [{ value: "ITEM2" }]
  });
  store.dispatch({
    type: "ADD_INTERACTION_ITEM",
    payload: [{ value: "ITEM3" }]
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }, { value: "ITEM2" }, { value: "ITEM3" }],
    loading: true,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "REMOVE_INTERACTION_ITEM",
    payload: 1
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }, { value: "ITEM3" }],
    loading: true,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "REMOVE_INTERACTION_ITEM",
    payload: 1
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }],
    loading: true,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "REMOVE_INTERACTION_ITEM",
    payload: 0
  });
  expect(store.getState().state).toEqual({
    items: [],
    loading: true,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "INTERACTIONS_NOT_LOADING"
  });
  store.dispatch({
    type: "ADD_INTERACTION_ITEM",
    payload: [{ value: "ITEM1" }]
  });
  store.dispatch({
    type: "ADD_INTERACTION_ITEM",
    payload: [{ value: "ITEM2" }]
  });
  store.dispatch({
    type: "INTERACTIONS_CREATED",
    payload: ["ing1", "ing2"]
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }, { value: "ITEM2" }],
    loading: false,
    error: false,
    ingredients: ["ing1", "ing2"]
  });
  store.dispatch({
    type: "RESET_ONLY_INTERACTIONS"
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }, { value: "ITEM2" }],
    loading: false,
    error: false,
    ingredients: []
  });
  store.dispatch({
    type: "INTERACTIONS_FAILED"
  });
  expect(store.getState().state).toEqual({
    items: [{ value: "ITEM1" }, { value: "ITEM2" }],
    loading: false,
    error: true,
    ingredients: []
  });
  store.dispatch({
    type: "INTERACTIONS_RESET"
  });
  expect(store.getState().state).toEqual({
    items: [],
    loading: false,
    error: false,
    ingredients: []
  });
});
