import { createStore, applyMiddleware, bindActionCreators } from "redux";
import reducer from "../../src/reducers/reducer-search.js";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for documents saves state correctly", () => {
  expect(store.getState().state).toEqual({
    valueInForm: "",
    valueHeader: "",
    sections: [],
    loading: false,
    resultsAll: [],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({
    type: "SET_SEARCH_VALUE_HEADER",
    payload: null
  });
  store.dispatch({
    type: "SET_SEARCH_VALUE_IN_FORM",
    payload: null
  });
  expect(store.getState().state).toEqual({
    valueInForm: "",
    valueHeader: "",
    sections: [],
    loading: false,
    resultsAll: [],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({
    type: "SET_SEARCH_VALUE_HEADER",
    payload: "pesH"
  });
  store.dispatch({
    type: "SET_SEARCH_VALUE_IN_FORM",
    payload: "pes"
  });
  expect(store.getState().state).toEqual({
    valueInForm: "pes",
    valueHeader: "pesH",
    sections: [],
    loading: false,
    resultsAll: [],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({
    type: "ALL_RESULTS_LOADING"
  });
  expect(store.getState().state).toEqual({
    valueInForm: "pes",
    valueHeader: "pesH",
    sections: [],
    loading: false,
    resultsAll: [],
    loadingValues: [],
    loadingResultsAll: true
  });
  store.dispatch({
    type: "ALL_RESULTS",
    payload: ["pesh", "suchpesH"]
  });
  expect(store.getState().state).toEqual({
    valueInForm: "pes",
    valueHeader: "pesH",
    sections: [],
    loading: false,
    resultsAll: ["pesh", "suchpesH"],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({
    type: "SUGGEST_LOADING",
    payload: "a"
  });
  store.dispatch({
    type: "SUGGEST_LOADING",
    payload: "ab"
  });
  store.dispatch({
    type: "SUGGEST_LOADING",
    payload: "abc"
  });
  store.dispatch({
    type: "SUGGEST_LOADING",
    payload: "abcd"
  });
  store.dispatch({
    type: "SUGGEST",

    payload: {
      value: "abcd",
      suggestions: [
        { value: "a", _type: "type1" },
        { value: "b", _type: "type2" },
        { value: "c", _type: "type1" },
        { value: "d", _type: "type1" },
        { value: "e", _type: "type2" }
      ]
    }
  });
  expect(store.getState().state).toEqual({
    valueInForm: "pes",
    valueHeader: "pesH",
    sections: [
      {
        name: "type1",
        items: [
          { value: "a", _type: "type1" },
          { value: "c", _type: "type1" },
          { value: "d", _type: "type1" }
        ]
      },
      {
        name: "type2",
        items: [{ value: "b", _type: "type2" }, { value: "e", _type: "type2" }]
      }
    ],
    loading: false,
    resultsAll: ["pesh", "suchpesH"],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({
    type: "SUGGEST_LONG_LOADING",
    payload: "abc"
  });
  expect(store.getState().state).toEqual({
    valueInForm: "pes",
    valueHeader: "pesH",
    sections: [
      {
        name: "type1",
        items: [
          { value: "a", _type: "type1" },
          { value: "c", _type: "type1" },
          { value: "d", _type: "type1" }
        ]
      },
      {
        name: "type2",
        items: [{ value: "b", _type: "type2" }, { value: "e", _type: "type2" }]
      }
    ],
    loading: false,
    resultsAll: ["pesh", "suchpesH"],
    loadingValues: [],
    loadingResultsAll: false
  });
  store.dispatch({ type: "RESET_RESULTS" });
  expect(store.getState().state).toEqual({
    valueInForm: "",
    valueHeader: "",
    sections: [],
    resultsAll: [],
    loadingValues: [],
    loadingResultsAll: false,
    loading: false
  });
});
