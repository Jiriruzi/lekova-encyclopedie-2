import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for documents saves state correctly", () => {
  expect(store.getState().state).toEqual({});
  let doc1 = {
    doc1ingProp1: "val1",
    doc1ingProp2: "val2"
  };
  let doc2 = {
    doc2ingProp1: "val1",
    doc2ingProp2: "val2"
  };
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "Ingredient",
      doc: doc1
    }
  });
  expect(store.getState().state).toEqual({
    Ingredient: { doc: doc1, loading: false, error: false }
  });
  store.dispatch({
    type: "DOCUMENT_NOT_FOUND",
    payload: "Ingredient"
  });
  expect(store.getState().state).toEqual({
    Ingredient: { loading: false, error: true }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "ATCConcept",
      doc: doc2
    }
  });
  expect(store.getState().state).toEqual({
    Ingredient: { loading: false, error: true },
    ATCConcept: { loading: false, error: false, doc: doc2 }
  });
  store.dispatch({
    type: "DOCUMENT_LOADING",
    payload: "MedicinalProduct"
  });
  expect(store.getState().state).toEqual({
    Ingredient: { loading: false, error: true },
    ATCConcept: { loading: false, error: false, doc: doc2 },
    MedicinalProduct: { loading: true }
  });
  store.dispatch({
    type: "DOCUMENT_FOUND",
    payload: {
      type: "MedicinalProduct",
      doc: doc1
    }
  });
  expect(store.getState().state).toEqual({
    Ingredient: { loading: false, error: true },
    ATCConcept: { loading: false, error: false, doc: doc2 },
    MedicinalProduct: { loading: false, error: false, doc: doc1 }
  });
});
