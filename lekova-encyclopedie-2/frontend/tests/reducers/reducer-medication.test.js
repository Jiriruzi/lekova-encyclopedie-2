import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-medication.js";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for documents saves state correctly", () => {
  expect(store.getState().state).toEqual({});
  store.dispatch({
    type: "MEDICATIONS_LOADING"
  });
  expect(store.getState().state).toEqual({
    loading: true
  });
  store.dispatch({
    type: "GET_MEDICATION",
    payload: [
      { value: "item1", key: 0 },
      { value: "item2", key: 1 },
      { value: "item3", key: 2 }
    ]
  });
  expect(store.getState().state).toEqual({
    items: [
      { value: "item1", key: 0 },
      { value: "item2", key: 1 },
      { value: "item3", key: 2 }
    ],
    loading: false
  });
  store.dispatch({
    type: "UPDATE_MEDICAMENT",
    payload: { key: 1, value: "item2Updated" }
  });
  expect(store.getState().state).toEqual({
    items: {
      0: { value: "item1", key: 0 },
      1: { value: "item2Updated", key: 1 },
      2: { value: "item3", key: 2 }
    },
    loading: false
  });
  store.dispatch({
    type: "REMOVE_MEDICAMENT",
    payload: 1
  });
  expect(store.getState().state).toEqual({
    items: { 0: { value: "item1", key: 0 }, 2: { value: "item3", key: 2 } },
    loading: false
  });
});
