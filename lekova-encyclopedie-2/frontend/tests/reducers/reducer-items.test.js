import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-items.js";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for items in form saves state correctly", () => {
  expect(store.getState().state).toEqual({ nextKey: 0, keys: [], items: [] });
  store.dispatch({ type: "ADD_ITEM", payload: { value: "ITEM1" } });
  expect(store.getState().state).toEqual({
    nextKey: 1,
    keys: [0],
    items: [{ value: "ITEM1" }]
  });
  store.dispatch({ type: "ADD_ITEM", payload: { value: "ITEM2" } });
  expect(store.getState().state).toEqual({
    nextKey: 2,
    keys: [0, 1],
    items: [{ value: "ITEM1" }, { value: "ITEM2" }]
  });
  store.dispatch({ type: "EDIT_ITEM", payload: 1 });
  expect(store.getState().state).toEqual({
    nextKey: 2,
    keys: [0, 1],
    items: [{ value: "ITEM1" }, { edit: true, value: "ITEM2" }]
  });
  store.dispatch({ type: "EDIT_ITEM", payload: 0 });
  expect(store.getState().state).toEqual({
    nextKey: 2,
    keys: [0, 1],
    items: [{ value: "ITEM1", edit: true }, { edit: true, value: "ITEM2" }]
  });
  store.dispatch({ type: "STORNO_ITEM", payload: 0 });
  store.dispatch({
    type: "CONFIRM_ITEM",
    payload: { key: 1, medicament: { value: "ITEM2EDITED" } }
  });
  expect(store.getState().state).toEqual({
    nextKey: 2,
    keys: [0, 1],
    items: [
      { value: "ITEM1", edit: false },
      { edit: false, value: "ITEM2EDITED" }
    ]
  });
  store.dispatch({ type: "RESET_FORM" });
  expect(store.getState().state).toEqual({
    nextKey: 0,
    keys: [],
    items: []
  });
});
