import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-userHistory";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for documents saves state correctly", () => {
  expect(store.getState().state).toEqual({});
  store.dispatch({ type: "HISTORY_LOADING" });
  expect(store.getState().state).toEqual({
    loading: true,
    history: []
  });
  store.dispatch({ type: "GET_HISTORY", payload: ["A", "B", "C"] });
  expect(store.getState().state).toEqual({
    loading: false,
    history: ["A", "B", "C"]
  });
});
