import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-userFormsResponses.js";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for documents saves state correctly", () => {
  expect(store.getState().state).toEqual({ loading: false });
  store.dispatch({ type: "RESPONSE_LOADING" });
  expect(store.getState().state).toEqual({ loading: true });
  store.dispatch({ type: "CHANGE_PASSWORD_RESPONSE", payload: "CHANGED" });
  expect(store.getState().state).toEqual({
    loading: false,
    changePasswordResponse: "CHANGED"
  });
});
