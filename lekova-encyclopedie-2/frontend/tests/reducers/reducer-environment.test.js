import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-environment";
import thunk from "redux-thunk";
import { combineReducers } from "redux";

Object.defineProperty(window.navigator, "language", { value: "en" });

const store = createStore(
  combineReducers({ state: reducer }),
  applyMiddleware(thunk)
);

test("reducer for environment saves state correctly", () => {
  expect(store.getState().state).toEqual({
    language: "en",
    context: "enc:",
    showUserBox: false
  });
  store.dispatch({ type: "SET_LANGUAGE", payload: "cs" });
  expect(store.getState().state).toEqual({
    language: "cs",
    context: "enc:",
    showUserBox: false
  });
  store.dispatch({ type: "SHOW_USER_BOX", payload: true });
  expect(store.getState().state).toEqual({
    language: "cs",
    context: "enc:",
    showUserBox: true
  });
  store.dispatch({ type: "SHOW_USER_BOX", payload: false });
  expect(store.getState().state).toEqual({
    language: "cs",
    context: "enc:",
    showUserBox: false
  });
  store.dispatch({ type: "HIDE_SHOW_USER_BOX" });
  expect(store.getState().state).toEqual({
    language: "cs",
    context: "enc:",
    showUserBox: true
  });
  store.dispatch({ type: "HIDE_SHOW_USER_BOX" });
  expect(store.getState().state).toEqual({
    language: "cs",
    context: "enc:",
    showUserBox: false
  });
});
