import utils from "../../src/libs/utils.js";
import moment from "moment";

let now = 50 * 24 * 60 * 60 * 1000;
var MockDate = require("mockdate");
MockDate.set(now);

test("makePath works correctly", () => {
  expect(utils.makePath("ěščřžýáíé123456789/////")).toBe(
    "ěščřžýáíé123456789%2F%2F%2F%2F%2F"
  );
  expect(utils.makePath("https://www.facebook.com/")).toBe(
    "https%3A%2F%2Fwww.facebook.com%2F"
  );
});

test("getValueFromArrayPaths works correctly", () => {
  utils.getValueFromArrayPaths(
    {
      property1: "value1",
      property2: "value2",
      property3: { subproperty: "value3" }
    },
    "",
    "cs",
    [["property1"], ["property2"], ["property3", "subproperty"], ["property4"]]
  );
  expect(
    utils.getValueFromArrayPaths(
      {
        property1: "value1",
        property2: "value2",
        property3: { subproperty: "value3" }
      },
      "",
      "cs",
      [
        ["property1"],
        ["property2"],
        ["property3", "subproperty"],
        ["property4"]
      ]
    )
  ).toEqual({ lang: false, value: "value1 value2 value3" });
  expect(
    utils.getValueFromArrayPaths(
      {
        property1: { "@language": "cs", "@value": "value1" },
        property2: { "@language": "cs", "@value": "value2" },
        property3: { subproperty: { "@language": "cs", "@value": "value3" } }
      },
      "",
      "cs",
      [["property1"], ["property2"], ["property3", "subproperty"]]
    )
  ).toEqual({ lang: true, value: "value1 value2 value3" });
  expect(
    utils.getValueFromArrayPaths({}, "", "cs", [
      ["property1"],
      ["property2"],
      ["property3", "subproperty"]
    ])
  ).toEqual({ lang: true, value: "" });
});

test("validateEmail works correctly", () => {
  expect(utils.validateEmail("a@a.cz")).toBeTruthy();
  expect(utils.validateEmail("adas@aads.czs")).toBeTruthy();
  expect(utils.validateEmail("qwd.qa@adsdsa.sk")).toBeTruthy();
  expect(utils.validateEmail("a@a")).toBeFalsy();
  expect(utils.validateEmail("aa")).toBeFalsy();
  expect(utils.validateEmail("aa.cs")).toBeFalsy();
  expect(utils.validateEmail("@a.sk")).toBeFalsy();
});

test("createXPlacedNumber works correctly", () => {
  expect(utils.createXPlacedNumber(20, 5)).toBe("00020");
  expect(utils.createXPlacedNumber(10, 2)).toBe("10");
  expect(utils.createXPlacedNumber(20, 1)).toBe("20");
});

test("getValueFromPath works correctly", () => {
  expect(
    utils.getValueFromArrayPath(
      {
        property1: [
          { "@language": "en", "@value": "value1En" },
          { "@language": "cs", "@value": "value1Cs" }
        ]
      },
      "",
      "cs",
      ["property1"]
    )
  ).toEqual({ lang: true, value: "value1Cs" });
  expect(
    utils.getValueFromArrayPath(
      {
        property3: { subproperty: { "@language": "cs", "@value": "value3" } }
      },
      "",
      "cs",
      ["property3", "subproperty"]
    )
  ).toEqual({ lang: true, value: "value3" });
  expect(
    utils.getValueFromArrayPath(
      {
        property2: [{ "@language": "en", "@value": "value2En" }]
      },
      "",
      "cs",
      ["property2"]
    )
  ).toEqual({ lang: false, value: "value2En" });
  expect(
    utils.getValueFromArrayPath({}, "", "cs", ["property3", "subproperty"])
  ).toEqual({ lang: false, value: null });
});

test("getUrl works correctly", () => {
  expect(utils.getUrl("Ingredient", "id")).toBe(
    "/documents/le-ingredient-detail/id"
  );
});

test("makeLink works correctly", () => {
  expect(utils.makeLink("Ingredient", "id")).toBe("/Ingredient/id");
});

test("getLink works correctly", () => {
  expect(
    utils.getLink(
      {
        "@id": "id",
        contraindicatedWith: [{ "@type": "Ingredient" }]
      },
      "contraindicatedWith"
    )
  ).toBe("/Ingredient/id");
  expect(
    utils.getLink(
      {
        "@id": "id",
        contraindicatedWith: [{}]
      },
      "contraindicatedWith"
    )
  ).toBe("/Ingredient/id");
});

test("recalculateNextDose works correctly", () => {
  expect(
    utils.recalculateNextDose({
      nextDose: 49 * 24 * 60 * 60 * 1000,
      howOftenType: "days",
      howOften: 2
    })
  ).toEqual(new Date(51 * 24 * 60 * 60 * 1000));
  expect(
    utils.recalculateNextDose({
      nextDose: 51 * 24 * 60 * 60 * 1000,
      howOftenType: "days",
      howOften: 2
    })
  ).toEqual(new Date(51 * 24 * 60 * 60 * 1000));
  expect(
    utils.recalculateNextDose({
      nextDose: 50 * 24 * 60 * 60 * 1000 - 50,
      howOftenType: "hours",
      howOften: 2
    })
  ).toEqual(new Date(50 * 24 * 60 * 60 * 1000 + 2 * 60 * 60 * 1000 - 50));
});

test("repairUnicode works correctly", () => {
  expect(
    utils.repairUnicode(
      "Jeden z nejtypi\u010Dt\u011Bj\u0161\u00EDch p\u0159\u00EDznak\u016F onemocn\u011Bn\u00ED"
    )
  ).toBe("Jeden z nejtypičtějších příznaků onemocnění");
});

test("getValue works correctly", () => {
  expect(
    utils.getValue(
      {
        "enc:property1": { "@language": "cs", "@value": "value1" },
        "enc:property2": { "@language": "cs", "@value": "value2" },
        "enc:property3": {
          subproperty: { "@language": "cs", "@value": "value3" }
        },
        "enc:property4": [
          { "@language": "cs", "@value": "value4Cs" },
          { "@language": "en", "@value": "value4En" }
        ],
        property5: "value5"
      },
      "enc:",
      "cs",
      "property4"
    )
  ).toEqual({ lang: true, value: "value4Cs" });
  expect(
    utils.getValue(
      {
        "enc:property1": { "@language": "cs", "@value": "value1" },
        "enc:property2": { "@language": "cs", "@value": "value2" },
        "enc:property3": {
          subproperty: { "@language": "cs", "@value": "value3" }
        },
        "enc:property4": [
          { "@language": "cs", "@value": "value4Cs" },
          { "@language": "en", "@value": "value4En" }
        ],
        "enc:property5": "value5"
      },
      "enc:",
      "en",
      "property4"
    )
  ).toEqual({ lang: true, value: "value4En" });
  expect(
    utils.getValue(
      {
        "enc:property1": { "@language": "cs", "@value": "value1" },
        "enc:property2": { "@language": "cs", "@value": "value2" },
        "enc:property3": {
          subproperty: { "@language": "cs", "@value": "value3" }
        },
        "enc:property4": [
          { "@language": "cs", "@value": "value4Cs" },
          { "@language": "en", "@value": "value4En" }
        ],
        "enc:property5": "value5"
      },
      "enc:",
      "cs",
      "property5"
    )
  ).toEqual({ lang: false, value: "value5" });
  expect(
    utils.getValue(
      {
        "enc:property1": { "@language": "cs", "@value": "value1" },
        "enc:property2": { "@language": "cs", "@value": "value2" },
        "enc:property3": {
          subproperty: { "@language": "cs", "@value": "value3" }
        },
        "enc:property4": [{ "@language": "en", "@value": "value4En" }],
        "enc:property5": "value5"
      },
      "enc:",
      "cs",
      "property4"
    )
  ).toEqual({ lang: false, value: "value4En" });
  expect(
    utils.getValue(
      {
        "enc:property1": { "@value": "value1" },
        "enc:property2": { "@language": "cs", "@value": "value2" },
        "enc:property3": {
          subproperty: { "@language": "cs", "@value": "value3" }
        },
        "enc:property4": [{ "@language": "en", "@value": "value4En" }],
        "enc:property5": "value5"
      },
      "enc:",
      "cs",
      "property1"
    )
  ).toEqual({ lang: false, value: "value1" });
});

test("isDateValid works correctly", () => {
  expect(utils.isDateValid(15, 10, 2016)).toBeTruthy();
  expect(utils.isDateValid(28, 2, 2013)).toBeTruthy();
  expect(utils.isDateValid(31, 1, 1974)).toBeTruthy();

  expect(utils.isDateValid(0, 4, 1994)).toBeFalsy();
  expect(utils.isDateValid(0, "a", 1994)).toBeFalsy();
  expect(utils.isDateValid(31, 4, 1994)).toBeFalsy();

  expect(utils.isDateValid(29, 2, 2004)).toBeTruthy();
  expect(utils.isDateValid(29, 2, 2004)).toBeTruthy();
  expect(utils.isDateValid(29, 2, 1997)).toBeFalsy();
  expect(utils.isDateValid(29, 2, 2100)).toBeFalsy();
});

test("getCookie works correctly", () => {
  expect(utils.getCookie("username")).toEqual(null);
  Object.defineProperty(window.document, "cookie", {
    writable: true,
    value: "username=petr"
  });
  expect(utils.getCookie("username")).toEqual("petr");
});

test("getLanguage works correctly when cookie is not present", () => {
  Object.defineProperty(window.navigator, "language", { value: "en" });
  expect(utils.getLanguage()).toEqual("en");
});

test("getLanguage works correctly when cookie is present", () => {
  Object.defineProperty(window.document, "cookie", {
    writable: true,
    value: "language=cs"
  });
  expect(utils.getLanguage()).toEqual("cs");
});
