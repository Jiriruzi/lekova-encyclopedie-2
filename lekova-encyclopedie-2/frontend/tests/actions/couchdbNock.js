import nock from "nock";

export const medicinalProduct = {
  "@id": "validId",
  "@type": "enc:MedicinalProduct",
  "enc:hasActiveIngredient": [
    {
      "@type": "enc:Ingredient",
      "@id": "validId2"
    },
    {
      "@type": "enc:Ingredient",
      "@id": "validId1"
    }
  ]
};
export const ingredient1 = {
  "@type": "enc:Ingredient",
  "@id": "validId1",
  "enc:title": "ing1",
  "enc:hasInteractionWith": {
    "@type": "enc:Ingredient",
    "@id": "validId2",
    "enc:title": "ing2"
  }
};
export const ingredient2 = {
  "@type": "enc:Ingredient",
  "@id": "validId2",
  "enc:title": "ing2"
};
export const ingredient1WithInter = {
  ...ingredient1,
  "enc:hasInteractionWith": ingredient2
};
export const ingredient2WithInter = {
  ...ingredient2,
  "enc:hasInteractionWith": ingredient1
};

export default function couchdbMock() {
  let scope = nock("http://localhost:5900");

  scope
    .get("/documents/le-medicinal-product-detail/validId")
    .reply(200, medicinalProduct)
    .persist();

  scope
    .get("/documents/le-medicinal-product-detail/invalidId")
    .reply(404)
    .persist();

  scope
    .get("/documents/le-ingredient-detail/invalidId")
    .reply(404, medicinalProduct)
    .persist();
  scope
    .get("/documents/le-ingredient-detail/validId1")
    .reply(200, ingredient1)
    .persist();
  scope
    .get("/documents/le-ingredient-detail/validId2")
    .reply(200, ingredient2)
    .persist();
  scope.post("/user/register", { username: "valid" }).reply(200);
  scope.post("/user/register", { username: "existing" }).reply(409);
  scope.post("/user/register").reply(400, { username: "invalid" });
  scope.delete("/user/remove_medicament", { id: "valid" }).reply(200);
  scope.delete("/user/remove_medicament", { id: "invalid" }).reply(400);
  scope.post("/user/update_history").reply(200);
  scope.post("/user/get_session").reply(200);
  scope.post("/user/edit_user").reply(200);
  scope.post("/user/send_password_link").reply(200);
  scope.post("/user/login").reply(200);
  scope.post("/user/get_medication").reply(200);
  scope.post("/user/update_medicament").reply(200);
  scope.post("/user/get_history").reply(200);
  scope.post("/user/change_password").reply(200);
  scope.post("/user/reset_password").reply(200);
  scope.post("/user/logout").reply(200);
  scope.post("/user/update_medication").reply(200);

  return scope;
}
