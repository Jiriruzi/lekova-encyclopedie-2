import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/formActions";
import configureStore from "redux-mock-store";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);

beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

test("action addItem works well", () => {
  store.dispatch(actions.addItem({ value: "Item" }));
  expect(store.getState()).toEqual([actions.addItem({ value: "Item" })]);
});

test("action removeItem works well", () => {
  store.dispatch(actions.removeItem(0));
  expect(store.getState()).toEqual([actions.removeItem(0)]);
});

test("action stornoItem works well", () => {
  store.dispatch(actions.stornoItem(0));
  expect(store.getState()).toEqual([actions.stornoItem(0)]);
});
test("action editItem works well", () => {
  store.dispatch(actions.editItem(0));
  expect(store.getState()).toEqual([actions.editItem(0)]);
});
test("action confirmItem works well", () => {
  store.dispatch(actions.confirmItem(0, { value: "ItemConfirmed" }));
  expect(store.getState()).toEqual([
    actions.confirmItem(0, { value: "ItemConfirmed" })
  ]);
});

test("action resetForm works well", () => {
  store.dispatch(actions.resetForm());
  expect(store.getState()).toEqual([actions.resetForm()]);
});
