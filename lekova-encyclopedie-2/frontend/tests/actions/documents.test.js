import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/documentsFetches";
import couchdbMock, {
  medicinalProduct,
  ingredient1,
  ingredient2,
  ingredient1WithInter,
  ingredient2WithInter
} from "./couchdbNock";
import configureStore from "redux-mock-store";
import fetch from "isomorphic-fetch";

global.fetch = (url, conf) => fetch("http://localhost:5900" + url, conf);

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);
let scope;
beforeAll(() => {
  scope = couchdbMock();
});
beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

test("action getDocument works well when valid id and type are received", done => {
  store
    .dispatch(actions.getDocument("MedicinalProduct", "validId"))
    .then(() => {
      expect(store.getState()).toEqual([
        actions.documentLoadingAction("MedicinalProduct"),
        actions.documentFoundAction("MedicinalProduct", medicinalProduct)
      ]);
      done();
    });
});

test("action getDocument works well when invalid id is received", done => {
  let errorThrowed = false;
  store
    .dispatch(actions.getDocument("MedicinalProduct", "invalidId"))
    .then(function(result) {})
    .catch(function(e) {
      errorThrowed = true;
    })
    .then(() => {
      expect(store.getState()).toEqual([
        actions.documentLoadingAction("MedicinalProduct"),
        actions.documentNotFoundAction("MedicinalProduct")
      ]);
      done();
    });
});

test("action makeInteractions works well", () => {
  store = mockStore({
    interactions: {
      items: [medicinalProduct],
      loading: false,
      error: false,
      ingredients: [ingredient1WithInter, ingredient2WithInter]
    }
  });
  store.dispatch(actions.makeInteractions("enc:"));
  expect(store.getState()).toEqual({
    interactions: {
      error: false,
      items: [medicinalProduct],
      ingredients: [ingredient1WithInter, ingredient2WithInter],
      loading: false
    }
  });
});
test("action addInteractionItem - add medicinal product works well when all data are valid", done => {
  store
    .dispatch(
      actions.addInteractionItem(
        { _type: "medicinalProduct", _id: "validId" },
        "enc:"
      )
    )
    .then(() => {
      expect(store.getState()).toEqual([
        actions.interactionsLoadingAction(),
        actions.addInteractionItemAction(
          [
            [
              { ...ingredient2, medicinalProduct },
              { ...ingredient1, medicinalProduct }
            ]
          ],
          "enc:"
        )
      ]);
      done();
    });
});

test("action addInteractionItem - add ingredient works well when all data are valid", done => {
  store
    .dispatch(
      actions.addInteractionItem(
        { _type: "ingredient", _id: "validId1" },
        "enc:"
      )
    )
    .then(() => {
      expect(store.getState()).toEqual([
        { type: "INTERACTIONS_LOADING" },
        {
          payload: [
            {
              "@id": "validId1",
              "@type": "enc:Ingredient",
              "enc:hasInteractionWith": {
                "@id": "validId2",
                "@type": "enc:Ingredient",
                "enc:title": "ing2"
              },
              "enc:title": "ing1"
            }
          ],
          type: "ADD_INTERACTION_ITEM"
        }
      ]);
      done();
    });
});

test("action addInteractionItem throws error when some data are broken", done => {
  let errorThrowed = false;
  store
    .dispatch(
      actions.addInteractionItem(
        { _type: "ingredient", _id: "invalidId" },
        "enc:"
      )
    )
    .then(function(result) {})
    .catch(function(e) {
    })
    .then(() => {
      expect(store.getState()).toEqual([
        actions.interactionsLoadingAction(),
        actions.interactionsFailedAction()
      ]);
      done();
    });
});

test("action removeInteractionItem works well", () => {
  store.dispatch(actions.removeInteractionItem(1));
  expect(store.getState()).toEqual([
    actions.interactionsLoadingAction(),
    actions.removeInteractionItemAction(1)
  ]);
});
