import { createStore, applyMiddleware } from "redux";
import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/environmentActions";
import configureStore from "redux-mock-store";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);
let scope;

beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

test("action selectLanguage works fires action and changes cookies", () => {
  Object.defineProperty(window.document, "cookie", {
    writable: true,
    value: "language=cs"
  });
  store.dispatch(actions.selectLanguage("en"));
  expect(store.getState()).toEqual([{ type: "SET_LANGUAGE", payload: "en" }]);
  expect(document.cookie).toEqual("language=en");
});

test("action resetResponses works correctly", () => {
  store.dispatch(actions.resetResponses());
  expect(store.getState()).toEqual([{ type: "RESET_RESPONSES" }]);
});

test("action hideUserBox works correctly", () => {
  store.dispatch(actions.hideUserBox());
  expect(store.getState()).toEqual([{ type: "SHOW_USER_BOX", payload: false }]);
});

test("action showUserBox works correctly when on desktop", () => {
  store.dispatch(actions.showUserBox());
  expect(store.getState()).toEqual([{ type: "SHOW_USER_BOX", payload: true }]);
});
test("action hideShowUserBox works correctly on pc", () => {
  store.dispatch(actions.hideShowUserBox());
  expect(store.getState()).toEqual([{ type: "HIDE_SHOW_USER_BOX" }]);
});

test("action hideShowUserBox works correctly on other devices", () => {
  navigator.__defineGetter__("userAgent", function() {
    return "Android"; // customized user agent
  });
  store.dispatch(actions.showUserBox());
  expect(store.getState()).toEqual([{ type: "DO_NOTHING" }]);
});
