import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/userFetches";
import configureStore from "redux-mock-store";
import fetch from "isomorphic-fetch";
import couchdbMock from "./couchdbNock.js";

global.fetch = (url, conf) => fetch("http://localhost:5900" + url, conf);

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);
let scope;

beforeAll(() => {
  scope = couchdbMock();
});
beforeEach(() => {
  store.clearActions();
});

test("action addUser works well with ok response", done => {
  store.dispatch(actions.addUser({ username: "valid" }, () => {})).then(() => {
    expect(store.getState()).toEqual([
      { type: "RESPONSE_LOADING" },
      { type: "REGISTRATION_RESPONSE", payload: "Registered" }
    ]);
    done();
  });
});

test("action addUser works well when user already exists", done => {
  store
    .dispatch(actions.addUser({ username: "existing" }, () => {}))
    .then(() => {
      expect(store.getState()).toEqual([
        { type: "RESPONSE_LOADING" },
        { type: "REGISTRATION_RESPONSE", payload: "ExistingUser" }
      ]);
      done();
    });
});

test("action addUser works well when user already exists", done => {
  store
    .dispatch(actions.addUser({ username: "invalid" }, () => {}))
    .then(() => {
      expect(store.getState()).toEqual([
        { type: "RESPONSE_LOADING" },
        { type: "REGISTRATION_RESPONSE", payload: "SomethingWrongRegistration" }
      ]);
      done();
    });
});

test("action removeMedicament works well with ok response", done => {
  let posted = false;
  window.postMessage = (arg1, arg2) => {
    posted = true;
  };
  store.dispatch(actions.removeMedicament({ id: "valid" }, 1)).then(() => {
    expect(posted).toBeTruthy();
    done();
  });
});

test("action removeMedicament works well with not ok response", done => {
  let reload = false;
  let alerted = false;
  location.reload = () => {
    reload = true;
  };
  global.alert = () => {
    alerted=true;
  }
  store.dispatch(actions.removeMedicament({ id: "invalid" }, 2)).then(() => {
    expect(reload).toBeTruthy();
    expect(alerted).toBeTruthy();
    done();
  });
});

test("action updateHistory works well with ok response", done => {
  let alert = false;
  global.alert = () => {
    alert=true;
  }
  store
    .dispatch(actions.updateHistory("type", "validId", "title", 2))
    .then(() => {
      expect(alert).toBeFalsy();
      done();
    });
});
