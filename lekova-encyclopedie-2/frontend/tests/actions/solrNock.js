import nock from "nock";

export const solrResponse = {
  responseHeader: {
    status: 0,
    QTime: 96,
    params: {
      q: "val",
      indent: "on",
      json: "",
      wt: "json"
    }
  },
  response: {
    numFound: 2464,
    start: 0,
    docs: [
      {
        _type: "diseaseOrFinding",
        enc_title: "Sepse ",
        _id: "http://linked.opendata.cz/resource/ndfrt/disease/N0000002716",
        _version_: 1616108252252078081
      },
      {
        _type: "diseaseOrFinding",
        enc_title: "Sepse ",
        _id: "http://linked.opendata.cz/resource/ndfrt/disease/N0000003861",
        _version_: 1616108252366372870
      },
      {
        _type: "ingredient",
        enc_title: "Seganserin ",
        _id:
          "http://linked.opendata.cz/resource/drug-encyclopedia/ingredient/M0134236",
        _version_: 1616108254187749387
      }
    ]
  }
};

export default function solrMock() {
  let scope = nock("http://localhost:5900")
    .filteringPath(function(path) {
      return "/";
    })
    .get("/")
    .reply(200, solrResponse).persist();
  return scope;
}
