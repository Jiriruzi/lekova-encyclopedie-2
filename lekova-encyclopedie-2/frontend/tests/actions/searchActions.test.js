import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/searchActions";
import configureStore from "redux-mock-store";
import solrMock, { solrResponse } from "./solrNock";
import fetch from "isomorphic-fetch";

global.fetch = (url, conf) => fetch("http://localhost:5900" + url, conf);

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);
let scope;

beforeAll(() => {
  scope = solrMock();
});
beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

test("action getAllResults works well", done => {
  store.dispatch(actions.getAllResults("se", "cs")).then(() => {
    expect(store.getState()).toEqual([
      { type: "RESET_ALL_RESULTS" },
      { type: "ALL_RESULTS_LOADING" },
      { type: "ALL_RESULTS", payload: solrResponse.response.docs }
    ]);
    done();
  });
});

test("action suggest works well", done => {
  store.dispatch(actions.suggest("se", "cs", () => "someQuery")).then(() => {
    setTimeout(() => {
      expect(store.getState()).toEqual([
        {
          payload: "se",
          type: "SUGGEST_LOADING"
        },
        {
          payload: {
            suggestions: solrResponse.response.docs,
            value: "se"
          },
          type: "SUGGEST"
        },
        {
          type: "SUGGEST_LONG_LOADING",
          payload: "se"
        }
      ]);
      done();
    }, 1000);
  });
});
test("action suggest works well - suggests only when user is not typing for a while", done => {
  store
    .dispatch(actions.suggest("se", "cs", () => "someQuery"))
    .then(() => store.dispatch(actions.suggest("se", "cs", () => "someQuery")))
    .then(() => {
      expect(store.getState()).toEqual([
        { payload: "se", type: "SUGGEST_LOADING" },
        {
          payload: {
            suggestions: solrResponse.response.docs,
            value: "se"
          },
          type: "SUGGEST"
        },
        { payload: "se", type: "SUGGEST_LOADING" },
        {
          payload: {
            suggestions: solrResponse.response.docs,
            value: "se"
          },
          type: "SUGGEST"
        }
      ]);
      done();
    });
});

test("action setSearchValueHeader works well", () => {
  store.dispatch(actions.setSearchValueHeader("val"));
  expect(store.getState()).toEqual([actions.setSearchValueHeader("val")]);
});

test("action setSearchValueInForm works well", () => {
  store.dispatch(actions.setSearchValueInForm("val"));
  expect(store.getState()).toEqual([actions.setSearchValueInForm("val")]);
});

test("action resetResults works well", () => {
  store.dispatch(actions.resetResults());
  expect(store.getState()).toEqual([actions.resetResults()]);
});

test("action resetResults works well", () => {
  store.dispatch(actions.resetSearchValueInForm());
  expect(store.getState()).toEqual([actions.resetSearchValueInForm()]);
});
