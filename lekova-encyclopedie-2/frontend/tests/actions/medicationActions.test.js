import reducer from "../../src/reducers/reducer-documents.js";
import thunk from "redux-thunk";
import * as actions from "../../src/actions/medicationActions";
import configureStore from "redux-mock-store";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);
let store = mockStore(reducer);

let now = 1542754799000;
var MockDate = require("mockdate");
MockDate.set(now);

beforeEach(() => {
  store = mockStore(reducer);
  store.clearActions();
});

test("action removeMedicamentFront works well", () => {
  store.dispatch(actions.removeMedicamentFront(0));
  expect(store.getState()).toEqual([actions.removeMedicamentFront(0)]);
});

test("action editMedicament works well", () => {
  store.dispatch(actions.editMedicament(0));
  expect(store.getState()).toEqual([actions.editMedicament(0)]);
});
test("action stornoMedicament works well", () => {
  store.dispatch(actions.stornoMedicament(0));
  expect(store.getState()).toEqual([actions.stornoMedicament(0)]);
});
test("action confirmMedicament works well", () => {
  store.dispatch(actions.confirmMedicament(0, { value: "ItemConfirmed" }));
  expect(store.getState()).toEqual([
    actions.confirmMedicament(0, { value: "ItemConfirmed" })
  ]);
});

test("action updateMedicamentFront works well", () => {
  store.dispatch(
    actions.updateMedicamentFront({
      value: "ItemToUpdate",
      nextDose: Date.now() - 1000
    })
  );
  expect(store.getState()).toEqual([
    actions.updateMedicamentFront({
      value: "ItemToUpdate",
      nextDose: Date.now() - 1000
    })
  ]);
});
