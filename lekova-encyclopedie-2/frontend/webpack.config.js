const webpack = require("webpack");
const path = require("path");
const CompressionPlugin = require("compression-webpack-plugin");
const helpers = require("helpers");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  resolve: {
    alias: {
      Actions: path.resolve(__dirname, "src/actions"),
      Components: path.resolve(__dirname, "src/components"),
      Libs: path.resolve(__dirname, "src/libs"),
      Images: path.resolve(__dirname, "images"),
      Modules: path.resolve(__dirname, "node_modules")
    }
  }
};
