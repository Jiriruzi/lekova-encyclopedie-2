// class that stores functions for handling problems with database
class utils {
  // replaces '/'' for %2F and ':' for '%3A'
  static makePath(text) {
    text = this.repairUnicode(text);
    text = text.replace(/\//g, "%2F");
    text = text.replace(":", "%3A");
    return text;
  }
  static getValueFromArrayPaths(datDoc, context, language, arrays) {
    let lang = true;
    let value = "";
    let used = false;
    if (arrays && !(arrays[0] instanceof Array)) {
      return (
        utils.getValueFromArrayPath(datDoc, context, language, arrays[0]) ||
        utils.getTitleIfMissing(datDoc, arrays[0], context)
      );
    }
    for (let i = 0; i < arrays.length; i++) {
      let add = utils.getValueFromArrayPath(
        datDoc,
        context,
        language,
        arrays[i]
      );
      if (!add.value) {
        add = {
          value: !used
            ? utils.getTitleIfMissing(
                datDoc[context + arrays[i][0]],
                arrays[i][1],
                context
              ) || utils.getTitleIfMissing(datDoc, arrays[i][0], context)
            : null,
          lang: true
        };
        used = true;
      }
      if (!add.lang) {
        lang = false;
      }
      if (value === "" && add.value && add.value !== "undefined") {
        value = `${add.value}`;
      } else if (add.value && add.value !== "undefined") {
        value = `${value} ${add.value}`;
      }
    }
    if (!value) {
      const tmp = this.getTitleIfMissing(datDoc, arrays[0], context);
      if (tmp) {
        value = tmp;
      }
    }
    return {
      value,
      lang
    };
  }
  static getTitleIfMissing(doc, prop, context) {
    if (!doc) {
      return null;
    }
    if (doc["@id"] && doc["@id"].includes("atc")) {
      prop = "hasATCConcept";
    }
    const document = doc[context + prop] || doc;
    if (!doc["@id"]) {
      return null;
    }
    switch (prop) {
      case "hasATCConcept":
        return document["@id"].replace(/^.*[\\\/]/, "");
      case "hasPregnancyCategory":
      case "hasUnit":
        return document["@id"].replace(/^.*[\\\/]/, "");
      case "hasInteractionWith":
        return document["@id"].replace(/^.*[\\\/]/, "");
    }
    return null;
  }
  static getValueFromArrayPath(doc, context, language, array) {
    for (let i = 0; i + 1 < array.length; i++) {
      if (doc[context + array[i]] !== undefined) {
        doc = doc[context + array[i]];
      } else if (doc[array[i]] !== undefined) {
        doc = doc[array[i]];
      } else {
        return {
          value: null,
          lang: false
        };
      }
    }
    if (array.length > 0 && array[array.length - 1].includes(":")) {
      context = "";
    }
    if (doc[0]) {
      [doc] = doc;
    }
    let value = this.getValue(doc, context, language, array[array.length - 1]);
    if (value.value === true || value.value === "true") {
      value = { ...value, value: language === "cs" ? "Ano" : "Yes" };
    }
    if (value.value === false || value.value === "false") {
      value = { ...value, value: language === "cs" ? "Ne" : "No" };
    }
    return value;
  }
  // returns name of the databases depending on the type
  static getDatabase(type) {
    switch (type) {
      case "Ingredient":
        return "/le-ingredient-detail";
      case "Finding":
        return "/le-finding-detail";
      case "Configuration":
      case "PackTable":
      case "Vocabulary":
        return "/le-configuration";
      case "ActiveIngredient":
        return "/le-ingredient-detail";
      case "MedicinalProduct":
        return "/le-medicinal-product-detail";
      case "MedicinalProductPackaging":
        return "/le-medicinal-product-packaging-detail";
      case "PIL":
        return "/le-medicinal-product-packaging-pil";
      case "Price":
        return "/le-medicinal-product-packaging-price";
      case "DiseaseOrFinding":
        return "/le-disease-or-finding-detail";
      case "PhysiologicEffect":
        return "/le-physiologic-effect-detail";
      case "Pharmacokinetics":
        return "/le-pharmacokinetics-detail";
      case "PharmacologicalAction":
        return "/le-pharmacological-action-detail";
      case "MechanismOfAction":
        return "/le-mechanism-of-action-detail";
      case "ATCConcept":
        return "/le-atc-concept-detail";
    }
    return null;
  }
  static getTypeFromProp(prop) {
    switch (prop) {
      case "hasATCConcept":
        return "ATCConcept";
      case "broaderTransitive":
        return "ATCConcept";
      case "narrowerTransitive":
        return "ATCConcept";
      case "mayPrevent":
        return "Ingredient";
      case "mayTreat":
        return "Ingredient";
      case "contraindicatedWith":
        return "Ingredient";
      case "hasMechanismOfAction":
        return "MechanismOfAction";
      case "hasPharmacologicalAction":
        return "PharmacologicalAction";
      case "hasPharmacokinetics":
        return "Pharmacokinetics";
      case "hasPhysiologicEffect":
        return "PhysiologicEffect";
      case "hasActiveIngredient":
        return "Ingredient";
      case "hasMedicinalProduct":
        return "MedicinalProduct";
      case "hasMedicinalProductPackaging":
        return "MedicinalProductPackaging";
      case "interactionWith":
        return "Ingredient";
    }
    return null;
  }
  // if the type in  the url is ok
  static rightType(type) {
    return this.getDatabase(type) != null;
  }
  // return rhe url for specific type and id
  static getUrl(type, id) {
    if (!type || !id) return null;
    id = `/${this.makePath(id)}`;
    const database = this.getDatabase(
      type.charAt(0).toUpperCase() + type.slice(1)
    );
    if (database === null) {
      return null;
    }
    const theUrl = `/documents${database}${id}`;
    return theUrl;
  }
  static makeLink(type, id) {
    return `/${type.charAt(0).toUpperCase() + type.slice(1)}/${this.makePath(
      id
    )}`;
  }
  // returns link of some doc, that can be used in url
  static inBussines(doc, context, prop) {
    switch (prop) {
      case "hasMedicinalProductPackaging":
        return this.getValue(doc, context, null, "deliveredInLastThreeMonths")
          .value;
      default:
        return true;
    }
  }
  static getLink(doc, prop) {
    if (!doc) {
      return null;
    }
    const id = doc["@id"];
    let type = "";
    if (doc["@type"] === undefined) {
      type = this.getTypeFromProp(prop);
    } else {
      for (let i = 0; i < doc["@type"].length; i++) {
        if (doc["@type"][i] === ":") {
          type = doc["@type"].substring(i + 1, doc["@type"].length);
          break;
        }
      }
    }

    return type ? this.makeLink(type, id) : "/";
  }
  static getCookie(cname) {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(";");
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === " ") {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }
  static getLanguage() {
    const lang =
      this.getCookie("language") ||
      navigator.language ||
      navigator.userLanguage;
    if ("en".localeCompare(lang) === 0) {
      return "en";
    }
    return "cs";
  }
  static recalculateNextDose(item) {
    let nextDose = null;
    if (item.nextDose) {
      ({ nextDose } = item);
      if (item.howOften && item.howOftenType) {
        nextDose = new Date(nextDose).getTime();
        const nowTime = new Date(Date.now()).getTime();
        if (nextDose < nowTime) {
          const diff = nowTime - nextDose;
          let howOftenType = 1;
          if (item.howOftenType === "weeks") {
            howOftenType = 7 * 24;
          } else if (item.howOftenType === "days") {
            howOftenType = 24;
          } else if (item.howOftenType === "months") {
            howOftenType = 30 * 24;
          }

          const howOften = item.howOften * howOftenType * 60 * 60 * 1000;
          const rest = diff % howOften;
          nextDose = nowTime + (rest > 0 ? howOften - rest : 0);
        }
      }
      nextDose = new Date(nextDose);
    }
    return nextDose;
  }
  // returns text encoded with url encoded characters
  static repairUnicode(text) {
    const r = /\\u([\d\w]{4})/gi;
    if (typeof text === "string" || text instanceof String) {
      text = text.replace(r, (match, grp) =>
        String.fromCharCode(parseInt(grp, 16))
      );
    } else {
      text = unescape(text);
    }
    if (
      text instanceof String &&
      text.length > 1 &&
      (text[0] === "\n" || text[0] === "\r" || text[0] === "\r\n")
    ) {
      text = text.substring(2);
    }
    text = text.trim();
    return text.replace(/(?:\r\n\r\n|\r\r|\n\n)/g, "\n");
  }

  // returns array from document
  static getArray(doc, context, property) {
    if (doc) {
      return doc[context + property];
    }
    return null;
  }
  // returns requested value from a document
  static getValue(doc, context, language, property) {
    if (doc === undefined) {
      return {
        value: null,
        lang: false
      };
    } else if (doc[context + property] === undefined || Object.keys(doc[context + property]).length === 0 && doc[context + property].constructor === Object
    ) {
      return doc["@value"]
        ? {
            value: this.repairUnicode(doc["@value"]),
            lang: false
          }
        : {
            value: null,
            lang: false
          };
    }
    if (doc[context + property]["@language"] !== undefined) {
      return {
        value: this.repairUnicode(doc[context + property]["@value"]),
        lang: doc[context + property]["@language"] === language
      };
    } else if (doc[context + property][0] !== undefined) {
      for (let i = 0; doc[context + property][i] !== undefined; i++) {
        if (doc[context + property][i]["@language"] === language) {
          return {
            value: this.repairUnicode(doc[context + property][i]["@value"]),
            lang: true
          };
        }
      }
    }
    if (doc[context + property]["@value"] !== undefined) {
      const tmp = doc[context + property]["@language"];
      return {
        value: this.repairUnicode(doc[context + property]["@value"]),
        lang: (tmp && tmp === language) || false
      };
    }
    if (doc[context + property] !== undefined) {
      const tmp = doc[context + property]["@language"];
      if (doc[context + property][0] && doc[context + property][0]["@value"]) {
        return {
          value: this.repairUnicode(doc[context + property][0]["@value"]),
          lang: tmp === language || false
        };
      }
      let val =
        doc[context + property] instanceof Array
          ? doc[context + property][0]
          : doc[context + property];
      if (
        val instanceof String &&
        val.includes("ttp") &&
        doc[context + property] instanceof Array &&
        doc[context + property][1]
      ) {
        [, val] = doc[context + property];
      }
      return {
        value: this.repairUnicode(val),
        lang: (tmp && tmp === language) || false
      };
    }
    return {
      value: null,
      lang: false
    };
  }
  static validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  static createXPlacedNumber(number, places) {
    let numStr = `${number}`;
    for (let i = 0; i + numStr.length < places; ) {
      numStr = `0${numStr}`;
    }
    return numStr;
  }
  static isDateValid(day, month, year) {
    day = Number(day);
    month = Number(month);
    year = Number(year);
    if (day === 0) {
      return false;
    }
    switch (month) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12:
        if (day > 31) {
          return false;
        }
        return true;
      case 2:
        if (year % 4 === 0) {
          if (year % 400 === 0) {
            return true;
          } else if (year % 100 === 0) {
            return false;
          }
          if (day > 29) {
            return false;
          }

          return true;
        }
        if (day > 28) {
          return false;
        }
        return true;
      case 4:
      case 6:
      case 9:
      case 11:
        if (day > 30) {
          return false;
        }
        return true;
      default:
        return false;
    }
  }
}

export default utils;
