import React from "react";

export const Vocabulary = {
  Ingredient: {
    en: "Active ingredients",
    cs: "Aktivní látky"
  },
  IngredientOne: {
    en: "Active ingredient",
    cs: "Aktivní látka"
  },
  ActiveIngredient: {
    en: "Active ingredients",
    cs: "Aktivní látky"
  },
  MedicinalProduct: {
    en: "Medicinal products",
    cs: "Léčivé přípravky"
  },
  MedicinalProductOne: {
    en: "Medicinal product",
    cs: "Léčivý přípravek"
  },
  MedicinalProductPackaging: {
    en: "Medicinal product packagings",
    cs: "Balení léčivého přípravku"
  },
  Title: {
    en: "Title",
    cs: "Název"
  },
  Contraindication: {
    en: "Contraindication",
    cs: "Kontraindikace"
  },
  Description: {
    en: "Description",
    cs: "Popis"
  },
  MechanismOfAction: {
    en: "Mechanisms of action",
    cs: "Mechanismy účinku"
  },
  None: {
    en: "None",
    cs: "Žádné"
  },
  Pharmacokinetics: {
    en: "Pharmacokinetics",
    cs: "Farmakokinetika"
  },
  PharmacologicalAction: {
    en: "Pharmacological actions",
    cs: "Farmakologické účinky"
  },
  PhysiologicEffect: {
    en: "Physiologic effects",
    cs: "Fyziologické účinky"
  },
  PregnancyCategory: {
    en: "Pregnancy categories",
    cs: "Použití v těhotenství"
  },
  Treatment: {
    en: "May treat",
    cs: "Může léčit"
  },
  MayBeTreated: {
    en: "May be treated",
    cs: "Může být léčeno"
  },
  Cheapest: {
    en: "Cheap alternatives",
    cs: "Levné alternativy"
  },
  CheapestAtc: {
    en: "Cheap packages",
    cs: "Levná balení"
  },
  CheapestDescAtc: {
    en:
      "Shows medicinal product packagings with this ATC concept, which have lowest additional payment counted on one usual or recommended daily dosage.",
    cs:
      "Zobrazuje balení léčivých prípravků s touto ATC skupinou, které mají nejnižší doplatek přepočítaný na jednu obvyklou, či doporučenou denní dávku obsaženou v balení."
  },
  CheapestDesc: {
    en:
      "Shows cheapest alternatives (medicinal product packagings) with this ATC concept, which have lowest additional payment counted on one usual or recommended daily dosage.",
    cs:
      "Zobrazuje návrhy alternativních léčiv (balení léčivých prípravků), které mají stejnou ATC skupinu a mají nejnižší doplatek přepočítaný na jednu obvyklou, či doporučenou denní dávku obsaženou v balení."
  },
  Indication: {
    en: "Indication",
    cs: "Indikace"
  },
  PackagingSize: {
    en: "Packaging size",
    cs: "Velikost balení"
  },
  RouteOfAdministration: {
    en: "Rout of administration",
    cs: "Podání"
  },
  Strength: {
    en: "Strength",
    cs: "Síla"
  },
  TitleSupplement: {
    en: "Title supplement",
    cs: "Doplněk názvu"
  },
  ATCConcept: {
    en: "ATC concepts",
    cs: "ATC koncepty"
  }, ATCConceptDetail: {
    en: "ATC concepts",
    cs: "ATC koncepty"
  },
  IndicationGroup: {
    en: "Indication group",
    cs: "Indikační skupina"
  },
  SPC: {
    en: "SPC",
    cs: "SPC"
  },
  SPCDesc: {
    en:
      "Document SPC is summary about medicinal product, that server to medics and medical expert as key source of information about the medicinal product",
    cs:
      "Dokument SPC je souhrn údajů o přípravku, který slouží lékařům a zdravotnickým odborníkům jako klíčový zdroj informací o léčivém přípravku."
  },
  PIL: {
    en: "Package leaflet",
    cs: "Příbalový leták"
  },
  Notation: {
    en: "Notation",
    cs: "Notace"
  },
  Price: {
    en: "Price",
    cs: "Cena"
  },
  PriceAverage: {
    en: "Average price",
    cs: "Průměrná cena"
  },
  PriceReimbursement: {
    en: "Reimbursement",
    cs: "Úhrada"
  },
  Prevention: {
    en: "Prevention",
    cs: "Prevence"
  },
  MayBePrevented: {
    en: "May be prevented",
    cs: "Může být preventováno"
  },
  MayPrevent: {
    en: "May be prevented",
    cs: "Může být preventováno"
  },
  MayTreat: {
    en: "May be prevented",
    cs: "Může být preventováno"
  },
  DefinedDailyDoses: {
    en: "Daily doses",
    cs: "Denní dávka"
  },
  DeliveredInLastThreeMonths: {
    en: "Delivered in last three months",
    cs: "Doručeno v posl. 3 měs."
  },
  RegistrationState: {
    en: "Registracni stat",
    cs: "Registration state"
  },
  Name: {
    en: "Name",
    cs: "Jméno"
  },
  Username: {
    en: "Username",
    cs: "Uživatelské jméno"
  },
  Sex: {
    en: "Gender",
    cs: "Pohlaví"
  },
  Birthday: {
    en: "Birthday",
    cs: "Datum narození"
  },
  Male: {
    en: "Male",
    cs: "Muž"
  },
  Female: {
    en: "Female",
    cs: "Žena"
  },
  Submit: {
    en: "Submit",
    cs: "Uložit"
  },
  MandatoryField: {
    en: "required",
    cs: "povinné"
  },
  MandatoryFieldNotification: {
    en: "Required for notifications",
    cs: "Povinné pro notifikace"
  },
  From: {
    en: "From",
    cs: "Od"
  },
  To: {
    en: "To",
    cs: "Do"
  },
  Today: {
    en: "Today",
    cs: "Dnes"
  },
  Last7Days: {
    en: "Last 7 days",
    cs: "Posledních 7 dní"
  },
  Older6Months: {
    en: "Older than 6 months",
    cs: "Starší 6 měsíců"
  },
  ThisMonth: {
    en: "This month",
    cs: "Tento měsíc"
  },
  Day: {
    en: "Day",
    cs: "Den"
  },
  Month: {
    en: "Month",
    cs: "Měsíc"
  },
  Year: {
    en: "Year",
    cs: "Rok"
  },
  Password: {
    en: "Password",
    cs: "Heslo"
  },
  Expert: {
    en: "Expert in healthcare",
    cs: "Profesionál ve zdravotnictví"
  },
  Layman: {
    cs: "Běžný uživatel",
    en: "Common user"
  },
  ConfirmationPassword: {
    en: "Confirm password",
    cs: "Potvrďte heslo"
  },
  DifferentPasswords: {
    en: "Passwords are not same",
    cs: "Hesla se neshodují"
  },
  MissingEmail: {
    en: "Fill in the email",
    cs: "Vyplňte email"
  },
  MissingPassword: {
    en: "Fill in the password",
    cs: "Vyplňte heslo"
  },
  MissingUsername: {
    en: "Fill in the username",
    cs: "Vyplňte uživatelské heslo"
  },
  MissingDate: {
    en: "Fill in the birthday",
    cs: "Vyplňte den narození"
  },
  InvalidDate: {
    en: "Invalid date",
    cs: "Neplatné datum"
  },
  Date: {
    en: "Date",
    cs: "Datum"
  },
  VisitDate: {
    en: "Date of visit",
    cs: "Datum návštěvy"
  },
  InvalidDates: {
    en: "One or more invalid dates",
    cs: "Jedno nebo více neplatných dat"
  },
  MissingGender: {
    en: "Fill in the gender",
    cs: "Vyplňte pohlaví"
  },
  Registration: {
    en: "Registration",
    cs: "Registrace"
  },
  InvalidEmail: {
    en: "Invalid email",
    cs: "Neplatný email"
  },
  InvalidUsername: {
    en: "Invalid username",
    cs: "Neplatné uživatelské jméno"
  },
  InvalidPassword: {
    en: "Invalid password, it can contain only digits and letters.",
    cs: "Neplatné heslo, múže obsahovat jen písmena a číslice."
  },
  ExistingUser: {
    en: "User already exists.",
    cs: "Tento uživatel již existuje."
  },
  NonExistingUser: {
    en: "User doesn`t exist.",
    cs: "Tento uživatel neexistuje."
  },
  Registered: {
    en: "User registration successfully completed.",
    cs: "Uživatel úspěšně registrován."
  },
  SomethingWrongRegistration: {
    en: "Something went wrong during registration.",
    cs: "Při registraci došlo k chybě."
  },
  SomethingWrong: {
    en: "Something went wrong.",
    cs: "Došlo k chybě."
  },
  Login: {
    en: "Login",
    cs: "Přihlášení"
  },
  Logout: {
    en: "Logout",
    cs: "Odhlásit se"
  },
  MedicationOk: {
    en: "No interactions in your medication found.",
    cs: "Ve vaší medikaci nebyly nalazeny žádné interakce."
  },
  MedicationNotOk: {
    cs: "Pozor - ve vaší medikaci byly nalezeny interakce.",
    en: "Warning - some interactions were found in your medication."
  },
  Logged: {
    en: "Logged",
    cs: "Přihlášen"
  },
  BadCredentials: {
    en: "The username or password you have entered is invalid.",
    cs: "Chybně zadané heslo nebo uživatelské jméno."
  },
  User: {
    en: "User",
    cs: "Uživatel"
  },
  EditUser: {
    en: "Edit user",
    cs: "Upravit účet"
  },
  Medication: {
    en: "Medication",
    cs: "Medikace"
  },
  Medicament: {
    en: "Medicament",
    cs: "Medikament"
  },
  PasswordChange: {
    en: "Change password",
    cs: "Změna hesla"
  },
  PasswordChanged: {
    en: "Change of password successful",
    cs: "Heslo změněno"
  },
  NewPassword: {
    en: "New password",
    cs: "Nové heslo"
  },
  Relog: {
    en: "Relog with new password",
    cs: "Je potřeba se přihlásit s novým heslem"
  },
  Results: {
    en: "Found items",
    cs: "Nalezeno položek"
  },
  WrongPassword: {
    en: "Wrong password",
    cs: "Špatné heslo"
  },
  Doses: {
    en: "Dose",
    cs: "Dávka"
  },
  NextDose: {
    en: "Next dose",
    cs: "Příští dávka"
  },
  NextDoseDay: {
    en: "Next dose day",
    cs: "Příští dávka den"
  },
  NextDoseHour: {
    en: "time",
    cs: "čas"
  },
  HowOften: {
    en: "Hours between doses",
    cs: "Hodin mezi dávkami"
  },
  DoseDescription: {
    en: "Dose description",
    cs: "Popis dávky"
  },
  Note: {
    en: "Note",
    cs: "Poznámka"
  },
  Remove: {
    en: "Remove?",
    cs: "Odstranit?"
  },
  Search: {
    en: "Search",
    cs: "Hledat"
  },
  Searching: {
    en: "Searching",
    cs: "Vyhledávám"
  },
  SearchResults: {
    en: "Search results",
    cs: "Výsledky vyhledávání"
  },
  NothingFound: {
    en: "Nothing found.",
    cs: "Nic nenalezeno."
  },
  SPCNotFound: {
    en: "SPC not found.",
    cs: "SPC nenalezen."
  },
  Add: {
    en: "Add",
    cs: "Přidat"
  },
  InvalidNumberOfHours: {
    en: "Invalid number of hours",
    cs: "Neplatný počet hodin"
  },
  InvalidNumberOfDays: {
    en: "Invalid number of days",
    cs: "Neplatný počet dnů"
  },
  Download: {
    en: "Download",
    cs: "Stáhnout"
  },
  DiseaseOrFinding: {
    en: "Findings",
    cs: "Nálezy"
  },
  Interactions: {
    en: "Interactions",
    cs: "Interakce"
  },
  Detail: {
    en: "Detail",
    cs: "Detail"
  },
  All: {
    cs: "Všechno",
    en: "All"
  },
  Filter: {
    cs: "Filtrovat podle",
    en: "Filter by"
  },
  FilterByIngredient: {
    cs: "Filtrovat podle aktivní látky",
    en: "Filter by active ingredient"
  },
  TypeHistory: {
    cs: "Typ lékařského pojmu",
    en: "Type of medicinal concept"
  },
  Welcome: {
    en: "Welcome in Drug encyclopedia!",
    cs: "Vítejte v Lékové encyklopedii!"
  },
  TypeTitle: {
    en: "Title of medicinal concept",
    cs: "Název lékařského pojmu"
  },
  Weeks: {
    en: "Weeks",
    cs: "Týdny"
  },  NoMedication: {
    en: "You have no medication saved yet.",
    cs: "Zatím nemáte zadanou žádnou medikaci."
  },
  Days: {
    en: "Days",
    cs: "Dny"
  },
  Hours: {
    en: "Hours",
    cs: "Hodiny"
  },
  Months: {
    en: "Months",
    cs: "Měsíce"
  },
  Tablets: {
    en: "Tablets",
    cs: "Tablety"
  },
  Capsules: {
    en: "Capsules",
    cs: "Kapsle"
  },
  CoatedTablets: {
    en: "Coated tablets",
    cs: "Potahované tablety"
  },
  Premix: {
    en: "Premix",
    cs: "Premix"
  },
  Powder: {
    en: "Powder",
    cs: "Prášek"
  },
  Paste: {
    en: "Paste",
    cs: "Pasta"
  },
  Gel: {
    en: "Gel",
    cs: "Gel"
  },
  Ointment: {
    en: "Ointment",
    cs: "Mast"
  },
  Creme: {
    en: "Creme",
    cs: "Krém"
  },
  Foam: {
    en: "Foam",
    cs: "Pěna"
  },
  Injcetion: {
    en: "Injcetion",
    cs: "Injekce"
  },
  SpotOn: {
    en: "Spot on",
    cs: "Přípravek na kůži"
  },
  Spray: {
    en: "Spray",
    cs: "Sprej"
  },
  Suppository: {
    en: "Suppository",
    cs: "Čípek"
  },
  Aerosol: {
    en: "Aerosol",
    cs: "Aerosol"
  },
  Drops: {
    en: "Drops",
    cs: "Kapky"
  },
  Liquid: {
    en: "Months",
    cs: "Měsíce"
  },

  Sespension: {
    en: "Sespension",
    cs: "Suspenze"
  },
  Emulsion: {
    en: "Emulsion",
    cs: "Emulze"
  },
  InteractionControl: {
    en: "Control of interactions",
    cs: "Kontrola interakcí"
  },
  EditMedicament: {
    en: "Edit medicament",
    cs: "Upravit medikament"
  },
  AddMedicament: {
    en: "Add medicament",
    cs: "Přidat medikament"
  },
  InteractionControlDescButt: {
    en:
      "Checks whether there are any potential interactions among the inserted medicinal products and active ingredients.",
    cs:
      "Prověří, zda mezi zadanými účinnými látkami a léčivými přípravky existují potenciální interakce."
  },
  PositiveList: {
    en: "Positive list",
    cs: "Pozitivní list"
  },
  PositiveListDesc: {
    en: (
      <div class="positive-list-desc">
        <a href="http://szpcr.cz/pozitivni_list">Positive list</a> is a list of
        medicinal products recommended by{" "}
        <a href="http://szpcr.cz">the Union of health insurance companies</a> to
        prescribe.
      </div>
    ),
    cs: (
      <div class="positive-list-desc">
        <a href="http://szpcr.cz/pozitivni_list">Pozitivní list</a> je list
        léčivých přípravků doporučených{" "}
        <a href="http://szpcr.cz">Svazem zdravotních pojišťoven</a> k
        předepisování.
      </div>
    )
  },
  PositiveListDescLong: {
    en: (
      <div class="positive-list-desc">
        <br />
        <div class="bold">
          The Positive List of the SZP of the Czech Republic{" "}
        </div>
        is a tool for prescribing doctors, that gives them information, which
        medicinal products (hereinafter referred to as MPs) are from the point
        of view of health insurance companies least cost demanding. That
        informations is based on a comparison of the amount of the
        reimbursement. These are MPs, which SZP ČR recommends the most for
        prescription.
        <br />
        <br />
        Basis for the Positive List are MPs paid from public health funds
        according to the list of reimbursed medicinal products and food for
        special medical purposes (list of prices and reimbursements, SCAU) that
        publishes SÚKL and listed in dial HVLP SZP ČR in relevant month to date
        of processing Positive list.
        <br />
        <br />
        MPs are included in the Positive Sheet in accordance with the valid{" "}
        <a href="http://szpcr.cz/wp-content/uploads/2017/04/Pravidla-tvorby-Pozitivn%C3%ADho-listu-SZP-%C4%8CR_20170701.pdf">
          Rules creating Positive List
        </a>
        , which are published on the SZP ČR website.
        <br />
        <br />
        <div class="bold">
          The Positive List SZP ČR contains selected medicinal products from
          groups of medicinal products{" "}
        </div>
        , defined by the same active substance (ATC group), the route of
        administration, the dosage form and the amount of the active substance
        in the batch (in unit of weight, volume or dosage form) and containing
        MPs of varying amounts reimbursement equivalent.
      </div>
    ),
    cs: (
      <div class="positive-list-desc">
        <br />
        <div class="bold">Pozitivní list SZP ČR </div>
        je pomůcka pro předepisující lékaře poskytující lékaři informaci, které
        léčivé přípravky (dále jen LP) jsou z pohledu zdravotních pojišťoven
        nákladově nejméně náročné, a to na základě porovnání výše úhradového
        ekvivalentu. Jedná se o LP, které jsou SZP ČR v maximální možné míře
        doporučovány k preskripci.
        <br />
        <br />
        Základem pro Pozitivní list jsou LP hrazené z prostředků veřejného
        zdravotního pojištění dle Seznamu hrazených léčivých přípravků a
        potravin pro zvláštní lékařské účely (Seznam cen a úhrad, SCAU) vydaného
        Státním ústavem pro kontrolu léčiv (SÚKL) a zařazené v číselníku HVLP
        SZP ČR v příslušném měsíci k datu zpracování Pozitivního listu.
        <br />
        <br />
        Léčivé přípravky jsou zařazovány na Pozitivní list v souladu s platnými{" "}
        <a href="http://szpcr.cz/wp-content/uploads/2017/04/Pravidla-tvorby-Pozitivn%C3%ADho-listu-SZP-%C4%8CR_20170701.pdf">
          Pravidly tvorby Pozitivního listu
        </a>
        , jež jsou publikována na webových stránkách SZP ČR.
        <br />
        <br />
        <div class="bold">
          V Pozitivním listu SZP ČR jsou uvedeny vybrané léčivé přípravky ze
          skupin vzájemně zaměnitelných léčivých přípravků
        </div>
        , definovaných shodnou léčivou látkou (ATC skupinou), cestou podání,
        lékovou formou a množstvím léčivé látky v dávce (v jednotce hmotnosti,
        objemu nebo lékové formy) a obsahujících LP s odlišnou výší úhradového
        ekvivalentu.
      </div>
    )
  },
  PositiveListDescButt: {
    en:
      "List of medicinal products which are recomended for prescription by the Union of health insurance companies.",
    cs:
      "List léčivých přípravků doporučených Svazem zdravotních pojišťoven k předepisování."
  },
  Delivered: {
    en: "On market",
    cs: "Na trhu"
  },
  Source: {
    en: "Source",
    cs: "Zdroj"
  },
  ReadMore: {
    en: " Read more...",
    cs: " Číst více..."
  },
  ReadLess: {
    en: " Read less...",
    cs: " Číst méně..."
  },
  IsOnPositiveList: {
    en: "This packaging is in Positive list.",
    cs: "Toto balení je na pozitivním listu."
  },
  PackagingDetail: {
    en: "Packaging detail",
    cs: "Detail balení"
  },
  BroaderATC: {
    en: "Broader ATC concepts",
    cs: "Obecnější ATC koncepty"
  },
  NarrowerATC: {
    en: "Narrower ATC concepts",
    cs: "Konkrétnější ATC koncepty"
  },
  Critical: {
    en: "Critical interaction",
    cs: "Kritická interakce"
  },
  InvalidFrequency: {
    en: "Invalid frequency",
    cs: "Neplatná frekvence"
  },
  HowOftenDays: {
    en: "Frequency - days",
    cs: "Dní mezi dávkami"
  },
  AddedDate: {
    en: "Add time",
    cs: "Datum přidání"
  },
  SortBy: {
    en: "Sort by",
    cs: "Řadit dle"
  },
  HowOftenHours: {
    en: "Frequency - hours",
    cs: "Hodin mezi dávkami"
  },
  DoseType: {
    en: "DosageType",
    cs: "Léková forma"
  },
  Amount: {
    en: "Amount",
    cs: "Množství"
  },
  SetNotifications: {
    en: "Set notifications",
    cs: "Nastavit notifikace"
  },
  AddMedication: {
    en: "Add medication",
    cs: "Přidat medikaci"
  },
  SetMedicament: {
    en: "Set medicament",
    cs: "Nastavit medikament"
  },
  InteractionsButt: {
    cs:
      "prověří, zda mezi zadanými účinnými látkami a léčivými přípravky existují potenciální interakce.",
    en:
      "checks whether there are any potential interactions among the inserted medicinal products and active ingredients."
  },
  InteractionsDesc: {
    cs:
      "Zadejte léčivé přípravky či aktivní látky a prověřte, zda mezi nimi existují interakce.",
    en:
      "Enter active ingredients or medicinal prodcuts and check, whether there are any interactions amongst them."
  },
  NoInteractions: {
    en: "No interactions found.",
    cs: "Žádné interakce nenalezeny."
  },
  MedicationDesc: {
    cs: (
      <div class="medication-desc">
        Zde si můžete nastavit vlastní medikaci.
        <br />
        <br />
        <div class="notification-desc"> Notifikace</div> - Pro fungování
        notifikací medikace je potřeba nainstalovat prohlížečové rozšíření (
        <div class="bold">po instalaci stránku obnovte - zmáčkněte F5 </div>
        ):
      </div>
    ),
    en: (
      <div class="medication-desc">
        Here you can set your medication.
        <br />
        <br />
        <div class="notification-desc"> Notification</div> For medication
        notification it is neccessary to install browser extension (
        <div class="bold">after installation, refresh this page - press F5</div>
        ):
      </div>
    )
  },

  Profile: {
    cs: "Profil",
    en: "Profil"
  },
  InteractionsFound: {
    cs: "Nalezeny interakce",
    en: "Interactions found"
  },
  History: {
    cs: "Historie vyhledávání",
    en: "Search history"
  },
  HistoryLong: {
    cs: "Historie vyhledaných pojmů na Lékové encyklopedii.",
    en: "History of searched concepts on Drug encyclopedia."
  },
  Type: {
    cs: "Typ",
    en: "Type"
  },

  MedFormDesc: {
    cs: "* Povinné údaje pro funkci notifíkací",
    en: "* Mandatory fields for notifications"
  },
  Own: {
    cs: "Vlastní",
    en: "Own"
  },
  Experience: {
    cs: "Zkušenosti s léky",
    en: "Experience with drugs"
  },
  Frequency: {
    cs: "Frekvence",
    en: "Frequency"
  },
  NoPil: {
    cs: "Příbalový leták nenalezen.",
    en: "Package leaflet not found."
  },
  ForgottenPassword: {
    cs: "Zapomenuté heslo?",
    en: "Forgotten password?"
  },
  Sent: {
    cs: "Instrukce pro resetování hesla zaslány na email uživatele.",
    en: "Instructions for reseting password sent to the user`s email."
  },
  Edited: {
    cs: "Upraveno",
    en: "Edited"
  },
  TryLater: {
    cs: "Něco se nepovedlo, prosím zkuste akci zopakovat za chvíli.",
    en: "Something went wrong, please try again a bit later."
  },
  OutdatedLink: {
    cs:
      "Neplatný odkaz,  pravděpodobně jste buď zadali špatné uživatelské jméno nebo platnost odkazu vypršela.",
    en:
      "Link is not valid, probably username input is wrong or your link is outdated."
  },
  AlreadyEmailed: {
    cs:
      "Na emailu máte již platný odkaz, v případě problémů vyzkoušejte akci za 30 minut.",
    en:
      "Valid link to password reset was already sent, in case of problems repeat action in 30 minutes."
  },
  Saved: {
    cs: "Uloženo",
    en: "Saved"
  }, Save: {
    cs: "Uložit",
    en: "Save"
  },
  NoAlternatives: {
    cs: "Žádná balení nebyla nalezena.",
    en: "No packages were found."
  },
  MedNote: {
    cs: (
      <div>
        <div class="star">*</div> Povinná pole
        <br />
        <div class="double-star">**</div> Povinná pole pro notifikace
      </div>
    ),
    en: (
      <div>
        <div class="star">*</div> Mandatory fields
        <br />
        <div class="double-star">**</div>
        Mandatory fields for notifications
      </div>
    )
  },
  NoMoreInfo: {
    cs: "Žádné další informace nebyly nalezeny",
    en: "No info found"
  },
  PageNotFound: {
    cs: "Je nám líto, ale stránka nebyla nalezena.",
    en: "We are sorry, but the page could not be found."
  }
};
