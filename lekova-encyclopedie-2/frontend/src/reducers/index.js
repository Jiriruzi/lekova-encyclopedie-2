import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import environmentReducer from './reducer-environment.js';
import searchReducer from './reducer-search.js';
import userFormsResponsesReducer from './reducer-userFormsResponses';
import documentsReducer from './reducer-documents';
import userReducer from './reducer-user';
import itemsReducer from './reducer-items';
import interactionsReducer from './reducer-interactions';
import medicationReducer from './reducer-medication';
import historyReducer from './reducer-userHistory';


const allReducers = combineReducers({
    environment: environmentReducer,
    routing: routerReducer,
    search: searchReducer,
    userFormsResponses: userFormsResponsesReducer,
    documents: documentsReducer,
    user: userReducer,
    items: itemsReducer,
    interactions: interactionsReducer,
    medication: medicationReducer,
    userHistory: historyReducer
});

export default allReducers;
