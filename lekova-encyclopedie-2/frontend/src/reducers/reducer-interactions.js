export default function(
  state = {
    items: [],
    loading: false,
    error: false,
    ingredients: []
  },
  action
) {
  switch (action.type) {
    case "INTERACTIONS_LOADING": {
      state = {
        ...state,
        loading: true,
        error: false,
        ingredients: []
      };
      break;
    }
    case "INTERACTIONS_NOT_LOADING": {
      state = {
        ...state,
        loading: false,
        error: false,
        ingredients: []
      };
      break;
    }
    case "ADD_INTERACTION_ITEM": {
      state = {
        ...state,
        items: [...state.items, ...action.payload],
        ingredients: []
      };
      break;
    }
    case "REMOVE_INTERACTION_ITEM": {
      state = {
        items: [
          ...state.items.slice(0, action.payload),
          ...state.items.slice(action.payload + 1)
        ],
        loading: true,
        error: false,
        ingredients: []
      };
      break;
    }
    case "INTERACTIONS_FAILED": {
      state = {
        ...state,
        ingredients: [],
        loading: false,
        error: true
      };
      break;
    }
    case "INTERACTIONS_CREATED": {
      state = {
        ...state,
        error: false,
        ingredients: action.payload,
        loading: false
      };
      break;
    }
    case "INTERACTIONS_RESET": {
      state = {
        items: [],
        loading: false,
        error: false,
        ingredients: []
      };
      break;
    }
    case "RESET_ONLY_INTERACTIONS": {
      state = {
        ...state,
        ingredients: []
      };
      break;
    }
  }
  return state;
}
