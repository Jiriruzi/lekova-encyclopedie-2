const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case "GET_MEDICATION": {
      state = { items: action.payload, loading: false };
      break;
    }
    case "MEDICATIONS_LOADING": {
      state = {
        ...state,
        loading: true
      };
      break;
    }
    case "REMOVE_MEDICAMENT": {
      delete state.items[action.payload];
      state = JSON.parse(JSON.stringify(state));
      break;
    }
    case "UPDATE_MEDICAMENT": {
      state = {
        ...state,
        items: { ...state.items, [action.payload.key]: action.payload },
        loading: false
      };
      break;
    }
  }
  return state;
}
