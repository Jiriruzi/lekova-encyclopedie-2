const initialState = {
  valueInForm: "",
  valueHeader: "",
  sections: [],
  resultsAll: [],
  loadingValues: [],
  loadingResultsAll: false,
  loading: false
};

const getSections = docs => {
  const suggestions = docs;
  const sections = [];
  for (let i = 0; i < suggestions.length; i++) {
    {
      let j = 0;
      for (; j < sections.length; j++) {
        if (suggestions[i]._type === sections[j].name) {
          break;
        }
      }
      if (j === sections.length) {
        sections.push({
          name: suggestions[i]._type,
          items: []
        });
      }
      sections[j].items.push(suggestions[i]);
    }
  }
  return sections;
};

export default function(state = initialState, action) {
  switch (action.type) {
    case "SUGGEST": {
      for (let i = state.loadingValues.length - 1; i >= 0; i--) {
        if (state.loadingValues[i] === action.payload.value) {
          state = {
            ...state,
            sections: getSections(action.payload.suggestions),
            loadingValues: [...state.loadingValues.slice(i + 1)],
            loading: false
          };
        }
      }
      break;
    }
    case "SUGGEST_LOADING": {
      state = {
        ...state,
        loadingValues: [...state.loadingValues, action.payload]
      };
      break;
    }
    case "SUGGEST_LONG_LOADING": {
      if (state.loadingValues.length - 1) {
        state = {
          ...state,
          loading:
            state.loadingValues[state.loadingValues.length - 1] ===
            action.payload
        };
      }
      break;
    }
    case "ALL_RESULTS": {
      state = {
        ...state,
        loadingResultsAll: false,
        resultsAll: action.payload
      };
      break;
    }
    case "RESET_RESULTS": {
      state = initialState;
      break;
    }
    case "RESET_ALL_RESULTS": {
      state = {
        ...state,
        loadingResultsAll: false,
        resultsAll: []
      };
      break;
    }
    case "ALL_RESULTS_LOADING": {
      state = {
        ...state,
        loadingResultsAll: true,
        resultsAll: []
      };
      break;
    }
    case "SET_SEARCH_VALUE_HEADER": {
      if (action.payload === null) {
        state = {
          ...state,
          sections: [],
          valueHeader: state.valueHeader ? state.valueHeader : ""
        };
      } else {
        state = {
          ...state,
          sections: state.sections,
          valueHeader: action.payload
        };
      }
      break;
    }
    case "SET_SEARCH_VALUE_IN_FORM": {
      if (action.payload === null) {
        state = {
          ...state,
          sections: [],
          valueInForm: state.valueInForm ? state.valueInForm : ""
        };
      } else {
        state = {
          ...state,
          sections: state.sections,
          valueInForm: action.payload
        };
      }
      break;
    }
    case "RESET_SEARCH_VALUE_IN_FORM": {
      state = { ...state, sections: [], valueInForm: "" };
      break;
    }
  }
  return state;
}
