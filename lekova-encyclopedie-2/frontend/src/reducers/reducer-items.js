const initialState = {nextKey: 0, keys: [], items: []};

export default function(state = initialState, action) {
    switch (action.type) {
        case 'REMOVE_ITEM': {
            state = {...state,
                keys: [...state.keys.slice(0, action.payload),
                    ...state.keys.slice(action.payload + 1)],
                items: [...state.items.slice(0, action.payload),
                    ...state.items.slice(action.payload + 1)]};
            break; }
        case 'ADD_ITEM': {
            state = {...state,
                nextKey: state.nextKey + 1,
                keys: [...state.keys, state.nextKey],
                items: [...state.items,
                    {...action.payload}]};
            break; }
        case 'RESET_FORM': {
            state = initialState;
            break; }
        case 'EDIT_ITEM': {
            state = {...state,
                items: [...state.items.slice(0, action.payload),
                    {...state.items[action.payload], edit: true},
                    ...state.items.slice(action.payload + 1)]};
            break; }
        case 'CONFIRM_ITEM': {
            state = {...state,
                items: [...state.items.slice(0, action.payload.key),
                    {...action.payload.medicament, edit: false},
                    ...state.items.slice(action.payload.key + 1)]};
            break; }
        case 'STORNO_ITEM': {
            state = {...state,
                items: [...state.items.slice(0, action.payload),
                    {...state.items[action.payload], edit: false},
                    ...state.items.slice(action.payload + 1)]};
            break; }
    }
    return state;
}
