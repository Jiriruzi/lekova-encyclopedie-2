export default function(
  state = {
    username: null,
    role: null,
    initialized: false,
    loading: false,
    error: false,
    data: {}
  },
  action
) {
  switch (action.type) {
    case "USER_SESSION": {
      state = {
        ...state,
        username: action.payload.username,
        role: action.payload.role,
        initialized: true,
        loading: false
      };
      break;
    }
    case "USER_LOADING": {
      state = {
        ...state,
        loading: true
      };
      break;
    }
    case "USER_FOUND": {
      state = {
        ...state,
        loading: false,
        data: action.payload
      };
      break;
    }
    case "USER_NOT_FOUND": {
      state = {
        ...state,
        loading: false,
        error: action.payload
      };
      break;
    }
  }
  return state;
}
