export default function(state = {}, action = { type: "" }) {
  switch (action.type) {
    case "DOCUMENT_FOUND": {
      state = {
        ...state,
        [action.payload.type]: {
          loading: false,
          error: false,
          doc: action.payload.doc
        }
      };
      break;
    }
    case "DOCUMENT_NOT_FOUND": {
      state = {
        ...state,
        [action.payload]: {
          loading: false,
          error: true
        }
      };
      break;
    }
    case "DOCUMENT_LOADING": {
      state = {
        ...state,
        [action.payload]: {
          loading: true
        }
      };
      break;
    }
  }
  return state;
}
