export default function(state = {
    loading: false
}, action) {
    switch (action.type) {
        case 'REGISTRATION_RESPONSE': {
            state = {...state,
                loading: false,
                registrationResponse: action.payload};
            break;
        }
        case 'LOGIN_RESPONSE': {
            state = {...state,
                loading: false,
                loginResponse: action.payload};
            break;
        } case 'MEDICATION_RESPONSE': {
            state = {...state,
                loading: false,
                medicationResponse: action.payload};
            break;
        }
        case 'CHANGE_PASSWORD_RESPONSE': {
            state = {...state,
                loading: false,
                changePasswordResponse: action.payload};
            break;
        }
        case 'RESET_RESPONSES': {
            state = {loading: false};
            break;
        }
        case 'EDIT_RESPONSE': {
            state = {...state,
                loading: false,
                editResponse: action.payload};
            break;
        }
        case 'RESPONSE_LOADING': {
            state = {...state, loading: true};
            break;
        }
        case 'UPDATE_MEDICAMENT_RESPONSE': {
            state = {...state, addedId: action.payload};
            break;
        }
        case 'RESET_PASSWORD_RESPONSE': {
            state = {...state,
                loading: false,
                resetResponse: action.payload};
            break;
        }
    }
    return state;
}
