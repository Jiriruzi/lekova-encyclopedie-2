import utils from 'Libs/utils.js';

export default function(state = {
    language: utils.getLanguage(),
    context: 'enc:',
    showUserBox: false
}, action) {
    switch (action.type) {
        case 'SET_LANGUAGE': {
            state = {...state,
                language: action.payload};
            break; }
        case 'SHOW_USER_BOX': {
            state = {...state,
                showUserBox: action.payload};
            break; }
        case 'HIDE_SHOW_USER_BOX': {
            state = {...state,
                showUserBox: !state.showUserBox};
            break; }
    }
    return state;
}

