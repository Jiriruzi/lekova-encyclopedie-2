const initialState = {};

export default function(state = initialState, action) {
    switch (action.type) {
        case 'GET_HISTORY': {
            state = {loading: false, history: action.payload};
            break; }
        case 'HISTORY_LOADING': {
            state = {loading: true, history: []};
            break; }
    }
    return state;
}
