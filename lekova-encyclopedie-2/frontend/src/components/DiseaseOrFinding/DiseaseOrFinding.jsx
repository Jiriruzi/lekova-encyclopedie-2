import React from 'react';
import {connect} from 'react-redux';
import PageContainer from 'Components/PageContainer/PageContainer.jsx';
import InfoFromConf from 'Components/InfoFromConf/InfoFromConf.jsx';
import {getDocument} from 'Actions/documentsFetches.js';
import {bindActionCreators} from 'redux';
import utils from 'Libs/utils.js';
import {Vocabulary} from 'Libs/Vocabulary.js';
import {updateHistory} from 'Actions/userFetches.js';
import PropTypes from "prop-types";


/**  Special component for DiseaseOrFinding
 * that gets configuration file and data file and than display data requiered data
 */
export class DiseaseOrFinding extends React.Component {
    componentWillMount() {
        this.props.getDocument('DiseaseOrFinding', this.props.id);
        this.props.getDocument('Configuration', this.props.type);

    }

    componentWillReceiveProps(nextProps) {
        if (this.props.type !== nextProps.type) {
            this.props.getDocument('Configuration', nextProps.type);
            this.props.getDocument(nextProps.type, nextProps.id);
        } else if (this.props.id !== nextProps.id) {
            this.props.getDocument(nextProps.type, nextProps.id);
        }
        if (nextProps.user &&
            nextProps.user.role === 'expert' && nextProps.document && nextProps.document.doc
             && nextProps.document !== this.props.document) {
            this.props.updateHistory(this.props.type, this.props.id, nextProps.document.doc[`${this.props.context}title`]);
        }
    }


    render() {
        let title = '';
        if (this.props.document) {
            title = `${utils.getValue(
                this.props.document.doc,
                this.props.context,
                this.props.language,
                'title').value}`;
        }
        let confDoc = {};
        let confList = [];
        const isLoading = (!this.props.document || this.props.document.loading)
                || (!this.props.conf || this.props.conf.loading);
        const error = (this.props.document && this.props.document.error) || (this.props.conf && this.props.conf.error);
        let {props} = this;
        let component = null;
        if (!isLoading && !error) {
            props = {...props, document: {...props.document, doc: {...props.document.doc}}};
            confDoc = {...props.conf.doc};
            confList = confDoc.list;
            if (props.component && props.component !== '?detail') {
                confDoc = {...confDoc, basicInfo: []};
                confList = [];
                // Adding some configuration
                for (let i = 0; i < props.conf.doc.list.length; i++) {
                    if (this.props.component.toLowerCase() === `?${props.conf.doc.list[i].property.toLowerCase()}`) {
                        confList.push({...props.conf.doc.list[i], label: null, class: `${props.conf.doc.list[i].class} col-xs-12`});
                    }
                }
                component = (<div>
                    <div className="col-xs-6 col-md-6 bold" >{Vocabulary.Ingredient[props.language]}</div>

                    <div className="col-xs-6 col-md-6 bold">{Vocabulary.MedicinalProduct[props.language]}</div>
                    <InfoFromConf {...props} conf={{...this.props.conf, doc: {...confDoc, list: confList}}} />
                </div>);
            } else {
                confList = [];
                for (let i = 0; i < props.conf.doc.list.length; i++) {
                    confList.push({...props.conf.doc.list[i], class: `${props.conf.doc.list[i].class} col-xs-12 col-md-4`, sublist: null});
                }
                component = <InfoFromConf {...props} conf={{...this.props.conf, doc: {...confDoc, list: confList}}} />;
            }
        }
        return (
            <PageContainer
                type="DiseaseOrFinding"
                id={this.props.id}
                language={this.props.language}
                context={this.props.context}
                routes={[{path: 'detail', label: 'Detail'}, {path: 'mayTreat', label: 'Treatment'}, {path: 'mayPrevent', label: 'Prevention'},
                    {path: 'contraindicatedWith', label: 'Contraindication'}]}
                title={title}
                isLoading={isLoading}
                error={error}
            >
                {component}
            </PageContainer>

        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        component: ownProps.location.search,
        context: state.environment.context,
        language: state.environment.language,
        id: ownProps.match ? ownProps.match.params.id : null,
        user: state.user,
        type: 'DiseaseOrFinding',
        document: state.documents.DiseaseOrFinding,
        conf: state.documents.Configuration
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        getDocument,
        updateHistory
    }, dispatch);
}

DiseaseOrFinding.propTypes = {

    /**
     * Which component should of the document should be displayed
     */
    component: PropTypes.string,
    context: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    /**
     * id of document
     */
    id: PropTypes.string.isRequired,
    /**
     * Object containing info about user
     */
    user: PropTypes.object,
    /**
     * If the ATCConcept is shown on page determined for ATCConcept or its showing some info about ATCConcept in another document
     */
    ownPage: PropTypes.bool,
    /**
     * Type of the document
     */
    type: PropTypes.string.isRequired,
    /**
     * Data of this document
     */
    document: PropTypes.object,
    /**
     * Configuration for this document
     */
    conf: PropTypes.object,
  };

export default connect(mapStateToProps, matchDispatchToProps)(DiseaseOrFinding);
