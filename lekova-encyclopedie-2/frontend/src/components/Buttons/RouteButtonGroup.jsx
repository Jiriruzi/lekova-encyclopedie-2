import React from "react";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary";
import PropTypes from "prop-types";

/**
 * Component rendering buttons to switch between info of chosen file
 * @param {object} props 
 */
export function RouteButtonGroup(props) {
  const buttons = [];
  for (let i = 0; props.routes && i < props.routes.length; i++) {
    buttons.push(
      <button
        key={`button${i}`}
        className={`btn-group btn btn-primary vcenter button-router${
          props.location.search === `?${props.routes[i].path}` ||
          (props.location.search === "" && i === 0)
            ? " active"
            : ""
        }`}
        type="radio"
        onClick={() => {
          props.history.push(
            `${props.location.pathname}?${props.routes[i].path}`
          );
        }}
      >
        {Vocabulary[props.routes[i].label][props.language]}
      </button>
    );
  }
  return <div className="route-buttons col-xs-12 row drug-row">{buttons}</div>;
}

function mapStateToProps(state, ownProps) {
  return {
    location: ownProps.location,
    language: state.environment.language
  };
}
RouteButtonGroup.propTypes = {
  /**
   * History location object
   */
  location: PropTypes.object.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Array of objects with name for the button and search param for the url
   */
  routes: PropTypes.array,
};
export default withRouter(connect(mapStateToProps)(RouteButtonGroup));
