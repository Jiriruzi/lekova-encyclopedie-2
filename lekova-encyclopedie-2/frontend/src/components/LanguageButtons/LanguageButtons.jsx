import React from "react";
import { bindActionCreators } from "redux";
import {
  selectLanguage,
  showUserBox,
  hideUserBox,
  hideShowUserBox
} from "Actions/environmentActions.js";
import { connect } from "react-redux";
import PropTypes from "prop-types";

export class LanguageButtons extends React.Component {
  componentWillReceiveProps(newProps) {
    if (this.props.location !== newProps.location) {
      this.props.hideUserBox();
    }
  }

  activeLanguage(lang) {
    if (lang === this.props.environment.language) {
      return "active";
    }
    return "";
  }

  render() {
    return [
      <div key="1" className="hidden-xs col-sm-2 col-md-2 col-lg-2 btn-group  vcenter">
        <button
          className={`btn btn-primary ${this.activeLanguage("en")}`}
          type="radio"
          onClick={() => {
            this.props.selectLanguage("en");
          }}
        >
          en
        </button>
        <button
          className={`btn btn-primary ${this.activeLanguage("cs")}`}
          type="radio"
          onClick={() => {
            this.props.selectLanguage("cs");
          }}
        >
          cs
        </button>
      </div>,
      <div key="2" className="hidden-sm hidden-md hidden-lg hidden-xl col-xs-1 btn-group  vcenter">
        <button
          className={"btn btn-primary"}
          type="radio"
          onClick={() => {
            this.props.selectLanguage(
              this.props.environment.language === "cs" ? "en" : "cs"
            );
          }}
        >
          {this.props.environment.language === "cs" ? "en" : "cs"}
        </button>
      </div>
    ];
  }
}
function mapStateToProps(state) {
  return {
    environment: state.environment
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectLanguage,
      hideUserBox,
      hideShowUserBox,
      showUserBox
    },
    dispatch
  );
}

LanguageButtons.propTypes = {
  /**
   * Applications environment object {language, context}
   */
  environment: PropTypes.object.isRequired,
  /**
   * History locatin object
   */
  location: PropTypes.object,
};

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(LanguageButtons);
