import React from "react";
import { Vocabulary } from "Libs/Vocabulary.js";
import PropTypes from "prop-types";
import { connect } from "react-redux";

/**
 * Component that renders when file is not found
 */
export function NoMatch(props) {
  return (
    <div>
      <div className="title not-found not-found-title">Error 404</div>
      <div className="not-found title">
        {Vocabulary.PageNotFound[props.language]}
      </div>
    </div>
  );
}

function mapStateToProps(state) {
   return {
        language: state.environment.language
    }
}
export default connect(
  mapStateToProps)(NoMatch);
