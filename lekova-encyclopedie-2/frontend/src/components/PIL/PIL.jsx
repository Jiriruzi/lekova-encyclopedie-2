import React from "react";
import { bindActionCreators } from "redux";
import utils from "Libs/utils.js";
import { connect } from "react-redux";
import { getDocument } from "Actions/documentsFetches.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faDownload from "@fortawesome/fontawesome-free-solid/faDownload";
import PropTypes from "prop-types";

/**
 * Component that renders PIL
 */
export class PIL extends React.Component {
  getHtml(doc) {
    let text = utils.getValue(
      doc,
      this.props.context,
      this.props.language,
      "pilHTML"
    ).value;
    if (text == null) {
      return (
        <div className="row col-xs-12">
          {Vocabulary.NoPil[this.props.language]}
        </div>
      );
    }
    text = utils.repairUnicode(text);
    text = { __html: text };
    return (
      <div className="row col-xs-12">
        <a
          className="fa-button fa-download"
          href={
            utils.getValue(
              doc,
              this.props.context,
              this.props.language,
              "pilLink"
            ).value
          }
        >
          {Vocabulary.Download[this.props.language]}{" "}
          {Vocabulary.PIL[this.props.language]}&nbsp;
          <FontAwesomeIcon icon={faDownload} />
        </a>
        <div className="col-xs-12" />
        {<div className="scroll-box pil" dangerouslySetInnerHTML={text} />}{" "}
      </div>
    );
  }

  render() {
    if (!this.props.document || this.props.document.loading) {
      return null;
    } else if (this.props.document.error) {
      return (
        <div className="row col-xs-12">
          {Vocabulary.NoPil[this.props.language]}{" "}
        </div>
      );
    }
    return this.getHtml(this.props.document.doc);
  }
}

function mapStateToProps(state) {
  return {
    document: state.documents.PIL
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument
    },
    dispatch
  );
}
PIL.propTypes = {
  /**
   * Which component should of the document should be displayed
   */
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Data of this document
   */
  document: PropTypes.object,

};

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(PIL);
