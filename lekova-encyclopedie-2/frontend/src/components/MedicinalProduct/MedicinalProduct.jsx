import React from "react";
import { connect } from "react-redux";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import ATCConcept from "Components/ATCConcept/ATCConcept.jsx";
import InfoFromConf from "Components/InfoFromConf/InfoFromConf.jsx";
import { getDocument } from "Actions/documentsFetches.js";
import { bindActionCreators } from "redux";
import utils from "Libs/utils.js";
import PIL from "Components/PIL/PIL.jsx";
import SPC from "Components/SPC/SPC.jsx";
import { updateHistory } from "Actions/userFetches.js";
import PropTypes from "prop-types";

/**
 * Special component for MedicinalProduct
 * that gets configuration file and data file and than display data requiered data
 */

export class MedicinalProduct extends React.Component {
  componentWillMount() {
    this.props.getDocument(this.props.type, this.props.id);
    this.props.getDocument("Configuration", this.props.type);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.type !== nextProps.type) {
      this.props.getDocument("Configuration", nextProps.type);
      this.props.getDocument(nextProps.type, nextProps.id);
    } else if (this.props.id !== nextProps.id) {
      this.props.getDocument(nextProps.type, nextProps.id);
    }
    if (
      nextProps.user &&
      nextProps.user.role === "expert" &&
      nextProps.document &&
      nextProps.document.doc &&
      nextProps.document !== this.props.document
    ) {
      this.props.updateHistory(
        this.props.type,
        this.props.id,
        nextProps.document.doc[`${this.props.context}title`]
      );
    }
  }
  // component needs also data of any medicinalProductPackaging with this medicinal product and ATCConcept of this medicinal product
  componentWillUpdate(newProps) {
    if (
      this.props.document &&
      newProps.document &&
      newProps.document.doc &&
      this.props.document.doc !== newProps.document.doc
    ) {
      if (
        newProps.document.doc[
          `${this.props.context}hasMedicinalProductPackaging`
        ]
      ) {
        if (
          newProps.document.doc[
            `${this.props.context}hasMedicinalProductPackaging`
          ] instanceof Array &&
          newProps.document.doc[
            `${this.props.context}hasMedicinalProductPackaging`
          ].length > 0
        ) {
          this.props.getDocument(
            "MedicinalProductPackaging",
            newProps.document.doc[
              `${this.props.context}hasMedicinalProductPackaging`
            ][0]["@id"]
          );
          this.props.getDocument(
            "PIL",
            newProps.document.doc[
              `${this.props.context}hasMedicinalProductPackaging`
            ][0]["@id"]
          );
        } else if (
          !(
            newProps.document.doc[
              `${this.props.context}hasMedicinalProductPackaging`
            ] instanceof Array
          )
        ) {
          this.props.getDocument(
            "MedicinalProductPackaging",
            newProps.document.doc[
              `${this.props.context}hasMedicinalProductPackaging`
            ]["@id"]
          );
          this.props.getDocument(
            "PIL",
            newProps.document.doc[
              `${this.props.context}hasMedicinalProductPackaging`
            ]["@id"]
          );
        }
      }
    }
  }

  render() {
    let title = "";
    if (this.props.document) {
      title = `${
        utils.getValue(
          this.props.document.doc,
          this.props.context,
          this.props.language,
          "title"
        ).value
      }${` ${utils.getValue(
        this.props.document.doc,
        this.props.context,
        this.props.language,
        "hasTitleSupplement"
      ).value || ""}`}`;
    }
    const routes = [
      { path: "detail", label: "Detail" },
      { path: "pil", label: "PIL" },
      { path: "spc", label: "SPC" },
      { path: "cheapest", label: "Cheapest" }
    ];
    let isLoading =
      !this.props.document ||
      this.props.document.loading ||
      (!this.props.conf || this.props.conf.loading);
    let error =
      (this.props.document && this.props.document.error) ||
      (this.props.conf && this.props.conf.error);
    let { props } = this;
    let component = null;

    if (!isLoading && !error) {
      props = {
        ...props,
        document: { ...props.document, doc: { ...props.document.doc } }
      };
      component = <InfoFromConf {...props} />;
      //handling different components
      if (props.component === "?pil" && props.pil) {
        component = <PIL {...props} />;
        const isLoadingPil = props.pil.isLoading;
        isLoading = isLoading || isLoadingPil;
      } else if (props.component === "?spc" && props.pack) {
        isLoading = props.pack.loading;
        const errorPack = props.pack.error;
        error = errorPack;
        if (props.pack.doc) {
          component = <SPC {...props} doc={props.pack.doc} />;
        }
      } else if (props.component === "?cheapest") {
        if (this.props.document.doc[`${this.props.context}hasATCConcept`]) {
          let atcId = utils.getLink(
            this.props.document.doc[`${this.props.context}hasATCConcept`],
            "hasATCConcept"
          );
          atcId = atcId.split("/");
          atcId = atcId[atcId.length - 1];
          component = (
            <ATCConcept {...props} notOwnPage routes={routes} id={atcId} />
          );
        }
      }
    }
    return (
      <PageContainer
        type="MedicinalProduct"
        id={this.props.id}
        language={this.props.language}
        context={this.props.context}
        routes={routes}
        title={title}
        isLoading={isLoading}
        error={error}
      >
        {component}
      </PageContainer>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    component: ownProps.location.search,
    context: state.environment.context,
    language: state.environment.language,
    id: ownProps.match ? ownProps.match.params.id : null,
    type: "MedicinalProduct",
    document: state.documents.MedicinalProduct,
    conf: state.documents.Configuration,
    pil: state.documents.PIL,
    pack: state.documents.MedicinalProductPackaging,
    user: state.user
  };
}

MedicinalProduct.propTypes = {
  /**
   * Which component should of the document should be displayed
   */
  component: PropTypes.string,
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * id of document
   */
  id: PropTypes.string.isRequired,
  /**
   * Object containing info about user
   */
  user: PropTypes.object,
  /**
   * Type of the document
   */
  type: PropTypes.string.isRequired,
  /**
   * Data of this document
   */
  document: PropTypes.object,
  /**
   * Configuration for this document
   */
  conf: PropTypes.object,
  /**
   * PIL for this medicinal product
   */

  pil: PropTypes.object,
  /**
   * Document of packaging of this medicinal product to get some additional info
   */

  pack: PropTypes.object
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument,
      updateHistory
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(MedicinalProduct);
