import React from "react";
import { connect } from "react-redux";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import { getDocument } from "Actions/documentsFetches.js";
import { bindActionCreators } from "redux";
import { Vocabulary } from "Libs/Vocabulary";
import Table from "Components/Table/Table.jsx";
import utils from "Libs/utils.js";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faChevronCircleDown from "@fortawesome/fontawesome-free-solid/faChevronCircleDown";
import faChevronCircleUp from "@fortawesome/fontawesome-free-solid/faChevronCircleUp";
import PropTypes from "prop-types";

/**
 *  Special component for medicinalProductPackaging that gets configuration file and data file and than display data requiered data
 */
export class PositiveList extends React.Component {
  constructor(props) {
    super(props);
    this.filter = this.filter.bind(this);
    this.writing = this.writing.bind(this);
    this.state = {
      ing: "",
      writing: 0,
      readMore: false
    };
  }
  componentWillMount() {
    this.props.getDocument("MedicinalProductPackaging", "positive-list");
    this.props.getDocument("Configuration", "MedicinalProduct");
  }
/**
 *  If the user is writing, dont do anything, if not, set the filter
 * @param {number} now 
 * @param {string} value 
 */
  filter(now, value) {
    if (this.state.writing === now) {
      this.setState({
        ing: value
      });
    }
  }
  /**
   *  if the user is writing handle the state
   * @param {event} e 
   */
  writing(e) {
    const value = e.target.value;
    const now = Date.now();
    this.setState({ writing: now });
    setTimeout(this.filter, 747, now, value);
  }
  /**
   *  Filters items that should be displayed based on the input
   */
  getFilteredItems() {
    let newDoc = null;
    if (this.props.document && this.props.document.doc) {
      newDoc = this.props.document.doc;
      if (this.state.ing.toLowerCase()) {
        newDoc = [];
        for (
          let i = 0;
          i <
          this.props.document.doc[
            `${this.props.context}hasMedicinalProductPackaging`
          ].length;
          i++
        ) {
          const pack = this.props.document.doc[
            `${this.props.context}hasMedicinalProductPackaging`
          ][i];
          if (pack[`${this.props.context}hasActiveIngredient`]) {
            if (
              pack[`${this.props.context}hasActiveIngredient`] instanceof Array
            ) {
              for (
                let j = 0;
                j < pack[`${this.props.context}hasActiveIngredient`].length;
                j++
              ) {
                if (
                  utils
                    .getValue(
                      this.props.document.doc[
                        `${this.props.context}hasMedicinalProductPackaging`
                      ][i][`${this.props.context}hasActiveIngredient`][j],
                      this.props.context,
                      this.props.language,
                      "title"
                    )
                    .value.toLowerCase()
                    .includes(this.state.ing.toLowerCase())
                ) {
                  newDoc.push(pack);
                }
              }
            } else if (
              utils
                .getValue(
                  this.props.document.doc[
                    `${this.props.context}hasMedicinalProductPackaging`
                  ][i][`${this.props.context}hasActiveIngredient`],
                  this.props.context,
                  this.props.language,
                  "title"
                )
                .value.toLowerCase()
                .includes(this.state.ing.toLowerCase())
            ) {
              newDoc.push(pack);
            }
          }
        }
        newDoc = {
          [`${this.props.context}hasMedicinalProductPackaging`]: newDoc
        };
      }
    }
    return newDoc;
  }
  render() {
    const isLoading =
      !this.props.document ||
      this.props.document.loading ||
      (!this.props.conf || this.props.conf.loading);

    const error =
      (this.props.document && this.props.document.error) ||
      (this.props.conf && this.props.conf.error);
    let table = {};
    let newDoc = this.getFilteredItems();
    if (
      this.props.conf &&
      this.props.conf.doc &&
      this.props.conf.doc.table &&
      this.props.conf.doc.table[0]
    ) {
      [table] = JSON.parse(JSON.stringify(this.props.conf.doc.table));
      const del = table.items[0];
      table.items.push(del);
      table.items.splice(0, 1, {
        label: "ActiveIngredient",
        path: [["hasActiveIngredient", "title"]],
        class: "left",
        multiValue: true
      });
      table.items[1].label = "MedicinalProductPackaging";
    }
    return (
      <PageContainer
        id={this.props.id}
        language={this.props.language}
        context={this.props.context}
        title={Vocabulary.PositiveList[this.props.language]}
        isLoading={isLoading}
        error={error}
      >
        <div className="col-xs-12 positive-list-desc">
          {Vocabulary.PositiveListDesc[this.props.language]}
          <a
            style={{ cursor: "pointer" }}
            onClick={() => this.setState({ readMore: !this.state.readMore })}
            title={
              this.state.readMore
                ? Vocabulary.ReadMore[this.props.language]
                : Vocabulary.ReadLess[this.props.language]
            }
          >
            {" "}
            <FontAwesomeIcon
              icon={
                this.state.readMore ? faChevronCircleUp : faChevronCircleDown
              }
              className="fa-expand"
            />
          </a>
          {this.state.readMore ? (
            <div>{Vocabulary.PositiveListDescLong[this.props.language]}</div>
          ) : null}
        </div>
        <div className="row col-xs-12 important-prop">
          <div className="basic-label filter">
            {Vocabulary.FilterByIngredient[this.props.language]}:
          </div>
        </div>
        <div className="col-md-6 col-12">
          <input
            className="form-control filtring-search"
            onChange={this.writing}
            title={Vocabulary.IngredientOne[this.props.language]}
            placeholder={Vocabulary.IngredientOne[this.props.language]}
          />
          <div className="packaging-prop">
            {Vocabulary.Results[this.props.language]}:
            {newDoc &&
            newDoc[`${this.props.context}hasMedicinalProductPackaging`]
              ? newDoc[`${this.props.context}hasMedicinalProductPackaging`]
                  .length
              : 0}
          </div>
        </div>
        <Table
          doNotSort
          withoutTitle
          language={this.props.language}
          context={this.props.context}
          datDoc={newDoc}
          confDoc={table}
        />
      </PageContainer>
    );
  }
}
function mapStateToProps(state) {
  return {
    context: state.environment.context,
    language: state.environment.language,
    document: state.documents.MedicinalProductPackaging,
    conf: state.documents.Configuration
  };
}
PositiveList.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Part of data from which should be the information taken
   */
  datDoc: PropTypes.object,
  /**
   * Configuration for this info of the document
   */
  confDoc: PropTypes.object
};
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(PositiveList);
