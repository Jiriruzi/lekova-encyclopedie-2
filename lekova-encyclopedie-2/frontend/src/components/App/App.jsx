import React from 'react';
import Header from 'Components/App/Header.jsx';
import Main from 'Components/App/Main.jsx';
/**
 * Main component of application
 */
export const App = () => (
    <div>
        <Header />
        <Main />
    </div>
);
export default App;
