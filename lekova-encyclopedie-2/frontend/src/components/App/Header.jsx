import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import SearchBarHeader from "Components/Search/SearchBarHeader.jsx";
import {
  selectLanguage,
  showUserBox,
  hideUserBox,
  hideShowUserBox
} from "Actions/environmentActions.js";
import UserBox from "Components/User/UserBox.jsx";
import LanguageButtons from "Components/LanguageButtons/LanguageButtons.jsx";
import * as LogoUrl from "Images/encyklopedie_logo-homepage.png";
import { withRouter } from "react-router-dom";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faUserCircle from "@fortawesome/fontawesome-free-solid/faUserCircle";
import faTablets from "@fortawesome/fontawesome-free-solid/faTablets";
import PropTypes from "prop-types";
/**
 * Aplications header
 */
export class Header extends React.Component {
  componentWillReceiveProps(newProps) {
    if (this.props.location !== newProps.location) {
      this.props.hideUserBox();
    }
  }

  getUsernameStyle() {
    let marg = 0;
    if (this.props.user.username) {
      const fontSize = "14px";
      const test = document.getElementById("test");
      test.style.fontSize = fontSize;
      test.style.fontWeight = 700;
      test.style.fontFamily = "Helvetica Neue, Helvetica, Arial, sans-serif";
      test.innerHTML = this.props.user.username;
      const width = test.clientWidth;
      marg = (-width + 29) / 2;
    }
    return {
      marginTop: "-4px",
      marginLeft: `${marg}px`,
      zIndex: 1001,
      color: "#fff",
      textShadow: " 1px 0 0 #000, 0 -1px 0 #000, 0 1px 0 #000, -1px 0 0 #000"
    };
  }
  /**
   * Return css class 'active' if lang is current app language
   * @param {string} lang
   */
  activeLanguage(lang) {
    if (lang === this.props.environment.language) {
      return "active";
    }
    return "";
  }

  render() {
    return (
      <div className="container-fluid header">
        <div className="row">
          <Link to="/">
            <img
              className="col-xs-12 hidden-md hidden-xl hidden-lg logo-row logo"
              alt="LELogo"
              src={LogoUrl.default}
            />
          </Link>
        </div>
        <div className="row header-row">
          <div className="logo-image hidden-sm hidden-xs">
            <Link to="/">
              <img
                className="col-md-5 col-lg-5 col-xl-5 logo"
                alt="LELogo"
                src={LogoUrl.default}
              />
            </Link>
          </div>
          <div className="col-xs-7 col-sm-7 col-md-4 col-lg-4 col-lg-4 vcenter">
            <div className="col-xs-9 search-bar">
              <SearchBarHeader language={this.props.environment.language} />
            </div>
            <div
              style={{
                paddingLeft: "0px"
              }}
              className="col-xs-3 hidden-sm-up"
            >
              <Link
                to="/Interactions"
                className="interactions-button img-button"
              >
                <em className="fa fa-tablets">
                  <FontAwesomeIcon icon={faTablets} className="fa-inverse" />
                </em>
              </Link>
            </div>
          </div>
          <div
            onMouseEnter={this.props.showUserBox}
            onMouseLeave={this.props.hideUserBox}
            className="col-xs-4 col-sm-3 col-md-1 col-lg-1 vcenter user-box-container"
          >
            <button
              style={{
                marginLeft: "-6px"
              }}
              id="user-button"
              className="img-button"
              onClick={this.props.hideShowUserBox}
            >
              <em className="user-icon fa">
                <FontAwesomeIcon icon={faUserCircle} />
                <div className="username" style={this.getUsernameStyle()}>
                  {this.props.user.username}
                </div>
              </em>
            </button>
            <UserBox
              role={this.props.user.role}
              show={this.props.environment.showUserBox}
              username={this.props.user.username}
              language={this.props.environment.language}
            />
          </div>
          <LanguageButtons />
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    environment: state.environment,
    user: state.user
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      selectLanguage,
      hideUserBox,
      hideShowUserBox,
      showUserBox
    },
    dispatch
  );
}

Header.propTypes = {
  /**
   * Object determining applacation`s context, language and info if user box is opened
   */

  environment: PropTypes.object.isRequired,
  /**
   * Object containing info about logged user
   */
  user: PropTypes.object.isRequired,
  /**
   * History location object
   */
  location: PropTypes.object.isRequired
};
export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(Header)
);
