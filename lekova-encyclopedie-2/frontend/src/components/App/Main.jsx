import React, { lazy, Suspense } from "react";
import { Switch, withRouter, Route } from "react-router-dom";
const Search = lazy(() => import("Components/Search/Search.jsx"));
const NoMatch = lazy(() => import("Components/NoMatch/NoMatch.jsx"));
import MedicinalProductPackaging from "Components/MedicinalProductPackaging/MedicinalProductPackaging.jsx";
import MedicinalProduct from "Components/MedicinalProduct/MedicinalProduct.jsx";
import DiseaseOrFinding from "Components/DiseaseOrFinding/DiseaseOrFinding.jsx";
import Ingredient from "Components/Ingredient/Ingredient.jsx";
import DrugsProp from "Components/DrugsProp/DrugsProp.jsx";
import ATCConcept from "Components/ATCConcept/ATCConcept.jsx";
const PositiveList = lazy(() =>
  import("Components/PositiveList/PositiveList.jsx")
);
const RegistrationForm = lazy(() =>
  import("Components/User/RegistrationForm.jsx")
);
const Medication = lazy(() => import("Components/User/Medication.jsx"));
const EncHistory = lazy(() => import("Components/User/EncHistory.jsx"));
const PasswordChange = lazy(() => import("Components/User/PasswordChange.jsx"));
const ForgottenPassword = lazy(() =>
  import("Components/User/ForgottenPassword.jsx")
);
const EditUser = lazy(() => import("Components/User/EditUser.jsx"));
const ResetPassword = lazy(() => import("Components/User/ResetPassword.jsx"));
const EditMedicament = lazy(() => import("Components/User/EditMedicament.jsx"));
const Login = lazy(() => import("Components/User/Login.jsx"));
const InteractionsWithForm = lazy(() =>
  import("Components/Interactions/InteractionsWithForm.jsx")
);
import Home from "Components/Home/Home.jsx";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setSearchValueHeader, resetResults } from "Actions/searchActions.js";
import { getSession } from "Actions/userFetches.js";
import Spinner from "react-spinkit";
import utils from "Libs/utils.js";
import { selectLanguage } from "Actions/environmentActions.js";

export class Main extends React.Component {
  componentWillMount() {
    this.props.getSession();
    this.props.selectLanguage(utils.getLanguage())
  }
  componentWillUpdate(nextProps) {
    if (nextProps.location.pathname !== this.props.location.pathname) {
      this.props.getSession();
      this.props.setSearchValueHeader("");
      this.props.resetResults("");
    }
  }
  render() {
    return (
      <main>
        <Suspense
          fallback={
            <div className="page-spinner">
              <Spinner name="ball-spin-fade-loader" color="#3ea898" />
            </div>
          }
        >
          <Switch>
            <Route
              exact
              path="/MedicinalProductPackaging/:id"
              component={MedicinalProductPackaging}
            />
            <Route
              exact
              path="/MedicinalProduct/:id"
              component={MedicinalProduct}
            />
            <Route exact path="/Ingredient/:id" component={Ingredient} />
            <Route
              exact
              path="/DiseaseOrFinding/:id"
              component={DiseaseOrFinding}
            />
            <Route exact path="/ATCConcept/:id" component={ATCConcept} />
            <Route
              exact
              path="/Registration"
              component={props => <RegistrationForm {...props} />}
            />
            <Route
              exact
              path="/EditUser"
              component={props => <EditUser {...props} />}
            />
            <Route
              exact
              path="/PositiveList"
              component={props => <PositiveList {...props} />}
            />
            <Route
              exact
              path="/Login"
              component={props => <Login {...props} />}
            />
            <Route
              exact
              path="/Interactions"
              component={props => <InteractionsWithForm {...props} />}
            />
            <Route
              exact
              path="/Medication"
              component={props => <Medication {...props} />}
            />
            <Route
              exact
              path="/History"
              component={props => <EncHistory {...props} />}
            />
            <Route
              exact
              path="/PasswordChange"
              component={props => <PasswordChange {...props} />}
            />
            <Route
              exact
              path="/ResetPassword/:token"
              component={props => <ResetPassword {...props} />}
            />
            <Route
              exact
              path="/EditMedicament/:key"
              component={props => <EditMedicament {...props} />}
            />
            <Route
              exact
              path="/AddMedicament"
              component={props => <EditMedicament {...props} />}
            />
            <Route
              exact
              path="/ForgottenPassword"
              component={props => <ForgottenPassword {...props} />}
            />
            <Route path="/Search" component={props => <Search {...props} />} />
            <Route exact path="/" component={props => <Home {...props} />} />
            <Route exact path="/:type/:id" component={DrugsProp} />
            <Route path="*" component={props => <NoMatch {...props} />} />
          </Switch>
        </Suspense>
      </main>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getSession,
      setSearchValueHeader,
      resetResults,
      selectLanguage
    },
    dispatch
  );
}
export default withRouter(
  connect(
    null,
    matchDispatchToProps
  )(Main)
);
