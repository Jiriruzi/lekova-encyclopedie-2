import React from "react";
import { connect } from "react-redux";
import SearchBar from "Components/Search/SearchBar.jsx";
import { bindActionCreators } from "redux";
import {
  setSearchValueInForm,
  resetSearchValueInForm,
  suggest
} from "Actions/searchActions.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import PropTypes from "prop-types";
/**
 * Component for searchbars in forms
 */
export class SearchBarMedication extends React.Component {
  componentWillUnmount() {
    this.props.resetSearchValueInForm();
  }
  emptySuggestions() {
    if (this.props.loading || (this.props.addOwnValue && this.props.value)) {
      return " not-empty";
    }
    if (
      this.props.sections.length === 0 ||
      this.props.sections === null ||
      this.props.value === 0
    ) {
      return "";
    }
    return "not-empty";
  }
  emptyInput(evt) {
    if (this.props.value.length === 0) {
      evt.preventDefault();
    }
  }
  render() {
    return (
      <div
        className={
          (this.props.formType
            ? "form-control "
            : "medication-search vcenter search-bar ") + this.emptySuggestions()
        }
      >
        <SearchBar
          aditionalOnChange={this.props.aditionalOnChange}
          placeholder={Vocabulary.Add[this.props.environment.language]}
          addOwnValue={this.props.addOwnValue}
          setSearchValue={this.props.setSearchValueInForm}
          suggest={this.props.suggest}
          sections={this.props.sections}
          loading={this.props.loading}
          value={this.props.value}
          environment={this.props.environment}
          onSuggestionSelected={this.props.onSuggestionSelected}
          makeQuery={this.props.makeQuery}
          onEnter={this.props.onEnter}
          id="searchInForm"
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    loading: state.search.loading,
    sections: state.search.sections,
    value: state.search.valueInForm,
    environment: state.environment
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      suggest,
      setSearchValueInForm,
      resetSearchValueInForm
    },
    dispatch
  );
}
SearchBarMedication.propTypes = {
  /**
   * Browsing history of app
   */
  history: PropTypes.object,
  /**
   * Environment state of application (language, context)
   */
  environment: PropTypes.object.isRequired,
  /**
   * Value for search
   */
  value: PropTypes.string.isRequired,

  /**
   * Array of sections with suggestions
   */
  sections: PropTypes.array,
  /**
   * Additional function to call when onChange is fired
   */
  additionalOnChange: PropTypes.func,
  /**
   * Function returning query for the searchbar
   */
  makeQuery: PropTypes.func,
  loading: PropTypes.bool.isRequired
};
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(SearchBarMedication);
