import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import utils from "Libs/utils.js";
import { getAllResults } from "Actions/searchActions.js";
import { bindActionCreators } from "redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import PropTypes from "prop-types";

function renderResult(result) {
  return <div>{utils.repairUnicode(result.enc_title)}</div>;
}

function makeQuery(value) {
  value = decodeURI(value);
  value = value.replace(/ /g, "\\%20");
  if (!value[0]) {
    return "(enc_title:*)";
  }
  return `(
    (enc_title:${value})^4 OR (enc_title:${value[0].toUpperCase() +
    value.substring(
      1
    )}*)^3 OR (enc_title:${value})^3 OR (enc_title:${value.toUpperCase()})^3 OR
    (enc_title:${value}*)^2 OR
     (enc_title:${value.toUpperCase()}*)^2 OR
      (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^2 OR
       enc_title:*${value[0].toUpperCase() + value.substring(1)}* OR
        enc_title:*${value}* OR enc_title:*${value.toUpperCase()}* OR
         enc_title:*${value.toLowerCase()}*)`;
}

/**
 * Component displaying results of search
 */
export class Search extends React.Component {
  componentWillMount() {
    this.props.getAllResults(makeQuery(this.props.value), this.props.language);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.value !== nextProps.value) {
      this.props.getAllResults(makeQuery(nextProps.value), this.props.language);
    }
  }

  renderResults(results) {
    const items = [];
    const types = [];
    const sections = {};
    for (let i = 0; i < results.length; i++) {
      if (!sections[results[i]._type]) {
        sections[results[i]._type] = [];
        types.push(results[i]._type);
      }
      sections[results[i]._type].push(results[i]);
    }

    types.sort((a, b) =>
      Vocabulary[a[0].toUpperCase() + a.substring(1)][
        this.props.language
      ].localeCompare(
        Vocabulary[b[0].toUpperCase() + b.substring(1)][this.props.language]
      )
    );
    for (let i = 0; i < types.length; i++) {
      sections[types[i]].sort((a, b) =>
        utils
          .repairUnicode(a.enc_title)
          .localeCompare(utils.repairUnicode(b.enc_title))
      );
      sections[types[i]] = sections[types[i]].map((item, j) => (
        <div key={j}>
          <Link to={`/${item._type}/${utils.makePath(item._id)}`}>
            {renderResult(item)}
          </Link>
        </div>
      ));
    }
    let trinity = [];
    for (let i = 0; i < types.length; i++) {
      trinity.push(
        <div key={i} className={`col-xs-6 col-md-4 finding-list`}>
          <div className="list-label">
            {
              Vocabulary[types[i][0].toUpperCase() + types[i].substring(1)][
                this.props.language
              ]
            }
          </div>
          <div className={`normal-text${types.length / 3 > 1 ? " list" : ""}`}>
            {sections[types[i]]}
          </div>
        </div>
      );

      if (i % 3 === 2 || i + 1 === types.length) {
        items.push(<div class="row">{trinity}</div>);
        trinity = [];
      }
    }
    return items.length ? items : null;
  }

  render() {
    return (
      <PageContainer
        isLoading={this.props.loading}
        title={`${Vocabulary.SearchResults[this.props.language]}: ${
          this.props.value
        }`}
      >
        <div className="row col-xs-12">
          {Vocabulary.Results[this.props.language]}:{" "}
          {this.props.results.length || 0}
        </div>
        <div>{this.renderResults(this.props.results)}</div>
      </PageContainer>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getAllResults
    },
    dispatch
  );
}
function mapStateToProps(state, ownProps) {
  return {
    results: state.search.resultsAll,
    loading: state.search.loadingResultsAll,
    value: ownProps.location.search ? ownProps.location.search.substr(1) : "",
    language: state.environment.language,
    context: state.environment.context
  };
}
Search.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Value for search
   */
  value: PropTypes.string.isRequired,
  /**
   * Found results
   */
  results: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired
};

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(Search);
