import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import utils from 'Libs/utils.js';
import SearchBar from 'Components/Search/SearchBar.jsx';
import {setSearchValueHeader, suggest} from 'Actions/searchActions.js';
import {withRouter} from 'react-router-dom';
import PropTypes from "prop-types";



const getType = (suggestion) => {
    let type = suggestion;
    if (!type[0]) {
        return null;
    }
    type = type[0].toUpperCase() + type.substring(1);
    switch (type) {
        case 'ActiveIngredient':
            return 'Ingredient';
    }
    return type;
};

function makeQuery(value) {
    value = value.replace(/ /g, '\\%20');
    if (!value[0]) {
        return '(enc_title:*)';
    }
    return `((enc_title:${value})^4 OR (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^3 OR (enc_title:${value})^3 OR (enc_title:${value.toUpperCase()})^3 OR
    (enc_title:${value}*)^2 OR
     (enc_title:${value.toUpperCase()}*)^2 OR
        (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^2 OR
        enc_title:*${value[0].toUpperCase() + value.substring(1)}* OR
        enc_title:*${value}* OR
        enc_title:*${value.toUpperCase()}* OR
        enc_title:*${value.toLowerCase()}*) AND (_type:medicinalProduct OR _type:ingredient OR _type:diseaseOrFinding)`;
}
/**
 * Searchbar for searchbar in header
 */
export class SearchBarHeader extends React.Component {
    constructor(props) {
        super(props);
        this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
        this.redirectToSearch = this.redirectToSearch.bind(this);
    }
    onSuggestionSelected(event, suggestionValue) {
        this.props.history.push(
            `/${getType(suggestionValue.suggestion._type)}/${utils.makePath(suggestionValue.suggestion._id)}`
        );
    }
    emptySuggestions() {
        if (this.props.loading) {
            return ' not-empty';
        }
        if (this.props.sections.length === 0 || this.props.sections === null || this.props.value === 0) {
            return '';
        }
        return ' not-empty';
    }
    emptyInput(evt) {
        if (this.props.value.length === 0) {
            evt.preventDefault();
        }
    }
    redirectToSearch() {
        this.props.history.push(`/Search?${this.props.value}`);
    }
    render() {
        return (
            <div className="input-group">
                <button onClick={this.redirectToSearch} className="vcenter search-button">
                    <span className="glyphicon glyphicon-search" />
                </button>
                <div className={`vcenter search-bar${this.emptySuggestions()}`}>
                    <SearchBar
                        setSearchValue={this.props.setSearchValueHeader}
                        suggest={this.props.suggest}
                        sections={this.props.sections}
                        value={this.props.value}
                        environment={this.props.environment}
                        onSuggestionSelected={this.onSuggestionSelected}
                        makeQuery={makeQuery}
                        loading={this.props.loading}
                        onEnter={this.redirectToSearch}
                        id="searchInHeader"
                    />

                </div>
            </div >
        );
    }
}

function mapStateToProps(state) {
    return {loading: state.search.loading,
        sections: state.search.sections,
        value: state.search.valueHeader,
        environment: state.environment};
}
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        suggest,
        setSearchValueHeader
    }, dispatch);
}

SearchBarHeader.propTypes = {
    /**
     * Browsing history of app 
     */
    history: PropTypes.object.isRequired,
    /**
     * Environment state of application (language, context)
     */
    environment: PropTypes.object.isRequired,
    /**
     * Value for search
     */
    value: PropTypes.string.isRequired, 

    /**
     * Array of sections with suggestions
     */
    sections: PropTypes.array,

    loading: PropTypes.bool.isRequired
  };
export default withRouter(connect(mapStateToProps, matchDispatchToProps)(SearchBarHeader));
