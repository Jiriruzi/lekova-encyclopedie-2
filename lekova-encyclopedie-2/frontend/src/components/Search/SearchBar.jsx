import React from "react";
import Autosuggest from "react-autosuggest";
import { Vocabulary } from "Libs/Vocabulary.js";
import utils from "Libs/utils.js";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";

const getType = suggestion => {
  let type = suggestion;
  type = type[0].toUpperCase() + type.substring(1);
  return type;
};

const getSuggestionValue = suggestion => suggestion.enc_title;

function getSectionSuggestions(section) {
  return section.items;
}
function onSuggestionsClearRequested() {}
/**
 * General component for searchbars 
 */
export class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(
      this
    );
    this.onChange = this.onChange.bind(this);
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
    this.renderSuggestion = this.renderSuggestion.bind(this);
    this.keyPressedOrSugesstionSelected = this.keyPressedOrSugesstionSelected.bind(
      this
    );
  }

  onSuggestionSelected(event, suggestionValue) {
    if (this.props.loading && suggestionValue !== "Own") {
      return;
    }
    this.props.onSuggestionSelected(event, suggestionValue);
  }
  onChange(event, { newValue }) {
    this.props.setSearchValue(newValue);
    if (this.props.aditionalOnChange) {
      this.props.aditionalOnChange(newValue);
    }
  }

  onSuggestionsFetchRequested({ value }) {
    this.props.suggest(
      value,
      this.props.environment.language,
      this.props.makeQuery
    );
  }

  keyPressedOrSugesstionSelected(e, value) {
    if (this.eventControll === e && e.key === "Enter") {
      this.eventControll = null;
      e.preventDefault();
      return;
    }
    if (!value) {
      if (e.key === "Enter" && this.props.onEnter) {
        this.props.onEnter(this.props.value || "");
        e.preventDefault();
      }
    } else {
      this.eventControll = e;
      this.onSuggestionSelected(e, value);
    }
  }

  emptyInput(evt) {
    if (this.props.value.length === 0) {
      evt.preventDefault();
    }
  }
  renderSectionTitle(section, language) {
    if (this.props.loading && !section.own) {
      return <div>{Vocabulary[section.name][language]}</div>;
    }
    return <div>{Vocabulary[getType(section.name)][language]}</div>;
  }

  renderSuggestion(suggestion) {
    if (this.props.loading && !suggestion.own) {
      return (
        <div className={this.props.loading ? "do-not-highlight" : ""}>
          {suggestion}
        </div>
      );
    }
    return (
      <div className="highlight">
        {utils.repairUnicode(suggestion.enc_title)}
      </div>
    );
  }

  render() {
    const inputProps = {
      title: Vocabulary.Search[this.props.environment.language],
      placeholder:
        this.props.placeholder ||
        Vocabulary.Search[this.props.environment.language],
      value: this.props.value || "",
      onChange: this.onChange,
      onKeyPress: this.keyPressedOrSugesstionSelected
    };
    // if the suggestions are loading, display spinner
    let suggestions = this.props.loading
      ? [
          {
            items: [
              <div className="search-spinner">
                <Spinner fadeIn="none" name="circle" color="#3ea898" />
              </div>
            ],
            name: "Searching"
          }
        ]
      : this.props.sections;
    if (this.props.addOwnValue && this.props.value) {
      suggestions = [
        {
          name: "Own",
          items: [
            {
              own: true,
              _type: "medicinalProduct",
              enc_title: this.props.value,
              _id: ""
            }
          ]
        },
        ...suggestions
      ];
    }
    return (
      <Autosuggest
        suggestions={suggestions}
        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
        onSuggestionsClearRequested={onSuggestionsClearRequested}
        getSuggestionValue={getSuggestionValue}
        renderSuggestion={this.renderSuggestion}
        inputProps={inputProps}
        onSuggestionSelected={this.keyPressedOrSugesstionSelected}
        multiSection
        id={this.props.id}
        getSectionSuggestions={getSectionSuggestions}
        renderSectionTitle={section =>
          this.renderSectionTitle(section, this.props.environment.language)
        }
      />
    );
  }
}
SearchBar.propTypes = {
  /**
   * Value for search
   */
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string,
  /**
   * Array of sections with suggestions
   */
  sections: PropTypes.array,
  /**
   * Additional function to call when onChange is fired
   */
  additionalOnChange: PropTypes.func,
  /**
   * Function returning query for the searchbar
   */
  makeQuery: PropTypes.func,
  loading: PropTypes.bool.isRequired
};
export default SearchBar;
