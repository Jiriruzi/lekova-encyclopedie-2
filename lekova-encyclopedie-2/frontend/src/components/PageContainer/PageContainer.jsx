import React from "react";
import RouteButtonGroup from "Components/Buttons/RouteButtonGroup.jsx";
import { Vocabulary } from "Libs/Vocabulary.js";
import Spinner from "react-spinkit";
import NoMatch from "Components/NoMatch/NoMatch.jsx";
import PropTypes from "prop-types";

// Container for displaying document pages

export function PageContainer(props) {
  if (props.isLoading) {
    return (
      <div className="page-spinner">
        <Spinner name="ball-spin-fade-loader" color="#3ea898" />
      </div>
    );
  } else if (props.error) {
    return <NoMatch />;
  }
  return (
    <div>
      <div className="row col-xs-12 title">{props.title}</div>
      {props.type ? (
        <div className="row col-xs-12 bold title-type">
          {Vocabulary[props.type][props.language]}
        </div>
      ) : null}
      <RouteButtonGroup routes={props.routes} />
      {props.children}
    </div>
  );
}

PageContainer.propTypes = {
  language: PropTypes.string,
  title: PropTypes.string,
  /**
   * Routes containing names of the routes and search params
   */
  routes: PropTypes.array
};

export default PageContainer;
