import React from "react";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import TextContainer from "Components/TextContainer/TextContainer.jsx";
import PropTypes from "prop-types";

/** Components that renders basic infrom of some file, with this patter
 * label:value ( for example: Title: Ibuprofen)
 */
export function BasicInfo(props) {
  let value;
  if (props.confDoc.property instanceof Array) {
    value = utils.getValueFromArrayPaths(
      props.datDoc,
      props.context,
      props.language,
      props.confDoc.property
    );
  } else {
    value = utils.getValue(
      props.datDoc,
      props.context,
      props.language,
      props.confDoc.property
    );
  }
  if (!value.value && !props.confDoc.justLabel) {
    return null;
  }
  let label = "";
  if (
    props.confDoc &&
    props.confDoc.label &&
    props.confDoc.label[props.language]
  ) {
    label = props.confDoc.label[props.language];
  } else if (
    props.confDoc.label &&
    Vocabulary[props.confDoc.label][props.language]
  ) {
    label = Vocabulary[props.confDoc.label][props.language];
  }
  if (!value) return null;
  return (
    <div
      className={`${
        props.confDoc.class === undefined ? "" : props.confDoc.class
      }`}
    >
      <div className="basic-label">{label}</div>
      <TextContainer
        className={props.confDoc.value ? props.confDoc.value.class : ""}
        value={value}
      />
    </div>
  );
}

BasicInfo.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Part of data from which should be the information taken
   */
  datDoc: PropTypes.object.isRequired,
  /**
   * Configuration for this info of the document
   */
  confDoc: PropTypes.object.isRequired
};

export default BasicInfo;
