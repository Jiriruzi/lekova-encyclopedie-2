import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
/**
 * Home text
 * @param {object} props 
 */
export function HomeDescription(props) {
  let text;
  if (props.language === "cs") {
    text = (
      <div>
        <div>
          Nevíte, jak začít s Lékovou encyklopedií pracovat? Zadejte do
          vyhledávacího políčka text a aplikace Vám sama začne našeptávat. Pak
          buď vyberete z našeptávače nebo můžete vyhledávat pomocí tlačítka hledat.
          Co můžete v Lékové encyklopedii najít? Účinné látky jako{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965">
            Ibuprofen
          </Link>
          ,{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0013595">
            Methotrexát
          </Link>
          ,{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0029425">
            Simvastatin
          </Link>
          ... Léčivé přípravky jako{" "}
          <Link to="/MedicinalProduct/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fsukl%2Fmedicinal-product%2FANOPYRIN-100-MG">
            ANOPYRIN 100 MG
          </Link>
          ,{" "}
          <Link to="/MedicinalProduct/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fsukl%2Fmedicinal-product%2FDOPEGYT">
            DOPEGYT
          </Link>
          . Nálezy jako{" "}
          <Link to="/DiseaseOrFinding/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000002278">
            bolest
          </Link>
          ,{" "}
          <Link to="/DiseaseOrFinding/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000002858">
            sluneční spáleniny
          </Link>{" "}
          a k nim příslušné účinné látky. A spoustu dalších zajímavých informací
          v přehledné podobě. Můžete také zkontrolovat, zda mezi několika
          léčivými přípravky a účinnými látkami neexistují potenciální{" "}
          <Link to="/Interactions">interakce</Link>.
          <div />
          Registrovaní uživatelé také
          mohou využít dalších funkcí. Laici v oblasti zdravotnictví si zde mohou zaznamenávat a
          spravovat svou medikaci, expertům v oblasti zdravotnictví se bude
          zaznamenávat historie vyhledávání a oni si tak budou moci jednodušeji
          osvěžit potřebné informace.
        </div>
        <div className="home-title">Zdroje dat</div>
        <div>
          Základním zdrojem dat o dostupných léčivých přípravcích v ČR jsou pro
          Lékovou encyklopedii data{" "}
          <a href="http://www.sukl.cz/">
            Státního ústavu pro kontrolu léčiv (SÚKL).
          </a>{" "}
          Tato data zahrnují informace o velikostech balení, ATC skupinách,
          účinných látkách atd. V neposlední řadě také SÚKL poskytuje tzv.
          Souhrné údaje o léčivých přípravcích (SPC), které jsou ve formě
          dokumentů dostupné ke všem registrovaným léčivým přípravkům. Další
          data, která aplikaci používá a která jsou vybrány na základě požadavků
          lékařů, pochází z následujících datových zdrojů:
          <div className="row drug-row col-xs-12" />
          <ul>
            <li>
              <a href="https://www.ncbi.nlm.nih.gov/mesh">
                MeSH (Medical Subject Headings)
              </a>{" "}
              - poměrně rozsáhlý slovník pojmů primárně sloužící pro anotaci
              vědeckých článků, který je přeložen částečně do češtiny.
            </li>
            <li>
              <a href="https://nlk.cz/2018/01/mesh-cz-2018/">
                Czech translation of MeSH
              </a>{" "}
              - český překlad MeSH zajištuje Národní lékařská knihovna.
            </li>
            <li>
              <a href="https://www.nlm.nih.gov/research/umls/sourcereleasedocs/current/NDFRT/">
                NDF-RT (National Drug File - Reference Terminology
              </a>{" "}
              - obsahuje data o indikacích, kontraindikacích, interakcích apod.
            </li>
            <li>
              <a href="https://www.drugbank.ca/">DrugBank</a> - databáze
              obsahující informace o interakcích a především chemických
              vlastnostech.
            </li>
            <li>
              <a href="https://www.meddra.org/">MedDRA</a> - slovník pro
              regulatorní účely především pro zápis nežádoucích účinků léčiv.
            </li>
            <li>
              <a href="https://dailymed.nlm.nih.gov/dailymed/index.cfm">
                FDA SPL
              </a>{" "}
              - částečně strukturované popisy léčivých přípravků v USA.
            </li>
          </ul>
        </div>
      </div>
    );
  } else {
    text = (
      <div>
        <div>
          Don't you know how to start? Insert letters into the search field and
          the application will suggest the possibilities. Then you can either
          choose from the suggested options or you can hit the search button.
          What can you find in the Drug Encyclopedia? Active ingredients such as{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0010965">
            Ibuprofen
          </Link>
          ,{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0013595">
            Methotrexate
          </Link>
          ,{" "}
          <Link to="/Ingredient/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fdrug-encyclopedia%2Fingredient%2FM0029425">
            Simvastatin
          </Link>
          ... Medicinal products such as{" "}
          <Link to="/MedicinalProduct/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fsukl%2Fmedicinal-product%2FANOPYRIN-100-MG">
            ANOPYRIN 100 MG
          </Link>
          ,{" "}
          <Link to="/MedicinalProduct/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fsukl%2Fmedicinal-product%2FDOPEGYT">
            DOPEGYT
          </Link>
          ... Findings such{" "}
          <Link to="/DiseaseOrFinding/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000002278">
            pain
          </Link>
          ,{" "}
          <Link to="/DiseaseOrFinding/http%3A%2F%2Flinked.opendata.cz%2Fresource%2Fndfrt%2Fdisease%2FN0000002858">
            sunburn
          </Link>{" "}
          and the relevant active ingredients. And many other information in a
          well-arranged form. You can also check{" "}
          <Link to="/Interactions">interactions</Link> among several active
          ingredients and/or medicinal products.
          <div/>Registered users can also use other functions. Laymans in healthcare can save and manage their medication. For experts in healthcare, it will save their browsing history in LE2, so they can easily mantain previously found information.</div>
        <div className="home-title">Data sources</div>
        <div>
          The basic source of available medicinal products in the Czech Republic
          is the{" "}
          <a href="http://www.sukl.cz/">
            State Institute for Drug Control (SIDC)
          </a>
          . This data includes information about sizes of packagings, ATC
          groups, active ingredients etc. The SIDC also provides the Summaries
          of Product Characteristics (SPCs) which are available for each
          medicinal product in the form a text document. Other data, that
          aplication uses, were selected on the basis of user requirements and
          comes from the following data sources:
          <div className="row drug-row col-xs-12" />
          <ul>
            <li>
              <a href="https://www.ncbi.nlm.nih.gov/mesh">
                MeSH (Medical Subject Headings)
              </a>{" "}
              - extensive dictionary serving primarily for indexing scientific
              literature which is partially translated into Czech.
            </li>
            <li>
              <a href="https://nlk.cz/2018/01/mesh-cz-2018/">
                Czech translation of MeSH
              </a>{" "}
              - Czech translation of MeSH is maintained by the National Medical
              Library.
            </li>
            <li>
              <a href="https://www.nlm.nih.gov/research/umls/sourcereleasedocs/current/NDFRT/">
                NDF-RT (National Drug File - Reference Terminology
              </a>{" "}
              - Reference Terminology - contains data about indications,
              contraindications, interactions etc.
            </li>
            <li>
              <a href="https://www.drugbank.ca/">DrugBank</a> - database
              containing information about interactions a chemical properties of
              the active ingredients.
            </li>
            <li>
              <a href="https://www.meddra.org/">MedDRA</a> - dictionary for
              regulatory activities intended for the description of adverse
              effects.
            </li>
            <li>
              <a href="https://dailymed.nlm.nih.gov/dailymed/index.cfm">
                FDA SPL
              </a>{" "}
              - partially structured descriptions of medicinal products from the
              USA.
            </li>
          </ul>
        </div>
      </div>
    );
  }
  return <div>{text}</div>;
}

HomeDescription.propTypes = {
  language: PropTypes.string.isRequired
};

export default HomeDescription;
