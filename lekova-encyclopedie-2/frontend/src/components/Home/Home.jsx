import React from "react";
import { Vocabulary } from "Libs/Vocabulary.js";
import { connect } from "react-redux";
import HomeDescription from "./HomeDescription.jsx";
import HomeButtons from "./HomeButtons.jsx";
import PropTypes from "prop-types";
/**
 * Home screen
 * @param {object} props 
 */
export function Home(props) {
  return (
    <div>
      <HomeButtons language={props.language} role={props.role} logged={props.logged} />
      <div className="col-xs-12 row home-title welcome">
        {Vocabulary.Welcome[props.language]}
      </div>
      <div className="col-xs-12 row">
        <HomeDescription language={props.language} />
      </div>
    </div>
  );
}

function mapStateToProps(state) {
  return {
    language: state.environment.language,
    logged: !!state.user.username,
    role: state.user.role
  };
}

Home.propTypes = {
  /**
   * if user is logged
   */
  logged: PropTypes.bool.isRequired,
  language: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(Home);
