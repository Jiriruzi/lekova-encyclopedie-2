import React from "react";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";
/**
 * Button in home screen
 */
export class HomeButton extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.props.history.push(this.props.to);
  }

  render() {
    return (
      <button
        className={`home-button ${this.props.className}`}
        onClick={this.handleClick}
      >
        <div className="home-title home-button-title">{this.props.title}</div>
        <div>{this.props.desc}</div>
      </button>
    );
  }
}

HomeButton.propTypes = {
  className: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  /**
   * Description of the button
   */
  desc: PropTypes.string 
};

export default withRouter(HomeButton);
