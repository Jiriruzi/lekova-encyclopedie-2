import React from "react";
import { Vocabulary } from "Libs/Vocabulary.js";
import HomeButton from "./HomeButton.jsx";
import PropTypes from "prop-types";
/**
 * Buttons in home screen
 * @param {object} props 
 */
export function HomeButtons(props) {
  return (
    <div className="drug-row row">
      <div className="col-md-5 col-xs-6">
        <HomeButton
          language={props.language}
          className="big-button"
          to="/Interactions"
          title={Vocabulary.InteractionControl[props.language]}
          desc={Vocabulary.InteractionControlDescButt[props.language]}
        />
      </div>

      <div className="col-md-5 col-xs-6">
        <HomeButton
          className="big-button"
          to="/PositiveList"
          language={props.language}
          title={Vocabulary.PositiveList[props.language]}
          desc={Vocabulary.PositiveListDescButt[props.language]}
        />
      </div>
      <div className="col-xs-2 home-user-buttons hidden-sm hidden-xs">
        <HomeButton
          className="small-button"
          to={props.logged ? "/EditUser" : "/Login"}
          language={props.language}
          title={
            props.logged
              ? Vocabulary.Profile[props.language]
              : Vocabulary.Login[props.language]
          }
        />
        <HomeButton
          className="small-button"
          to={props.logged ?  (props.role === "expert" ? "/History" : "/Medication" ) : "/Registration"}
          language={props.language}
          title=
            {props.logged ?  (props.role === "expert" ? Vocabulary.History[props.language] : Vocabulary.Medication[props.language]) : Vocabulary.Registration[props.language]}
          
        />
      </div>
    </div>
  );
}

HomeButtons.propTypes = {
  /**
   * if user is logged
   */
  logged: PropTypes.bool.isRequired,
  language: PropTypes.string.isRequired
};

export default HomeButtons;
