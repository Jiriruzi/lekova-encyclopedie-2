import React from "react";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import TextContainer from "Components/TextContainer/TextContainer.jsx";
import PropTypes from "prop-types";

/**
 * Component rendering interactions
 */
export class Interactions extends React.Component {
  /**
   * Function returning array of jsx elements of interactions
   */
  getInteractions() {
    const critInteractions = [];
    let interactions = [];
    if (!this.props.ingredients) {
      return null;
    }
    let ingredients;
    //Sorting interactions
    if (this.props.ingredients instanceof Array) {
      ingredients = this.props.ingredients.sort((it1, it2) => {
        const a = utils.getValue(
          it1,
          this.props.context,
          this.props.language,
          "title"
        );
        const b = utils.getValue(
          it2,
          this.props.context,
          this.props.language,
          "title"
        );
        if (a.value < b.value) return -1;
        if (a.value > b.value) return 1;
        return 0;
      });
    } else {
      ingredients = [this.props.ingredients];
    }
    // Looping through all ingredients that have created interactions inside, sorting ingredients inside ingredients.interactions
    for (let i = 0; i < ingredients.length; i++) {
      if (!ingredients[i]["enc:hasInteraction"]) {
        continue;
      }
      if (ingredients[i]["enc:hasInteraction"] instanceof Array) {
        ingredients[i]["enc:hasInteraction"] = ingredients[i][
          "enc:hasInteraction"
        ].sort((it1, it2) => {
          const a = utils.getValue(
            it1["enc:interactionWith"],
            this.props.context,
            this.props.language,
            "title"
          );
          const b = utils.getValue(
            it2["enc:interactionWith"],
            this.props.context,
            this.props.language,
            "title"
          );
          if (a.value < b.value) return -1;
          if (a.value > b.value) return 1;
          return 0;
        });
      } else {
        ingredients[i]["enc:hasInteraction"] = [
          ingredients[i]["enc:hasInteraction"]
        ];
      }
      // Pushing jsx interactions into returning array
      for (let j = 0; j < ingredients[i]["enc:hasInteraction"].length; j++) {
        const descs = [];
        if (!ingredients[i]["enc:hasInteraction"][j]["enc:hasDescription"]) {
          continue;
        }
        if (
          !(
            ingredients[i]["enc:hasInteraction"][j][
              "enc:hasDescription"
            ] instanceof Array
          )
        ) {
          ingredients[i]["enc:hasInteraction"][j]["enc:hasDescription"] = [
            ingredients[i]["enc:hasInteraction"][j]["enc:hasDescription"]
          ];
        }
        // Pushing descriptions of the interaction  into descs array
        for (
          let k = 0;
          k <
          ingredients[i]["enc:hasInteraction"][j]["enc:hasDescription"].length;
          k++
        ) {
          descs.push(
            <div className="row interaction-description" key={k}>
              <div className="col-xs-10">
                <TextContainer
                  value={utils.getValue(
                    ingredients[i]["enc:hasInteraction"][j][
                      "enc:hasDescription"
                    ][k],
                    this.props.context,
                    this.props.language,
                    ""
                  )}
                />
              </div>
              <div className="col-xs-2 source">
                {`${Vocabulary.Source[this.props.language]}: `}
                <a
                  href={
                    utils.getValue(
                      ingredients[i]["enc:hasInteraction"][j][
                        "enc:hasDescription"
                      ][k]["enc:source"],
                      this.props.context,
                      this.props.language,
                      "url"
                    ).value
                  }
                >
                  {
                    utils.getValue(
                      ingredients[i]["enc:hasInteraction"][j][
                        "enc:hasDescription"
                      ][k]["enc:source"],
                      this.props.context,
                      this.props.language,
                      "title"
                    ).value
                  }
                </a>
              </div>
            </div>
          );
        }
        // Look of the jsx depends whether the individual items of interactions are ingredient or MP
        let med = false;
        if (
          ingredients[i].medicinalProduct ||
          ingredients[i]["enc:hasInteraction"][j].medicinalProduct
        ) {
          med = true;
        }
        let leftIng = (
          <div>
            <TextContainer
              value={utils.getValue(
                ingredients[i],
                this.props.context,
                this.props.language,
                "title"
              )}
              to={utils.getLink(ingredients[i], "")}
            />
          </div>
        );
        let rightIng = (
          <div>
            <TextContainer
              value={utils.getValue(
                ingredients[i]["enc:hasInteraction"][j]["enc:interactionWith"],
                this.props.context,
                this.props.language,
                "title"
              )}
              to={utils.getLink(
                ingredients[i]["enc:hasInteraction"][j]["enc:interactionWith"],
                "interactionWith"
              )}
            />
          </div>
        );
        if (med) {
          if (ingredients[i].medicinalProduct) {
            leftIng = (
              <div>
                <TextContainer
                  value={utils.getValue(
                    ingredients[i].medicinalProduct,
                    this.props.context,
                    this.props.language,
                    "title"
                  )}
                  to={utils.getLink(ingredients[i], "medicinalProduct")}
                />
                <div>-</div>
                {leftIng}
              </div>
            );
          } else {
            leftIng = (
              <div>
                <br />
                {leftIng}
              </div>
            );
          }
          if (ingredients[i]["enc:hasInteraction"][j].medicinalProduct) {
            rightIng = (
              <div>
                <TextContainer
                  value={utils.getValue(
                    ingredients[i]["enc:hasInteraction"][j].medicinalProduct,
                    this.props.context,
                    this.props.language,
                    "title"
                  )}
                  to={utils.getLink(
                    ingredients[i]["enc:hasInteraction"][j],
                    "medicinalProduct"
                  )}
                />
                <div>-</div>
                {rightIng}
              </div>
            );
          } else {
            rightIng = (
              <div>
                <br />
                {rightIng}
              </div>
            );
          }
        }
        // Final jsx of the interaction
        const critical =
          ingredients[i]["enc:hasInteraction"][j]["enc:isCritical"];
        const toPushInt = (
          <div key={`${i}-${j}`} className="row col-xs-12">
            <div className={`interaction ${critical ? "critical" : ""}`}>
              {critical ? (
                <div className="col-xs-12 critical-desc">
                  {Vocabulary.Critical[this.props.language]}
                </div>
              ) : null}
              <div className="row interaction-title col-xs-10">
                <div className="interaction-item-title-container">
                  <div className="interaction-item-title float-right">
                    {leftIng}
                  </div>
                </div>
                <div className="x">{med ? <br /> : null}x</div>
                <div className="interaction-item-title-container">
                  <div className="interaction-item-title">{rightIng}</div>
                </div>
              </div>
              {descs}
            </div>
          </div>
        );
        // If critical should be first, then pushem them to the final array first
        if (this.props.criticalFirst && critical) {
          critInteractions.push(toPushInt);
        } else {
          interactions.push(toPushInt);
        }
      }
    }

    interactions = critInteractions.concat(interactions);
    const nothing =
      this.props.ingredients.length === 0 ? null : (
        <div className="row col-xs-12 no-interactions">
          {Vocabulary.NoInteractions[this.props.language]}
        </div>
      );
    // Add title if it should be displayed
    const returnInteractions = this.props.withTitle ? (
      <div>
        <div
          style={{
            fontSize: "15px"
          }}
          className="row col-xs-12 title"
        >
          {Vocabulary.Interactions[this.props.language]}
        </div>
        {interactions}
      </div>
    ) : (
      interactions
    );
    // Add children if it should be displayed
    if (this.props.withChildren) {
      return interactions.length ? (
        <div>
          {React.cloneElement(this.props.children, { hasInteraction: true })}
          {returnInteractions}
        </div>
      ) : (
        <div>
          {React.cloneElement(this.props.children, { hasInteraction: false })}
        </div>
      );
    }

    return interactions.length ? returnInteractions : nothing;
  }
  render() {
    return <div>{this.getInteractions()}</div>;
  }
}

Interactions.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Ingredients with interactions to show
   */
  ingredients: PropTypes.array.isRequired,
  /**
   * If critical interactions should be displayed first
   */
  criticalFirst: PropTypes.bool,
  /**
   * Children to render
   */
  children: PropTypes.object,
  /**
   * If the title interactions should be displayed
   */
  withTitle: PropTypes.bool,
  /**
   * If the children should be displayed
   */
  withChildren: PropTypes.bool
};

export default Interactions;
