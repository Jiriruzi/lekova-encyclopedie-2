import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import utils from "Libs/utils.js";
import RemoveButton from "Components/User/RemoveButton.jsx";
import SearchBarInForm from "Components/Search/SearchBarInForm.jsx";
import { addItem, removeItem, resetForm } from "Actions/formActions.js";
import { resetSearchValueInForm } from "Actions/searchActions.js";
import {
  addInteractionItem,
  removeInteractionItem,
  resetInteractions
} from "Actions/documentsFetches.js";
import PropTypes from "prop-types";

function makeQuery(value) {
  value = value.replace(/ /g, "\\%20");
  if (!value[0]) {
    return "(enc_title:*)";
  }
  return `((enc_title:${value})^4 OR (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^3 OR (enc_title:${value})^3 OR (enc_title:${value.toUpperCase()})^3 OR
        (enc_title:${value}*)^2 OR (enc_title:${value.toUpperCase()}*)^2 OR 
        (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^2 OR
        enc_title:*${value}* OR enc_title:*${value.toUpperCase()}* OR
        enc_title:*${value[0].toUpperCase() + value.substring(1)}* OR
        enc_title:*${value.toLowerCase()}*
    ) AND
        (_type:medicinalProduct OR _type:ingredient)`;
}
/**
 * Form for adding items and investigate them for interactions 
 */
export class InteractionsForm extends React.Component {
  constructor(props) {
    super(props);
    this.removeItem = this.removeItem.bind(this);
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
  }

  componentWillMount() {
    this.props.resetForm();
    this.props.resetInteractions();
    this.props.resetSearchValueInForm();
  }

  onSuggestionSelected(event, suggestionValue) {
    this.props.addItem(suggestionValue.suggestion);
    this.props.formApi.setValue(
      `items[${this.props.items.items.length}]`,
      suggestionValue.suggestion
    );
    this.props.addInteractionItem(
      suggestionValue.suggestion,
      this.props.context
    );
    this.props.resetSearchValueInForm();
  }removeItem(arrayId) {
    this.props.removeItem(arrayId);
    this.props.formApi.removeValue("items", arrayId);
    this.props.removeInteractionItem(arrayId, this.props.context);
  }

  renderItems() {
    const items = [];
    for (
      let i = 0;
      this.props.items && i < this.props.items.items.length;
      i++
    ) {
      items.push(
        <div className="linked-item" key={this.props.items.keys[i]}>
          <Link
            to={utils.makeLink(
              this.props.items.items[i]._type,
              this.props.items.items[i]._id
            )}
          >
            {utils.repairUnicode(this.props.items.items[i].enc_title)}
          </Link>
          <RemoveButton
            style={{
              marginTop: "0px"
            }}
            withTimes
            remove={this.removeItem}
            formApi={this.props.formApi}
            id={i}
          />
        </div>
      );
    }
    return items;
  }

  render() {
    return (
      <form onSubmit={this.props.formApi.submitForm} id="interactions">
        <div className="row col-xs-12 linked-list">{this.renderItems()}</div>
        <div className="col-xs-12">
          <SearchBarInForm
            language={this.props.language}
            makeQuery={makeQuery}
            onSuggestionSelected={this.onSuggestionSelected}
          />
        </div>
      </form>
    );
  }
}
InteractionsForm.propTypes = {
  /**
   * Items that should be investigated for interactions
   */
  items: PropTypes.object,
  context: PropTypes.string,
  /**
   * formApi of the parent form
   */
  formApi: PropTypes.object,
  language: PropTypes.string
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetInteractions,
      addInteractionItem,
      removeInteractionItem,
      resetForm,
      removeItem,
      addItem,
      resetSearchValueInForm
    },
    dispatch
  );
}

export default connect(
  null,
  matchDispatchToProps
)(InteractionsForm);
