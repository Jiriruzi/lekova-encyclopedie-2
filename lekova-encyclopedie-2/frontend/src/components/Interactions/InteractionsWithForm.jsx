import React from 'react';
import {connect} from 'react-redux';
import {Form} from 'react-form';
import {Vocabulary} from 'Libs/Vocabulary';
import {bindActionCreators} from 'redux';
import Spinner from 'react-spinkit';
import {makeInteractions, resetInteractions} from 'Actions/documentsFetches.js';
import {resetForm} from 'Actions/formActions.js';
import InteractionsForm from './InteractionsForm.jsx';
import Interactions from './Interactions.jsx';
import PropTypes from "prop-types";


export class InteractionsWithForm extends React.Component {

    componentWillMount() {
        this.props.resetInteractions();
        this.props.makeInteractions(this.props.context);
        this.props.resetForm();
    }

    componentWillUpdate(newProps) {
        if (this.props.interactions.items !== newProps.interactions.items) {
            this.props.makeInteractions(this.props.context);
        }
    }
    componentWillUnmount() {
        this.props.resetInteractions();
        this.props.resetForm();
    }
    render() {
        return (<div>
            <div className="title row">{Vocabulary.InteractionControl[this.props.language]}</div>
            <div className="col-xs-12 row display-linebreak">{Vocabulary.InteractionsDesc[this.props.language]}</div>
            <Form>
                {formApi => (<InteractionsForm formApi={formApi} {...this.props} />)}
            </Form>
            {
                this.props.interactions.loading ?
                    <div className="page-spinner"><Spinner name="ball-spin-fade-loader" color="#3ea898" /></div>
                    : <Interactions criticalFirst ingredients={this.props.interactions.ingredients} {...this.props} />}</div>);
    }
}
function mapStateToProps(state) {
    return {
        context: state.environment.context,
        language: state.environment.language,
        user: state.user,
        items: state.items,
        interactions: state.interactions
    };
}
function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        makeInteractions,
        resetInteractions,
        resetForm
    }, dispatch);
}



InteractionsWithForm.propTypes = {
    context: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    /**
     * Items to be investigate for interactions
     */
    items: PropTypes.object.isRequired,
    /**
     * Api of the parent form
     */
    formApi: PropTypes.object,
  };

export default connect(mapStateToProps, matchDispatchToProps)(InteractionsWithForm);
