import React from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

/**
 * Component rendering text
 */
export class TextContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = false;
  }

  render() {
    let text = this.props.value.value;
    let button = null;
    if (this.props.to && text) {
      text = <Link to={this.props.to}>{text}</Link>;
    }
    if (this.state.clickable) {
      button = (
        <button>
          <div className="fa fa-translation" />
        </button>
      );
    }
    return (
      <div className={`display-linebreak ${this.props.className || ""}`}>
        {button}
        {text}
      </div>
    );
  }
}
TextContainer.propTypes = {
  language: PropTypes.string,
  /**
   * Route for link
   */
  to: PropTypes.string,
  /**
   * Object with text value anf bool if the text is in right lnaguage
   */
  value: PropTypes.object
};
export default TextContainer;
