import React from "react";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary";
import { logout } from "Actions/userFetches.js";
import PropTypes from "prop-types";

// component that renders when file is not found

export class UserBox extends React.Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }

  getWidth() {
    let marg = -75 + 29 / 2;
    let width = 150;
    let newWidth = 0;
    if (this.props.username) {
      const fontSize = "14px";
      const test = document.getElementById("test");
      test.style.fontSize = fontSize;
      test.style.fontWeight = 700;
      test.style.fontFamily = "Helvetica Neue, Helvetica, Arial, sans-serif";
      test.innerHTML = this.props.username;
      newWidth = test.clientWidth;
      newWidth += 4;
      let corecture = 0;
      if (newWidth > width) {
        width = newWidth;
        corecture = 5;
      }
      marg = (-width + 29) / 2 - corecture;
    }
    return {
      minWidth: `${width}px`,
      marginLeft: `${marg}px`
    };
  }

  handleLogout() {
    this.props.logout();
  }

  renderRegisteredLines() {
    return (
      <div>
        <div
          style={{
            backgroundColor: "#3ea898",
            color: "#3ea898!important",
            zIndex: -5
          }}
          className="user-box-line user-box-header"
        >
          &nbsp;
        </div>
        {this.props.role === "expert" ? (
          <div className="">
            <Link className="user-box-line user-box-link" to="/History">
              {Vocabulary.History[this.props.language]}
            </Link>
          </div>
        ) : (
          <div className="">
            <Link className="user-box-line user-box-link" to="/Medication">
              {Vocabulary.Medication[this.props.language]}
            </Link>
          </div>
        )}
        {this.props.role === "expert" ? (
          <div className="">
            <Link className="user-box-line user-box-link" to="/PositiveList">
              {Vocabulary.PositiveList[this.props.language]}
            </Link>
          </div>
        ) : null}
        <div className="">
          <Link className="user-box-line user-box-link" to="/EditUser">
            {Vocabulary.EditUser[this.props.language]}
          </Link>
        </div>
        <div className="">
          <Link className="user-box-line user-box-link" to="/PasswordChange">
            {Vocabulary.PasswordChange[this.props.language]}
          </Link>
        </div>
        <Link
          to="#"
          onClick={this.handleLogout}
          className="user-box-line user-box-link"
        >
          {Vocabulary.Logout[this.props.language]}
        </Link>
      </div>
    );
  }

  renderUnregisteredLines() {
    return (
      <div>
        <div className="">
          <Link className="user-box-line user-box-link" to="/Login">
            {Vocabulary.Login[this.props.language]}
          </Link>
        </div>
        <div className="">
          <Link className="user-box-line user-box-link" to="/Registration">
            {Vocabulary.Registration[this.props.language]}
          </Link>
        </div>
      </div>
    );
  }
  render() {
    return this.props.show ? (
      <div style={this.getWidth()} className="user-box">
        {this.props.username
          ? this.renderRegisteredLines()
          : this.renderUnregisteredLines()}
      </div>
    ) : null;
  }
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      logout
    },
    dispatch
  );
}

UserBox.propTypes = {
  /**
   * Whether the box should be displayed
   */
  show: PropTypes.bool,
  username: PropTypes.string,
  /**
   * User role
   */
  role: PropTypes.string,
  language: PropTypes.string.isRequired
};

export default connect(
  null,
  matchDispatchToProps
)(UserBox);
