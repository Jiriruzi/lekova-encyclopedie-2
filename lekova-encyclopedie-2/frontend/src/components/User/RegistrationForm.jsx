import React from "react";
import { Form, Radio, RadioGroup, Text, Select } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import utils from "Libs/utils.js";
import { addUser } from "Actions/userFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import Spinner from "react-spinkit";
import EncDatePicker from "Components/User/EncDatePicker.jsx";
import PropTypes from "prop-types";

export function errorValidator(values) {
  let email = null;
  let username = null;
  if (!values.username) {
    username = "MissingUsername";
  } else if (!values.username.match("^[a-zA-Z]+$")) {
    username = "InvalidUsername";
  }
  if (!values.email) {
    email = "MissingEmail";
  } else if (!utils.validateEmail(values.email)) {
    email = "InvalidEmail";
  }
  return {
    birthday: values.birthday ? null : "MissingDate",
    username,
    password: values.password ? null : "MissingPassword",
    passwordConfirmation:
      values.password &&
      values.password.localeCompare(values.passwordConfirmation) === 0
        ? null
        : "DifferentPasswords",
    email,
    gender: values.gender ? null : "MissingGender"
  };
}

export class RegistrationForm extends React.Component {
  componentWillMount() {
    this.props.resetResponses();
  }

  componentWillReceiveProps(nextProps) {
    if (
      this.props.history.location.pathname === "/Registration" &&
      nextProps.user &&
      nextProps.user.username
    ) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <div>
        <div className="title bold">
          {Vocabulary.Registration[this.props.language]}
        </div>
        <Form
          defaultValues={{ role: "user" }}
          validate={errorValidator}
          onSubmit={(values, e, formApi) => {
            const birth = new Date(values.birthday);
            this.props.addUser(
              {
                ...values,
                birthday: {
                  day: birth.getDate(),
                  month: birth.getMonth() + 1,
                  year: birth.getFullYear()
                }
              },
              formApi.resetAll
            );
            window.scrollTo(0, 0);
          }}
          onSubmitFailure={(errors, onSubmitError, formApi) => {
            formApi.setTouched("birthday", true);
          }}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="userRegister">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                  {this.props.userFormsResponses.registrationResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.registrationResponse.localeCompare(
                          "Registered"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.registrationResponse
                        ? Vocabulary[
                            this.props.userFormsResponses.registrationResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>
                    {Vocabulary.Username[this.props.language]}:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.username &&
                        formApi.touched.username
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="username"
                        className="form-control"
                        field="username"
                      />
                      {formApi.errors &&
                      formApi.errors.username &&
                      formApi.touched.username
                        ? Vocabulary[formApi.errors.username][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <label>
                    E-mail:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.email &&
                        formApi.touched.email
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="Email"
                        type="email"
                        className="form-control"
                        field="email"
                      />
                      {formApi.errors &&
                      formApi.errors.email &&
                      formApi.touched.email
                        ? Vocabulary[formApi.errors.email][this.props.language]
                        : null}
                    </div>
                  </label>
                  <label>
                    {Vocabulary.Password[this.props.language]}:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.password &&
                        formApi.touched.password
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="Password"
                        className="form-control"
                        type="password"
                        field="password"
                      />
                      {formApi.errors &&
                      formApi.errors.password &&
                      formApi.touched.password
                        ? Vocabulary[formApi.errors.password][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <label>
                    {Vocabulary.ConfirmationPassword[this.props.language]}
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.passwordConfirmation &&
                        formApi.touched.passwordConfirmation
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="Password confirmation"
                        className="form-control"
                        type="password"
                        field="passwordConfirmation"
                      />
                      {formApi.errors &&
                      formApi.errors.passwordConfirmation &&
                      formApi.touched.passwordConfirmation
                        ? Vocabulary[formApi.errors.passwordConfirmation][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <label>
                    {Vocabulary.Birthday[this.props.language]}:
                    <div />
                    <EncDatePicker
                      maxToday
                      setTouched={formApi.setTouched}
                      setValue={formApi.setValue}
                      value="birthday"
                      language={this.props.language}
                    />
                    <div
                      className={
                        formApi.errors &&
                        formApi.touched.birthday &&
                        formApi.errors.birthday
                          ? "has-error"
                          : ""
                      }
                    >
                      {formApi.errors &&
                      formApi.errors.birthday &&
                      formApi.touched.birthday
                        ? Vocabulary[formApi.errors.birthday][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <label>
                    {Vocabulary.Experience[this.props.language]}:
                    <Select
                      field="role"
                      placeholder={
                        this.props.language === "en" ? "Select" : "Vyber"
                      }
                      options={[
                        {
                          label: Vocabulary.Layman[this.props.language],
                          value: "user"
                        },
                        {
                          label: Vocabulary.Expert[this.props.language],
                          value: "expert"
                        }
                      ]}
                      className="form-control"
                    />
                  </label>
                  <legend title="Gender" className="gender-label">
                    {Vocabulary.Sex[this.props.language]}:
                  </legend>

                  <fieldset>
                    <RadioGroup field="gender">
                      <label className="gender">
                        {Vocabulary.Male[this.props.language]}
                        <Radio value="male" id="radio-input-male" />
                      </label>
                      <label className="gender">
                        {Vocabulary.Female[this.props.language]}
                        <Radio value="female" id="radio-input-female" />
                      </label>
                    </RadioGroup>
                  </fieldset>

                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.gender &&
                      formApi.touched.gender
                        ? "has-error"
                        : ""
                    }
                  >
                    {formApi.errors &&
                    formApi.errors.gender &&
                    formApi.touched.gender
                      ? Vocabulary[formApi.errors.gender][this.props.language]
                      : null}
                  </div>
                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Submit[this.props.language]}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Form>{" "}
        {this.props.userFormsResponses.loading ? (
          <div className="page-spinner">
            <Spinner
              fadeIn="none"
              name="ball-spin-fade-loader"
              color="#3ea898"
            />
          </div>
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      addUser,
      resetResponses
    },
    dispatch
  );
}
RegistrationForm.propTypes={
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object
}

export default connect(
  mapStateToProps,
  matchDispatchToProps
)(RegistrationForm);
