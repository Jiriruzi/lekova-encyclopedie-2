import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getMedication, removeMedicament } from "Actions/userFetches.js";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import RemoveButton from "Components/User/RemoveButton.jsx";
import RedirButton from "Components/User/RedirButton.jsx";
import MedicamentDropdown from "Components/User/MedicamentDropdown.jsx";
import {
  removeMedicamentFront,
  editMedicament,
  stornoMedicament,
  confirmMedicament
} from "Actions/medicationActions.js";
import { resetSearchValueInForm } from "Actions/searchActions.js";
import {
  addInteractionItem,
  removeInteractionItem
} from "Actions/documentsFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import { withRouter } from "react-router-dom";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faAngleDown from "@fortawesome/fontawesome-free-solid/faAngleDown";
import faAngleUp from "@fortawesome/fontawesome-free-solid/faAngleUp";
import ContainerDimensions from "react-container-dimensions";
import faEdit from "@fortawesome/fontawesome-free-solid/faEdit";
import faFilePdf from "@fortawesome/fontawesome-free-solid/faFilePdf";
import PropTypes from "prop-types";

export class MedicationRow extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      open: false
    }
    this.removeMedicamentAllAct = this.removeMedicamentAllAct.bind(this)
    this.redirEdit = this.redirEdit.bind(this)
    this.redirect = this.redirect.bind(this)
    this.redirSpc = this.redirSpc.bind(this)
    this.openClose = this.openClose.bind(this)
  }

  removeMedicamentAllAct(arrayId) {
    if (confirm(Vocabulary.Remove[this.props.language])) {
      this.props.removeMedicamentFront(this.props.medicament.key);
      this.props.removeMedicament(this.props.medicament.key);
      this.props.removeInteractionItem(arrayId, this.props.context);
    }
  }
  redirect() {
    this.props.history.push(
      utils.makeLink("MedicinalProduct", this.props.medicament._id)
    );
  }
  redirEdit() {
    this.props.history.push(`/EditMedicament/${this.props.medicament.key}`);
  }
  redirSpc() {
    this.props.history.push(
      `${utils.makeLink("MedicinalProduct", this.props.medicament._id)}?pil`
    );
  }
  openClose() {
    this.setState({
      open: !this.state.open
    });
  }

  render() {
    const nextDose = utils.recalculateNextDose(this.props.medicament);
    return (
      <div className="list-link">
        <div onClick={this.openClose}>
          <li className="mdc-list-item">
            <span className="mdc-list-item__text">
              <button
                className="link-button"
                disabled={!this.props.medicament._id}
                onClick={this.redirect}
              >
                {utils.repairUnicode(this.props.medicament.enc_title)}
              </button>
              <span
                className="mdc-list-item__secondary-text"
                style={
                  !nextDose || (this.state && this.state.open)
                    ? {
                        visibility: "hidden"
                      }
                    : null
                }
              >
                {new Date(nextDose).toLocaleString(this.props.language)}
              </span>
            </span>

            <span className="mdc-list-item__meta">
              <RedirButton
                icon={faFilePdf}
                className="button-no-border fa-button fa-pdf"
                redir={this.redirSpc}
                disabled={!this.props.medicament._id}
              />
              <RedirButton
                icon={faEdit}
                className="button-no-border fa-button fa-edit"
                redir={this.redirEdit}
              />
              <RemoveButton
                id={this.props.medicament.arrayId}
                remove={this.removeMedicamentAllAct}
              />
            </span>
          </li>
          {this.state.open ? (
            <div className="dropped-list">
              <ContainerDimensions>
                {({ width }) => (
                  <MedicamentDropdown {...this.props} width={width} />
                )}
              </ContainerDimensions>
            </div>
          ) : null}
          <div className="dropped-list fa-dropdown">
            <button
              type="button"
              style={{
                color: "gray"
              }}
              onClick={this.openClose}
              className="button-no-border fa-button"
            >
              <FontAwesomeIcon
                icon={this.state.open ? faAngleUp : faAngleDown}
              />
            </button>
          </div>
        </div>
      </div>
    );
  }
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetResponses,
      removeMedicamentFront,
      removeMedicament,
      getMedication,
      editMedicament,
      addInteractionItem,
      removeInteractionItem,
      resetSearchValueInForm,
      stornoMedicament,
      confirmMedicament
    },
    dispatch
  );
}

MedicationRow.propTypes = {
  context: PropTypes.string,
  language: PropTypes.string,
  /**
   * Medicament data to display
   */
  medicament: PropTypes.object,
  /**
   * App router history
   */
  history: PropTypes.object
};

export default withRouter(
  connect(
    null,
    matchDispatchToProps
  )(MedicationRow)
);
