import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import MedicationList from "Components/User/MedicationList.jsx";
import { Vocabulary } from "Libs/Vocabulary";
import {
  resetInteractions,
  makeInteractions
} from "Actions/documentsFetches.js";
import Interactions from "Components/Interactions/Interactions.jsx";
import Spinner from "react-spinkit";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import { getMedication } from "Actions/userFetches.js";
import PropTypes from "prop-types";

export class Medication extends React.Component {
  componentWillMount() {
    this.props.makeInteractions(this.props.context);
    this.props.getMedication(this.props.context);
  }

  componentWillReceiveProps(newProps) {
    if (
      newProps.user.initialized &&
      (!newProps.user.username || newProps.user.role !== "user")
    ) {
      if (
        newProps.user.role !== "user" &&
        this.props.history.location.pathname !== "/"
      )
        this.props.history.push("/");
      else if (this.props.history.location.pathname !== "/Login")
        this.props.history.push("/Login");
    }
    if (
      (this.props.interactions.items.length > 0 ||
        newProps.interactions.items.length > 0) &&
      this.props.interactions.items !== newProps.interactions.items
    ) {
      this.props.makeInteractions(this.props.context);
    }
  }

  render() {
    return (
      <PageContainer
        loading={this.props.medication.loading}
        title={Vocabulary.Medication[this.props.language]}
      >
        <div>
          <div className="row col-xs-12 medication-desc">
            {Vocabulary.MedicationDesc[this.props.language]}{" "}
            <a
              target="_blank"
              href="https://chrome.google.com/webstore/detail/l%C3%A9kov%C3%A1-encyclopedie-medik/hmlmblbhfmjddejiljgackhlhondibao"
              //  href="https://chrome.google.com/webstore/detail/l%C3%A9kov%C3%A1-encyclopedie-medik/lbbohoponfieolnpipfcfgeoccpafgkd"
            >
              Google Chrome
            </a>
            ,{" "}
            <a
              target="_blank"
              href="https://addons.mozilla.org/en-US/firefox/addon/le-medikace-demo/"
              // href="https://addons.mozilla.org/cs/firefox/addon/le-medikace/"
            >
              Firefox
            </a>.
          </div>
          {this.props.interactions.loading ? (
            <div className="row col-xs-12 page-spinner">
              <Spinner
                fadeIn="half"
                name="ball-spin-fade-loader"
                color="#3ea898"
              />
            </div>
          ) : (
            <Interactions
              withTitle
              withChildren
              ingredients={this.props.interactions.ingredients}
              {...this.props}
            >
              <MedicationList
                language={this.props.language}
                context={this.props.context}
                medication={this.props.medication.items || {}}
                hasInteraction={false}
              />
            </Interactions>
          )}
        </div>
      </PageContainer>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    context: state.environment.context,
    user: state.user,
    medication: state.medication,
    interactions: state.interactions
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetInteractions,
      makeInteractions,
      getMedication
    },
    dispatch
  );
}
Medication.propTypes = {
  context: PropTypes.string,
  language: PropTypes.string,
  /**
   * Object containing users info
   */
  user: PropTypes.object,
  /**
   * Users medication
   */
  medication: PropTypes.object,
  /**
   * App router history
   */
  history: PropTypes.object,
  /**
   * Ingredients with interactions to show
   */
  Interactions: PropTypes.array
};
export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(Medication)
);
