import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";

// component that renders when file is not found


export class RedirButton extends React.Component {
  
  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
    } else {
      this.props.redir();
    }
  }

  render() {
    return (
      <button
        disabled={this.props.disabled}
        type="button"
        onClick={this.handleClick}
        className={this.props.className}
      >
        <FontAwesomeIcon icon={this.props.icon} />
      </button>
    );
  }
}

RedirButton.propTypes = {
  /**
   * redir function
   */
  redir: PropTypes.func.isRequired,
  /**
   * Font awesome icon
   */
  icon: PropTypes.object.isRequired
};

export default RedirButton;
