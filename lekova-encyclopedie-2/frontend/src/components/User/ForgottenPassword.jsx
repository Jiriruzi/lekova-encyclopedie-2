import React from "react";
import { Form, Text } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import { sendPasswordLink } from "Actions/userFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";

export function errorValidator(values) {
  let username = null;
  if (!values.username) {
    username = "MissingUsername";
  }
  return {
    username
  };
}

export class ForgottenPassword extends React.Component {
  componentWillMount() {
    this.props.resetResponses();
    if (this.props.user.username) {
      this.props.history.push("/");
    }
  }
  componentWillUpdate(nextProps) {
    if (nextProps.user && nextProps.user.username) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <div>
        <div className="title bold">
          {Vocabulary.PasswordChange[this.props.language]}
        </div>
        <Form
          validate={errorValidator}
          onSubmit={(values, e, formApi) => {
            this.props.sendPasswordLink(values, this.props.language);
            window.scrollTo(0, 0);
          }}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="passwordForgotten">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                  {this.props.userFormsResponses.resetResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.resetResponse.localeCompare(
                          "Sent"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.resetResponse
                        ? Vocabulary[
                            this.props.userFormsResponses.resetResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>
                    {Vocabulary.Username[this.props.language]}:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.username &&
                        formApi.touched.username
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="username"
                        className="form-control"
                        field="username"
                      />
                      {formApi.errors &&
                      formApi.errors.username &&
                      formApi.touched.username
                        ? Vocabulary[formApi.errors.username][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Submit[this.props.language]}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Form>
        {this.props.userFormsResponses.loading ? (
          <div className="page-spinner">
            <Spinner
              fadeIn="none"
              name="ball-spin-fade-loader"
              color="#3ea898"
            />
          </div>
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      sendPasswordLink,
      resetResponses
    },
    dispatch
  );
}
ForgottenPassword.propTypes={
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ForgottenPassword);
