import React from 'react';
import {Vocabulary} from 'Libs/Vocabulary.js';
import utils from 'Libs/utils.js';
import PropTypes from "prop-types";

export class MedicamentDropdown extends React.Component {


    renderDropDown() {
        return this.props.width > 680 ? this.renderAsTable() : this.renderAsCard();
    }
    renderAsCard() {
        return (<div>
            <div className="bold">
                {Vocabulary.MedicinalProductOne[this.props.language]}
            </div>
            <div>
                {utils.repairUnicode(this.props.medicament.enc_title)}
            </div>
            <div className="bold">
                {Vocabulary.NextDose[this.props.language]}
            </div>
            <div>
                {this.props.medicament.nextDose ?
                    new Date(utils.recalculateNextDose(this.props.medicament)).toLocaleString(this.props.language)
                    : '-'}
            </div>
            <div className="bold">
                {Vocabulary.Frequency[this.props.language]}
            </div>
            <div>
                {this.props.medicament.howOftenType ?
                    `${Vocabulary[this.props.medicament.howOftenType.charAt(0).toUpperCase() +
        this.props.medicament.howOftenType.slice(1)][this.props.language]} - ${this.props.medicament.howOften}`
                    : ' -'}
            </div><div className="bold">
                {Vocabulary.DoseType[this.props.language]}
            </div>
            <div>
                {this.props.medicament.type || ' -'}
            </div><div className="bold">
                {Vocabulary.Amount[this.props.language]}
            </div>
            <div>{this.props.medicament.amount || ' -'}
            </div><div className="bold">
                {Vocabulary.Description[this.props.language]}
            </div>
            <div>{this.props.medicament.description || ' -'}
            </div>
        </div>);

    }
    renderAsTable() {
        return (<table className="medication-table">
            <tbody>
                <tr>
                    <th>
                        {Vocabulary.NextDose[this.props.language]}
                    </th>
                    <th>
                        {Vocabulary.Frequency[this.props.language]}
                    </th><th>
                        {Vocabulary.DoseType[this.props.language]}
                    </th><th>
                        {Vocabulary.Amount[this.props.language]}
                    </th><th>
                        {Vocabulary.Description[this.props.language]}
                    </th></tr>
                <tr>
                    <td>
                        {this.props.medicament.nextDose ?
                            new Date(utils.recalculateNextDose(this.props.medicament)).toLocaleString(this.props.language)
                            : '-'}
                    </td>
                    <td>
                        {this.props.medicament.howOftenType ?
                            `${Vocabulary[this.props.medicament.howOftenType.charAt(0).toUpperCase() +
            this.props.medicament.howOftenType.slice(1)][this.props.language]} - ${this.props.medicament.howOften}`
                            : ' -'}
                    </td>
                    <td>
                        {this.props.medicament.type || ' -'}
                    </td>
                    <td>{this.props.medicament.amount || ' -'}
                    </td>
                    <td>{this.props.medicament.description || ' -'}
                    </td>
                </tr>
            </tbody>
        </table>);
    }
    render() {
        return this.renderDropDown();
    }
}
MedicamentDropdown.propTypes={
    language: PropTypes.string.isRequired,
    /**
     * Width of the parent container
     */
    width: PropTypes.number.isRequired,
    /**
     * Medicament to be displayed
     */
    medicament: PropTypes.object.isRequired
  }

export default MedicamentDropdown;
