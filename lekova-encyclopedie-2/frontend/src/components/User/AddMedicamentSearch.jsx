import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import SearchBarInForm from "Components/Search/SearchBarInForm.jsx";
import { resetSearchValueInForm } from "Actions/searchActions.js";
import {
  addInteractionItem,
  removeInteractionItem
} from "Actions/documentsFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import { updateMedicament } from "Actions/userFetches.js";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

function makeQuery(value) {
  value = value.replace(/ /g, "\\%20");
  return `(
    (enc_title:${value})^4 OR (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^3 OR (enc_title:${value})^3 OR (enc_title:${value.toUpperCase()})^3 OR
    (enc_title:${value}*)^2 OR
        (enc_title:${value.toUpperCase()}*)^2 OR
        (enc_title:${value[0].toUpperCase() + value.substring(1)}*)^2 OR
        enc_title:*${value[0].toUpperCase() + value.substring(1)}* OR
        enc_title:*${value}* OR
        enc_title:*${value.toUpperCase()}* OR
        enc_title:*${value.toLowerCase()}*
    ) AND
        _type:medicinalProduct`;
}
/**
 * Search for adding medicaments
 */
export class AddMedicamentSearch extends React.Component {
  constructor(props) {
    super(props);
    this.onSuggestionSelected = this.onSuggestionSelected.bind(this);
    this.onChange = this.onChange.bind(this);
    this.state = {
      loading: false
    };
  }

  onChange(value) {
    this.props.formApi.setValue("_id", undefined);
    this.props.formApi.setValue("enc_title", value);
  }
  onSuggestionSelected(event, suggestionValue) {
    this.props.formApi.setValue(
      "enc_title",
      suggestionValue.suggestion.enc_title
    );
    this.props.formApi.setValue("_type", suggestionValue.suggestion._type);
    this.props.formApi.setValue("_id", suggestionValue.suggestion._id);
  }

  render() {
    return (
      <div className="add-medicament-form">
        <SearchBarInForm
          formType
          aditionalOnChange={this.onChange}
          onEnter={this.onEnter}
          language={this.props.language}
          makeQuery={makeQuery}
          onSuggestionSelected={this.onSuggestionSelected}
        />
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    userFormsResponses: state.userFormsResponses,
    value: state.search.valueInForm
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetResponses,
      addInteractionItem,
      removeInteractionItem,
      resetSearchValueInForm,
      updateMedicament
    },
    dispatch
  );
}
AddMedicamentSearch.propTypes = {
  language: PropTypes.string,
  /**
   * FormApi of parent form
   */
  formApi: PropTypes.object.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * Value in the input
   */
  value: PropTypes.string
};
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(AddMedicamentSearch);
