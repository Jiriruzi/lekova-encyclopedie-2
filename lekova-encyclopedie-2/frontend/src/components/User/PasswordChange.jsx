import React from "react";
import { Form, Text } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import { withRouter } from "react-router-dom";
import { changePassword } from "Actions/userFetches.js";
import Spinner from "react-spinkit";
import { resetResponses } from "Actions/environmentActions.js";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import PropTypes from "prop-types";

export function errorValidator(values) {
  return {
    password: values.password ? null : "MissingPassword",
    newPassword: values.newPassword ? null : "MissingPassword",
    passwordConfirmation:
      values.newPassword &&
      values.newPassword.localeCompare(values.passwordConfirmation) === 0
        ? null
        : "DifferentPasswords"
  };
}

export class PasswordChange extends React.Component {

  componentWillReceiveProps(nextProps) {
    if (
      this.props.history.location.pathname === "/PasswordChange" &&
      nextProps.user &&
      nextProps.user.initialized &&
      !nextProps.user.username
    ) {
      this.props.history.push("/Login");
    }
  }

  render() {
    return (
      <PageContainer title={Vocabulary.PasswordChange[this.props.language]}>
        <Form
          validate={errorValidator}
          onSubmit={(values, e, formApi) => {
            this.props.changePassword(
              values,
              this.props.user.username,
              formApi
            );
            window.scrollTo(0, 0);
          }}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="passwordChange">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                  {this.props.userFormsResponses.changePasswordResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.changePasswordResponse.localeCompare(
                          "PasswordChanged"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.changePasswordResponse
                        ? Vocabulary[
                            this.props.userFormsResponses.changePasswordResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>
                    {Vocabulary.Password[this.props.language]}:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.password &&
                        formApi.touched.password
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="password"
                        className="form-control"
                        type="password"
                        field="password"
                      />
                      {formApi.errors &&
                      formApi.errors.password &&
                      formApi.touched.password
                        ? Vocabulary[formApi.errors.password][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>
                  <label>
                    {Vocabulary.NewPassword[this.props.language]}:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.newPassword &&
                        formApi.touched.newPassword
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="New password"
                        className="form-control"
                        type="password"
                        field="newPassword"
                      />
                      {formApi.errors &&
                      formApi.errors.newPassword &&
                      formApi.touched.newPassword
                        ? Vocabulary[formApi.errors.newPassword][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>

                  <label>
                    {Vocabulary.ConfirmationPassword[this.props.language]}
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.passwordConfirmation &&
                        formApi.touched.passwordConfirmation
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="New password confirmation"
                        className="form-control"
                        type="password"
                        field="passwordConfirmation"
                      />
                      {formApi.errors &&
                      formApi.errors.passwordConfirmation &&
                      formApi.touched.passwordConfirmation
                        ? Vocabulary[formApi.errors.passwordConfirmation][
                            this.props.language
                          ]
                        : null}
                    </div>
                  </label>

                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Save[this.props.language]}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Form>{" "}
        {this.props.userFormsResponses.loading ? (
          <div className="page-spinner">
            <Spinner
              fadeIn="none"
              name="ball-spin-fade-loader"
              color="#3ea898"
            />
          </div>
        ) : null}
      </PageContainer>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      changePassword,
      resetResponses
    },
    dispatch
  );
}

PasswordChange.propTypes={
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object
}

export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(PasswordChange)
);
