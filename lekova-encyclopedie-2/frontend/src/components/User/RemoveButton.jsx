import React from "react";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faTrashAlt from "@fortawesome/fontawesome-free-solid/faTrashAlt";
import faTimes from "@fortawesome/fontawesome-free-solid/faTimes";
import PropTypes from "prop-types";

// component that renders when file is not found


export class RemoveButton extends React.Component {

  constructor(props){
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick(e) {
    e.stopPropagation();
    this.props.remove(this.props.id, this.props.formApi);
  }

  render() {
    return (
      <button
        type="button"
        onClick={this.handleClick}
        className={`button-no-border ${
          this.props.withTimes ? "" : "fa-button "
        }fa-trash`}
      >
        <FontAwesomeIcon icon={this.props.withTimes ? faTimes : faTrashAlt} />
      </button>
    );
  }
}
RemoveButton.propTypes = {
  /**
   * Id if item to remove
   */
  id: PropTypes.number,
  /**
   * When true icon will be 'times' othewise it will be 'trash'
   */
  withTimes: PropTypes.bool,
  /**
   * formApi of the parent form
   */
  formApi: PropTypes.object
};
export default RemoveButton;
