import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getMedication } from "Actions/userFetches.js";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import EncDatePicker from "Components/User/EncDatePicker.jsx";
import AddMedicamentSearch from "Components/User/AddMedicamentSearch.jsx";
import { Form, Checkbox, Select, Text, TextArea } from "react-form";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import { withRouter } from "react-router-dom";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import faTimes from "@fortawesome/fontawesome-free-solid/faTimes";
import { updateMedicament } from "Actions/userFetches.js";
import { addInteractionItem } from "Actions/documentsFetches.js";
import { Combobox } from "react-widgets";
import { setSearchValueInForm } from "Actions/searchActions.js";
import "react-widgets/dist/css/react-widgets.css";
import PropTypes from "prop-types";

export function errorValidator(values) {
  let howOften = false;
  let encTitle = false;
  if (values.alarm) {
    howOften = true;
    if (
      values.howOften &&
      values.howOften > 0 &&
      values.howOftenType &&
      (values.howOftenType === "hours" ||
        values.howOften * 1 === Math.floor(values.howOften * 1))
    ) {
      howOften = false;
    }
  }
  if (!values.enc_title) {
    encTitle = true;
  }
  return { howOften, enc_title: encTitle };
}
/**
 * Form for editing medicament of user
 */
export class EditMedicament extends React.Component {
  constructor(props) {
    super(props);
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.state = {
      doseType: undefined
    };
  }
  componentWillMount() {
    if (!this.props.medicament && this.props.medKey) {
      this.props.getMedication(this.props.environment.context);
    }
    if (this.props.medicament) {
      this.setState({
        doseType: this.props.medicament.type
      });
      this.props.setSearchValueInForm(this.props.medicament.enc_title);
    }
  }

  componentWillReceiveProps(newProps) {
    if (
      this.props.history.location.pathname.includes("Medicament") &&
      newProps.user.initialized &&
      (!newProps.user.username || newProps.user.role !== "user")
    ) {
      if (!newProps.user.username) {
        this.props.history.push("/Login");
      } else {
        this.props.history.push("/");
      }
    }
    if (this.props.medicament !== newProps.medicament) {
      this.setState({
        doseType: newProps.medicament.type
      });
      this.props.setSearchValueInForm(newProps.medicament.enc_title);
    }
  }
  /**
   * Handling change of medication type
   * @param {string} value
   */
  handleTypeChange(value) {
    this.setState({
      doseType: value
    });
  }

  render() {
    return (
      <PageContainer
        title={
          !this.props.medKey
            ? Vocabulary.AddMedicament[this.props.environment.language]
            : Vocabulary.EditMedicament[this.props.environment.language]
        }
        isLoading={this.props.medication.loading && this.props.medKey}
        error={
          !this.props.medication.loading &&
          !this.props.medicament &&
          this.props.medKey
        }
        type="SetMedicament"
        language={this.props.environment.language}
      >
        {this.props.medicament || !this.props.medKey ? (
          <Form
            validate={errorValidator}
            onSubmit={values => {
              this.props.updateMedicament(
                {
                  ...this.props.medicament,
                  ...values,
                  arrayId: undefined,
                  type: this.state.doseType
                },
                this.props.environment.context,
                0,
                () => this.props.history.push("/Medication")
              );
            }}
            validateOnSubmit
            defaultValues={this.props.medicament}
          >
            {formApi => (
              <form
                onSubmit={formApi.submitForm}
                className="form-group col-12 col-md-6"
                id="editMedicament"
              >
                <label>
                  {
                    Vocabulary.MedicinalProductOne[
                      this.props.environment.language
                    ]
                  }{" "}
                  ({Vocabulary.MandatoryField[this.props.environment.language]}
                  ):
                  <div
                    className={
                      formApi.errors && formApi.errors.enc_title
                        ? "has-error"
                        : ""
                    }
                  >
                    <AddMedicamentSearch formApi={formApi} />
                  </div>
                  {formApi.errors && formApi.errors.enc_title ? (
                    <div className="has-error">
                      {
                        Vocabulary.MandatoryField[
                          this.props.environment.language
                        ]
                      }
                    </div>
                  ) : null}
                </label>
                <div className="notification-form-wrapper">
                  <label>
                    {Vocabulary.Frequency[this.props.environment.language]}:
                    <div
                      className={`notification-form${
                        formApi.touched &&
                        formApi.errors &&
                        formApi.errors.howOften &&
                        formApi.touched.howOften &&
                        formApi.touched.howOftenType
                          ? " has-error"
                          : ""
                      }`}
                    >
                      <Text
                        title="Frequency"
                        className="frequency form-control"
                        field={"howOften"}
                        type="number"
                        min="0"
                        step={
                          formApi.values &&
                          formApi.values &&
                          formApi.values.howOftenType !== "hours"
                            ? "1"
                            : "0.1"
                        }
                      />
                      <Select
                        title="Frequency time type"
                        field={"howOftenType"}
                        options={[
                          {
                            label: "",
                            value: ""
                          },
                          {
                            label:
                              Vocabulary.Hours[this.props.environment.language],
                            value: "hours"
                          },
                          {
                            label:
                              Vocabulary.Days[this.props.environment.language],
                            value: "days"
                          },
                          {
                            label:
                              Vocabulary.Weeks[this.props.environment.language],
                            value: "weeks"
                          },
                          {
                            label:
                              Vocabulary.Months[
                                this.props.environment.language
                              ],
                            value: "months"
                          }
                        ]}
                        className="frequency form-control"
                      />
                      {formApi.touched &&
                      formApi.errors &&
                      formApi.errors.howOften &&
                      formApi.touched.howOften &&
                      formApi.touched.howOftenType ? (
                        <div className="has-error">
                          {
                            Vocabulary.InvalidFrequency[
                              this.props.environment.language
                            ]
                          }
                        </div>
                      ) : null}
                    </div>
                    <div
                      style={{
                        paddingBottom: "10px",
                        fontWeight: "normal",
                        color: "gray"
                      }}
                    >
                      {
                        Vocabulary.MandatoryFieldNotification[
                          this.props.environment.language
                        ]
                      }
                    </div>
                  </label>
                  <label>
                    {Vocabulary.NextDose[this.props.environment.language]}:
                    <div className="notification-form">
                      <EncDatePicker
                        setError={formApi.setError}
                        setTouched={formApi.setTouched}
                        setValue={formApi.setValue}
                        value={"nextDose"}
                        language={this.props.environment.language}
                        withTime
                        startDate={
                          this.props.medicament
                            ? utils.recalculateNextDose(this.props.medicament)
                            : null
                        }
                      />
                      <div
                        style={{
                          paddingBottom: "10px",
                          fontWeight: "normal",
                          color: "gray"
                        }}
                      >
                        {
                          Vocabulary.MandatoryFieldNotification[
                            this.props.environment.language
                          ]
                        }
                      </div>
                    </div>
                  </label>
                  <label>
                    {
                      Vocabulary.SetNotifications[
                        this.props.environment.language
                      ]
                    }
                    :
                    <Checkbox
                      title="Notifications"
                      className="check-box"
                      field={"alarm"}
                    />
                  </label>
                </div>

                <div />
                <label>
                  {Vocabulary.DoseType[this.props.environment.language]}:
                  <Combobox
                    title="Type"
                    value={this.state.doseType}
                    onChange={this.handleTypeChange}
                    data={[
                      Vocabulary.Tablets[this.props.environment.language],
                      Vocabulary.Capsules[this.props.environment.language],
                      Vocabulary.CoatedTablets[this.props.environment.language],
                      Vocabulary.Premix[this.props.environment.language],
                      Vocabulary.Powder[this.props.environment.language],
                      Vocabulary.Paste[this.props.environment.language],
                      Vocabulary.Gel[this.props.environment.language],
                      Vocabulary.Ointment[this.props.environment.language],
                      Vocabulary.Creme[this.props.environment.language],
                      Vocabulary.Foam[this.props.environment.language],
                      Vocabulary.Injcetion[this.props.environment.language],
                      Vocabulary.SpotOn[this.props.environment.language],
                      Vocabulary.Spray[this.props.environment.language],
                      Vocabulary.Suppository[this.props.environment.language],
                      Vocabulary.Aerosol[this.props.environment.language],
                      Vocabulary.Drops[this.props.environment.language],
                      Vocabulary.Liquid[this.props.environment.language],
                      Vocabulary.Sespension[this.props.environment.language],
                      Vocabulary.Emulsion[this.props.environment.language]
                    ].sort((s1, s2) =>
                      s1.localeCompare(s2, "cz", { sensitivity: "variant" })
                    )}
                  />
                </label>
                <label>
                  {Vocabulary.Amount[this.props.environment.language]}:
                  <Text
                    title={Vocabulary.Amount[this.props.environment.language]}
                    className="form-control"
                    field={"amount"}
                  />
                </label>
                <label>
                  {Vocabulary.Description[this.props.environment.language]}:
                  <TextArea
                    title={
                      Vocabulary.Description[this.props.environment.language]
                    }
                    className="form-control"
                    field={"description"}
                  />
                </label>
                <button type="submit" className="btn btn-info btn-submit">
                  {Vocabulary.Save[this.props.environment.language]}
                </button>
                <button
                  type="button"
                  onClick={() => this.props.history.push("/Medication")}
                  className="btn btn-danger btn-storno"
                >
                  <FontAwesomeIcon icon={faTimes} />
                </button>
              </form>
            )}
          </Form>
        ) : null}
      </PageContainer>
    );
  }
}

function mapStateToProps(state, ownProps) {
  const medKey = ownProps.match ? ownProps.match.params.key : null;
  return {
    user: state.user,
    medicament:
      state.medication && state.medication.items && medKey
        ? state.medication.items[medKey]
        : null,
    medication: state.medication,
    environment: state.environment,
    medKey
  };
}
EditMedicament.propTypes = {
  /**
   * Object containing users info
   */
  user: PropTypes.object,
  /**
   * Medicament to be edited
   */
  medicament: PropTypes.object,
  /**
   * Users medication
   */
  medication: PropTypes.object,
  /**
   * Object containing apps state (language, context)
   */
  environment: PropTypes.object.isRequired,
  /**
   * Id of the medicament
   */
  medKey: PropTypes.string,
  /**
   * App router history
   */
  history: PropTypes.object
};
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getMedication,
      updateMedicament,
      setSearchValueInForm,
      addInteractionItem
    },
    dispatch
  );
}

export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(EditMedicament)
);
