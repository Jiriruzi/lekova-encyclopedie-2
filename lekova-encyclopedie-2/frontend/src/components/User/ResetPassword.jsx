import React from "react";
import { Form, Text } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import { withRouter } from "react-router-dom";
import { resetPassword } from "Actions/userFetches.js";
import Spinner from "react-spinkit";
import { resetResponses } from "Actions/environmentActions.js";
import PropTypes from "prop-types";

export function errorValidator(values) {
  let username = null;
  if (!values.username) {
    username = "MissingUsername";
  } else if (!values.username.match("[a-zA-Z0-9]*")) {
    username = "InvalidUsername";
  }
  return {
    username,
    newPassword: values.newPassword ? null : "MissingPassword",
    passwordConfirmation:
      values.newPassword &&
      values.newPassword.localeCompare(values.passwordConfirmation) === 0
        ? null
        : "DifferentPasswords"
  };
}

export class ResetPassword extends React.Component {
  componentWillMount() {
    this.props.resetResponses();
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.props.history.location.pathname === "/ResetPassword" &&
      nextProps.user &&
      nextProps.user.username
    ) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <div>
        <div className="title bold">
          {Vocabulary.PasswordChange[this.props.language]}
        </div>
        <Form
          validate={errorValidator}
          onSubmit={(values, e, formApi) => {
            this.props.resetPassword(values, this.props.token, formApi);
            window.scrollTo(0, 0);
          }}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="resetPassword">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                  {this.props.userFormsResponses.resetResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.resetResponse.localeCompare(
                          "PasswordChanged"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.resetResponse
                        ? Vocabulary[
                            this.props.userFormsResponses.resetResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>{Vocabulary.Username[this.props.language]}:</label>
                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.username &&
                      formApi.touched.username
                        ? "has-error"
                        : ""
                    }
                  >
                    <Text className="form-control" field="username" />
                    {formApi.errors &&
                    formApi.errors.username &&
                    formApi.touched.username
                      ? Vocabulary[formApi.errors.username][this.props.language]
                      : null}
                  </div>
                  <label>{Vocabulary.NewPassword[this.props.language]}:</label>
                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.newPassword &&
                      formApi.touched.newPassword
                        ? "has-error"
                        : ""
                    }
                  >
                    <Text
                      className="form-control"
                      type="password"
                      field="newPassword"
                    />
                    {formApi.errors &&
                    formApi.errors.newPassword &&
                    formApi.touched.newPassword
                      ? Vocabulary[formApi.errors.newPassword][
                          this.props.language
                        ]
                      : null}
                  </div>
                  <label>
                    {Vocabulary.ConfirmationPassword[this.props.language]}
                  </label>
                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.passwordConfirmation &&
                      formApi.touched.passwordConfirmation
                        ? "has-error"
                        : ""
                    }
                  >
                    <Text
                      className="form-control"
                      type="password"
                      field="passwordConfirmation"
                    />
                    {formApi.errors &&
                    formApi.errors.passwordConfirmation &&
                    formApi.touched.passwordConfirmation
                      ? Vocabulary[formApi.errors.passwordConfirmation][
                          this.props.language
                        ]
                      : null}
                  </div>
                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Submit[this.props.language]}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Form>
        {this.props.userFormsResponses.loading ? (
          <div className="page-spinner">
            <Spinner
              fadeIn="none"
              name="ball-spin-fade-loader"
              color="#3ea898"
            />
          </div>
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state, ownProps) {
  return {
    token: ownProps.match.params.token,
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetPassword,
      resetResponses
    },
    dispatch
  );
}
ResetPassword.propTypes = {
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.string,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object,
  /**
   * Token for this password change
   */
  token: PropTypes.string.isRequired
};
export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(ResetPassword)
);
