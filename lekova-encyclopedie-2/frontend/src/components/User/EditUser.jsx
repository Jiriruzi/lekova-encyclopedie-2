import React from "react";
import { Form, Radio, RadioGroup, Text } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import utils from "Libs/utils.js";
import { getUser, editUser } from "Actions/userFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import Spinner from "react-spinkit";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import EncDatePicker from "Components/User/EncDatePicker.jsx";
import { withRouter } from "react-router-dom";
import PropTypes from "prop-types";

export function errorValidator(values) {
  let email = null;

  if (!values.email) {
    email = "MissingEmail";
  } else if (!utils.validateEmail(values.email)) {
    email = "InvalidEmail";
  }
  return {
    email,
    gender: values.gender ? null : "MissingGender",
    birthday: values.birthday ? null : "MissingDate"
  };
}

export class EditUser extends React.Component {
  componentWillMount() {
    if (this.props.user.username) this.props.getUser(this.props.user.username);
  }
  componentWillReceiveProps(nextProps) {
    if (
      this.props.history.location.pathname === "/EditUser" &&
      nextProps.user &&
      nextProps.user.initialized &&
      !nextProps.user.username
    ) {
      this.props.history.push("/Login");
    }
    if (!this.props.user.username && nextProps.user.username) {
      this.props.getUser(nextProps.user.username);
    }
  }

  render() {
    const date = new Date(Date.now());
    if (!this.props.user.data.email)
      return (
        <div className="page-spinner">
          <Spinner name="ball-spin-fade-loader" color="#3ea898" />
        </div>
      );
    return (
      <PageContainer
        title={Vocabulary.EditUser[this.props.language]}
        isLoading={this.props.user.loading}
        error={this.props.user.error}
      >
        <Form
          validate={errorValidator}
          onSubmit={values => {
            const birth = new Date(values.birthday);
            let birthday = {
              day: birth.getDate(),
              month: birth.getMonth() + 1,
              year: birth.getFullYear()
            };
            if (!birthday.day) birthday = this.props.user.data.birthday;
            this.props.editUser({
              ...values, birthday
            });
            window.scrollTo(0, 0);
          }}
          defaultValues={this.props.user.data}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="userRegister">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                  {this.props.userFormsResponses.editResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.editResponse.localeCompare(
                          "Edited"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.editResponse
                        ? Vocabulary[
                            this.props.userFormsResponses.editResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>
                    E-mail:
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.email &&
                        formApi.touched.email
                          ? "has-error"
                          : ""
                      }
                    >
                      <Text
                        title="Email"
                        type="email"
                        className="form-control"
                        field="email"
                      />
                      {formApi.errors &&
                      formApi.errors.email &&
                      formApi.touched.email
                        ? Vocabulary[formApi.errors.email][this.props.language]
                        : null}
                    </div>
                  </label>

                  <label>{Vocabulary.Birthday[this.props.language]}:</label>
                  <div />
                  <EncDatePicker
                    maxToday
                    startDate={this.props.user.data.birthday}
                    setTouched={formApi.setTouched}
                    setValue={formApi.setValue}
                    value="birthday"
                    language={this.props.language}
                  />
                  <div
                    className={
                      formApi.errors &&
                      formApi.touched.birthday &&
                      formApi.errors.birthday
                        ? "has-error"
                        : ""
                    }
                  >
                    {formApi.errors &&
                    formApi.errors.birthday &&
                    formApi.touched.birthday
                      ? Vocabulary[formApi.errors.birthday][this.props.language]
                      : null}
                  </div>
                  <label className="gender-label">
                    {Vocabulary.Sex[this.props.language]}:
                    <RadioGroup field="gender">
                      <Radio value="male" id="radio-input-male" />
                      <label className="gender">
                        {Vocabulary.Male[this.props.language]}
                      </label>
                      <Radio value="female" id="radio-input-female" />
                      <label className="gender">
                        {Vocabulary.Female[this.props.language]}
                      </label>
                    </RadioGroup>
                    <div
                      className={
                        formApi.errors &&
                        formApi.errors.gender &&
                        formApi.touched.gender
                          ? "has-error"
                          : ""
                      }
                    >
                      {formApi.errors &&
                      formApi.errors.gender &&
                      formApi.touched.gender
                        ? Vocabulary[formApi.errors.gender][this.props.language]
                        : null}
                    </div>
                  </label>

                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Submit[this.props.language]}
                  </button>
                </div>
              </div>
            </form>
          )}
        </Form>{" "}
      </PageContainer>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}

EditUser.propTypes={
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getUser,
      editUser,
      resetResponses
    },
    dispatch
  );
}

export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(EditUser)
);
