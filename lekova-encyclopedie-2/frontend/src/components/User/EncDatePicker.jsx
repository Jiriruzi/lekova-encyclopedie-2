import React from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import PropTypes from "prop-types";

import "react-datepicker/dist/react-datepicker.css";
import "react-datepicker/dist/react-datepicker-cssmodules.css";
/**
 * General application date picker
 */
export class EncDatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: this.props.startDate
        ? moment(this.props.startDate).set({ seconds: 0, milliseconds: 0 })
        : null
    };
    this.props.setValue(this.props.value, this.props.startDate || null);
    this.props.setTouched(this.props.value, false);
    this.onChangeRaw = this.onChangeRaw.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  /**
   * Handling situations, when user changes date manually
   * @param {event} event
   */
  onChangeRaw(event) {
    moment.locale(this.props.language);
    const date = moment(event.target.value, "LLL").set({
      seconds: 0,
      milliseconds: 0
    });
    if (!event.target.value) {
      if (this.props.setError) {
        this.props.setError(this.props.value, null);
      }
      this.props.setValue(this.props.value, "");
    } else if (date.isValid()) {
      this.props.setValue(this.props.value, date._d);
      this.setState({
        startDate: date
      });
      if (this.props.setError) {
        this.props.setError(this.props.value, null);
      }
    } else if (this.props.setError) {
      this.props.setError(this.props.value, "InvalidDate");
    }
  }

  handleChange(date) {
    this.props.setValue(this.props.value, date._d);
    this.setState({
      startDate: date
    });
    if (this.calendar && this.calendar.cancelFocusInput)
      this.calendar.cancelFocusInput();
  }

  render() {
    return (
      <div onClick={e => this.calendar.state.open && e.preventDefault()}>
        <DatePicker
          ref={r => (this.calendar = r)}
          selected={this.state.startDate}
          onChange={this.handleChange}
          locale={this.props.language}
          key={this.props.language}
          title="Date"
          showYearDropdown
          showMonthDropdown
          showTimeSelect={this.props.withTime}
          dropdownMode="select"
          maxDate={this.props.maxToday ? moment() : null}
          openToDate={this.state.startDate}
          timeCaption={this.props.withTime ? "time" : null}
          timeFormat={this.props.withTime ? "HH:mm" : null}
          dateFormat={this.props.withTime ? "LLL" : "LL"}
          className="form-control"
          onChangeRaw={this.onChangeRaw}
        />
      </div>
    );
  }
}
EncDatePicker.propTypes = {
  /**
   * On what date should be datepicker displayed
   */
  startDate: PropTypes.object,
  /**
   * Whether it should show time
   */
  withTime: PropTypes.bool,
  /**
   * Whether the maximum selectable date is today
   */
  maxToday: PropTypes.bool,
  /**
   * Value in input
   */
  value: PropTypes.string,
  /**
   * SetError from parent form api
   */
  setError: PropTypes.func,
  /**
   * setValue from parent form api
   */
  setValue: PropTypes.func
};
export default EncDatePicker;
