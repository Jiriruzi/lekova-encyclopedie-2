import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { Vocabulary } from "Libs/Vocabulary.js";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import { getHistory } from "Actions/userFetches.js";
import utils from "Libs/utils.js";
import { Link } from "react-router-dom";
import moment from "moment";
import PropTypes from "prop-types";

function getHistoryTypeOptions(language) {
  return [
    {
      label: Vocabulary.All[language],
      value: ""
    }
  ].concat(
    [
      {
        label: Vocabulary.Ingredient[language],
        value: "Ingredient"
      },
      {
        label: Vocabulary.MedicinalProduct[language],
        value: "MedicinalProduct"
      },
      {
        label: Vocabulary.MedicinalProductPackaging[language],
        value: "MedicinalProductPackaging"
      },
      {
        label: Vocabulary.DiseaseOrFinding[language],
        value: "DiseaseOrFinding"
      },
      {
        label: Vocabulary.MechanismOfAction[language],
        value: "MechanismOfAction"
      },
      {
        label: Vocabulary.Pharmacokinetics[language],
        value: "Pharmacokinetics"
      },
      {
        label: Vocabulary.PharmacologicalAction[language],
        value: "PharmacologicalAction"
      },
      {
        label: Vocabulary.PhysiologicEffect[language],
        value: "PhysiologicEffect"
      },
      {
        label: Vocabulary.ATCConcept[language],
        value: "ATCConcept"
      }
    ].sort((a, b) => a.label.localeCompare(b.label))
  );
}

export function getHistoryDateOptions(language) {
  const now = moment().locale(language);
  return [
    {
      label: Vocabulary.All[language],
      value: `${0}-${moment(now)
        .endOf("day")
        .unix() * 1000}`
    }
  ].concat([
    {
      label: Vocabulary.Today[language],
      value: `${moment(now)
        .startOf("day")
        .unix() * 1000}-${moment(now)
        .endOf("day")
        .unix() * 1000}`
    },
    {
      label: Vocabulary.Last7Days[language],
      value: `${moment(now)
        .subtract(7, "days")
        .startOf("day")
        .unix() * 1000}-${moment(now)
        .endOf("day")
        .unix() * 1000}`
    },
    {
      label: Vocabulary.ThisMonth[language],
      value: `${moment(now)
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .endOf("day")
        .unix() * 1000}`
    },
    {
      label: moment(now)
        .subtract(1, "months")
        .startOf("month")
        .format("MMMM"),
      value: `${moment(now)
        .subtract(1, "months")
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .subtract(1, "months")
        .endOf("month")
        .unix() * 1000}`
    },
    {
      label: moment(now)
        .subtract(2, "months")
        .startOf("month")
        .format("MMMM"),
      value: `${moment(now)
        .subtract(2, "months")
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .subtract(2, "months")
        .endOf("month")
        .unix() * 1000}`
    },
    {
      label: moment(now)
        .subtract(3, "months")
        .startOf("month")
        .format("MMMM"),
      value: `${moment(now)
        .subtract(3, "months")
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .subtract(3, "months")
        .endOf("month")
        .unix() * 1000}`
    },
    {
      label: moment(now)
        .subtract(4, "months")
        .startOf("month")
        .format("MMMM"),
      value: `${moment(now)
        .subtract(4, "months")
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .subtract(4, "months")
        .endOf("month")
        .unix() * 1000}`
    },
    {
      label: moment(now)
        .subtract(5, "months")
        .startOf("month")
        .format("MMMM"),
      value: `${moment(now)
        .subtract(5, "months")
        .startOf("month")
        .unix() * 1000}-${moment(now)
        .subtract(5, "months")
        .endOf("month")
        .unix() * 1000}`
    },
    {
      label: Vocabulary.Older6Months[language],
      value: `${0}-${moment(now)
        .subtract(6, "months")
        .endOf("month")
        .unix() * 1000}`
    }
  ]);
}
export class EncHistory extends React.Component {
  constructor(props) {
    super(props);
    this.filter = this.filter.bind(this);
    this.writing = this.writing.bind(this);
    this.setType = this.setType.bind(this);
    this.setDate = this.setDate.bind(this);
    this.state = {
      value: "",
      type: "",
      date: {
        from: 0,
        to:
          moment()
            .endOf("day")
            .unix() * 1000
      },
      writing: 0
    };
  }
  componentWillMount() {
    this.props.getHistory(this.props.context);
  }

  componentWillReceiveProps(newProps) {
    if (
      newProps.user.initialized &&
      this.props.history.location.pathname === "/History" &&
      (!newProps.user.username || newProps.user.role !== "expert")
    ) {
      if (!newProps.user.username) {
        this.props.history.push("/Login");
      } else if (newProps.user.role !== "expert") {
        this.props.history.push("/");
      }
    }
  }
  setType(e) {
    this.setState({ type: e.target.value });
  }
  setDate(e) {
    const [from, to] = e.target.value.split("-");
    this.setState({ date: { from, to } });
  }
  writing(e) {
    const value = e.target.value;
    const now = Date.now();
    this.setState({ writing: now });
    setTimeout(this.filter, 747, now, value);
  }
  filter(now, value) {
    if (this.state.writing === now) {
      this.setState({
        value
      });
    }
  }
  getFilteredHistory() {
    const his = [];
    for (
      let i = 0;
      this.props.userHistory.history &&
      i < this.props.userHistory.history.length;
      i++
    ) {
      const name = utils.getValue(
        this.props.userHistory.history[i],
        this.props.context,
        this.props.language,
        "title"
      ).value;
      if (
        name &&
        name.toLowerCase().includes(this.state.value.toLowerCase()) &&
        (this.props.userHistory.history[i]["@type"] === this.state.type ||
          !this.state.type) &&
        this.state.date.to > this.props.userHistory.history[i].time &&
        this.state.date.from < this.props.userHistory.history[i].time
      ) {
        his.push({
          type: this.props.userHistory.history[i]["@type"],
          id: this.props.userHistory.history[i]["@id"],
          name: name
        });
      }
    }
    return his;
  }

  renderHistory(history) {
    return history.map((his, i) => (
      <tr key={i}>
        <td>
          <Link to={utils.makeLink(his.type, his.id)}>{his.name}</Link>
        </td>
        <td>{Vocabulary[his.type][this.props.language]}</td>
        <td>
          {new Date(this.props.userHistory.history[i].time).toLocaleString(
            this.props.language
          )}
        </td>
      </tr>
    ));
  }

  render() {
    let his = this.renderHistory(this.getFilteredHistory());
    return (
      <PageContainer
        isLoading={this.props.userHistory.loading}
        title={Vocabulary.History[this.props.language]}
      >
        <div style={{ paddingBottom: "20px" }} className="row col-xs-12">
          {Vocabulary.HistoryLong[this.props.language]}
        </div>
        <div className="row col-xs-12 important-prop">
          <div className="basic-label filter">
            {Vocabulary.Filter[this.props.language]}:
          </div>
        </div>
        <div className="row col-xs-12">
          <table className="drug-table">
            <thead>
              <tr>
                <td className="table-prop-label history-head col-xs-4">
                  {Vocabulary.TypeTitle[this.props.language]}:
                </td>
                <td className="table-prop-label history-head col-xs-4">
                  {Vocabulary.TypeHistory[this.props.language]}:
                </td>
                <td className="table-prop-label history-head col-xs-4">
                  {Vocabulary.VisitDate[this.props.language]}:
                </td>
              </tr>
              <tr>
                <td className="table-prop-label history-head col-xs-4">
                  <input
                    title={Vocabulary.TypeTitle[this.props.language]}
                    className="form-control history-form"
                    onChange={this.writing}
                  />
                </td>
                <td className="table-prop-label history-head col-xs-4">
                  <select
                    title="Type"
                    onChange={this.setType}
                    className="form-control history-form"
                  >
                    {getHistoryTypeOptions(this.props.language).map(item => (
                      <option key={item.value} value={item.value}>
                        {item.label.charAt(0).toUpperCase() +
                          item.label.slice(1)}
                      </option>
                    ))}
                  </select>
                </td>
                <td className="table-prop-label history-head col-xs-4">
                  {" "}
                  <select
                    title="Type"
                    onChange={this.setDate}
                    className="form-control history-form"
                  >
                    {getHistoryDateOptions(this.props.language).map(item => (
                      <option key={item.label} value={item.value}>
                        {item.label}
                      </option>
                    ))}
                  </select>
                </td>
              </tr>
              <tr>
                <td colSpan="3" className="table-prop-label results-in-table">
                  {Vocabulary.Results[this.props.language]}:{his.length || 0}
                </td>
              </tr>
            </thead>
            <tbody>{his}</tbody>
          </table>
        </div>
      </PageContainer>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    context: state.environment.context,
    user: state.user,
    userHistory: state.userHistory
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getHistory
    },
    dispatch
  );
}

EncHistory.propTypes = {
  language: PropTypes.string.isRequired,
  context: PropTypes.string.isRequired,
  user: PropTypes.object.isRequired,
  /**
   * Object with users history of visited documents
   */
  userHistory: PropTypes.object.isRequired,
  /**
   * Router history object
   */
  history: PropTypes.object.isRequired
}

export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(EncHistory)
);
export { EncHistory as PureEncHistory };
