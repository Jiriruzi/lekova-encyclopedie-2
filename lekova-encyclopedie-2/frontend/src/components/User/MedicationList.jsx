import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { addItem, removeItem } from "Actions/formActions.js";
import { resetSearchValueInForm } from "Actions/searchActions.js";
import {
  addInteractionItem,
  removeInteractionItem
} from "Actions/documentsFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import MedicationRow from "Components/User/MedicationRow.jsx";
import { withRouter } from "react-router-dom";
import faPlus from "@fortawesome/fontawesome-free-solid/faPlus";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import PropTypes from "prop-types";

function sortTitles(a, b) {
  return utils
    .repairUnicode(a.enc_title)
    .localeCompare(utils.repairUnicode(b.enc_title));
}
function sortDates(a, b) {
  if (!b.nextDose && a.nextDose) {
    return -1;
  }
  if (!a.nextDose && b.nextDose) {
    return 1;
  }
  if (!b.nextDose && !a.nextDose) {
    return 0;
  }
  return new Date(a.nextDose).getTime() - new Date(b.nextDose).getTime();
}
function sortKeys(a, b) {
  return a.key - b.key;
}
export class MedicationList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sort: sortTitles
    };
    this.setSort = this.setSort.bind(this);
  }
  setSort(e) {
    switch (e.target.value) {
      case "title": {
        this.setState({ sort: sortTitles });
        break;
      }
      case "key": {
        this.setState({ sort: sortKeys });
        break;
      }
      case "date": {
        this.setState({ sort: sortDates });
        break;
      }
    }
  }

  getWarning(medication) {
    let result;
    if (medication.length === 0) {
      return null;
    }
    if (this.props.hasInteraction) {
      result = (
        <div className="error bold">
          {Vocabulary.MedicationNotOk[this.props.language]}
        </div>
      );
    } else {
      result = (
        <div className="success bold">
          {Vocabulary.MedicationOk[this.props.language]}
        </div>
      );
    }
    return (
      <li
        style={{
          cursor: "auto",
          width: "100%"
        }}
        className="mdc-list-item"
      >
        <span
          style={{
            width: "100%"
          }}
          className="mdc-list-item__text"
        >
          {result}
        </span>
      </li>
    );
  }
  renderMedication(medication) {
    let l = 0;
    medication.forEach(m => m.key && m.key > l && (l = m.key));
    const medicationHtml = [];
    medication = medication.sort((a, b) => a.key - b.key);
    if (this.props.medication.loading === true) {
      return [];
    }
    for (let i = 0; i < medication.length; i++) {
      medication[i].arrayId = i;
    }
    medication = medication.sort(this.state.sort);
    for (let i = 0; i < medication.length; i++) {
      medicationHtml.push(
        <MedicationRow
          language={this.props.language}
          medicament={medication[i]}
          key={medication[i].key || ++l}
        />
      );
    }
    return medicationHtml;
  }

  render() {
    const medication = Object.values(this.props.medication);
    let medHtml = this.renderMedication(medication);
    return (
      <div className="row col-xs-12">
        <ul className="mdc-list">
          {this.getWarning(medication)}
          <li
            className="mdc-list-item"
            style={{
              cursor: "auto"
            }}
          >
            <span className="mdc-list-item__text">
              <span className="mdc-list-item__secondary-text">
                {Vocabulary.SortBy[this.props.language]}
              </span>
              <select
                style={{ marginBottom: "2px" }}
                onChange={this.setSort}
                className="form-control history-form"
                defaultValue={"title"}
                title={Vocabulary.Type[this.props.language]}
              >
                {[
                  {
                    label: Vocabulary.Title[this.props.language],
                    value: "title"
                  },
                  {
                    label: Vocabulary.NextDose[this.props.language],
                    value: "date"
                  },
                  {
                    label: Vocabulary.AddedDate[this.props.language],
                    value: "key"
                  }
                ]
                  .sort((a, b) => a.label.localeCompare(b.label))
                  .map(item => (
                    <option key={item.value} value={item.value}>
                      {item.label}
                    </option>
                  ))}
              </select>
            </span>
            <span className="mdc-list-item__meta">
              <button
                type="button"
                onClick={() => this.props.history.push("/AddMedicament")}
                className="button-no-border fa-button fa-plus"
              >
                <FontAwesomeIcon icon={faPlus} />
              </button>
            </span>
          </li>
          <li role="separator" className="mdc-list-divider" />
          {medHtml.length ? (
            medHtml
          ) : (
            <div
              style={{
                paddingTop: "8px",
                textAlign: "center",
                fontSize: "12px"
              }}
              class="bold"
            >
              {Vocabulary.NoMedication[this.props.language]}
            </div>
          )}
        </ul>
      </div>
    );
  }
}

MedicationList.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Users medication
   */
  medication: PropTypes.object,
  /**
   * Whether the medication has any interactions
   */
  hasInteraction: PropTypes.bool.isRequired,
  /**
   * App router history
   */
  history: PropTypes.object
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      resetResponses,
      removeItem,
      addItem,
      addInteractionItem,
      removeInteractionItem,
      resetSearchValueInForm
    },
    dispatch
  );
}

export default withRouter(
  connect(
    null,
    matchDispatchToProps
  )(MedicationList)
);
