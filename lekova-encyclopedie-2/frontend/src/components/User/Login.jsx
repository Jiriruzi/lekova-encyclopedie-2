import React from "react";
import { Form, Text } from "react-form";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import { withRouter } from "react-router-dom";
import { login } from "Actions/userFetches.js";
import { resetResponses } from "Actions/environmentActions.js";
import { Link } from "react-router-dom";
import Spinner from "react-spinkit";
import PropTypes from "prop-types";

export function errorValidator(values) {
  return {
    password: values.password ? null : "MissingPassword",
    username: values.username ? null : "MissingUsername"
  };
}

export class Login extends React.Component {
  componentWillMount() {
    this.props.resetResponses();
    if (this.props.user.username) {
      this.props.history.push("/");
    }
  }
  componentWillUpdate(nextProps) {
    if (
      this.props.history.location.pathname === "/Login" &&
      nextProps.user &&
      nextProps.user.username
    ) {
      this.props.history.push("/");
    }
  }

  render() {
    return (
      <div>
        <div className="title bold row">
          {Vocabulary.Login[this.props.language]}
        </div>
        <Form
          validate={errorValidator}
          onSubmit={(values, e, formApi) => {
            this.props.login(values, formApi, this.props.history.goBack);
          }}
        >
          {formApi => (
            <form onSubmit={formApi.submitForm} id="userLogin">
              <div className="row">
                <div className="form-group col-12 col-md-6 ">
                 {this.props.userFormsResponses.loginResponse ? (
                    <div
                      className={`form-message ${
                        this.props.userFormsResponses.loginResponse.localeCompare(
                          "Logged"
                        ) === 0
                          ? "success"
                          : "error"
                      }`}
                    >
                      {this.props.userFormsResponses.loginResponse !== "Logged"
                        ? Vocabulary[
                            this.props.userFormsResponses.loginResponse
                          ][this.props.language]
                        : null}
                    </div>
                  ) : null}
                  <label>{Vocabulary.Username[this.props.language]}:</label>
                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.username &&
                      formApi.touched.username
                        ? "has-error"
                        : ""
                    }
                  >
                    <Text
                      title="username"
                      className="form-control"
                      field="username"
                    />
                    {formApi.errors &&
                    formApi.errors.username &&
                    formApi.touched.username
                      ? Vocabulary[formApi.errors.username][this.props.language]
                      : null}
                  </div>
                  <label>{Vocabulary.Password[this.props.language]}:</label>
                  <div
                    className={
                      formApi.errors &&
                      formApi.errors.password &&
                      formApi.touched.password
                        ? "has-error"
                        : ""
                    }
                  >
                    <Text
                      title="Password"
                      className="form-control"
                      type="password"
                      field="password"
                    />
                    {formApi.errors &&
                    formApi.errors.password &&
                    formApi.touched.password
                      ? Vocabulary[formApi.errors.password][this.props.language]
                      : null}
                  </div>
                  <button type="submit" className="btn btn-info btn-submit">
                    {Vocabulary.Login[this.props.language]}
                  </button>
                  <div className="drug-row">
                    <Link to="/ForgottenPassword">
                      {Vocabulary.ForgottenPassword[this.props.language]}
                    </Link>
                  </div>
                </div>
              </div>
            </form>
          )}
        </Form>{" "}
        {this.props.userFormsResponses.loading ? (
          <div className="page-spinner">
            <Spinner
              fadeIn="none"
              name="ball-spin-fade-loader"
              color="#3ea898"
            />
          </div>
        ) : null}
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    language: state.environment.language,
    userFormsResponses: state.userFormsResponses,
    user: state.user
  };
}
function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      login,
      resetResponses
    },
    dispatch
  );
}
Login.propTypes={
  language: PropTypes.string.isRequired,
  /**
   * Result of the posted form
   */
  userFormsResponses: PropTypes.object,
  /**
   * User objet to edit
   */
  user: PropTypes.object,
  /**
   * Router history object
   */
  history: PropTypes.object
}
export default withRouter(
  connect(
    mapStateToProps,
    matchDispatchToProps
  )(Login)
);
