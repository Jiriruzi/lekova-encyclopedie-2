import React from 'react';
import {Document, Page} from 'react-pdf/dist/entry.webpack';
import {Vocabulary} from 'Libs/Vocabulary.js';
import Spinner from 'react-spinkit';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import faDownload from '@fortawesome/fontawesome-free-solid/faDownload';
import PropTypes from "prop-types";
/**
 * Component displaying SPC
 */
export class SPC extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            numPages: 0
        };
        this.onDocumentLoad = this.onDocumentLoad.bind(this);
    }


    onDocumentLoad({numPages}) {
        this.setState({numPages});
    }
    getPages() {
        const pages = [];
        for (let i = 1;
            i <= this.state.numPages; i++) {
            pages.push(<Page
                scale={1.5}
                key={i}
                loading={null}
                pageNumber={i}
                renderTextLayer={false}
            />);
        }
        return pages;
    }
    getUrl() {
        if (!this.props.doc || !this.props.doc[`${this.props.context}hasSPCDocument`]) {
            return null;
        }
        const url = this.props.doc[`${this.props.context}hasSPCDocument`]['@id'] || this.props.doc[`${this.props.context}hasSPCDocument`];
        return {realUrl: url,
            url: `/sukl_spc/${url}`};
    }


    render() {
        const url = this.getUrl();
        if (!url) {
            return (<div className="row col-xs-12">
                {Vocabulary.SPCNotFound[this.props.language]}
            </div>);
        }
        return (
            <div>
                <Document
                    onLoadSuccess={this.onDocumentLoad}
                    error={<div className="row col-xs-12">
                        {Vocabulary.SPCNotFound[this.props.language]}
                    </div>}
                    loading={<div className="page-spinner spc-spinn"><Spinner name="ball-spin-fade-loader" color="#3ea898" /></div>}
                    file={url.url}
                >
                    <div className="row col-xs-12">
                        {Vocabulary.SPCDesc[this.props.language]}</div>
                    <div className="row col-xs-12">
                        <a className="fa-button fa-download" href={url.realUrl}>
                            {Vocabulary.Download[this.props.language]} SPC&nbsp;<FontAwesomeIcon icon={faDownload} /></a>
                    </div>
                    <div className="row col-xs-12 spc">
                        {
                            this.getPages()
                        }
                    </div>
                </Document>
            </div>
        );
    }
}

SPC.propTypes = {
    /**
     * Which component should of the document should be displayed
     */
    context: PropTypes.string.isRequired,
    language: PropTypes.string.isRequired,
    /**
     * Data of this document
     */
    doc: PropTypes.object,
  
  };
  
export default SPC;
