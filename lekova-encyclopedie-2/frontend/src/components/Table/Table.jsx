import React from "react";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import TextContainer from "Components/TextContainer/TextContainer.jsx";
import PropTypes from "prop-types";

/**
 * Component that renders list-type of information
 */
export class Table extends React.Component {
  // Return the head of table
  getLabels(keyTr) {
    let key = 0;
    const labels = [];
    for (let i = 0; i < this.props.confDoc.items.length; i++) {
      let label = "";
      if (
        this.props.confDoc.items[i].label &&
        this.props.confDoc.items[i].label[this.props.language]
      ) {
        label = this.props.confDoc.items[i].label[this.props.language];
      } else if (
        this.props.confDoc.items[i].label &&
        Vocabulary[this.props.confDoc.items[i].label]
      ) {
        label =
          Vocabulary[this.props.confDoc.items[i].label][this.props.language];
      }
      labels.push(
        <td
          colSpan={this.props.confDoc.items[i].span || 1}
          className="table-prop-label"
          key={key++}
        >
          {label}
        </td>
      );
    }
    return <tr key={keyTr}>{labels}</tr>;
  }
  /**
   *  Returns data for column of the row from document and arrayPath, if multivalue is true, then there will be more values of the given column and both will be displayed
   * @param {object} doc
   * @param {array} arrayPath
   * @param {number} key
   * @param {bool} multiValue
   */
  getItem(doc, arrayPath, key, multiValue) {
    let value;
    if (
      multiValue &&
      doc[this.props.context + arrayPath[0][0]] instanceof Array
    ) {
      value = [];
      for (
        let i = 0;
        i < doc[this.props.context + arrayPath[0][0]].length;
        i++
      ) {
        const newArrayPath = JSON.parse(JSON.stringify(arrayPath));
        newArrayPath[0].shift();
        value.push({
          value: {
            ...utils.getValueFromArrayPaths(
              doc[this.props.context + arrayPath[0][0]][i],
              this.props.context,
              this.props.language,
              newArrayPath
            ),
            to: utils.getLink(
              arrayPath[0].length > 1
                ? doc[this.props.context + arrayPath[0][0]][i]
                : doc,
              arrayPath[0][0]
            )
          }
        });
      }
    } else {
      value = utils.getValueFromArrayPaths(
        doc,
        this.props.context,
        this.props.language,
        arrayPath
      );
      value = {
        ...value,
        to: utils.getLink(
          arrayPath[0].length > 1
            ? doc[this.props.context + arrayPath[0][0]]
            : doc,
          arrayPath[0][0]
        )
      };
    }
    return { key, span: 1, class: this.props.confDoc.items[key].class, value };
  }
  /**
   * Returns the data for a row
   * @param {object} doc
   */
  getLine(doc) {
    const line = [];
    for (let i = 0; i < this.props.confDoc.items.length; i++) {
      line.push({
        ...this.getItem(
          doc,
          this.props.confDoc.items[i].path,
          i,
          this.props.confDoc.items[i].multiValue
        ),
        span: this.props.confDoc.items[i].span || 1
      });
    }
    return line;
  }
  getHead() {
    let keyTr = 0;
    const lines = [];
    lines.push(this.getLabels(keyTr++));
    return lines;
  }
  /**
   * Returns data for all rows
   */
  getLines() {
    const lines = [];
    let keyTr = 0;
    const doc = utils.getArray(
      this.props.datDoc,
      this.props.context,
      this.props.confDoc.property
    );
    if (doc !== undefined) {
      if (doc[0] === undefined) {
        const line = this.getLine(doc, keyTr++);
        const inBussines = utils.inBussines(
          doc,
          this.props.context,
          this.props.confDoc.property
        );
        lines.push({ values: [...line], inBussines });
      } else if (doc !== null) {
        for (let i = 0; i < doc.length; i++) {
          const line = this.getLine(doc[i], i + keyTr);
          const inBussines = utils.inBussines(
            doc[i],
            this.props.context,
            this.props.confDoc.property
          );
          lines.push({ values: [...line], inBussines });
        }
      }
    }
    return lines;
  }
/**
 * Renders rows of the given data
 * @param {array} lines 
 */
  renderLines(lines) {
    if (!this.props.doNotSort) {
      lines = lines.sort((a, b) => {
        const inBa = a.inBussines !== "false" || a.inBussines === true;
        const inBb = b.inBussines !== "false" || b.inBussines === true;
        if (inBa && !inBb) {
          return -1;
        }
        if (!inBa && inBb) {
          return 1;
        }
        const valA =
          a.values[0].value instanceof Array
            ? a.values[0].value[0].value
            : a.values[0].value.value;
        const valB =
          b.values[0].value instanceof Array
            ? b.values[0].value[0].value
            : b.values[0].value.value;
        if (valA instanceof String) {
          return valA.localeCompare(valB);
        }
        if (valA < valB) return -1;
        if (valA > valB) return 1;
        return 0;
      });
    }
    return lines.map((item, i) => (
      <tr
        key={i}
        className={
          item.inBussines !== "false" || item.inBussines === true
            ? ""
            : "not-in-bussines"
        }
      >
        {item.values.map(item2 => (
          <td colSpan={item2.span} key={item2.key} className={item2.class}>
            {item2.value instanceof Array ? (
              item2.value.map((item3, i) => (
                <TextContainer
                  key={i}
                  to={item3.value.to}
                  value={item3.value}
                />
              ))
            ) : (
              <TextContainer to={item2.value.to} value={item2.value} />
            )}
          </td>
        ))}
      </tr>
    ));
  }
  render() {
    const lines = this.getLines();
    if (lines.length === 0) {
      return null;
    }
    let label = "";
    if (
      this.props.confDoc.label &&
      this.props.confDoc.label[this.props.language]
    ) {
      label = this.props.confDoc.label[this.props.language];
    } else if (
      this.props.confDoc.label &&
      Vocabulary[this.props.confDoc.label]
    ) {
      label = Vocabulary[this.props.confDoc.label][this.props.language];
    }

    return (
      <div className="col-xs-12">
        {this.props.withoutTitle ? null : (
          <div className="table-label">{label}</div>
        )}
        <div className="table-container">
          <table
            key={`table${this.props.datDoc["@id"]}`}
            className={`drug-table ${
              this.props.confDoc.class ? this.props.confDoc.class : ""
            }`}
          >
            <thead key={`thead${this.props.datDoc["@id"]}`}>
              {this.getHead()}
            </thead>
            <tbody key={`tbody${this.props.datDoc["@id"]}`}>
              {this.renderLines(lines)}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
Table.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Part of data from which should be the information taken
   */
  datDoc: PropTypes.object,
  /**
   * Type of the document
   */
  type: PropTypes.string,
  /**
   * Configuration for this info of the document
   */
  confDoc: PropTypes.object.isRequired,
  /**
   * Whether the items should not be sorted
   */
  doNotSort: PropTypes.bool,
  /**
   * Whether the title should not be displayed
   */
  withoutTitle: PropTypes.bool
};
export default Table;
