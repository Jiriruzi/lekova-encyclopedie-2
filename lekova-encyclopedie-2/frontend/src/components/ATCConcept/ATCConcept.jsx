import React from "react";
import { connect } from "react-redux";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import InfoFromConf from "Components/InfoFromConf/InfoFromConf.jsx";
import utils from "Libs/utils.js";
import { getDocument } from "Actions/documentsFetches.js";
import { bindActionCreators } from "redux";
import Table from "Components/Table/Table.jsx";
import { updateHistory } from "Actions/userFetches.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import PropTypes from "prop-types";

/**
 * Component for rendering ATCConcept document
 */
export class ATCConcept extends React.Component {
  componentWillMount() {
    this.props.getDocument(this.props.type, this.props.id);
    if (this.props.ownPage) {
      this.props.getDocument("Configuration", this.props.type);
    }
    this.props.getDocument("PackTable", "MedicinalProduct");
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.type !== nextProps.type) {
      this.props.getDocument("Configuration", nextProps.type);
      this.props.getDocument(nextProps.type, nextProps.id);
      this.props.getDocument("PackTable", "MedicinalProduct");
    } else if (this.props.id !== nextProps.id) {
      this.props.getDocument(nextProps.type, nextProps.id);
    }
    if (
      nextProps.user &&
      nextProps.user.role === "expert" &&
      nextProps.document &&
      nextProps.document.doc &&
      nextProps.document !== this.props.document
    ) {
      this.props.updateHistory(
        this.props.type,
        this.props.id,
        nextProps.document.doc[`${this.props.context}title`]
      );
    }
  }
  /**
   * Returns closest narrower Atc concepts for this document
   * @param {Object} doc
   */
  getOnlyClosestNarrower(doc) {
    const closest = [];
    if (
      doc &&
      doc[`${this.props.context}narrowerTransitive`] &&
      doc[`${this.props.context}narrowerTransitive`] instanceof Array
    ) {
      const nar = doc[`${this.props.context}narrowerTransitive`];
      for (let i = 0; i < nar.length; i++) {
        closest.push(nar[i]);
      }
      for (let i = 0; i < closest.length; i++) {
        for (let j = 0; j < closest.length; j++) {
          if (i !== j && closest[j]["@id"].includes(closest[i]["@id"])) {
            closest.splice(j, 1);
            if (j < i) {
              i--;
            }
            j--;
          }
        }
      }
    }
    return closest;
  }

  render() {
    const isLoading =
      !this.props.document ||
      this.props.document.loading ||
      ((!this.props.packConf || this.props.packConf.loading) &&
        this.props.component === "?cheapest") ||
      (!this.props.conf || this.props.conf.loading);
    let component = null;
    if (!isLoading) {
      component = (
        <InfoFromConf
          {...this.props}
          document={{
            ...this.props.document,
            doc: {
              ...this.props.document.doc,
              [`${
                this.props.context
              }narrowerTransitive`]: this.getOnlyClosestNarrower(
                this.props.document.doc
              )
            }
          }}
        />
      );
      // handling different components
      if (this.props.component === "?cheapest") {
        if (
          this.props.document &&
          this.props.document.doc &&
          (!this.props.document.doc[
            `${this.props.context}hasCheapestAlternatives`
          ] ||
            this.props.document.doc[
              `${this.props.context}hasCheapestAlternatives`
            ].length === 0)
        ) {
          component = (
            <div
              style={{ paddingTop: "15px", fontWeight: "bold" }}
              className="row col-xs-12"
            >
              {Vocabulary.NoAlternatives[this.props.language]}
            </div>
          );
        } else {
          component = (
            <div>
              <Table
                language={this.props.language}
                context={this.props.context}
                datDoc={this.props.document ? this.props.document.doc : null}
                confDoc={
                  this.props.packConf &&
                  this.props.packConf.doc &&
                  this.props.packConf.doc.table &&
                  this.props.packConf.doc.table[0]
                    ? {
                        ...this.props.packConf.doc.table[0],
                        property: "hasCheapestAlternatives"
                      }
                    : {}
                }
              />
            </div>
          );
        }
        component = (
          <div>
            <div className="row col-xs-12">
              {this.props.location.pathname.toLowerCase().includes("atc")
                ? Vocabulary.CheapestDescAtc[this.props.language]
                : Vocabulary.CheapestDesc[this.props.language]}
            </div>
            {component}
          </div>
        );
      }
    }
    return this.props.ownPage ? (
      <PageContainer
        type={this.props.type}
        language={this.props.language}
        title={
          this.props.document
            ? utils.getValue(
                this.props.document.doc,
                this.props.context,
                this.props.language,
                "title"
              ).value
            : null
        }
        isLoading={isLoading}
        error={
          (this.props.document && this.props.document.error) ||
          (this.props.conf && this.props.conf.error)
        }
        routes={
          this.props.routes || [
            { path: "detail", label: "Detail" },
            { path: "cheapest", label: "CheapestAtc" }
          ]
        }
      >
        {component}
      </PageContainer>
    ) : (
      component
    );
  }
}

ATCConcept.propTypes = {
  /**
   * History location object
   */
  location: PropTypes.object.isRequired,
  /**
   * Which component should of the document should be displayed
   */
  component: PropTypes.string,
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * id of document
   */
  id: PropTypes.string.isRequired,
  /**
   * Object containing info about user
   */
  user: PropTypes.object,
  /**
   * If the ATCConcept is shown on page determined for ATCConcept or its showing some info about ATCConcept in another document
   */
  ownPage: PropTypes.bool,
  /**
   * Type of the document
   */
  type: PropTypes.string.isRequired,
  /**
   * Data of this document
   */
  document: PropTypes.object,
  /**
   * Configuration for this document
   */
  conf: PropTypes.object,
  /**
   * Configuration of medicinal product packaging for showing some info
   */
  packConf: PropTypes.object
};
function mapStateToProps(state, ownProps) {
  let id;
  if (ownProps.id) {
    id = ownProps.id;
  } else {
    id = ownProps.match ? ownProps.match.params.id : null;
  }
  return {
    location: ownProps.location,
    component: ownProps.location.search,
    context: state.environment.context,
    language: state.environment.language,
    id,
    user: state.user,
    ownPage: !ownProps.notOwnPage,
    type: "ATCConcept",
    document: ownProps.match ? state.documents.ATCConcept : null,
    conf: state.documents.Configuration,
    packConf: state.documents.PackTable
  };
}

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument,
      updateHistory
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(ATCConcept);
