import React from "react";
import BasicInfo from "Components/BasicInfo/BasicInfo.jsx";
import List from "Components/List/List.jsx";
import Table from "Components/Table/Table.jsx";
import { Vocabulary } from "Libs/Vocabulary.js";
import PropTypes from "prop-types";
import utils from "Libs/utils.js";
/**
 * Component taking data and displaying it with the given configuration
 */
export class InfoFromConf extends React.Component {
  /**
   * Pushes jsx objects with info gotten from document using configuration
   * @param {object} datDoc
   */
  getSections(datDoc) {
    const confDoc = this.props.conf.doc;
    const sections = [];
    let empty = true;
    let key = 0;
    for (
      let i = 0;
      confDoc.basicInfo !== undefined && i < confDoc.basicInfo.length;
      i++
    ) {
      if (
        datDoc[this.props.context + confDoc.basicInfo[i].property] &&
        utils.getValue(
          datDoc,
          this.props.context,
          this.props.language,
          confDoc.basicInfo[i].property
        ).value
      ) {
        empty = false;
      }
      sections.push(
        <BasicInfo
          key={key++}
          language={this.props.language}
          context={this.props.context}
          datDoc={datDoc}
          confDoc={confDoc.basicInfo[i]}
        />
      );
    }
    const lists = [];
    for (
      let i = 0;
      confDoc.list !== undefined && i < confDoc.list.length;
      i++
    ) {
      if (
        datDoc[this.props.context + confDoc.list[i].property] &&
        utils.getValue(
          datDoc,
          this.props.context,
          this.props.language,
          confDoc.list[i].property
        ).value
      ) {
        empty = false;
      }
      lists.push(
        <List
          listLikeTableLabel={this.props.listLikeTableLabel}
          key={`list${i}`}
          language={this.props.language}
          context={this.props.context}
          datDoc={datDoc}
          confDoc={confDoc.list[i]}
          type={confDoc._id}
        />
      );
      if (confDoc.list[i].rowEnd) {
        lists.push(
          <div key={`newLineList${i}`} className="col-xs-12 new-line" />
        );
      }
    }
    sections.push(
      <div key={key++} className="row">
        {lists}
      </div>
    );
    for (
      let i = 0;
      confDoc.table !== undefined && i < confDoc.table.length;
      i++
    ) {
      if (
        datDoc[this.props.context + confDoc.table[i].property] &&
        utils.getValue(
          datDoc,
          this.props.context,
          this.props.language,
          confDoc.table[i].property
        ).value
      ) {
        empty = false;
      }
      sections.push(
        <Table
          key={key++}
          language={this.props.language}
          context={this.props.context}
          datDoc={datDoc}
          confDoc={confDoc.table[i]}
        />
      );
    }
    if (sections.length === 0) {
      sections.push(<NoMatch />);
    }
    return !empty ? (
      sections
    ) : (
      <div
        style={{ paddingTop: "15px", fontWeight: "bold" }}
        className="row col-xs-12"
      >
        {Vocabulary.NoMoreInfo[this.props.language]}
      </div>
    );
  }

  render() {
    return <div>{this.getSections(this.props.document.doc)}</div>;
  }
}

InfoFromConf.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Data of the document
   */
  document: PropTypes.object,
  /**
   * Configuration for the type of document
   */
  confDoc: PropTypes.object,
  /**
   * If the list should be displayed as table, this prop contains header for the table
   */
  listLikeTableLabel: PropTypes.object
};

export default InfoFromConf;
