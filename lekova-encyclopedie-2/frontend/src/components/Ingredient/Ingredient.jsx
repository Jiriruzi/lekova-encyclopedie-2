import React from "react";
import { connect } from "react-redux";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import InfoFromConf from "Components/InfoFromConf/InfoFromConf.jsx";
import Interactions from "Components/Interactions/Interactions.jsx";
import { getDocument } from "Actions/documentsFetches.js";
import { bindActionCreators } from "redux";
import utils from "Libs/utils.js";
import { updateHistory } from "Actions/userFetches.js";
import PropTypes from "prop-types";

/** 
 * Special component for Ingredient
 * that gets configuration file and data file and than display data requiered data
 */
export class Ingredient extends React.Component {
  componentWillMount() {
    this.props.getDocument(this.props.type, this.props.id);
    this.props.getDocument("Configuration", this.props.type);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.type !== nextProps.type) {
      this.props.getDocument("Configuration", nextProps.type);
      this.props.getDocument(nextProps.type, nextProps.id);
    } else if (this.props.id !== nextProps.id) {
      this.props.getDocument(nextProps.type, nextProps.id);
    }
    if (
      nextProps.user &&
      nextProps.user.role === "expert" &&
      nextProps.document &&
      nextProps.document.doc &&
      nextProps.document !== this.props.document
    ) {
      this.props.updateHistory(
        this.props.type,
        this.props.id,
        nextProps.document.doc[`${this.props.context}title`]
      );
    }
  }
  getComponent(todoBien) {
    if (!todoBien) {
      return null;
    }
    switch (this.props.search) {
      case "?interactions": {
        return (
          <Interactions
            {...this.props}
            ingredients={[this.props.document.doc]}
          />
        );
      }
      default: {
        return <InfoFromConf {...this.props} />;
      }
    }
  }

  render() {
    const isLoading =
      !this.props.document ||
      this.props.document.loading ||
      (!this.props.conf || this.props.conf.loading);
    const error =
      (this.props.document && this.props.document.error) ||
      (this.props.conf && this.props.conf.error);
    const todoBien = !isLoading && !error;
    const component = this.getComponent(todoBien);
    return (
      <PageContainer
        type="Ingredient"
        id={this.props.id}
        language={this.props.language}
        context={this.props.context}
        routes={[
          { path: "detail", label: "Detail" },
          { path: "interactions", label: "Interactions" }
        ]}
        title={
          this.props.document
            ? utils.getValue(
                this.props.document.doc,
                this.props.context,
                this.props.language,
                "title"
              ).value
            : null
        }
        isLoading={isLoading}
        error={error}
      >
        {component}
      </PageContainer>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    search: ownProps.location.search,
    context: state.environment.context,
    language: state.environment.language,
    id: ownProps.match ? ownProps.match.params.id : null,
    type: "Ingredient",
    document: state.documents.Ingredient,
    conf: state.documents.Configuration,
    user: state.user
  };
}

Ingredient.propTypes = {
  /**
   * Which component should of the document should be displayed
   */
  search: PropTypes.string,
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * id of document
   */
  id: PropTypes.string.isRequired,
  /**
   * Object containing info about user
   */
  user: PropTypes.object,
  /**
   * Type of the document
   */
  type: PropTypes.string.isRequired,
  /**
   * Data of this document
   */
  document: PropTypes.object,
  /**
   * Configuration for this document
   */
  conf: PropTypes.object
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument,
      updateHistory
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(Ingredient);
