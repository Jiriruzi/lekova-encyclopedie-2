import React from "react";
import utils from "Libs/utils.js";
import { Vocabulary } from "Libs/Vocabulary.js";
import TextContainer from "Components/TextContainer/TextContainer.jsx";
import PropTypes from "prop-types";

/**
 * Component that renders list-type of information
 */ 

export class List extends React.Component {
  /**
   * Returns item of the list with this structure { value: { value: text to display, lang}, sublistst - list of properties for this item}
   * @param {object} doc 
   * @param {object} confDoc 
   * @param {bool} withSublist 
   * @param {number} keyIt 
   */
  getItem(doc, confDoc, withSublist, keyIt) {
    let value;
    if (confDoc.value && confDoc.value.property instanceof Array) {
      value = utils.getValueFromArrayPaths(
        doc,
        this.props.context,
        this.props.language,
        confDoc.value.property
      );
    } else {
      value = utils.getValue(
        doc,
        this.props.context,
        this.props.language,
        confDoc.value.property
      );
    }
    if (!value.value) {
      value = {
        value: utils.getTitleIfMissing(
          doc,
          confDoc.property,
          this.props.context
        ),
        lang: true
      };
    }
    // getting sublists
    if (withSublist) {
      const sublists = [];
      for (let i = 0; i < this.props.confDoc.sublist.length; i++) {
        let label = "";
        if (
          this.props.confDoc.sublist[i].label &&
          this.props.confDoc.sublist[i].label[this.props.language]
        ) {
          label = this.props.confDoc.sublist[i].label[this.props.language];
        } else if (
          this.props.confDoc.sublist[i].label &&
          Vocabulary[this.props.confDoc.sublist[i].label]
        ) {
          label =
            Vocabulary[this.props.confDoc.sublist[i].label][
              this.props.language
            ];
        }
        const subl = {
          values: this.getItems(
            {
              ...confDoc,
              property: this.props.confDoc.sublist[i].property,
              value: {
                property: this.props.confDoc.sublist[i].propOfProp,
                class: this.props.confDoc.sublist[i].class || ""
              }
            },
            doc[this.props.context + this.props.confDoc.sublist[i].property],
            keyIt,
            false
          ),
          label
        };
        subl.values = subl.values.sort((a, b) => {
          if (a.value.value < b.value.value) return -1;
          if (a.value.value > b.value.value) return 1;
          return 0;
        });
        sublists.push(subl);
      }
      value = {
        ...value,
        sublists
      };
    }
    return value;
  }
  // calling this.getItem(...) for individual items of the list
  getItems(confDoc, doc, keyIt, withSublist) {
    const items = [];
    let isEmpty = true;
    if (doc !== undefined) {
      if (doc[0] === undefined) {
        const line = this.getItem(doc, confDoc, withSublist, keyIt);
        if (line.value) {
          isEmpty = false;
        }
        const key = `${this.props.type}${confDoc.property}${keyIt.i++}`;
        items.push({
          key,
          className: confDoc.value.class,
          language: this.props.language,
          value: line,
          to: utils.getLink(doc, confDoc.property)
        });
      } else if (doc != null) {
        for (let i = 0; i < doc.length; i++) {
          const line = this.getItem(doc[i], confDoc, withSublist, keyIt);
          if (line.value) {
            isEmpty = false;
          }
          const key = `${this.props.type}${confDoc.property}${keyIt.i++}`;
          items.push({
            key,
            className: confDoc.value.class,
            language: this.props.language,
            value: line,
            to: utils.getLink(doc[i], confDoc.property)
          });
        }
      }
    }
    return isEmpty ? [] : items;
  }
// Gets items, sorts them and them renders  them with the given configuration
  render() {
    if (!this.props.confDoc || !this.props.confDoc.value) {
      return null;
    }
    const keyIt = { i: 0 };
    let label = "";
    if (
      this.props.confDoc.label &&
      this.props.confDoc.label[this.props.language]
    ) {
      label = this.props.confDoc.label[this.props.language];
    } else if (
      this.props.confDoc.label &&
      Vocabulary[this.props.confDoc.label]
    ) {
      label = Vocabulary[this.props.confDoc.label][this.props.language];
    }
    const doc = utils.getArray(
      this.props.datDoc,
      this.props.context,
      this.props.confDoc.property
    );
    const withSublist = !!this.props.confDoc.sublist;
    let items = this.getItems(this.props.confDoc, doc, keyIt, withSublist);
    for (
      let i = 0;
      this.props.confDoc.value.connect &&
      i < this.props.confDoc.value.connect.length;
      i++
    ) {
      if (
        doc &&
        doc[this.props.context + this.props.confDoc.value.connect[i].property]
      ) {
        items = items.concat(
          this.getItems(
            { ...this.props.confDoc },
            doc[
              this.props.context + this.props.confDoc.value.connect[i].property
            ],
            keyIt,
            withSublist
          )
        );
      }
    }
    if (items.length === 0) {
      return null;
    }
    items = items.sort((a, b) => {
      if (a.value.value < b.value.value) return -1;
      if (a.value.value > b.value.value) return 1;
      return 0;
    });
    items = items.map(item => (
      <div key={item.key} className={item.className}>
        <TextContainer
          className="list-text"
          language={item.language}
          value={item.value}
          to={item.to}
        />
        {item.value.sublists
          ? item.value.sublists.map((sublist, i) => (
              <div className="sublist-container" key={i}>
                {sublist.label ? (
                  <div className="sublist-label"> {sublist.label}:</div>
                ) : null}
                <div className="sublist">
                  {sublist.values.map(subitem => (
                    <div className={subitem.className} key={subitem.key}>
                      <TextContainer
                        language={subitem.language}
                        value={subitem.value}
                        to={subitem.to}
                      />
                    </div>
                  ))}
                </div>
              </div>
            ))
          : null}
      </div>
    ));
    return (
      <div
        className={`${
          this.props.confDoc.class === undefined ? "" : this.props.confDoc.class
        }`}
      >
        {this.props.listLikeTableLabel ? this.props.listLikeTableLabel : null}

        {label ? <div className="bold list-label">{label}</div> : null}
        <div className={`list${withSublist ? " list-with-sublist" : ""}`}>
          {items}
        </div>
      </div>
    );
  }
}

List.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * Part of data from which should be the information taken 
   */
  datDoc: PropTypes.object.isRequired,
  /**
   * Type of the document
   */
  type: PropTypes.string,
  /**
   * Configuration for this info of the document
   */
  confDoc: PropTypes.object.isRequired,

};

export default List;
