import React from "react";
import { connect } from "react-redux";
import PageContainer from "Components/PageContainer/PageContainer.jsx";
import InfoFromConf from "Components/InfoFromConf/InfoFromConf.jsx";
import utils from "Libs/utils.js";
import { getDocument } from "Actions/documentsFetches.js";
import { bindActionCreators } from "redux";
import { Vocabulary } from "Libs/Vocabulary.js";
import { updateHistory } from "Actions/userFetches.js";
import PropTypes from "prop-types";

/**
 *  Special component for DrugsProp
 * that gets configuration file and data file and than display data requiered data
 */
export class DrugsProp extends React.Component {
  componentWillMount() {
    this.props.getDocument(this.props.type, this.props.id);
    this.props.getDocument("Configuration", this.props.type);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.type !== nextProps.type) {
      this.props.getDocument("Configuration", nextProps.type);
      this.props.getDocument(nextProps.type, nextProps.id);
    } else if (this.props.id !== nextProps.id) {
      this.props.getDocument(nextProps.type, nextProps.id);
    }
    if (
      nextProps.user &&
      nextProps.user.role === "expert" &&
      nextProps.document &&
      nextProps.document.doc &&
      nextProps.document !== this.props.document
    ) {
      this.props.updateHistory(
        this.props.type,
        this.props.id,
        nextProps.document.doc[`${this.props.context}title`]
      );
    }
  }
  render() {
    return (
      <PageContainer
        type={this.props.type}
        language={this.props.language}
        title={
          this.props.document
            ? utils.getValue(
                this.props.document.doc,
                this.props.context,
                this.props.language,
                "title"
              ).value
            : null
        }
        isLoading={
          !this.props.document ||
          this.props.document.loading ||
          (!this.props.conf || this.props.conf.loading)
        }
        error={
          (this.props.document && this.props.document.error) ||
          (this.props.conf && this.props.conf.error)
        }
      >
        <InfoFromConf
          {...this.props}
          listLikeTableLabel={
            <div>
              <div className="ing-mp-label">
                {Vocabulary.Ingredient[this.props.language]}
              </div>
              <div className="ing-mp-label">
                {Vocabulary.MedicinalProduct[this.props.language]}
              </div>
            </div>
          }
        />
      </PageContainer>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    context: state.environment.context,
    language: state.environment.language,
    id: ownProps.match ? ownProps.match.params.id : null,
    type: ownProps.match ? ownProps.match.params.type : null,
    document: ownProps.match
      ? state.documents[ownProps.match.params.type]
      : null,
    conf: state.documents.Configuration,
    user: state.user
  };
}

DrugsProp.propTypes = {
  context: PropTypes.string.isRequired,
  language: PropTypes.string.isRequired,
  /**
   * id of document
   */
  id: PropTypes.string.isRequired,
  /**
   * Object containing info about user
   */
  user: PropTypes.object,
  /**
   * If the ATCConcept is shown on page determined for ATCConcept or its showing some info about ATCConcept in another document
   */
  ownPage: PropTypes.bool,
  /**
   * Type of the document
   */
  type: PropTypes.string.isRequired,
  /**
   * Data of this document
   */
  document: PropTypes.object,
  /**
   * Configuration for this document
   */
  conf: PropTypes.object
};

function matchDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      getDocument,
      updateHistory
    },
    dispatch
  );
}
export default connect(
  mapStateToProps,
  matchDispatchToProps
)(DrugsProp);
