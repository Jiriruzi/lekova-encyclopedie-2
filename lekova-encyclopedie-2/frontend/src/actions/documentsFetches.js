import "whatwg-fetch";
import utils from "Libs/utils.js";

export function documentLoadingAction(type) {
  return { type: "DOCUMENT_LOADING", payload: type };
}

export function documentNotFoundAction(type) {
  return { type: "DOCUMENT_NOT_FOUND", payload: type };
}
export function addInteractionItemAction(newItem) {
  return {
    type: "ADD_INTERACTION_ITEM",
    payload: newItem
  };
}
export function removeInteractionItemAction(id) {
  return {
    type: "REMOVE_INTERACTION_ITEM",
    payload: id
  };
}

export function interactionsFailedAction() {
  return {
    type: "INTERACTIONS_FAIL"
  };
}

export function interactionsResetAction() {
  return {
    type: "INTERACTIONS_RESET"
  };
}
export function interactionsLoadingAction() {
  return {
    type: "INTERACTIONS_LOADING"
  };
}
export function interactionsCreatedAction(interactionsIng) {
  return {
    type: "INTERACTIONS_CREATED",
    payload: interactionsIng
  };
}
export function documentFoundAction(type, response) {
  return {
    type: "DOCUMENT_FOUND",
    payload: {
      type,
      doc: response
    }
  };
}
/**
 * Fetching data from backend
 * @param {string} type 
 * @param {string} id 
 */
export function getDocument(type, id) {
  const url = utils.getUrl(type, id);
  if (!url) {
    return dispatch => {
      dispatch(documentNotFoundAction(type));
    };
  }
  return dispatch => {
    dispatch(documentLoadingAction(type));
    return fetch(url, {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "*/*",
        Accept: "*/*"
      }
    })
      .then(response => {
        if (!response.ok || !type || !id || response.status >= 300) {
          throw Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(
        response => {
          dispatch(documentFoundAction(type, response));
        },
        () => dispatch(documentNotFoundAction(type))
      );
  };
}
/**
 * Resets state of interactions reducer
 */
export function resetInteractions() {
  return dispatch => dispatch(interactionsResetAction());
}
function checkInteractionFetch(response, dispatch) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
/**
 * Gets all ingredients of specific item
 * @param {Object} item 
 * @param {string} context 
 * @param {*} dispatch 
 */
export function getAllIngredients(item, context, dispatch) {
  const promises = [];
  if (!item) {
    return Promise.all(promises);
  }
  if (item && item._type === "ingredient") {
    promises.push(
      fetch(utils.getUrl("ingredient", item._id), {
        method: "GET",
        credentials: "same-origin",
        headers: {
          "Content-Type": "*/*",
          Accept: "*/*"
        }
      })
        .then(response => checkInteractionFetch(response, dispatch))
        .then(response => response.json())
    );
  } else if (!item._id) {
    promises.push({});
  } else {
    promises.push(
      fetch(utils.getUrl("medicinalProduct", item._id), {
        method: "GET",
        credentials: "same-origin",
        headers: {
          "Content-Type": "*/*",
          Accept: "*/*"
        }
      })
        .then(response => checkInteractionFetch(response, dispatch))
        .then(response => response.json())
        .then(response => {
          const medPromises = [];
          if (!response[`${context}hasActiveIngredient`]) {
            return null;
          }

          if (response[`${context}hasActiveIngredient`] instanceof Array) {
            for (
              let j = 0;
              j < response[`${context}hasActiveIngredient`].length;
              j++
            ) {
              medPromises.push(
                fetch(
                  utils.getUrl(
                    "ingredient",
                    response[`${context}hasActiveIngredient`][j]["@id"]
                  ),
                  {
                    method: "GET",
                    credentials: "same-origin",
                    headers: {
                      "Content-Type": "*/*",
                      Accept: "*/*"
                    }
                  }
                )
                  .then(response2 => checkInteractionFetch(response2, dispatch))
                  .then(response2 => response2.json())
                  .then(response2 => ({
                    ...response2,
                    medicinalProduct: response
                  }))
              );
            }
          } else {
            medPromises.push(
              fetch(
                utils.getUrl(
                  "ingredient",
                  response[`${context}hasActiveIngredient`]["@id"]
                ),
                {
                  method: "GET",
                  credentials: "same-origin",
                  headers: {
                    "Content-Type": "*/*",
                    Accept: "*/*"
                  }
                }
              )
                .then(response2 => checkInteractionFetch(response2, dispatch))
                .then(response2 =>
                  response2.json().then(response3 => ({
                    ...response3,
                    medicinalProduct: response
                  }))
                )
            );
          }
          return Promise.all(medPromises);
        })
    );
  }
  return Promise.all(promises);
}
/**
 * Creates array of ingredients to show in interactions component
 * @param {string} context 
 */
export function makeInteractions(context) {
  return (dispatch, getState) => {
    const ingredients = JSON.parse(
      JSON.stringify(getState().interactions.items)
    );
    let oneArrayIngredients = [];
    for (let i = 0; i < ingredients.length; i++) {
      if (ingredients[i] instanceof Array) {
        oneArrayIngredients = oneArrayIngredients.concat(ingredients[i]);
      } else {
        oneArrayIngredients.push(ingredients[i]);
      }
    }
    for (let i = 0; i < oneArrayIngredients.length; i++) {
      if (!oneArrayIngredients[i]) {
        oneArrayIngredients[i] = {
          skip: true
        };
        continue;
      }
      for (let k = i + 1; k < oneArrayIngredients.length; k++) {
        if (!oneArrayIngredients[k]) {
          oneArrayIngredients[k] = {
            skip: true
          };
          continue;
        }
        if (
          oneArrayIngredients[i]["@id"] === oneArrayIngredients[k]["@id"] &&
          ((!oneArrayIngredients[i].medicinalProduct &&
            !oneArrayIngredients[k].medicinalProduct) ||
            (oneArrayIngredients[k].medicinalProduct &&
              oneArrayIngredients[i].medicinalProduct &&
              oneArrayIngredients[i].medicinalProduct["@id"] ===
                oneArrayIngredients[k].medicinalProduct["@id"]))
        ) {
          oneArrayIngredients[k].skip = true;
        }
      }
    }
    const interactionsIng = [];
    for (let i = 0; i < oneArrayIngredients.length; i++) {
      if (oneArrayIngredients[i].skip) {
        continue;
      }

      const ingInter = [];
      if (
        oneArrayIngredients[i][`${context}hasInteraction`] &&
        !(oneArrayIngredients[`${context}hasInteraction`] instanceof Array)
      ) {
        oneArrayIngredients[`${context}hasInteraction`] = [
          oneArrayIngredients[`${context}hasInteraction`]
        ];
      }
      for (
        let j = 0;
        oneArrayIngredients[i][`${context}hasInteraction`] &&
        j < oneArrayIngredients[i][`${context}hasInteraction`].length;
        j++
      ) {
        for (let k = i + 1; k < oneArrayIngredients.length; k++) {
          if (oneArrayIngredients[k].skip) {
            continue;
          }
          if (
            oneArrayIngredients[i][`${context}hasInteraction`][j][
              `${context}interactionWith`
            ] &&
            oneArrayIngredients[i][`${context}hasInteraction`][j][
              `${context}interactionWith`
            ]["@id"] &&
            oneArrayIngredients[k]["@id"] &&
            oneArrayIngredients[k]["@id"] ===
              oneArrayIngredients[i][`${context}hasInteraction`][j][
                `${context}interactionWith`
              ]["@id"]
          ) {
            ingInter.push({
              ...oneArrayIngredients[i][`${context}hasInteraction`][j],
              medicinalProduct: oneArrayIngredients[k].medicinalProduct
            });
          }
        }
      }
      const ingredient = oneArrayIngredients[i];
      ingredient[`${context}hasInteraction`] = ingInter;
      interactionsIng.push(ingredient);
    }
    dispatch(interactionsCreatedAction(interactionsIng));
  };
}
/**
 * Adds item to array in redux of items we want to investigate for interactions
 * @param {Object} item 
 * @param {string} context 
 */
export function addInteractionItem(item, context) {
  return dispatch => {
    dispatch(interactionsLoadingAction());
    return getAllIngredients(item, context, dispatch).then(
      newItem => {
        dispatch(addInteractionItemAction(newItem, context));
      },
      () => dispatch(interactionsFailedAction())
    );
  };
}
/**
 * Removes item from array of items we want to investigate for interactions
 * @param {number} id 
 */
export function removeInteractionItem(id) {
  return dispatch => {
    dispatch(interactionsLoadingAction());
    dispatch(removeInteractionItemAction(id));
  };
}
