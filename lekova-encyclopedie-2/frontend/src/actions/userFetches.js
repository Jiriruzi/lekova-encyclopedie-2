import "whatwg-fetch";
import utils from "Libs/utils.js";
import {
  getAllIngredients,
  addInteractionItem
} from "Actions/documentsFetches.js";
import { updateMedicamentFront } from "Actions/medicationActions.js";
/**
 * Fetching user data for registering
 * @param {Object} user
 * @param {Function} resetState
 */
export function addUser(user, resetState) {
  return dispatch => {
    dispatch({
      type: "RESPONSE_LOADING"
    });
    return fetch("/user/register", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    }).then(
      response => {
        let registrationResult;
        if (response.status <= 202) {
          registrationResult = "Registered";
          resetState();
        } else if (response.status === 409) {
          registrationResult = "ExistingUser";
        } else if (response.status >= 400) {
          registrationResult = "SomethingWrongRegistration";
        }
        dispatch({
          type: "REGISTRATION_RESPONSE",
          payload: registrationResult
        });
      },
      () =>
        dispatch({
          type: "REGISTRATION_RESPONSE",
          payload: "SomethingWrongRegistration"
        })
    );
  };
}

/**
 * Fetches data for removing medicament from users medication
 * @param {number} id
 * @param {number} tryN
 */
export function removeMedicament(id, tryN) {
  return dispatch =>
    fetch("/user/remove_medicament", {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin",
      body: JSON.stringify(id)
    })
      .then(response => {
        if (response.status > 202) {
          if (!tryN) {
            tryN = 1;
          }
          tryN++;
          if (tryN < 3) {
            removeMedicament(id, tryN);
          } else {
            location.reload();
            alert("Something went wrong on the server");
          }
        }
      })
      .then(() => {
        window.postMessage("ENC_UPDATE", "*");
      });
}

/**
 *
 * @param {string} type
 * @param {string} id
 * @param {string} title
 * @param {number} tryN
 */
export function updateHistory(type, id, title, tryN) {
  return dispatch =>
    fetch("/user/update_history", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin",
      body: JSON.stringify({
        "@type": type,
        "@id": id,
        "enc:title": title,
        time: Date.now()
      })
    }).then(response => {
      if (response.status > 202) {
        if (!tryN) {
          tryN = 1;
        }
        tryN++;
        if (tryN < 3) {
          updateHistory(type, id, title, tryN);
        } else {
          location.reload();
          alert("Something went wrong on the server");
        }
      }
    });
}
/**
 * Fetches data to edit user
 * @param {Object} user
 */
export function editUser(user) {
  return dispatch => {
    dispatch({
      type: "RESPONSE_LOADING"
    });
    return fetch("/user/edit_user", {
      method: "PUT",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    }).then(
      response => {
        let result;
        if (response.status <= 202) {
          result = "Edited";
        } else {
          result = "SomethingWrong";
        }
        dispatch({
          type: "EDIT_RESPONSE",
          payload: result
        });
      },
      () =>
        dispatch({
          type: "EDIT_RESPONSE",
          payload: "SomethingWrong"
        })
    );
  };
}
/**
 * Fetch to get users data
 */
export function getUser() {
  return dispatch => {
    dispatch({
      type: "USER_LOADING"
    });
    return fetch("/user/get_user", {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "*/*",
        Accept: "*/*"
      }
    })
      .then(response => {
        if (!response.ok) {
          dispatch({
            type: "USER_NOT_FOUND",
            payload: "SomethingWrong"
          });
          throw Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(
        response => {
          dispatch({
            type: "USER_FOUND",
            payload: {
              ...response,
              birthday: {
                ...response.birthday,
                month: response.birthday.month - 1
              }
            }
          });
        },
        () =>
          dispatch({
            type: "USER_NOT_FOUND",
            payload: "SomethingWrong"
          })
      );
  };
}
/**
 *
 * @param {Object} user
 * @param {string} language
 */
export function sendPasswordLink(user, language) {
  return dispatch => {
    dispatch({
      type: "RESPONSE_LOADING"
    });
    return fetch("/user/send_password_link", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        username: user.username,
        language
      })
    }).then(response => {
      let result;
      if (response.status <= 202) {
        if (response.text() === null) {
          result = "TryLater";
        } else {
          result = "Sent";
        }
      } else if (response.status === 404) {
        result = "NonExistingUser";
      } else if (response.status === 400) {
        result = "TryLater";
      } else if (response.status === 401) {
        result = "AlreadyEmailed";
      } else {
        result = "SomethingWrong";
      }
      dispatch({
        type: "RESET_PASSWORD_RESPONSE",
        payload: result
      });
    });
  };
}
/**
 * Fetches users session info
 */
export function getSession() {
  return dispatch =>
    fetch("/user/get_session", {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          document.cookie = `username=${""}`;
          dispatch({
            type: "USER_SESSION",
            payload: { username: null, role: null }
          });
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(
        response => {
          const isLogged = !!(
            response &&
            response.userCtx &&
            response.userCtx.name
          );
          document.cookie = `username=${response.userCtx.name}`;
          dispatch({
            type: "USER_SESSION",
            payload: {
              username:
                response && response.userCtx && response.userCtx.name
                  ? response.userCtx.name
                  : null,
              role:
                response &&
                response.userCtx &&
                response.userCtx.roles &&
                response.userCtx.roles[0]
                  ? response.userCtx.roles[0]
                  : null
            }
          });
          return isLogged;
        },
        () =>
          dispatch({
            type: "USER_SESSION",
            payload: { role: null, username: null }
          })
      );
}
/**
 *
 * @param {Object} user
 * @param {Object} formApi
 * @param {Object} goBack
 */
export function login(user, formApi, goBack) {
  return dispatch => {
    dispatch({
      type: "RESPONSE_LOADING"
    });
    return fetch("/user/login", {
      credentials: "same-origin",
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(user)
    })
      .then(response => {
        if (response.status === 200) {
          dispatch({
            type: "LOGIN_RESPONSE",
            payload: "Logged"
          });
          if (goBack) {
            goBack();
          }
        } else {
          dispatch({
            type: "LOGIN_RESPONSE",
            payload: "BadCredentials"
          });
          formApi.setValue("password", "");
          formApi.setTouched("password", false);
        }
      })
      .then(() => dispatch(getSession()))
      .then(() => {
        window.postMessage("ENC_UPDATE", "*");
      });
  };
}
/**
 * Fetches users medication
 * @param {string} context
 */
export function getMedication(context = "enc:") {
  return dispatch => {
    dispatch({
      type: "MEDICATIONS_LOADING"
    });
    return fetch("/user/get_medication", {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(
        response => {
          const keys = Object.keys(response);
          for (let i = 0; i < keys.length; i++) {
            const item = response[keys[i]];
            const nextDose = utils.recalculateNextDose(item);
            response[keys[i]] = { ...item, nextDose };
          }
          dispatch({
            type: "GET_MEDICATION",
            payload: response instanceof Array ? {} : response
          });
          return Object.values(response).sort((a, b) => a.key - b.key);
        },
        () =>
          dispatch({
            type: "GET_MEDICATION",
            payload: {}
          })
      )
      .then(response => {
        const promises = [];
        for (let i = 0; i < response.length; i++) {
          promises.push(
            getAllIngredients(
              { ...response[i], _type: "MedicinalProduct" },
              context,
              dispatch
            )
          );
        }
        Promise.all(promises).then(responses => {
          for (let i = 0; i < responses.length; i++) {
            dispatch({
              type: "ADD_INTERACTION_ITEM",
              payload: responses[i]
            });
          }
        });
        dispatch({
          type: "INTERACTIONS_NOT_LOADING",
          payload: response
        });
      });
  };
}
/**
 * Fetches data to update users data
 * @param {Object} medicament
 * @param {string} context
 * @param {string} tryN
 */
export function updateMedicament(medicament, context, tryN, redirect) {
  return dispatch => {
    dispatch({
      type: "MEDICATIONS_LOADING"
    });
    dispatch({
      type: "UPDATE_MEDICAMENT",
      payload: medicament
    });
    fetch("/user/update_medicament", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin",
      body: JSON.stringify({
        medicament: {
          ...medicament,
          own: undefined,
          edit: undefined,
          add: undefined
        }
      })
    })
      .then(response => {
        if (response.status > 202) {
          if (!tryN) {
            tryN = 1;
          }
          tryN++;
          if (tryN < 3) {
            updateMedicament(medicament, context, tryN);
          } else {
            location.reload();
            alert("Something went wrong on the server");
          }
        }
        return response.json();
      })
      .then(
        response => {
          redirect();
          if (!medicament.key) {
            dispatch(addInteractionItem(medicament, context));
          }
        },
        () => {}
      )
      .then(() => {
        console.log("POSTED");
        window.postMessage("ENC_UPDATE", "*");
      });
  };
}
/**
 * Fetches users history
 */
export function getHistory() {
  return dispatch => {
    dispatch({
      type: "HISTORY_LOADING"
    });
    return fetch("/user/get_history", {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(
        response => {
          dispatch({
            type: "GET_HISTORY",
            payload: response
          });
        },
        () => {}
      );
  };
}
/**
 * Fetches data to change users password
 * @param {Object} values
 * @param {string} username
 * @param {Object} formApi
 */
export function changePassword(values, username, formApi) {
  return dispatch =>
    dispatch(getSession())
      .then(isLogged => {
        if (!isLogged) {
          throw Error("User is not logged.");
        }
      })
      .then(() => {
        dispatch({
          type: "RESPONSE_LOADING"
        });
        return fetch("/user/change_password", {
          credentials: "same-origin",
          method: "PUT",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            password: values.password,
            newPassword: values.newPassword,
            username
          })
        });
      })
      .then(response => {
        let result;
        if (response.status <= 202) {
          result = "PasswordChanged";
          dispatch(
            login(
              {
                username,
                password: values.newPassword
              },
              formApi,
              null
            )
          );
          formApi.resetAll();
        } else {
          result = "WrongPassword";
          formApi.setValue("password", "");
          formApi.setTouched("password", false);
        }
        dispatch({
          type: "CHANGE_PASSWORD_RESPONSE",
          payload: result
        });
      })
      .then(() => dispatch(getSession()));
}
/**
 * Fetches data for reseting password of a user
 * @param {Object} values
 * @param {string} token
 * @param {Object} formApi
 */
export function resetPassword(values, token, formApi) {
  return dispatch => {
    dispatch({
      type: "RESPONSE_LOADING"
    });
    return fetch("/user/reset_password", {
      credentials: "same-origin",
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        password: values.newPassword,
        username: values.username,
        token
      })
    }).then(response => {
      let resetResult;
      if (response.status <= 202) {
        resetResult = "PasswordChanged";
      } else if (response.status === 400) {
        resetResult = "OutdatedLink";
      } else {
        resetResult = "SomethingWrong";
      }
      formApi.resetAll();
      dispatch({
        type: "RESET_PASSWORD_RESPONSE",
        payload: resetResult
      });
    });
  };
}
/**
 * Logouts user
 */
export function logout() {
  return dispatch =>
    fetch("/user/logout", {
      credentials: "same-origin",
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          throw Error(response.statusText);
        }
        return response.json();
      })
      .then(() => dispatch(getSession()), () => {})
      .then(() => {
        window.postMessage("ENC_UPDATE", "*");
      });
}
/**
 * Fetches data to update users medication
 * @param {Object} medication
 * @param {string} language
 *  @param {Function} redirect
 */
export function updateMedication(medication, language, redirect) {
  return dispatch => {
    return dispatch(getSession())
      .then(isLogged => {
        if (!isLogged) {
          throw Error("User is not logged.");
        }
        dispatch({
          type: "RESPONSE_LOADING"
        });
      })
      .then(() =>
        fetch("/user/update_medication", {
          credentials: "same-origin",
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(medication)
        })
      )
      .then(response => {
        redirect();
        if (!response.ok) {
          throw Error(response.statusText);
        }
        let resetResult;
        if (response.status <= 202) {
          resetResult = "Saved";
        } else {
          resetResult = "SomethingWrong";
        }
        dispatch({
          type: "MEDICATION_RESPONSE",
          payload: resetResult
        });
        return response;
      })
      .then(() => {
        window.postMessage(
          {
            language
          },
          "*"
        );
      });
  };
}
