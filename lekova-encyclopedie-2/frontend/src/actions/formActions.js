export const addItem = item => ({
    type: 'ADD_ITEM',
    payload: item
});


export const removeItem = key => ({
    type: 'REMOVE_ITEM',
    payload: key
});
export const editItem = key => ({
    type: 'EDIT_ITEM',
    payload: key
});
export const stornoItem = key => ({
    type: 'STORNO_ITEM',
    payload: key
});
export const confirmItem = (key, medicament) => ({
    type: 'CONFIRM_ITEM',
    payload: {key, medicament}
});

export const resetForm = () => ({type: 'RESET_FORM'});
