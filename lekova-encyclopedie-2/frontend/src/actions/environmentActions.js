export function selectLanguage(language) {
    document.cookie = `language=${language}`;
    return {type: 'SET_LANGUAGE',
        payload: language};
}

export function resetResponses() {
    return {
        type: 'RESET_RESPONSES'
    };
}


export function hideUserBox() {
    return {
        type: 'SHOW_USER_BOX',
        payload: false
    };
}

export function showUserBox() {
    if (/Android|Mobi|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return {
            type: 'DO_NOTHING'
        };
    }
    return {
        type: 'SHOW_USER_BOX',
        payload: true
    };
}
export function hideShowUserBox() {
    return {
        type: 'HIDE_SHOW_USER_BOX'
    };
}
