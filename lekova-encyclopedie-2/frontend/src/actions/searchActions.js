import "whatwg-fetch";
/**
 * Fetches solr document for required request
 * @param {string} value 
 * @param {string} language 
 * @param {Function} makeQuery 
 * @param {*} dispatch 
 */
function getSolrDoc(value, language, makeQuery, dispatch) {
  if (value.length < 1) {
    return { payload: { suggestions: [], value }, type: "SUGGEST" };
  }
  dispatch({ type: "SUGGEST_LOADING", payload: value });
  setTimeout(() => {
    dispatch({ type: "SUGGEST_LONG_LOADING", payload: value });
  }, 1000);

  const url = `/solr/le-search-labels-${language}/select?indent=on&q=${makeQuery(
    value
  )}&wt=json`;
  return fetch(url, {
    method: "GET",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(response => {
      if (!response.ok) {
        dispatch({ payload: { suggestions: [], value }, type: "SUGGEST" });
        throw Error(response.statusText);
      }
      return response;
    })
    .then(response => response.json())
    .then(
      response => {
        dispatch({
          payload: { suggestions: response.response.docs, value },
          type: "SUGGEST"
        });
      },
      () => dispatch({ payload: { suggestions: [], value }, type: "SUGGEST" })
    );
}

export function suggest(value, language, makeQuery) {
  return dispatch => getSolrDoc(value, language, makeQuery, dispatch);
}
export const setSearchValueHeader = value => ({
  type: "SET_SEARCH_VALUE_HEADER",
  payload: value
});
export const setSearchValueInForm = value => ({
  type: "SET_SEARCH_VALUE_IN_FORM",
  payload: value
});

export const resetSearchValueInForm = () => ({
  type: "RESET_SEARCH_VALUE_IN_FORM"
});
export const resetResults = () => ({
  type: "RESET_RESULTS"
});
/**
 * Returns all items that are relevant for the query
 */
export function getAllResults(query, language) {
  const url = `/solr/le-search-labels-${language}/select?indent=on&q=${query}&rows=1000&wt=json`;
  return dispatch => {
    dispatch({ type: "RESET_ALL_RESULTS" });
    dispatch({ type: "ALL_RESULTS_LOADING" });
    return fetch(url, {
      method: "GET",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        if (!response.ok) {
          dispatch({ type: "ALL_RESULTS", payload: [] });
          throw Error(response.statusText);
        }
        return response;
      })
      .then(response => response.json())
      .then(
        response => {
          return dispatch({
            type: "ALL_RESULTS",
            payload: response.response.docs
          });
        },
        () => dispatch({ type: "ALL_RESULTS", payload: [] })
      );
  };
}
