import utils from 'Libs/utils.js';

export function updateMedicamentFront(item) {
    item = {...item, nextDose: utils.recalculateNextDose(item)};
    return {type: 'UPDATE_MEDICAMENT',
        payload: item};
}

export const removeMedicamentFront = key => ({
    type: 'REMOVE_MEDICAMENT',
    payload: key
});
export const editMedicament = key => ({
    type: 'EDIT_MEDICAMENT',
    payload: key
});
export const stornoMedicament = key => ({
    type: 'STORNO_MEDICAMENT',
    payload: key
});
export const confirmMedicament = (key, medicament) => ({
    type: 'CONFIRM_MEDICAMENT',
    payload: {key, medicament}
});
