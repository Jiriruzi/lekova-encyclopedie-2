import React from 'react';
import ReactDom from 'react-dom';
import {Router} from 'react-router';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import createHistory from 'history/createBrowserHistory';
import App from 'Components/App/App.jsx';
import allReducers from './reducers/index.js';
import '../css/styles.js';


const store = createStore(allReducers, applyMiddleware(thunk));
const history = createHistory();


ReactDom.render(
    <Provider store={store}>
        <Router
            history={history}
        >
            <App />
        </Router>
    </Provider>, document.getElementById('container'));
