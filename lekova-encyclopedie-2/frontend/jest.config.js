module.exports = {
  moduleNameMapper: {
    "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
      "<rootDir>/tests/__mocks__/fileMock.js",
    "\\.worker.js": "<rootDir>/tests/__mocks__/workerMock.js",
    "\\.(css|less)$": "identity-obj-proxy",
    "^@/(.*)$": "<rootDir>/$1",
    "Actions/(.*)$": "<rootDir>/src/actions/$1",
    "Components/(.*)$": "<rootDir>/src/components/$1",
    "Libs/(.*)$": "<rootDir>/src/libs/$1",
    "Images/(.*)$": "<rootDir>/images/$1",
    "Modules/(.*)$": "<rootDir>/node_modules/$1"
  },
  setupTestFrameworkScriptFile: "<rootDir>/tests/setupTests.js"
};
