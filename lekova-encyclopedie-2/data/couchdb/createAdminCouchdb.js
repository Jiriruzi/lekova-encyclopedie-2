const fetch = require("node-fetch");

const couchdbAdmin = "admin";
const couchdbAdminPassword = "admin";
return fetch(
  `http://localhost:5984/_node/nonode@nohost/_config/admins/${couchdbAdmin}`,
  {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(couchdbAdminPassword)
  }
)
  .then(res => res.json())
  .then(res => console.log(res))
  .then(
    fetch(
      `http://${couchdbAdmin}:${couchdbAdminPassword}@localhost:5984/_node/_local/_config/couch_httpd_auth/timeout`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify("7776000000")
      }
    )
  )
  .then(() => {
    let a = 0;        
    console.log("Creating test users for...")
    return (async function loop() {
      for (var i = 0; i < 1000; i++) {
        await fetch(
          "http://localhost:5900/user/register",
          {
            method: "POST",
            body: JSON.stringify({
              role: "user",
              birthday: { day: 29, month: 11, year: 2018 },
              username: "test" + i,
              email: "jirka.email@email.cz",
              password: "qqq",
              passwordConfirmation: "qqq",
              gender: "male"
            })
          }
        ).then(()=>{
          if(i%100===0)
          console.log("Almost done" + i/10 + "%")
        });
      }
    })();
  });
