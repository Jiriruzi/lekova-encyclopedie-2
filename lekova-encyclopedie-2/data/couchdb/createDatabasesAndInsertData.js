const nano = require("nano")("http://localhost:5984");
const fs = require("fs");
const json = require("big-json");

const databases = [
  "le-atc-concept-detail",
  "le-configuration",
  "le-disease-or-finding-detail",
  "le-ingredient-detail",
  "le-mechanism-of-action-detail",
  "le-medicinal-product-detail",
  "le-medicinal-product-packaging-detail",
  "le-medicinal-product-packaging-pil",
  "le-medicinal-product-packaging-price",
  "le-pharmacokinetics-detail",
  "le-pharmacological-action-detail",
  "le-physiologic-effect-detail"
];
const databasesWithUsers = ["_users", ...databases];

const chunk = 5000;

function sleep(millis) {
  return new Promise(resolve => setTimeout(resolve, millis));
}

// sequentially creating all databases
function createDatabases(n, resolve) {
  return nano.db.create(databasesWithUsers[n], (err, body) => {
    if (n < databasesWithUsers.length - 1) createDatabases(++n, resolve);
    else resolve();
    if (!err) {
      console.log(`database ${databasesWithUsers[n]} created!`);
    } else {
      if (err.statusCode === 412)
        console.log(`Database ${databasesWithUsers[n]} already exists`);
      else console.log(err);
    }
  });
}

// sequentially inserting data chunks to database
function insertDataToDatabasesByChunks(docs, databaseNum, i, resolve) {
  const inNum = i;
  const db = nano.use(databases[databaseNum]);
  const temparray = docs.slice(
    i,
    i + chunk < docs.length ? i + chunk : docs.length
  );
  return db.bulk({ docs: temparray }, {}, err => {
    if (!err) {
      console.log(
        `Data from ${inNum} to ${
          inNum + chunk > docs.length ? docs.length : inNum + chunk
        } inserted to ${databases[databaseNum]}!`
      );
    } else {
      console.log(err.message);
    }
    if (i + chunk < docs.length) {
      insertDataToDatabasesByChunks(docs, databaseNum, i + chunk, resolve);
    } else if (databaseNum < databases.length - 1) {
      console.log(`${databases[databaseNum]} done!`);
      insertDataToAllDatabases(++databaseNum, resolve);
    } else {
      resolve();
    }
  });
}

// sequentially inserting data to all databases
function insertDataToAllDatabases(databaseNum, resolve) {
  const readStream = fs.createReadStream(
    `./data/couchdb/${databases[databaseNum]}.json`,
    {
      encoding: "utf-8"
    }
  );
  const parseStream = json.createParseStream();

  parseStream.on("data", ObjWithDocs => {
    insertDataToDatabasesByChunks(ObjWithDocs.docs, databaseNum, 0, resolve);
  });
  readStream.pipe(parseStream);
}

let times = 0;
return new Promise(function(resolve, reject) {
  let timeOut = setInterval(() => {
    nano.request({}, err => {
      if (!err) {
        clearTimeout(timeOut);
        new Promise((resolvecd, rejectcd) => createDatabases(0, resolvecd))
          .then(
            () =>
              new Promise((resolveins, rejectins) =>
                insertDataToAllDatabases(0, resolveins)
              )
          )
          .then(() => resolve());
      } else {
        times++;
        if (times === 5) {
          clearTimeout(timeOut);
          console.log("Couldnt connect to couchdb.");
          reject();
        } else {
          console.log(
            "Can not connect to couchdb, will try again in 5 seconds."
          );
        }
      }
    });
  }, 5000);
});
