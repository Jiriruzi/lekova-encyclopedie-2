const fetch = require("node-fetch");

const ingredient = {
  type: "ingredient",
  _id: "id"
};

let tries = 0;
function prepareCouch() {
  return fetch(`http://localhost:8086/le-ingredient-detail`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/json"
    }
  })
    .then(() =>
      fetch(`http://localhost:8086/_users`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        }
      })
    )
    .then(() =>
      fetch(`http://localhost:8086/le-ingredient-detail`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(ingredient)
      })
    )
    .then(() =>
      fetch(`http://localhost:8086/_node/nonode@nohost/_config/admins/admin`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify("admin")
      })
        .then(res => res.json())
        .then(res => console.log(res))
    )
    .catch(() => {
      if (tries++ < 4) {
        console.log("Waiting for couchdb...");
        return new Promise((resolve, reject) => {
          setTimeout(() => {
            resolve(prepareCouch());
          }, 4744);
        });
      } else {
        console.error("Can not connect to couchdb!");
      }
    });
}

prepareCouch().then(() => console.log("Test couchdb prepared!"));
