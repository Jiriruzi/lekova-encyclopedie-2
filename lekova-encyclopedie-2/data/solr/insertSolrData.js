const fs = require("fs");
const json = require("big-json");
const fetch = require("node-fetch");

const dataTypes = [
  "diseaseOrFinding",
  "ingredient",
  "medicinalProduct",
  "medicinalProductPackaging",
  "mechanismOfAction",
  "pharmacokinetics",
  "pharmacologicalAction",
  "physiologicEffect",
  "atcConcept"
];
//inserting data to database
function insertDataToSolr(obj, type, lang) {
  fetch(
    `http://localhost:8983/solr/le-search-labels-${lang}/update?commit=true`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: obj
    }
  )
    .then(response => {
      if (response.ok) {
        console.log(`Data ${dataTypes[type]} ${lang} inserted to solr!`);
      } else {
        throw Error(response.statusText);
      }
      if (type < dataTypes.length - 1) loadAndInsertData(++type, lang);
      return response.json();
    })
    .then(res => console.log(res));
}

// sequentially inserting data to all databases
function loadAndInsertData(type, lang) {
  const obj = fs.readFileSync(
    `./data/solr/${dataTypes[type]}Solr${lang.charAt(0).toUpperCase() +
      lang.substr(1)}.json`,
    "utf8"
  );
  insertDataToSolr(obj, type, lang);
}

function insertDataToBothLanguages() {
  loadAndInsertData(0, "cs");
  loadAndInsertData(0, "en");
}

insertDataToBothLanguages();
